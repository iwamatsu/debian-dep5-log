Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./LICENSE
 ./pom.xml
 ./src/main/java/org/apache/felix/resolver/Activator.java
 ./src/main/java/org/apache/felix/resolver/Candidates.java
 ./src/main/java/org/apache/felix/resolver/FelixResolveContext.java
 ./src/main/java/org/apache/felix/resolver/Logger.java
 ./src/main/java/org/apache/felix/resolver/ResolutionError.java
 ./src/main/java/org/apache/felix/resolver/ResolverImpl.java
 ./src/main/java/org/apache/felix/resolver/SimpleHostedCapability.java
 ./src/main/java/org/apache/felix/resolver/Util.java
 ./src/main/java/org/apache/felix/resolver/WireImpl.java
 ./src/main/java/org/apache/felix/resolver/WrappedRequirement.java
 ./src/main/java/org/apache/felix/resolver/WrappedResource.java
 ./src/main/java/org/apache/felix/resolver/util/ArrayMap.java
 ./src/main/java/org/apache/felix/resolver/util/CandidateSelector.java
 ./src/main/java/org/apache/felix/resolver/util/CopyOnWriteSet.java
 ./src/main/java/org/apache/felix/resolver/util/OpenHashMap.java
 ./src/main/java/org/apache/felix/resolver/util/OpenHashMapList.java
 ./src/main/java/org/apache/felix/resolver/util/OpenHashMapSet.java
 ./src/main/java/org/apache/felix/resolver/util/ShadowList.java
 ./src/test/java/org/apache/felix/resolver/test/BigResolutionTest.java
 ./src/test/java/org/apache/felix/resolver/test/FELIX_4914_Test.java
 ./src/test/java/org/apache/felix/resolver/test/Main.java
 ./src/test/java/org/apache/felix/resolver/test/ResolverTest.java
 ./src/test/java/org/apache/felix/resolver/test/util/BundleCapability.java
 ./src/test/java/org/apache/felix/resolver/test/util/BundleRequirement.java
 ./src/test/java/org/apache/felix/resolver/test/util/CandidateComparator.java
 ./src/test/java/org/apache/felix/resolver/test/util/CapabilitySet.java
 ./src/test/java/org/apache/felix/resolver/test/util/ClauseParser.java
 ./src/test/java/org/apache/felix/resolver/test/util/GenericCapability.java
 ./src/test/java/org/apache/felix/resolver/test/util/GenericRequirement.java
 ./src/test/java/org/apache/felix/resolver/test/util/IdentityCapability.java
 ./src/test/java/org/apache/felix/resolver/test/util/IterativeResolver.java
 ./src/test/java/org/apache/felix/resolver/test/util/JsonReader.java
 ./src/test/java/org/apache/felix/resolver/test/util/PackageCapability.java
 ./src/test/java/org/apache/felix/resolver/test/util/PackageRequirement.java
 ./src/test/java/org/apache/felix/resolver/test/util/ResolveContextImpl.java
 ./src/test/java/org/apache/felix/resolver/test/util/ResourceImpl.java
 ./src/test/java/org/apache/felix/resolver/test/util/SimpleFilter.java
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libfelix-resolver-java.docs
 ./debian/libfelix-resolver-java.poms
 ./debian/maven.ignoreRules
 ./debian/maven.properties
 ./debian/maven.rules
 ./debian/patches/01-missing-test-dependencies.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/changelog.txt
 ./src/main/java/org/osgi/service/resolver/packageinfo
 ./src/test/resources/felix-4914.json
 ./src/test/resources/resolution.json
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./DEPENDENCIES
 ./NOTICE
 ./src/main/appended-resources/META-INF/DEPENDENCIES
Copyright: 2006-2017, The Apache Software Foundation
  2013, The Apache Software Foundation
License: Apache-2.0
 FIXME

Files: ./src/main/java/org/osgi/service/resolver/ResolutionException.java
 ./src/main/java/org/osgi/service/resolver/ResolveContext.java
Copyright: OSGi Alliance (2011, 2012).
License: Apache-2.0
 FIXME

Files: ./src/main/java/org/apache/felix/resolver/WrappedCapability.java
 ./src/main/java/org/osgi/service/resolver/HostedCapability.java
Copyright: OSGi Alliance (2012).
License: Apache-2.0
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0
  2013-2017, The Apache Software Foundation
  2018, Kai-Chung Yan (æ®·åè°) <seamlikok@gmail.com>
License: Apache-2.0
 FIXME

Files: ./src/main/appended-resources/META-INF/NOTICE
Copyright: OSGi Alliance (2000, 2015).
License: Apache-2.0
 FIXME

Files: ./src/main/java/org/osgi/service/resolver/Resolver.java
Copyright: OSGi Alliance (2006, 2012).
License: Apache-2.0
 FIXME

Files: ./src/main/java/org/osgi/service/resolver/package-info.java
Copyright: OSGi Alliance (2010, 2012).
License: Apache-2.0
 FIXME

