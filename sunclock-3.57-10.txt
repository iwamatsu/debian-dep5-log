Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./CHANGES
 ./INSTALL
 ./Imakefile
 ./MESSAGE
 ./MESSAGE.old
 ./Makefile.noimake
 ./README
 ./README_pl
 ./Sunclockrc
 ./TODO
 ./VMF.txt
 ./astro.c
 ./bitmaps.h
 ./coordinates.txt
 ./debian/compat
 ./debian/control
 ./debian/gitlab-ci.yml
 ./debian/patches/01_emx_directory.patch
 ./debian/patches/04_dont_strip_emx.patch
 ./debian/patches/06_CET-1CDT.patch
 ./debian/patches/07_zlib_largefile_workaround.patch
 ./debian/patches/08_manpage_hyphen.patch
 ./debian/patches/09_manpage_typo.patch
 ./debian/patches/10_hurd_MAXPATHLEN.patch
 ./debian/patches/11_fix-str-fmt.patch
 ./debian/patches/12_disable_zlib.patch
 ./debian/patches/13_emx_hardening.patch
 ./debian/patches/14_donotinstall_vmf.patch
 ./debian/patches/15_time.patch
 ./debian/patches/16_implicit_declaration.patch
 ./debian/patches/17_pointer-to-int-cast.patch
 ./debian/patches/18_init_hstrip.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/sunclock-maps.dirs
 ./debian/sunclock.desktop
 ./debian/sunclock.dirs
 ./debian/sunclock.docs
 ./debian/tests/control
 ./debian/watch
 ./dirlist.c
 ./editkit/CHANGELOG
 ./editkit/Imakefile
 ./editkit/Makefile
 ./editkit/Makefile.kit
 ./editkit/messages.def
 ./editkit/rc.example
 ./editkit/version.h
 ./i18n/Sunclock.de
 ./i18n/Sunclock.en
 ./i18n/Sunclock.es
 ./i18n/Sunclock.fr
 ./i18n/Sunclock.it
 ./i18n/Sunclock.nl
 ./i18n/Sunclock.no
 ./i18n/Sunclock.pl
 ./i18n/Sunclock.sv
 ./i18n/thommy.diff
 ./langdef.h
 ./readxpm.c
 ./sunclock.h
 ./sunclock.man
 ./sunclock.spec
 ./tildepath.c
 ./tools/Makefile
 ./tools/concat.c
 ./vmf/countries.vmf
 ./vmf/landwater.vmf
 ./vmf/timezones.vmf
 ./widgets.c
 ./wm_icons/mini-sunclock-16x14.xpm
 ./wm_icons/mini-sunclock-16x16.xpm
 ./wm_icons/sunclock.xpm
 ./wm_icons/sunclock2.xpm
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./specs/sunclock.spec
 ./specs/sunclock_huge_map.spec
 ./specs/sunclock_jpeg_big_maps.spec
 ./specs/sunclock_jpeg_medium_maps.spec
 ./specs/sunclock_maps_package.spec
Copyright: GPL
License: UNKNOWN
 FIXME

Files: ./editkit/edit.c
 ./editkit/edit.h
 ./editkit/term_bind_ws.c
 ./editkit/x11.c
 ./editkit/x11_bind_ws.c
Copyright: 1991, which is
  2002, Terry Loveall
License: public-domain
 FIXME

Files: ./readjpeg.c
 ./readpng.c
Copyright: ed |
License: NTP
 FIXME

Files: ./debian/patches/19_version.patch
 ./version.h
Copyright: J.-P. Demailly"
License: UNKNOWN
 FIXME

Files: ./debian/tests/desktopfile
 ./debian/tests/version
Copyright: 2018, Roland Rosenfeld <roland@debian.org>
  2019, Roland Rosenfeld <roland@debian.org>
License: UNKNOWN
 FIXME

Files: ./editkit/term_bind_em.c
 ./editkit/x11_bind_em.c
Copyright: 1991, which is
  2002, Jean-Pierre Demailly
  2002, Terry Loveall
License: public-domain
 FIXME

Files: ./sunclock.c
Copyright: NONE
License: GPL and/or public-domain
 FIXME

Files: ./readgif.c
Copyright: 1990-1991, 1993, David Koblas. |
  1996, Torsten Martinsen. |
License: NTP
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: NTP~disclaimer
 FIXME

Files: ./readvmf.c
Copyright: 2001, Jean-Pierre Demailly <demailly@ujf-grenoble.fr>
License: NTP~disclaimer
 FIXME

Files: ./editkit/MANUAL.emacs
Copyright: 1991
  2002, Jean-Pierre Demailly <demailly@ujf-grenoble.fr>
License: UNKNOWN
 FIXME

Files: ./editkit/MANUAL.wordstar
Copyright: 1991
  2002, Terry Loveall, <loveall@qwest.net>
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./editkit/README
Copyright: 2002, Jean-Pierre Demailly <demailly@ujf-grenoble.fr>
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: URL to https.
License: UNKNOWN
 FIXME

Files: ./editkit/termcap.c
Copyright: 1991, modified April 2002. Copyright (C) 1998,2002 Terry Loveall, <loveall@qwest.net>
License: UNKNOWN
 FIXME

