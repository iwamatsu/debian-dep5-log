Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Makefile
 ./configure.ac
 ./debian/cdebconf-gtk-udeb.install
 ./debian/cdebconf-gtk-udeb.templates
 ./debian/cdebconf-gtk.install
 ./debian/cdebconf-gtk.lintian-overrides
 ./debian/cdebconf-newt-udeb.install
 ./debian/cdebconf-newt-udeb.templates
 ./debian/cdebconf-priority.postinst
 ./debian/cdebconf-priority.templates
 ./debian/cdebconf-slang-udeb.templates
 ./debian/cdebconf-text-udeb.install
 ./debian/cdebconf-text-udeb.templates
 ./debian/cdebconf-udeb.dirs
 ./debian/cdebconf-udeb.install
 ./debian/cdebconf-udeb.templates
 ./debian/cdebconf.config
 ./debian/cdebconf.dirs
 ./debian/cdebconf.docs
 ./debian/cdebconf.install
 ./debian/cdebconf.lintian-overrides
 ./debian/cdebconf.postinst
 ./debian/cdebconf.preinst
 ./debian/cdebconf.templates
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/dirs
 ./debian/libdebconfclient0-dev.install
 ./debian/libdebconfclient0-udeb.install
 ./debian/libdebconfclient0.install
 ./debian/libdebconfclient0.symbols
 ./debian/po/POTFILES.in
 ./debian/po/am.po
 ./debian/po/be.po
 ./debian/po/bo.po
 ./debian/po/dz.po
 ./debian/po/et.po
 ./debian/po/gu.po
 ./debian/po/hi.po
 ./debian/po/km.po
 ./debian/po/kn.po
 ./debian/po/mk.po
 ./debian/po/mr.po
 ./debian/po/ne.po
 ./debian/po/output
 ./debian/po/ro.po
 ./debian/po/se.po
 ./debian/po/tg.po
 ./debian/po/ug.po
 ./debian/po2templates
 ./debian/source/format
 ./doc/TODO
 ./doc/coding.txt
 ./doc/confmodule.txt
 ./doc/design.txt
 ./doc/directives.txt
 ./doc/doxygen.cfg
 ./doc/modules.txt
 ./doc/plugins.txt
 ./doc/progress.txt
 ./doc/testing.txt
 ./globalmakeflags.in
 ./install-sh
 ./man/Makefile.in
 ./man/debconf.pod
 ./man/dpkg-preconfigure.pod
 ./man/dpkg-reconfigure.pod
 ./src/Makefile.in
 ./src/cdebconf.conf
 ./src/cdebconf.conf-dist.in
 ./src/client/confmodule.build
 ./src/client/confmodule.in
 ./src/commands-list.h.build
 ./src/commands-list.in
 ./src/commands.c
 ./src/commands.h
 ./src/common.h
 ./src/configuration.c
 ./src/configuration.h
 ./src/confmodule.c
 ./src/confmodule.h
 ./src/constants.h
 ./src/database.c
 ./src/database.h
 ./src/debconf-communicate.c
 ./src/debconf-copydb.c
 ./src/debconf-dumpdb.c
 ./src/debconf-escape.c
 ./src/debconf-get-selections.c
 ./src/debconf-loadtemplate.c
 ./src/debconf-set-selections.c
 ./src/debconf.c
 ./src/debconfclient.c
 ./src/debconfclient.h
 ./src/debug.c
 ./src/debug.h
 ./src/dpkg-preconfigure.c
 ./src/dpkg-reconfigure.c
 ./src/frontend.c
 ./src/frontend.h
 ./src/modules/Makefile
 ./src/modules/db/Makefile
 ./src/modules/db/http/Makefile
 ./src/modules/db/http/http.c
 ./src/modules/db/http/http.h
 ./src/modules/db/modules.mak
 ./src/modules/db/mysql/Makefile
 ./src/modules/db/mysql/create.sql
 ./src/modules/db/mysql/mysql.h
 ./src/modules/db/perldb/Makefile
 ./src/modules/db/perldb/perldb.c
 ./src/modules/db/perldb/perldb.h
 ./src/modules/db/rfc822db/Makefile.in
 ./src/modules/db/rfc822db/rfc822db.c
 ./src/modules/db/rfc822db/rfc822db.h
 ./src/modules/db/stack/Makefile
 ./src/modules/db/stack/stack.h
 ./src/modules/db/textdb/Makefile
 ./src/modules/db/textdb/textdb.c
 ./src/modules/db/textdb/textdb.h
 ./src/modules/frontend/Makefile
 ./src/modules/frontend/bogl/Makefile
 ./src/modules/frontend/bogl/bogl.c
 ./src/modules/frontend/corba/Makefile
 ./src/modules/frontend/corba/corba.c
 ./src/modules/frontend/corba/dcf-text.c
 ./src/modules/frontend/corba/dcf-text.gnorba
 ./src/modules/frontend/corba/dcf-textimpl.c
 ./src/modules/frontend/corba/dcf.idl
 ./src/modules/frontend/gtk/Makefile.in
 ./src/modules/frontend/gtk/cdebconf_gtk.doxygen
 ./src/modules/frontend/gtk/linker-script
 ./src/modules/frontend/modules.mak
 ./src/modules/frontend/ncurses/Makefile.in
 ./src/modules/frontend/ncurses/ncurses.c
 ./src/modules/frontend/ncurses/ncurses_fe.h
 ./src/modules/frontend/newt/Makefile.in
 ./src/modules/frontend/newt/cdebconf_newt.h
 ./src/modules/frontend/passthrough/Makefile.in
 ./src/modules/frontend/slang/Makefile.in
 ./src/modules/frontend/slang/slang_fe.h
 ./src/modules/frontend/text/Makefile.in
 ./src/modules/frontend/text/cdebconf_text.h
 ./src/plugin.c
 ./src/plugin.h
 ./src/priority.c
 ./src/priority.h
 ./src/question.c
 ./src/question.h
 ./src/rfc822.c
 ./src/rfc822.h
 ./src/strutl.c
 ./src/strutl.h
 ./src/template.c
 ./src/template.h
 ./src/test/Makefile
 ./src/test/align.config
 ./src/test/align.templates
 ./src/test/backup.config
 ./src/test/configtest.c
 ./src/test/dbtest.c
 ./src/test/entropy.config
 ./src/test/entropy.templates
 ./src/test/help.config
 ./src/test/help.templates
 ./src/test/indices.config
 ./src/test/indices.templates
 ./src/test/longselect.config
 ./src/test/longselect.templates
 ./src/test/metaget.config
 ./src/test/progress.config
 ./src/test/progress.templates
 ./src/test/questions/aj:foo
 ./src/test/questions/drow:multi
 ./src/test/questions/drow:note
 ./src/test/questions/drow:string
 ./src/test/templates/aj:foo
 ./src/test/templates/drow:multi
 ./src/test/templates/drow:note
 ./src/test/templates/drow:string
 ./src/test/terminal.config
 ./src/test/terminal.templates
 ./src/test/test.config
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/po/ar.po
 ./debian/po/bg.po
 ./debian/po/bs.po
 ./debian/po/ca.po
 ./debian/po/cs.po
 ./debian/po/cy.po
 ./debian/po/de.po
 ./debian/po/el.po
 ./debian/po/eo.po
 ./debian/po/es.po
 ./debian/po/fa.po
 ./debian/po/fi.po
 ./debian/po/fr.po
 ./debian/po/ga.po
 ./debian/po/gl.po
 ./debian/po/he.po
 ./debian/po/hr.po
 ./debian/po/hu.po
 ./debian/po/id.po
 ./debian/po/it.po
 ./debian/po/ja.po
 ./debian/po/ka.po
 ./debian/po/kk.po
 ./debian/po/ko.po
 ./debian/po/ku.po
 ./debian/po/lo.po
 ./debian/po/lt.po
 ./debian/po/nb.po
 ./debian/po/nl.po
 ./debian/po/nn.po
 ./debian/po/ru.po
 ./debian/po/sk.po
 ./debian/po/sl.po
 ./debian/po/sq.po
 ./debian/po/sv.po
 ./debian/po/ta.po
 ./debian/po/th.po
 ./debian/po/tl.po
 ./debian/po/tr.po
 ./debian/po/uk.po
 ./debian/po/zh_CN.po
 ./debian/po/zh_TW.po
Copyright: 2002-2008, 2010, 2012, 2015, 2017, Software in the Public Interest, Inc.
  2003, Software in the Public Interest, Inc.
  2003-2004, Software in the Public Interest, Inc.
  2003-2005, 2008, Software in the Public Interest, Inc.
  2003-2007, Software in the Public Interest, Inc.
  2003-2008, Software in the Public Interest, Inc.
  2003-2010, Software in the Public Interest, Inc.
  2003-2015, 2017, Software in the Public Interest, Inc.
  2004-2008, Software in the Public Interest, Inc.
  2004-2009, Software in the Public Interest, Inc.
  2004-2010, Software in the Public Interest, Inc.
  2005-2013, Software in the Public Interest, Inc.
  2006-2010, Software in the Public Interest, Inc.
  2006-2016, Software in the Public Interest, Inc.
License: UNKNOWN
 FIXME

Files: ./src/modules/db/mysql/mysql.c
 ./src/modules/db/stack/stack.c
 ./src/modules/frontend/gtk/align_text_renderer.c
 ./src/modules/frontend/gtk/align_text_renderer.h
 ./src/modules/frontend/gtk/cdebconf_gtk.c
 ./src/modules/frontend/gtk/cdebconf_gtk.h
 ./src/modules/frontend/gtk/choice_model.c
 ./src/modules/frontend/gtk/choice_model.h
 ./src/modules/frontend/gtk/descriptions.c
 ./src/modules/frontend/gtk/descriptions.h
 ./src/modules/frontend/gtk/di.c
 ./src/modules/frontend/gtk/di.h
 ./src/modules/frontend/gtk/fe_data.h
 ./src/modules/frontend/gtk/go.c
 ./src/modules/frontend/gtk/go.h
 ./src/modules/frontend/gtk/handlers.c
 ./src/modules/frontend/gtk/handlers.h
 ./src/modules/frontend/gtk/progress.c
 ./src/modules/frontend/gtk/progress.h
 ./src/modules/frontend/gtk/screenshot.c
 ./src/modules/frontend/gtk/screenshot.h
 ./src/modules/frontend/gtk/select_handlers.c
 ./src/modules/frontend/gtk/select_handlers.h
 ./src/modules/frontend/gtk/ui.c
 ./src/modules/frontend/gtk/ui.h
 ./src/modules/frontend/newt/newt.c
 ./src/modules/frontend/passthrough/passthrough.c
 ./src/modules/frontend/slang/slang.c
 ./src/modules/frontend/text/text.c
Copyright: 2000-2001, Randolph Chung and others under the following
  2000-2007, Randolph Chung and others under the following
  2000-2009, Randolph Chung and others under the following
License: BSD-2-clause
 FIXME

Files: ./debian/copyright
Copyright: Jason Gunthorpe <jgg@debian.org>
  Joey Hess <joeyh@debian.org>
  ed (c) 2000-2009 by Randolph Chung <tausq@debian.org>,
License: BSD-2-clause
 FIXME

Files: ./debian/po/vi.po
Copyright: 2010, Software in the Public Interest, Inc.
  <progfou@gmail.com>
  <vuhung16@bigfoot.com>, 2001.
  Quang Trung <vu.quang.trung@auf.org>
License: UNKNOWN
 FIXME

Files: ./debian/po/pl.po
Copyright: 2003, Software in the Public Interest, Inc.
  2004-2010, Bartosz FeÅski <fenio@debian.org>
License: UNKNOWN
 FIXME

Files: ./debian/po/bn.po
Copyright: 2005-2006, Debian Foundation.
License: UNKNOWN
 FIXME

Files: ./debian/po/ml.po
Copyright: 2006-2010, Debian Project
License: UNKNOWN
 FIXME

Files: ./debian/po/is.po
Copyright: 2002-2003, 2010-2012, Free Software Foundation, Inc.
  2003, Software in the Public Interest, Inc.
  2010, Free Software Foundation
License: UNKNOWN
 FIXME

Files: ./debian/po/lv.po
Copyright: 2003, Software in the Public Interest, Inc.
  Free Software Foundation, Inc., 2001,2003.
License: UNKNOWN
 FIXME

Files: ./debian/po/da.po
Copyright: Free Software Foundation, Inc., 2006.
License: UNKNOWN
 FIXME

Files: ./debian/po/pt.po
Copyright: 2003, Software in the Public Interest, Inc.
  2003-2014, Miguel Figueiredo <elmig@debianpt.org>
License: UNKNOWN
 FIXME

Files: ./debian/po/te.po
Copyright: 2007, Rosetta Contributors and Canonical Ltd 2007
License: UNKNOWN
 FIXME

Files: ./debian/po/ast.po
Copyright: 2008, Rosetta Contributors and Canonical Ltd 2008
License: UNKNOWN
 FIXME

Files: ./debian/po/sr.po
Copyright: 2008, THE cp6Linux'S COPYRIGHT HOLDER
  2010-2012, Software in the Public Interest, Inc.
License: UNKNOWN
 FIXME

Files: ./debian/po/eu.po
Copyright: 2003, Software in the Public Interest, Inc.
  Translations from KDE:
License: UNKNOWN
 FIXME

Files: ./debian/po/templates.pot
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./src/test/test.templates
Copyright: baz quux (C), foo baz (C), bar quux (C), moo cow (C), alpha (C), beta (C), gamma (C), delta (C), epsilon (C), unidentified-next-one, ${EXTRA}
License: UNKNOWN
 FIXME

Files: ./src/debconf-show.c
Copyright: case 'd':
License: UNKNOWN
 FIXME

Files: ./debian/po/pt_BR.po
Copyright: dia"
License: UNKNOWN
 FIXME

Files: ./debian/rules
Copyright: 1997, to 1999 by Joey Hess.
License: UNKNOWN
 FIXME

Files: ./debian/po/pa.po
Copyright: à¨"
License: UNKNOWN
 FIXME

Files: ./debian/po/si.po
Copyright: à·"
License: UNKNOWN
 FIXME

