Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Makefile.am
 ./api/.htaccess
 ./api/RestService/InternalClient.php
 ./api/RestService/Server.php
 ./api/configuration_example.php
 ./api/preferences_example.php
 ./apidoc/apidoc.json
 ./apidoc/doc.php
 ./apidoc/html/api_data.js
 ./apidoc/html/api_data.json
 ./apidoc/html/api_project.js
 ./apidoc/html/api_project.json
 ./apidoc/html/css/style.css
 ./apidoc/html/index.html
 ./apidoc/html/locales/de.js
 ./apidoc/html/locales/locale.js
 ./apidoc/html/locales/nl.js
 ./apidoc/html/locales/pl.js
 ./apidoc/html/locales/pt_br.js
 ./apidoc/html/locales/ru.js
 ./apidoc/html/locales/zh.js
 ./apidoc/html/main.js
 ./apidoc/html/utils/handlebars_helper.js
 ./apidoc/html/utils/send_sample_request.js
 ./configure.ac
 ./debian/README.Debian
 ./debian/README.source
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/homer-api-mysql.install
 ./debian/homer-api-postgresql.install
 ./debian/homer-api.apache2
 ./debian/homer-api.conf
 ./debian/homer-api.docs
 ./debian/homer-api.install
 ./debian/homer-api.kamailio-homer.default
 ./debian/homer-api.kamailio-homer.init
 ./debian/homer-api.kamailio-homer.service
 ./debian/homer-api.links
 ./debian/homer-api.lintian-overrides
 ./debian/homer-api.logrotate
 ./debian/homer-api.postinst
 ./debian/homer-api.postrm
 ./debian/rules
 ./debian/source/format
 ./debian/source/lintian-overrides
 ./debian/watch
 ./examples/dashboards/_1411848944046.json
 ./examples/dashboards/_1426001444630.json
 ./examples/dashboards/_1427728371642.json
 ./examples/dashboards/_1428431423814.json
 ./examples/dashboards/_1430318378410.json
 ./examples/dashboards/_1431279268787.json
 ./examples/dashboards/_1431721484444.json
 ./examples/dashboards/_1431860431685.json
 ./examples/dashboards/_1431943495.json
 ./examples/dashboards/_1432037872.json
 ./examples/dashboards/_1432821348.json
 ./examples/external/test.php
 ./examples/mysql/mysqld.service
 ./examples/mysql/sipcapture.cnf
 ./examples/pgsql/kamailio.cfg
 ./examples/sipcapture/el6/sipcapture.init
 ./examples/sipcapture/el6/sipcapture.sysconfig
 ./examples/sipcapture/el7/sipcapture.service
 ./examples/sipcapture/el7/sipcapture.sysconfig
 ./examples/sipcapture/sipcapture.kamailio
 ./examples/sipcapture/sipcapture.logrotated
 ./examples/sipcapture/sipcapture.opensips
 ./examples/sipcapture/sipcapture.rsyslogd
 ./examples/web/homer5.apache
 ./examples/web/homer5.nginx
 ./examples/web/homer5.php-fpm
 ./examples/web/nginx.te
 ./examples/web/php-fpm.te
 ./examples/web/sipcapture-dashboards.te
 ./homer-api.spec.in
 ./pkg/debian/changelog
 ./pkg/debian/compat
 ./pkg/debian/control
 ./pkg/debian/docs
 ./pkg/debian/rules
 ./pkg/debian/source/format
 ./scripts/homer_partremove
 ./scripts/homer_rotate
 ./scripts/old/homer_rotate
 ./scripts/old/rotation.conf
 ./scripts/old/sipcapture.crontab
 ./scripts/pgsql/homer_rotate
 ./scripts/pgsql/rotation.ini
 ./scripts/pgsql/sipcapture.crontab
 ./scripts/rotation.ini
 ./scripts/sipcapture.crontab
 ./sql/homer_databases.sql
 ./sql/homer_user.sql
 ./sql/homer_user_bootstrap.sql
 ./sql/postgres/homer_databases.sql
 ./sql/postgres/homer_user.sql
 ./sql/postgres/schema_configuration.sql
 ./sql/postgres/schema_data.sql
 ./sql/postgres/schema_statistic.sql
 ./sql/schema_configuration.sql
 ./sql/schema_data.sql
 ./sql/schema_statistic.sql
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./api/Database/DefaultConnector.php
 ./api/Database/Layer/mysql.php
 ./api/Database/Layer/pgsql.php
 ./api/Database/Layer/sqlite.php
 ./api/Database/PDOConnector.php
 ./api/RestApi/Admin.php
 ./api/RestApi/Alarm.php
 ./api/RestApi/Auth.php
 ./api/RestApi/Dashboard.php
 ./api/RestApi/Profile.php
 ./api/RestApi/Report.php
 ./api/RestApi/Search.php
 ./api/RestApi/Statistic.php
 ./api/RestService/Client.php
 ./api/Statistic/Internal.php
 ./api/autoload.php
 ./api/index.php
Copyright: 2011-2015, Alexandr Dubovikov <alexandr.dubovikov@gmail.com>
  2011-2015, Lorenzo Mangani <lorenzo.mangani@gmail.com> QXIP B.V.
License: AGPL-3+
 FIXME

Files: ./api/Authentication/Authentication.php
 ./api/Authentication/External.php
 ./api/Authentication/Internal.php
 ./api/Authentication/LDAP.php
Copyright: 2011-2012, Lorenzo Mangani <lorenzo.mangani@gmail.com>
  2011-2014, Alexandr Dubovikov <alexandr.dubovikov@gmail.com>
License: AGPL-3+
 FIXME

Files: ./scripts/homer_mysql_rotate.pl
 ./scripts/old/homer_mysql_new_table.pl
 ./scripts/old/homer_mysql_partrotate_unixtimestamp.pl
 ./scripts/pgsql/homer_pgsql_rotate.pl
Copyright: 2011-2014, Alexandr Dubovikov (alexandr.dubovikov@gmail.com)
  2011-2015, Alexandr Dubovikov (alexandr.dubovikov@gmail.com)
  2011-2016, Alexandr Dubovikov (alexandr.dubovikov@gmail.com)
License: GPL-3+
 FIXME

Files: ./debian/copyright
 ./pkg/debian/copyright
Copyright: -format/1.0/
License: AGPL-3+
 FIXME

Files: ./api/COPYING
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: AGPL-3
 FIXME

Files: ./README.md
Copyright: 
  *webHomer is released under GNU AGPLv3 license
  you have, the ownership of which remains in full with you. Developers can coordinate with the existing team via the [homer-dev](http:groups.google.com/group/homer-dev) mailing list. If you'd like to join our internal team and volounteer to help with the project's many needs, feel free to contact us anytime!
License: GPL
 FIXME

Files: ./scripts/homer_mysql_remove_partitions.pl
Copyright: 2016, Sebastian Damm (damm@sipgate.de)
License: GPL-3+
 FIXME

Files: ./apidoc/html/img/glyphicons-halflings-white.png
Copyright: 5xû°þ_ü
  $Ü¨0Þ'³Y-?
  ¢LÔ8ÄÃ·Z#½Aï¤O%ÕÀY)N¹U®5YêÑe¼dJÎE3dZØ°þÇ<Èx·ÇñØÉñä¶e @ùPÚ§ÏþÎFúTR
  3å8oÙ)k1'ûüd6«>T
  OÉÌ£¥k7 lIo°ý¨ý¶ßJ°F
  UßßB°LcKfáb×ö>a=Òb~¹R]aG%[ú÷×js@«<i[Ð¥*^.d;UI©R+ëOD2eÊÜ¶å ú¡QÃãN3©4"1£
  W±¾$Uû'
  qtcxx|H>¬Æø=ð:³ÅçýÎmÊjÕå¬ßÿìÕUßòÁóv£qìys©ÜLglþC6+[FÍSWg
  rìÓJÆçyBêæyÓFzÑõFN¢$¢HbÈÈÕ³*+jÕqòÑÎÀ Ú«kÝ¿UàX¯lºe·ìÄö¾Ä1ÕÊÚdà0d^õ-B%}ê
  qìÀbÒÓ|$½:Ü)Â/E¾%÷ânR¹qCàhnµÉ%õiÓÌ­º¶¶ß}lm
  Ñ*ý©^æÖ0:åýÏéÜ²ö3ÿ3
  ­_æ«	Ni-:
  ¯ÝsçkóAcrNôþä½Ñ
  ´:é|	ý,e¯SZ,o¿XríäÎËXº!ëRæÇÆò@áZøv 0Ôç> ?Á*ç®
  µ2ZíÆtÜß+åV¥ÔA¬6g</æQ
  ·Dÿô­R Ä2Xm£#a
  ¸¿^ÈdÕÚ²öcØÚú Õ,!ÄÐ1©øi&xi_VK@ip«Í9¯ÐÞVi%a;
  ÄùúúaõøÀÇÕÂÌT4Ïoà~by?wpÇjæ»¥ÖA
  È×+.(ecJ:zðªóWZ°ëªwïÒÙQþáðÅ~aÛÒêØÍöpç6,e5í¯,¬+¢Á,ýûÿð­óñ÷ÿt±võ%O^OøüO}ã× -Oüú7>e²ÚkC¦6£waô_þëC
  ×¯LîmÕøÈÎÑÊÄr@¦3uT
  àC_5Xý#ú[¿öwX3ábñÎå»«ÄR½{ùÎâ¢NKðAîÏÿée S«èÓeª|Ýã¹wñ¢ÇxâºÊÞsôño>ÖPåÔ6Ò;nVÛm¯fëI$àøÇûVÍJ- ÛJ%Ö¼0¯óUwûYÐÉSõóó×nuÿÒmÿè®Æù«xzµÒ-VÆ«ÚIµvnôWÿÚ_ÿqLZØÇòé"_X®zÃ÷Æ
  íÀjzÿêÝ pèºþ{¨¤ÖáÜlu:OñÁ®«ÆÌ)«s¤%Q@ãÍ$Þ<]f	
  ðÆâ¿oS·ß?ÅCá
  ôÈ3²r0ü=8TÂ¿ªi­J©ëÈ6uF
  ø@;¦îän*T
  ú¼]êe²JÊêÁ¤iÐB464^tïuÙ²þUÖ:G4'¿ò22YêpÎëÌu¦G'/PyÙ4?¡þè.ÕæSBP_>ÑëI
  ÿù0.àCµ¯@m¾çÓçß$-ßÄ/~|Y¥å[eþweQ ýÙ×¶&cëÊO4s|c§JåûwsïûXÍ8/ñ¼Î6Ï/
License: UNKNOWN
 FIXME

Files: ./apidoc/html/img/glyphicons-halflings.png
Copyright: o¯*U®>îä^«*ewä>Í«Ê§×á«
  *-T=]Ýúèãuu}¯£>Ý¨ÚNÐ­
  JUiT}rñW¾¨W'Ú¹uª¸)Ê¢é÷ËF"YUþ#ðüPã×¾çê&ÜíÐ
  Uôÿ®Ð6×ëskõÌ©n~ý[üqòÇ¸þ-ÿ¦`
  Ì®bT¢:£¯³ÊBá?6ñþ£ëo½J°Â1Î%óI
  ½ìÒ¦:)·Îj2OkÎª'
  ª[]<.Cw¯ ¯+W)¿ïb¼ª
  ¿¸S9>¿Ð}7ª6rÊ©µzuùÞíùà~å½4­ýoÄ¨
  ¿¸S9ÞÖ|Åºs%Ú>Ë_õo#§ü9Ø¤EU~ÿ/ÝÚ÷t(r[½QµªZuûOo;°ËèÛ!MrU»]ÛÎ0TÓêcpDÅÕ? .¬õÚcÎêPuí±Íç¬F²û²;¼ªL_¹©ª«³ùSÞÓb}¸R/J_Ëç+ßÔh¥2$õaµ£i
  ÃÆ;>¾GÔÕ^è
  ÊÉ*ú=±ªªÄçIÑxcáy}®öY(þ£ëoÖu
  Ï<1mkS¤ÉåÅ4iÏ*.{Nü8Xaj~éïÚânAx,ÿ×%fE:Ã|éYDV÷j
  ó>Uo½ÇõTU1cÆYuÖ¼ÅìcÞùa&÷£Â#C,pØÄ¸>kÚº
  ùùûèøJª:3çÑ´UGo)XðÄ.ÌÃ¼±ê*j÷¾?}ßãÅG~A{Y#ÆW/3ªêé¬¶¿!Ê¼ãø=ßïCögÅu	*Ôþõu_õðÞ®+«Qeë5Âw¯:øÊÏUéêËK©?UW1jÇÆS5/<«z7P^Öð÷ª<,S¤¿jUU8üÜªêè¬v,2_Æï_ßûiÚë»®î«^ª¥ÖR5^v¤öNl>G×¹]úg
License: UNKNOWN
 FIXME

Files: ./apidoc/html/locales/fr.js
Copyright: es :',
  faut :',
  nÃ©ral',
  nÃ©rÃ© avec',
  ponse',
  ponse.',
  sentative',
License: UNKNOWN
 FIXME

Files: ./apidoc/html/img/favicon.ico
Copyright: ½³
License: UNKNOWN
 FIXME

