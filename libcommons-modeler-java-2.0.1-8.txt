Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./LICENSE.txt
 ./build.properties.default
 ./build.properties.sample
 ./build.xml
 ./debian/pom.xml
 ./maven.xml
 ./project.properties
 ./project.xml
 ./src/java/org/apache/commons/modeler/AttributeInfo.java
 ./src/java/org/apache/commons/modeler/BaseAttributeFilter.java
 ./src/java/org/apache/commons/modeler/BaseModelMBean.java
 ./src/java/org/apache/commons/modeler/BaseNotification.java
 ./src/java/org/apache/commons/modeler/BaseNotificationBroadcaster.java
 ./src/java/org/apache/commons/modeler/ConstructorInfo.java
 ./src/java/org/apache/commons/modeler/FeatureInfo.java
 ./src/java/org/apache/commons/modeler/FieldInfo.java
 ./src/java/org/apache/commons/modeler/FixedNotificationFilter.java
 ./src/java/org/apache/commons/modeler/JndiJmx.java
 ./src/java/org/apache/commons/modeler/Main.java
 ./src/java/org/apache/commons/modeler/ManagedBean.java
 ./src/java/org/apache/commons/modeler/NotificationInfo.java
 ./src/java/org/apache/commons/modeler/OperationInfo.java
 ./src/java/org/apache/commons/modeler/ParameterInfo.java
 ./src/java/org/apache/commons/modeler/Registry.java
 ./src/java/org/apache/commons/modeler/RegistryMBean.java
 ./src/java/org/apache/commons/modeler/ant/Arg.java
 ./src/java/org/apache/commons/modeler/ant/JmxInvoke.java
 ./src/java/org/apache/commons/modeler/ant/JmxSet.java
 ./src/java/org/apache/commons/modeler/ant/MLETTask.java
 ./src/java/org/apache/commons/modeler/ant/ModelerTask.java
 ./src/java/org/apache/commons/modeler/ant/RegistryTask.java
 ./src/java/org/apache/commons/modeler/ant/ServiceTask.java
 ./src/java/org/apache/commons/modeler/ant/ant.properties
 ./src/java/org/apache/commons/modeler/ant/package.html
 ./src/java/org/apache/commons/modeler/mbeans-descriptors.dtd
 ./src/java/org/apache/commons/modeler/mbeans/MBeanProxy.java
 ./src/java/org/apache/commons/modeler/mbeans/SimpleRemoteConnector.java
 ./src/java/org/apache/commons/modeler/modules/MbeansDescriptorsDOMSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansDescriptorsDigesterSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansDescriptorsDynamicMBeanSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansDescriptorsIntrospectionSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansDescriptorsSerSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansSource.java
 ./src/java/org/apache/commons/modeler/modules/MbeansSourceMBean.java
 ./src/java/org/apache/commons/modeler/modules/ModelerSource.java
 ./src/java/org/apache/commons/modeler/modules/package.html
 ./src/java/org/apache/commons/modeler/package.html
 ./src/java/org/apache/commons/modeler/util/DomUtil.java
 ./src/java/org/apache/commons/modeler/util/IntrospectionUtils.java
 ./src/test/org/apache/commons/modeler/RegistryTestCase.java
 ./src/test/org/apache/commons/modeler/demo/Connector.java
 ./src/test/org/apache/commons/modeler/demo/Container.java
 ./src/test/org/apache/commons/modeler/demo/Demo.java
 ./src/test/org/apache/commons/modeler/demo/Engine.java
 ./src/test/org/apache/commons/modeler/demo/Server.java
 ./src/test/org/apache/commons/modeler/demo/Service.java
 ./src/test/org/apache/commons/modeler/demo/mbeans-descriptors.xml
 ./src/test/org/apache/commons/modeler/mbeans-descriptors.xml
 ./xdocs/building.xml
 ./xdocs/cvs-usage.xml
 ./xdocs/downloads.xml
 ./xdocs/index.xml
 ./xdocs/issue-tracking.xml
 ./xdocs/navigation.xml
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./RELEASE-NOTES.txt
 ./debian/ant.properties
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libcommons-modeler-java-doc.doc-base
 ./debian/libcommons-modeler-java-doc.install
 ./debian/libcommons-modeler-java.docs
 ./debian/libcommons-modeler-java.poms
 ./debian/patches/01_build_xml.patch
 ./debian/patches/02_mbeans_descriptor_dtd.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./src/conf/MANIFEST.MF
 ./xdocs/style/project.css
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: 2007, Marcus Better (Debian packaging)
  © 2002-2008 The Apache Software Foundation
License: Apache
 FIXME

Files: ./xdocs/images/modeler-logo-white.png
Copyright:  Sm©£ciÇ"â|r~y
  ¹S§N0zôhB@FFZæ%P6mÚÄ:¸å!<ûì³ZÛæÏ¯[åi¸­Í:t0}SZKJJ4AËìÖ­ôìÙz÷î­K)À¥àÆcùÁ@AAíÇ§~pâÄ	Ø¼y³2n¶Oö¬çÄQQQ­G
  -ËLãm±´¢{%iÜ¸óumB
  TQ<vAIÝë¸ÃUàRÍ%SPfµz¯9
  %¬ÛU__47nnºIw'>KJÒaÖ²ß¸òÓÓÓ¡Eì½¦:`ª®å ÑSu~/]3ÅÂPw|eÓsý³ø²[³¹ÕsåÊh8°à»+9A V>555ºªÍ´¶8zJJJ´8XyÙ©fÀT­»
  8ÀÔÕÕA-þ+uüàt6±LÓóKMè66ùÐ½·d
  @-
  FíãÆö«ÌÏÓì´H¥À)/TÞK!AÍÛ&Ç$¸r3GÎep¹fGDØ¥jw:5¹¿è¼	;dçÈ-=Që@¶;¡Û¢
  S'Ó%GZÐf9sæ(×h¦L'N%KÀÎ;aûöíðõ×_ÃÂ
  S§L~çÍ§£wÞ¼y¶ò2eV:^¯W>}ZÇ¤ñññbÝºu¿Èñæh;v¬éX®]»V¤¤¤(Ë;wÚð  Þÿ}­_ª««MÛ'i»wïÖ	HÇ#víÚehW]]¸råV
  S§Â
  Sâµ×^cÐª'O×^{¥ Ä|
  b,îJîÁó&³uËºº:-
  fu  ;wÖ¯_øøxCyû÷ï}ûöÁþýûµ6¦¤¤èêV=ü18p Î=«Ë¯8J/¥{ÿþý°aÃX°`Ûö
  ÛÇÜæ¿að~÷Ýw¬öÀÀT^6w^xx8¼xQWÞ
  G¥ÕITrÞ_'%ÕªBå`©©p0ÝQì
  ¢³²²ÒPFÇ5ÀÞ|óÍpôèQÝ÷sçÎApp°J33kËÈÈH8yò¤.Mnn.ôë×Ow¾Ýc%¥F`4Ìcóóó¡wïÞ¨$ø¹ÓUuH+_Eãy¥õçôJÍ¬mÂåÅsM+ï£|­! ï¥¥¥Úº¢ÛíòòrC»îºKÓàÜRÈÑ£GÁívC]]¸ÝnÝü
  µD0½N§
  ±´ÒàÚw¡è!f
  ¦Õ~L9G
  ¤«eK@rÀtª<Mtôè
  §ÞÔÌÇýJçÄ¸¿8sNèü³¶ðq+ù5æUN;á~£cFw¾¨úÆSá- 3`RÊþwRjÌ
  §2ígW¾­^<{÷îÕÑmÅ3»víGW7²í{öìÓ¦MÓµýÃ?Ôúå~S§N5å¡5kÖúE5²}Ó§O×*RSSumMIIÑxÈ._Ê~«¯¯
  §R
  ©ÕÕÕ¢ººZlÛ¶MDGGÛÊ-jjj
  ªª7Þx
  ­­555¢ªªJÌ;×Ðè¨¨(Q^^.ÊÊÊDyy¹(ZÃß{ï=CÇb	#Ë¯®®ÖéòåËsáwÌ1Z²K.iùTB½_¿~"33SºtI+£¬¬LÜÜ¶m-/  Dvv¶¸xñ¢öÒÒR#¢¢¢
  ´¦ªéJJJXÇÌ
  ·PÒÁ=Rsqë×´)ÏÐ'<<Ü håËõJÐ`!`µm.R
  Æ
  â[©å¢G
  û®VL|YÖ¶mÛàôéÓò¥
License: UNKNOWN
 FIXME

Files: ./NOTICE.txt
Copyright: 2001-2007, The Apache Software Foundation
License: UNKNOWN
 FIXME

Files: ./xdocs/images/modeler-logo-white.xcf
Copyright: ÿÿû  ÿÿþÔ  ÿ þ©ÿÿû  ÿÿþÔ  ÿþD  þÞÿ	ÿþò  ÿ þÞÿ	ÿþò  ÿþ  þøÿÿþ
License: UNKNOWN
 FIXME

