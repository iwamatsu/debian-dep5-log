Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./CHANGELOG.txt
 ./CONTRIBUTING.md
 ./build.xml
 ./debian/compat
 ./debian/control
 ./debian/libjmdns-java-doc.doc-base
 ./debian/libjmdns-java-doc.install
 ./debian/libjmdns-java.poms
 ./debian/maven.ignoreRules
 ./debian/maven.properties
 ./debian/maven.rules
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./fulllogging.properties
 ./p2/features/org.jmdns.feature/build.properties
 ./p2/features/org.jmdns.feature/pom.xml
 ./p2/features/pom.xml
 ./p2/pom.xml
 ./p2/repository/category.xml
 ./p2/repository/pom.xml
 ./p2/targetplatform/jmdns.target
 ./p2/targetplatform/pom.xml
 ./setversion.sh
 ./src/main/java/javax/jmdns/JmmDNS.java
 ./src/main/java/javax/jmdns/NetworkTopologyDiscovery.java
 ./src/main/java/javax/jmdns/NetworkTopologyEvent.java
 ./src/main/java/javax/jmdns/NetworkTopologyListener.java
 ./src/main/java/javax/jmdns/impl/DNSMessage.java
 ./src/main/java/javax/jmdns/impl/DNSTaskStarter.java
 ./src/main/java/javax/jmdns/impl/JmmDNSImpl.java
 ./src/main/java/javax/jmdns/impl/ListenerStatus.java
 ./src/main/java/javax/jmdns/impl/NameRegister.java
 ./src/main/java/javax/jmdns/impl/NetworkTopologyDiscoveryImpl.java
 ./src/main/java/javax/jmdns/impl/NetworkTopologyEventImpl.java
 ./src/main/java/javax/jmdns/impl/constants/DNSLabel.java
 ./src/main/java/javax/jmdns/impl/constants/DNSOperationCode.java
 ./src/main/java/javax/jmdns/impl/constants/DNSOptionCode.java
 ./src/main/java/javax/jmdns/impl/constants/DNSRecordClass.java
 ./src/main/java/javax/jmdns/impl/constants/DNSRecordType.java
 ./src/main/java/javax/jmdns/impl/constants/DNSResultCode.java
 ./src/main/java/javax/jmdns/impl/constants/package-info.java
 ./src/main/java/javax/jmdns/impl/package-info.java
 ./src/main/java/javax/jmdns/impl/tasks/package-info.java
 ./src/main/java/javax/jmdns/impl/tasks/resolver/package-info.java
 ./src/main/java/javax/jmdns/impl/tasks/state/package-info.java
 ./src/main/java/javax/jmdns/impl/util/ByteWrangler.java
 ./src/main/java/javax/jmdns/package-info.java
 ./src/main/resources/version.properties
 ./src/sample/java/samples/ITunesRemotePairing.java
 ./src/site/resources/css/maven-base.css
 ./src/site/resources/css/print.css
 ./src/site/resources/css/site.css
 ./src/site/site.vm
 ./src/site/site.xml
 ./src/site/xdoc/index.xml
 ./src/test/java/javax/jmdns/impl/JmDNSTest.java
 ./src/test/java/javax/jmdns/impl/ServiceInfoImplTest.java
 ./src/test/java/javax/jmdns/test/DNSCacheTest.java
 ./src/test/java/javax/jmdns/test/DNSMessageTest.java
 ./src/test/java/javax/jmdns/test/DNSStatefulObjectTest.java
 ./src/test/java/javax/jmdns/test/JmmDNSTest.java
 ./src/test/java/javax/jmdns/test/ServiceInfoTest.java
 ./src/test/java/javax/jmdns/test/TextUpdateTest.java
 ./src/test/java/javax/jmdns/util/test/ByteWranglerTest.java
 ./src/test/resources/javax/jmdns/impl/a_record_before_srv.bin
 ./src/test/resources/javax/jmdns/impl/a_record_before_srv.pcapng
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/main/java/javax/jmdns/JmDNS.java
 ./src/main/java/javax/jmdns/ServiceEvent.java
 ./src/main/java/javax/jmdns/ServiceInfo.java
 ./src/main/java/javax/jmdns/ServiceListener.java
 ./src/main/java/javax/jmdns/ServiceTypeListener.java
 ./src/main/java/javax/jmdns/impl/DNSEntry.java
 ./src/main/java/javax/jmdns/impl/DNSIncoming.java
 ./src/main/java/javax/jmdns/impl/DNSListener.java
 ./src/main/java/javax/jmdns/impl/DNSOutgoing.java
 ./src/main/java/javax/jmdns/impl/DNSQuestion.java
 ./src/main/java/javax/jmdns/impl/DNSRecord.java
 ./src/main/java/javax/jmdns/impl/HostInfo.java
 ./src/main/java/javax/jmdns/impl/JmDNSImpl.java
 ./src/main/java/javax/jmdns/impl/ServiceEventImpl.java
 ./src/main/java/javax/jmdns/impl/ServiceInfoImpl.java
 ./src/main/java/javax/jmdns/impl/SocketListener.java
 ./src/main/java/javax/jmdns/impl/constants/DNSConstants.java
 ./src/main/java/javax/jmdns/impl/constants/DNSState.java
 ./src/main/java/javax/jmdns/impl/tasks/RecordReaper.java
 ./src/main/java/javax/jmdns/impl/tasks/Responder.java
 ./src/main/java/javax/jmdns/impl/tasks/resolver/ServiceInfoResolver.java
 ./src/main/java/javax/jmdns/impl/tasks/resolver/ServiceResolver.java
 ./src/main/java/javax/jmdns/impl/tasks/resolver/TypeResolver.java
 ./src/main/java/javax/jmdns/impl/tasks/state/Announcer.java
 ./src/main/java/javax/jmdns/impl/tasks/state/Canceler.java
 ./src/main/java/javax/jmdns/impl/tasks/state/Prober.java
 ./src/main/java/javax/jmdns/impl/tasks/state/Renewer.java
 ./src/main/java/javax/jmdns/impl/util/NamedThreadFactory.java
Copyright: 2003-2005, Arthur van Hoff, Rick Blair
  2003-2012, Arthur van Hoff, Rick Blair
License: Apache-2.0
 FIXME

Files: ./LICENSE
 ./NOTICE.txt
 ./README.md
 ./p2/features/org.jmdns.feature/license.html
 ./pom.xml
 ./src/main/java/javax/jmdns/impl/DNSStatefulObject.java
 ./src/main/java/javax/jmdns/impl/tasks/DNSTask.java
 ./src/main/java/javax/jmdns/impl/tasks/resolver/DNSResolverTask.java
 ./src/main/java/javax/jmdns/impl/tasks/state/DNSStateTask.java
 ./src/sample/java/samples/TestShutdownHook.java
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./src/sample/java/com/strangeberry/jmdns/tools/Browser.java
 ./src/sample/java/com/strangeberry/jmdns/tools/Main.java
 ./src/sample/java/com/strangeberry/jmdns/tools/Responder.java
 ./src/sample/java/samples/DiscoverServiceTypes.java
 ./src/sample/java/samples/DiscoverServices.java
 ./src/sample/java/samples/ListServices.java
 ./src/sample/java/samples/OpenJmDNS.java
 ./src/sample/java/samples/RegisterService.java
Copyright: NONE
License: Apache-2.0 and/or LGPL-2.1+
 FIXME

Files: ./src/sample/java/com/strangeberry/jmdns/tools/package.html
 ./src/sample/java/samples/package.html
Copyright: NONE
License: LGPL-2.1+
 FIXME

Files: ./p2/features/org.jmdns.feature/feature.properties
Copyright: NONE
License: Apache
 FIXME

Files: ./src/main/java/javax/jmdns/impl/DNSCache.java
Copyright: 2003-2005, Arthur van Hoff Rick Blair
License: Apache-2.0
 FIXME

Files: ./src/test/java/javax/jmdns/test/DNSRecordTest.java
Copyright: 2015, JmDNS.
License: Apache-2.0
 FIXME

Files: ./src/site/resources/images/logos/logo.png
Copyright: ó***`Y(¥°mèºËÊÿnl,t:Ó4öÜxã
  ¼,UY6ÉDÒVVx&=ìc0^{°dÈBâEmß¾íííÐuýÒk¯½vÝ×¿þµsÿV( Gþ=HsË²`[6%ð²`¸3D"PÆ2M!À9c
  45F$,Wõ² &Ce«[ÓÜc:oÞ¼X4:ÏçK,)Á¥HxÙÐÐàê¶c«Óê(¥
  BàÇÄÆáä.@
  H°8 ÀLy ÈÃýësxý°âÿþ/«]·nÝW_}õsßúÖ·nú¨¼óÎ;oØ½»óLºª¹¹yWuuõ
  T555kÖ®}æ
  TªnöìÙ³m
  k×®]5<<2ãÂ
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  §º¿«kïk®¹öóç{îÚ-¥ºë®»®ÛµkWUax¢-³$òn¢T.
  ©	;wîÄâÅWuÖY/|<Á¿ßrË÷ûz{455Ïça&gÈ96!Áô4ä 9ÓfpsZ¾úÅ/^ó³wõ²ÿïÒíÇ/[²
  ©	cc£¸ÿþÐÒÒÓN;íÜ
  ©þ~__ÿ|òÉ?
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  ÇyøÅCÝÝo¬¹éËÞ	ÌÃ
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./src/site/resources/images/logos/opensource_java.png
Copyright: zÿí¾üÈä´öb{ÝÙ®ÚiÅeA}åHYFR
  
  4,ü,K7×v0ì>|b%`yr
License: UNKNOWN
 FIXME

Files: ./src/site/resources/images/logos/build-by-maven-white.png
Copyright: 
  mcccÃÃÃ|>ßÅÅ
  ã8ã>£ðsß,¥¥¥AÔÖÖîææóÂÞ½{ÇÇÇ-Ëå-h¦i:44´««
  ÌÌÌ$z||­V«Tªèèè¦¦¦'Nxzz:99)ÊyÃd23&&wR©Ôh4V«upp âëë;Uþ¼ÎvT6ûÇÕN·oooßÝÝ§cû^^^&	!tîÜ9¡wtt$¸9rOimm¥i hÉdÓWËÅÅeddd~à¨««#vËËË±°§§ÇÎÎ
  ô÷÷ß°aÃ¥KFãgíìì]]¥R©D"!ÂÕ«WÇÇÇûùù%&&VWWïÚµëîÝ»¡[·n%$$ À±cÇJå×
License: UNKNOWN
 FIXME

Files: ./src/site/resources/images/logos/build-by-maven-black.png
Copyright: -_f-Yóe¤ÿø^âïÆâz{{µZ­V«MNN¦ï½xñ"ëëë¦ÈÈÈÐó°ÙlCCCééétJQQN¹víÚéÓ§ÕjµÃá°ÛíoÞ¼ùî»ïV???Ã+¸qã^.ÂÂÂèÎ{{{7nÜ)ÒD§ÓÑüÒô½X8*CthnnÆYGAKPPÐüü<!daa!*
  4>>þÕ«W¥¹¹ùÒ¥K·oß ãöíÛçääô
  pÉ
  sÏ}ïÞï~ïsÀ¬É¬¤¦¦ÖÔÔÔÔÔTWWzxx¬ôQ(bY¶¤¤ÄÕÕU&Åâ¥£¹¹¹ßÄfÙ_ôØºu+!¤ªªª¶¶6<<¼¼¼|¥ÏÜÜÜøø8Ë²NNNçÎKMM¥£~~~1118 `rrR©T¶¶¶ÖÔÔ@IID"ôôô¤¤$www???tÞ¶m[JJJ^^µ Àúõë¯_¿^__ÄqÜ
  ÐØÐÐ@oÝº
  ß^¯Yk(F2
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./p2/features/org.jmdns.feature/feature.xml
Copyright: 2018, JmDNS.org and others
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Updated the Format URI
License: UNKNOWN
 FIXME

Files: ./src/site/resources/images/logos/opensource_bonjour.png
Copyright: d2[Ê ¯
License: UNKNOWN
 FIXME

Files: ./src/test/java/util/StringUtil.java
Copyright: Ã¨ÃªÃ­Ã¬Ã®Ã³Ã²Ã´Ã¶ÃÃºÃ¹Ã»Ã¼ÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃÃ";
License: UNKNOWN
 FIXME

Files: ./src/site/resources/images/logos/maven-feather.png
Copyright: ý7Va××ááãeçæáæ$)8 ØààááQWW:èî9sæ$&&âQ
License: UNKNOWN
 FIXME

