Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./audio.h
 ./bell-p-w.c
 ./bell-p-w.h
 ./cw_rx.c
 ./cw_rx.h
 ./fft.h
 ./fft_radix2.h
 ./g-l_dpss.c
 ./g-l_dpss.h
 ./g_about.h
 ./g_main.h
 ./g_options.h
 ./g_scope.c
 ./g_scope.h
 ./g_txmsg.c
 ./g_txmsg.h
 ./hparma.c
 ./hparma.h
 ./lmp.c
 ./lmp.h
 ./mtm.c
 ./mtm.h
 ./qrs.c
 ./qrs.h
 ./rcfile.h
 ./source.c
 ./source.h
 ./util.c
 ./util.h
Copyright: 2001, Claudio Girardi
  2001-2003, Claudio Girardi
  2001-2007, Claudio Girardi
  2001-2008, Claudio Girardi
  2002, Claudio Girardi
  2002-2008, Claudio Girardi
  2004, Claudio Girardi
  2005, Claudio Girardi
  2005-2008, Claudio Girardi
  2006, Claudio Girardi
  2007, Claudio Girardi
  2008, Claudio Girardi
License: GPL-2+
 FIXME

Files: ./AUTHORS
 ./ChangeLog
 ./HACKING
 ./INSTALL
 ./Makefile.am
 ./NEWS
 ./README
 ./TODO
 ./config.h.in
 ./configure.in
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/glfer.desktop
 ./debian/glfer.install
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./glfer.1
 ./glfer.lsm
 ./mkinstalldirs
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./g_about.c
 ./g_options.c
 ./glfer.h
 ./rcfile.c
Copyright: 2001-2002, Claudio Girardi
  2001-2009, Claudio Girardi
  2001-2010, Claudio Girardi
  2006, Edouard Griffiths for modifications implementing
  2008, Claudio Girardi
License: GPL-2+
 FIXME

Files: ./audio.c
 ./fft.c
 ./g_file_dialogs.c
 ./g_file_dialogs.h
Copyright: 2000, Vincent Arkesteijn
  2001-2005, Claudio Girardi
  2001-2007, Claudio Girardi
  2009, Claudio Girardi
License: GPL-2+
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./aclocal.m4
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2005, Free Software Foundation, Inc.
  1996-2005, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./wav_fmt.c
 ./wav_fmt.h
Copyright: 2001, Claudio Girardi
  David Monro 1996
License: GPL-2+
 FIXME

Files: ./avg.c
 ./avg.h
Copyright: 2006, Edouard Griffiths (F4EXB)
  2006-2008, Claudio Girardi
License: GPL-2+
 FIXME

Files: ./g_main.c
 ./glfer.c
Copyright: 2000, Vincent Arkesteijn
  2001-2008, Claudio Girardi
  2001-2009, Claudio Girardi
  2006, Edouard Griffiths for modifications implementing
License: GPL-2+
 FIXME

Files: ./depcomp
 ./missing
Copyright: 1996-1997, 1999-2000, 2002-2003, Free Software Foundation, Inc.
  1999-2000, 2003-2004, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2008, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./fft_radix2.c
Copyright: 1996-2000, Brian Gough
  2001, Claudio Girardi
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: 2000, Vincent Arkesteijn
  2001-2008, Claudio Girardi
  2006, Edouard Griffiths for modifications implementing
  from src/glfer.c:
License: GPL-2+
 FIXME

Files: ./install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: NTP
 FIXME

Files: ./mixer.c
Copyright: Craig Metz and Hannu Savolainen 1993.
License: UNKNOWN
 FIXME

