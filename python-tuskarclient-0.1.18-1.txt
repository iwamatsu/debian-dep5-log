Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./LICENSE
 ./doc/source/conf.py
 ./setup.cfg
 ./tuskarclient/__init__.py
 ./tuskarclient/client.py
 ./tuskarclient/common/auth.py
 ./tuskarclient/common/formatting.py
 ./tuskarclient/openstack/common/_i18n.py
 ./tuskarclient/openstack/common/apiclient/utils.py
 ./tuskarclient/osc/plugin.py
 ./tuskarclient/osc/v2/plan.py
 ./tuskarclient/osc/v2/role.py
 ./tuskarclient/shell.py
 ./tuskarclient/tests/common/test_auth.py
 ./tuskarclient/tests/common/test_formatting.py
 ./tuskarclient/tests/integration/test_help_command.py
 ./tuskarclient/tests/osc/test_plugin.py
 ./tuskarclient/tests/osc/v2/fakes.py
 ./tuskarclient/tests/osc/v2/test_plans.py
 ./tuskarclient/tests/osc/v2/test_roles.py
 ./tuskarclient/tests/test_client.py
 ./tuskarclient/tests/test_shell.py
 ./tuskarclient/tests/utils.py
 ./tuskarclient/tests/v2/test_client.py
 ./tuskarclient/tests/v2/test_plans.py
 ./tuskarclient/tests/v2/test_plans_shell.py
 ./tuskarclient/tests/v2/test_roles.py
 ./tuskarclient/tests/v2/test_roles_shell.py
 ./tuskarclient/v2/client.py
 ./tuskarclient/v2/plans.py
 ./tuskarclient/v2/plans_shell.py
 ./tuskarclient/v2/roles.py
 ./tuskarclient/v2/roles_shell.py
 ./tuskarclient/v2/shell.py
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./.coveragerc
 ./.gitreview
 ./.testr.conf
 ./.travis.yml
 ./MANIFEST.in
 ./README.rst
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/gbp.conf
 ./debian/python-tuskarclient-doc.doc-base
 ./debian/python-tuskarclient.manpages
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/source/_static/basic.css
 ./doc/source/_static/default.css
 ./doc/source/_static/header-line.gif
 ./doc/source/_static/jquery.tweet.js
 ./doc/source/_static/pygments.css
 ./doc/source/_static/tweaks.css
 ./doc/source/_theme/layout.html
 ./doc/source/_theme/theme.conf
 ./doc/source/cli/v2/index.rst
 ./doc/source/cli/v2/plans.rst
 ./doc/source/cli/v2/roles.rst
 ./doc/source/index.rst
 ./doc/source/man/tuskar.rst
 ./openstack-common.conf
 ./requirements.txt
 ./test-requirements.txt
 ./tools/tuskar_with_venv.sh
 ./tox.ini
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./tuskarclient/common/utils.py
 ./tuskarclient/tests/common/test_utils.py
Copyright: 2012, OpenStack LLC.
  2013, OpenStack LLC.
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/apiclient/client.py
Copyright: 2010, Jacob Kaplan-Moss
  2011, OpenStack Foundation
  2011, Piston Cloud Computing, Inc.
  2013, Alessio Ababilov
  2013, Grid Dynamics
  2013, OpenStack Foundation
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/apiclient/exceptions.py
Copyright: 2010, Jacob Kaplan-Moss
  2011, Nebula, Inc.
  2013, Alessio Ababilov
  2013, OpenStack Foundation
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/apiclient/base.py
Copyright: 2010, Jacob Kaplan-Moss
  2011, OpenStack Foundation
  2012, Grid Dynamics
  2013, OpenStack Foundation
License: Apache-2.0
 FIXME

Files: ./setup.py
Copyright: 2013, Hewlett-Packard Development Company, L.P.
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/gettextutils.py
Copyright: 2012, Red Hat, Inc.
  2013, IBM Corp.
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/uuidutils.py
Copyright: 2012, Intel Corporation.
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/apiclient/fake_client.py
Copyright: 2013, OpenStack Foundation
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/apiclient/auth.py
Copyright: 2013, OpenStack Foundation
  2013, Spanish National Research Council.
License: Apache-2.0
 FIXME

Files: ./tuskarclient/openstack/common/cliutils.py
Copyright: 2012, Red Hat, Inc.
License: Apache-2.0
 FIXME

Files: ./debian/copyright
Copyright: (c) 2012 Thomas Goirand <zigo@debian.org>
  (c) 2013, Hewlett-Packard Development Company, L.P.
  -format/1.0/
  2007-2011, the Sphinx team
License: Apache-2.0 and/or BSD-3-clause
 FIXME

Files: ./doc/source/_static/nature.css
Copyright: Copyright 2007-2011 by the Sphinx team, see AUTHORS.
License: BSD~unspecified
 FIXME

Files: ./doc/source/_static/header_bg.jpg
Copyright: mimP         ü«¸ÿ ÑÝ_.8öÖÎ×%:
  5ÖÞ4b§ËUéïm7<ì1ô¨ÛÛLÖñÙ&§L]p³µªÍM#²Sö­
  ¨ëQ:ÈÑÕF_¢fF;õÝÊA^>ÎöRË·­Z¯Fö6Î»[¯¯¯­Ó®©?
  «^õ[0Iº;>¹¬FR%#¤V¥£J¯À¤/I«ÓBR"£i'®Âuk0E#¯]y%Hã£&®cº¥SÑ61j>S°¤eÖ6yÜ£1àµc²Kz§¡/üf6çÀWXü
  ÑÇê½wzWå¯±3ÅÖé¤Á8Ë±µV¬£Gª
  ×ÙÉÖf;uó£_ÚrJFø:ÖZ"ý¢kBR-ºÖ-¸Gìëí3e²Õ³´Û_]·,îVÑnäínØ®'V17®µ	(äµZ#/®ªóül
License: UNKNOWN
 FIXME

Files: ./doc/source/_static/openstack_logo.png
Copyright: 1ÁwÝ?,À¦YTqÓô9eæÍÕÎ=ZLPqóH¼Á
  açë¯S
  dp>)æÀy0à<¬ß[wþOæèã«9Væüâc9æÀé§cÛþ/À f÷¡Î¢0    IEND®B`
  yæ±BÁ¶hitIøÛIopñV	8Í´ÌÂO¤Ûj©¢Æåb'}>v[ÞÚÌ$0¶¸'j÷­$t» Ì -¡
  ¨SCO cæ%¦N^
  ¯-ÅÊ8Î6µ=^e2Ãh1ãqZrfî¥ßJ*Tñå
  §w9¿
  ¬
  ÅX,
  Ê»0ÊïÌ"<
  Ñïgß­¯g©õõúâéÊ§
  ÚëËÄ{«k¶ôx÷r.§Þúi :ÝzHàä
  ç(-Y
License: UNKNOWN
 FIXME

