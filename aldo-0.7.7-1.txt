Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./include/astream.hh
 ./include/audioexcep.hh
 ./include/audioworkspace.hh
 ./include/blocks.hh
 ./include/datafile.hh
 ./include/dialog.hh
 ./include/exception.hh
 ./include/keyer.hh
 ./include/koch.hh
 ./include/menu.hh
 ./include/option.hh
 ./include/qrz.hh
 ./include/random.hh
 ./include/resources.hh
 ./include/section.hh
 ./include/skill.hh
 ./include/textfile.hh
 ./include/wave.hh
 ./src/astream.cc
 ./src/audioworkspace.cc
 ./src/blocks.cc
 ./src/blocks_exc.cc
 ./src/check.cc
 ./src/datafile.cc
 ./src/keyer.cc
 ./src/koch.cc
 ./src/koch_exc.cc
 ./src/main.cc
 ./src/menu.cc
 ./src/option.cc
 ./src/qrz.cc
 ./src/qrz_exc.cc
 ./src/random.cc
 ./src/section.cc
 ./src/setup.cc
 ./src/textfile.cc
 ./src/textfile_exc.cc
 ./src/wave.cc
Copyright: 2001-2007, Giuseppe "denever" Martino
  2002-2007, Giuseppe "denever" Martino
License: GPL-2+
 FIXME

Files: ./AUTHORS
 ./NEWS
 ./README
 ./THANKS
 ./aldo.1
 ./config.h.in
 ./configure.ac
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/dirs
 ./debian/docs
 ./debian/patches/series
 ./debian/patches/use-default-ld-search-path.patch
 ./debian/rules
 ./debian/source/format
 ./debian/watch
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./Makefile.in
 ./include/Makefile.in
 ./src/Makefile.in
Copyright: 1994-2011, Free Software
License: UNKNOWN
 FIXME

Files: ./Makefile.am
 ./include/Makefile.am
 ./src/Makefile.am
Copyright: 2005, Giuseppe Martino <denever@users.sf.net>
License: UNKNOWN
 FIXME

Files: ./config/depcomp
 ./config/missing
Copyright: 1996-1997, 1999-2000, 2002-2006, 2008-2012, Free Software Foundation, Inc.
  1999-2000, 2003-2007, 2009-2011, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./config/install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2011, Free Software Foundation,
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2010, Free Software
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./aclocal.m4
Copyright: 1996-2011, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

Files: ./ChangeLog
Copyright: docs: Update debian package info
License: UNKNOWN
 FIXME

