Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./Makefile.am
 ./NEWS
 ./README
 ./autogen.sh
 ./blktool-config.h.in
 ./blktool.h
 ./debian/compat
 ./debian/control
 ./debian/patches/0001-fix-typos-in-manpage.patch
 ./debian/patches/0002-fix-string-error.patch
 ./debian/patches/0003-Fix-3-d-argument-for-BLKROSET-it-must-be-const-int.patch
 ./debian/patches/0004-fix-ftbfs-glibc-2.28.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./ata.c
 ./blktool.c
 ./scsi.c
 ./util.c
Copyright: 2004, Jeff Garzik
License: UNKNOWN
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./aclocal.m4
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2004, Free Software Foundation, Inc.
  1996-2004, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./depcomp
 ./missing
Copyright: 1996-1997, 1999-2000, 2002-2003, Free Software Foundation, Inc.
  1999-2000, 2003, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 2003, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-2+
 FIXME

Files: ./configure.ac
Copyright: 2001, Philipp Rumpf
  2004, Henrique de Moraes Holschuh <hmh@debian.org>
License: GPL-2+
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, Free Software
License: UNKNOWN
 FIXME

Files: ./blktool.8
Copyright: 2004, Jeff Garzik.
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: bump license to gpl-2.0+ (was: gpl-2)
  link to GPL-2 instead of versionless GPL
  migrate to DEP-5 (machine-readable debian/copyright file)
License: UNKNOWN
 FIXME

