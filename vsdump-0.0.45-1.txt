Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./INSTALL
 ./LICENSE
 ./Makefile.am
 ./NEWS
 ./README
 ./TODO
 ./config.h
 ./config.h.in
 ./configure.in
 ./debian/compat
 ./debian/control
 ./debian/dirs
 ./debian/docs
 ./debian/patches/00list
 ./debian/patches/01_fix_path_chunks-parse-cmds.dpatch
 ./debian/rules
 ./debian/source.lintian-overrides
 ./debian/source/format
 ./debian/vsdump.1
 ./debian/watch
 ./src/Makefile.am
 ./src/names.h
 ./src/vsd_dump_stream_15.h
 ./src/vsd_dump_stream_23.h
 ./src/vsd_dump_stream_c.h
 ./src/vsd_fonts_colors_parse.h
 ./src/vsd_inflate.h
 ./src/vsd_parse_blocks.h
 ./src/vsd_parse_chunks.h
 ./src/vsd_parse_cmds.h
 ./src/vsd_pointers.h
 ./src/vsd_utils.h
 ./src/vsdump.h
 ./stamp-h1
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/chunks_parse_cmds.tbl
 ./src/vsd_dump_stream_15.c
 ./src/vsd_dump_stream_23.c
 ./src/vsd_dump_stream_c.c
 ./src/vsd_fonts_colors_parse.c
 ./src/vsd_inflate.c
 ./src/vsd_parse_blocks.c
 ./src/vsd_parse_chunks.c
 ./src/vsd_parse_cmds.c
 ./src/vsd_pointers.c
Copyright: 2006-2007, Valek Filippov (frob@df.ru)
License: UNKNOWN
 FIXME

Files: ./Makefile.in
 ./aclocal.m4
 ./src/Makefile.in
Copyright: 1994-2005, Free Software Foundation, Inc.
  1996-2005, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./depcomp
 ./missing
Copyright: 1996-1997, 1999-2000, 2002-2005, Free Software Foundation, Inc.
  1999-2000, 2003-2005, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./src/vsd_utils.c
 ./src/vsdump.c
Copyright: 2002-2006, Jody Goldberg (jody@gnome.org)
  2006-2007, Valek Filippov (frob@df.ru)
License: UNKNOWN
 FIXME

Files: ./COPYING_ASL2
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2006, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: Copyright (C) 2007 Valek Filippov
License: GPL-3+
 FIXME

Files: ./debian/changelog
Copyright: (format).
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

Files: ./ChangeLog
Copyright: was set for all .c and .py files.
License: UNKNOWN
 FIXME

