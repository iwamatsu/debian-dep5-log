Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./Makefile.am
 ./NEWS
 ./README.md
 ./autogen.sh
 ./bctoolbox-tester.pc.in
 ./bctoolbox.pc.in
 ./build/android/Android-mbedtls.mk
 ./build/android/Android-polarssl.mk
 ./build/android/Android-tester.mk
 ./build/osx/Info.plist.in
 ./configure.ac
 ./debian/TODO
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/libbctoolbox-dev.dirs
 ./debian/libbctoolbox-dev.install
 ./debian/libbctoolbox1.dirs
 ./debian/libbctoolbox1.install
 ./debian/libbctoolbox1.lintian-overrides
 ./debian/libbctoolbox1.shlibs
 ./debian/patches/deprecated_mbedtls_header
 ./debian/patches/fix-ftbfs-with-mbedtls-2.7.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./include/Makefile.am
 ./include/bctoolbox/Makefile.am
 ./src/Makefile.am
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./cmake/gitversion.h.in
 ./include/bctoolbox/crypto.h
 ./include/bctoolbox/defs.h
 ./include/bctoolbox/list.h
 ./include/bctoolbox/map.h
 ./include/bctoolbox/parser.h
 ./include/bctoolbox/tester.h
 ./include/bctoolbox/vconnect.h
 ./include/bctoolbox/vfs.h
 ./src/containers/list.c
 ./src/containers/map.cc
 ./src/crypto/mbedtls.c
 ./src/crypto/polarssl.c
 ./src/crypto/polarssl1.2.c
 ./src/parser.c
 ./src/tester.c
 ./src/utils.h
 ./src/vconnect.c
 ./src/vfs.c
 ./tester/bctoolbox_tester.c
 ./tester/bctoolbox_tester.h
 ./tester/containers.cc
 ./tester/parser.c
 ./tester/port.c
Copyright: 2010-2017, Belledonne Communications SARL
  2013, Belledonne Communications SARL
  2016, Belledonne Communications SARL
  2017, Belledonne Communications SARL
License: GPL-2+
 FIXME

Files: ./CMakeLists.txt
 ./build/CMakeLists.txt
 ./cmake/BcGitVersion.cmake
 ./cmake/BcToolboxCMakeUtils.cmake
 ./cmake/BcToolboxConfig.cmake.in
 ./cmake/FindMbedTLS.cmake
 ./cmake/FindPolarSSL.cmake
 ./config.h.cmake
 ./include/CMakeLists.txt
 ./src/CMakeLists.txt
 ./tester/CMakeLists.txt
Copyright: 2014, Belledonne Communications, Grenoble France
  2015, Belledonne Communications, Grenoble France
  2016, Belledonne Communications, Grenoble France
  2017, Belledonne Communications, Grenoble France
License: GPL-2+
 FIXME

Files: ./src/logging/logging.c
 ./src/utils/port.c
Copyright: 2016, Belledonne Communications SARL
License: LGPL-2.1+
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: AGPL-2+ and/or LGPL-2.1+
 FIXME

Files: ./src/utils/exception.cc
Copyright: 2010-2015, Belledonne Communications SARL
License: AGPL-3+
 FIXME

Files: ./include/bctoolbox/exception.hh
Copyright: 2016, Belledonne Communications SARL.
License: AGPL-3+
 FIXME

Files: ./include/bctoolbox/logging.h
Copyright: 2016, Belledonne Communications, France, Grenoble
License: LGPL-2.1+
 FIXME

Files: ./include/bctoolbox/port.h
Copyright: 2001, Simon MORLAT simon.morlat@linphone.org
License: LGPL-2.1+
 FIXME

Files: ./debian/changelog
Copyright: Add new patch to fix FTBFS with new mbedtls-2.7.0.
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

