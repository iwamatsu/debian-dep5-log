Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.travis.yml
 ./Changes
 ./MANIFEST
 ./META.json
 ./META.yml
 ./Makefile.PL
 ./README.md
 ./TODO
 ./debian/compat
 ./debian/control
 ./debian/libmemcached-libmemcached-perl.docs
 ./debian/libmemcached/const-xs.inc
 ./debian/libmemcached/lib/Memcached/libmemcached/const_hash.pl
 ./debian/libmemcached/lib/Memcached/libmemcached/constants.pm
 ./debian/libmemcached/lib/Memcached/libmemcached/func_hash.pl
 ./debian/patches/reproducible-build.patch
 ./debian/patches/series
 ./debian/patches/spelling.patch
 ./debian/patches/use-system-libmemcached.patch
 ./debian/rules
 ./debian/source/format
 ./debian/tests/pkg-perl/smoke-cleanup
 ./debian/tests/pkg-perl/smoke-env
 ./debian/tests/pkg-perl/smoke-setup
 ./debian/upstream/metadata
 ./debian/watch
 ./lib/Memcached/libmemcached/API.pm
 ./lib/Memcached/libmemcached/constants.pm
 ./lib/Memcached/libmemcached/memcached_analyze.pm
 ./lib/Memcached/libmemcached/memcached_auto.pm
 ./lib/Memcached/libmemcached/memcached_behavior.pm
 ./lib/Memcached/libmemcached/memcached_callback.pm
 ./lib/Memcached/libmemcached/memcached_create.pm
 ./lib/Memcached/libmemcached/memcached_delete.pm
 ./lib/Memcached/libmemcached/memcached_dump.pm
 ./lib/Memcached/libmemcached/memcached_flush.pm
 ./lib/Memcached/libmemcached/memcached_flush_buffers.pm
 ./lib/Memcached/libmemcached/memcached_generate_hash_value.pm
 ./lib/Memcached/libmemcached/memcached_get.pm
 ./lib/Memcached/libmemcached/memcached_memory_allocators.pm
 ./lib/Memcached/libmemcached/memcached_pool.pm
 ./lib/Memcached/libmemcached/memcached_quit.pm
 ./lib/Memcached/libmemcached/memcached_result_st.pm
 ./lib/Memcached/libmemcached/memcached_sasl.pm
 ./lib/Memcached/libmemcached/memcached_server_st.pm
 ./lib/Memcached/libmemcached/memcached_servers.pm
 ./lib/Memcached/libmemcached/memcached_set.pm
 ./lib/Memcached/libmemcached/memcached_stats.pm
 ./lib/Memcached/libmemcached/memcached_strerror.pm
 ./lib/Memcached/libmemcached/memcached_user_data.pm
 ./lib/Memcached/libmemcached/memcached_verbosity.pm
 ./lib/Memcached/libmemcached/memcached_version.pm
 ./libmemcached.pm
 ./libmemcached.xs
 ./ppport.h
 ./responses.csv
 ./responses.txt
 ./t/00-load.t
 ./t/01-import.t
 ./t/02-create.t
 ./t/02-subclass.t
 ./t/03-servers.t
 ./t/04-strerror.t
 ./t/05-behavior.t
 ./t/06-verbosity.t
 ./t/10-set-get.t
 ./t/11-set-get-by-key.t
 ./t/12-set-get-binary.t
 ./t/15-auto.t
 ./t/20-set.t
 ./t/21-set-by-key.t
 ./t/25-get.t
 ./t/26-get-cb.t
 ./t/27-set-cb.t
 ./t/28-get-prefix.t
 ./t/30-prepend-append.t
 ./t/31-prepend-append-by-key.t
 ./t/35-replace.t
 ./t/36-replace-by-key.t
 ./t/40-delete.t
 ./t/41-delete-by-key.t
 ./t/42-get_server_for_key.t
 ./t/50-quit.t
 ./t/51-flush.t
 ./t/60-stats.t
 ./t/80-result_st.t
 ./t/api-coverage.t
 ./t/extended-consistent-hashing.t
 ./t/lib/ExtUtils/ParseXS.pm
 ./t/lib/ExtUtils/xsubpp
 ./t/lib/libmemcached_test.pm
 ./t/pod-coverage.t
 ./t/pod.t
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: Artistic and/or BSD-3-clause and/or GPL-1
 FIXME

Files: ./typemap
Copyright: 2007, Tim Bunce
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: change Copyright-Format 1.0 URL to HTTPS.
License: UNKNOWN
 FIXME

