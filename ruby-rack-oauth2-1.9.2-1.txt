Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.document
 ./.rspec
 ./.travis.yml
 ./Gemfile
 ./README.rdoc
 ./Rakefile
 ./VERSION
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/patches/series
 ./debian/patches/simplecov
 ./debian/ruby-rack-oauth2.docs
 ./debian/ruby-tests.rake
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./lib/rack/oauth2.rb
 ./lib/rack/oauth2/access_token.rb
 ./lib/rack/oauth2/access_token/authenticator.rb
 ./lib/rack/oauth2/access_token/bearer.rb
 ./lib/rack/oauth2/access_token/legacy.rb
 ./lib/rack/oauth2/access_token/mac.rb
 ./lib/rack/oauth2/access_token/mac/sha256_hex_verifier.rb
 ./lib/rack/oauth2/access_token/mac/signature.rb
 ./lib/rack/oauth2/access_token/mac/verifier.rb
 ./lib/rack/oauth2/client.rb
 ./lib/rack/oauth2/client/error.rb
 ./lib/rack/oauth2/client/grant.rb
 ./lib/rack/oauth2/client/grant/authorization_code.rb
 ./lib/rack/oauth2/client/grant/client_credentials.rb
 ./lib/rack/oauth2/client/grant/jwt_bearer.rb
 ./lib/rack/oauth2/client/grant/password.rb
 ./lib/rack/oauth2/client/grant/refresh_token.rb
 ./lib/rack/oauth2/client/grant/saml2_bearer.rb
 ./lib/rack/oauth2/client/grant/token_exchange.rb
 ./lib/rack/oauth2/debugger.rb
 ./lib/rack/oauth2/debugger/request_filter.rb
 ./lib/rack/oauth2/server.rb
 ./lib/rack/oauth2/server/abstract.rb
 ./lib/rack/oauth2/server/abstract/error.rb
 ./lib/rack/oauth2/server/abstract/handler.rb
 ./lib/rack/oauth2/server/abstract/request.rb
 ./lib/rack/oauth2/server/abstract/response.rb
 ./lib/rack/oauth2/server/authorize.rb
 ./lib/rack/oauth2/server/authorize/code.rb
 ./lib/rack/oauth2/server/authorize/error.rb
 ./lib/rack/oauth2/server/authorize/extension.rb
 ./lib/rack/oauth2/server/authorize/extension/code_and_token.rb
 ./lib/rack/oauth2/server/authorize/token.rb
 ./lib/rack/oauth2/server/extension.rb
 ./lib/rack/oauth2/server/extension/pkce.rb
 ./lib/rack/oauth2/server/extension/response_mode.rb
 ./lib/rack/oauth2/server/rails.rb
 ./lib/rack/oauth2/server/rails/authorize.rb
 ./lib/rack/oauth2/server/rails/response_ext.rb
 ./lib/rack/oauth2/server/resource.rb
 ./lib/rack/oauth2/server/resource/bearer.rb
 ./lib/rack/oauth2/server/resource/bearer/error.rb
 ./lib/rack/oauth2/server/resource/error.rb
 ./lib/rack/oauth2/server/resource/mac.rb
 ./lib/rack/oauth2/server/resource/mac/error.rb
 ./lib/rack/oauth2/server/token.rb
 ./lib/rack/oauth2/server/token/authorization_code.rb
 ./lib/rack/oauth2/server/token/client_credentials.rb
 ./lib/rack/oauth2/server/token/error.rb
 ./lib/rack/oauth2/server/token/extension.rb
 ./lib/rack/oauth2/server/token/extension/example.rb
 ./lib/rack/oauth2/server/token/jwt_bearer.rb
 ./lib/rack/oauth2/server/token/password.rb
 ./lib/rack/oauth2/server/token/refresh_token.rb
 ./lib/rack/oauth2/server/token/saml2_bearer.rb
 ./lib/rack/oauth2/urn.rb
 ./lib/rack/oauth2/util.rb
 ./rack-oauth2.gemspec
 ./spec/helpers/time.rb
 ./spec/helpers/webmock_helper.rb
 ./spec/mock_response/errors/invalid_request.json
 ./spec/mock_response/resources/fake.txt
 ./spec/mock_response/tokens/_Bearer.json
 ./spec/mock_response/tokens/bearer.json
 ./spec/mock_response/tokens/legacy.json
 ./spec/mock_response/tokens/legacy.txt
 ./spec/mock_response/tokens/legacy_without_expires_in.txt
 ./spec/mock_response/tokens/mac.json
 ./spec/mock_response/tokens/unknown.json
 ./spec/rack/oauth2/access_token/authenticator_spec.rb
 ./spec/rack/oauth2/access_token/bearer_spec.rb
 ./spec/rack/oauth2/access_token/legacy_spec.rb
 ./spec/rack/oauth2/access_token/mac/sha256_hex_verifier_spec.rb
 ./spec/rack/oauth2/access_token/mac/signature_spec.rb
 ./spec/rack/oauth2/access_token/mac/verifier_spec.rb
 ./spec/rack/oauth2/access_token/mac_spec.rb
 ./spec/rack/oauth2/access_token_spec.rb
 ./spec/rack/oauth2/client/error_spec.rb
 ./spec/rack/oauth2/client/grant/authorization_code_spec.rb
 ./spec/rack/oauth2/client/grant/client_credentials_spec.rb
 ./spec/rack/oauth2/client/grant/jwt_bearer_spec.rb
 ./spec/rack/oauth2/client/grant/password_spec.rb
 ./spec/rack/oauth2/client/grant/refresh_token_spec.rb
 ./spec/rack/oauth2/client/grant/saml2_bearer_spec.rb
 ./spec/rack/oauth2/client_spec.rb
 ./spec/rack/oauth2/debugger/request_filter_spec.rb
 ./spec/rack/oauth2/oauth2_spec.rb
 ./spec/rack/oauth2/server/abstract/error_spec.rb
 ./spec/rack/oauth2/server/authorize/code_spec.rb
 ./spec/rack/oauth2/server/authorize/error_spec.rb
 ./spec/rack/oauth2/server/authorize/extensions/code_and_token_spec.rb
 ./spec/rack/oauth2/server/authorize/token_spec.rb
 ./spec/rack/oauth2/server/authorize_spec.rb
 ./spec/rack/oauth2/server/extension/pkce_spec.rb
 ./spec/rack/oauth2/server/extension/response_mode_spec.rb
 ./spec/rack/oauth2/server/resource/bearer/error_spec.rb
 ./spec/rack/oauth2/server/resource/bearer_spec.rb
 ./spec/rack/oauth2/server/resource/error_spec.rb
 ./spec/rack/oauth2/server/resource/mac/error_spec.rb
 ./spec/rack/oauth2/server/resource/mac_spec.rb
 ./spec/rack/oauth2/server/resource_spec.rb
 ./spec/rack/oauth2/server/token/authorization_code_spec.rb
 ./spec/rack/oauth2/server/token/client_credentials_spec.rb
 ./spec/rack/oauth2/server/token/error_spec.rb
 ./spec/rack/oauth2/server/token/jwt_bearer_spec.rb
 ./spec/rack/oauth2/server/token/password_spec.rb
 ./spec/rack/oauth2/server/token/refresh_token_spec.rb
 ./spec/rack/oauth2/server/token/saml2_bearer_spec.rb
 ./spec/rack/oauth2/server/token_spec.rb
 ./spec/rack/oauth2/util_spec.rb
 ./spec/spec_helper.rb
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: 2010, nov matake
License: Expat
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2010, Nov Matake <nov@matake.jp>
  2015, Balasankar C <balasankarc@autistici.org>
License: UNKNOWN
 FIXME

