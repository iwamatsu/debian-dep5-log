Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./HACKING
 ./INSTALL
 ./NEWS
 ./TODO
 ./conf/debian/webauth.conf
 ./conf/debian/webauth.load
 ./conf/debian/webauthldap.conf
 ./conf/debian/webauthldap.load
 ./conf/debian/webkdc.conf
 ./conf/debian/webkdc.load
 ./conf/token.acl
 ./config.h.in
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/libapache2-mod-webauth.NEWS
 ./debian/libapache2-mod-webauth.README.Debian
 ./debian/libapache2-mod-webauth.apache2
 ./debian/libapache2-mod-webauth.dirs
 ./debian/libapache2-mod-webauth.doc-base
 ./debian/libapache2-mod-webauth.docs
 ./debian/libapache2-mod-webauth.examples
 ./debian/libapache2-mod-webauth.lintian-overrides
 ./debian/libapache2-mod-webauth.postinst
 ./debian/libapache2-mod-webauth.postrm
 ./debian/libapache2-mod-webauthldap.README.Debian
 ./debian/libapache2-mod-webauthldap.apache2
 ./debian/libapache2-mod-webauthldap.docs
 ./debian/libapache2-mod-webauthldap.examples
 ./debian/libapache2-mod-webauthldap.lintian-overrides
 ./debian/libapache2-mod-webauthldap.postinst
 ./debian/libapache2-mod-webauthldap.postrm
 ./debian/libapache2-mod-webkdc.README.Debian
 ./debian/libapache2-mod-webkdc.apache2
 ./debian/libapache2-mod-webkdc.docs
 ./debian/libapache2-mod-webkdc.install
 ./debian/libapache2-mod-webkdc.lintian-overrides
 ./debian/libapache2-mod-webkdc.postinst
 ./debian/libapache2-mod-webkdc.postrm
 ./debian/libwebauth-dev.install
 ./debian/libwebauth-perl.install
 ./debian/libwebauth12.install
 ./debian/libwebauth12.symbols
 ./debian/libwebkdc-perl.NEWS
 ./debian/libwebkdc-perl.dirs
 ./debian/libwebkdc-perl.install
 ./debian/libwebkdc-perl.postrm
 ./debian/patches/0001-Fix-OpenSSL-library-probe.patch
 ./debian/patches/0002-Suppress-CGI-warnings-from-param-in-list-context.patch
 ./debian/patches/0003-Add-missing-word-in-generic-help-for-remember-checkb.patch
 ./debian/patches/0004-Set-secure-cookie-flag-properly-with-WebAuthSSLRetur.patch
 ./debian/patches/0005-Correctly-honor-WebKdcTokenMaxTTL-for-request-tokens.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/source/lintian-overrides
 ./debian/source/options
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./debian/webauth-tests.README.Debian
 ./debian/webauth-tests.install
 ./debian/webauth-utils.install
 ./debian/webauth-weblogin.NEWS
 ./debian/webauth-weblogin.README.Debian
 ./debian/webauth-weblogin.docs
 ./debian/webauth-weblogin.install
 ./debian/webkdc.conf
 ./docs/design-pwchange
 ./docs/diagrams/weblogin-flow.svg
 ./docs/install-multifactor
 ./docs/install-spnego
 ./docs/install-stanford
 ./docs/install-webkdc
 ./docs/mod_webauth.html.en
 ./docs/mod_webauth.xml
 ./docs/mod_webauthldap.html.en
 ./docs/mod_webauthldap.xml
 ./docs/mod_webkdc.html.en
 ./docs/mod_webkdc.xml
 ./docs/protocol.html
 ./docs/protocol.xml
 ./docs/test-plan
 ./docs/userauth
 ./docs/weblogin-config
 ./docs/weblogin-flow
 ./lib/libwebauth.map
 ./lib/libwebauth.pc.in
 ./lib/libwebauth.sym
 ./perl/MANIFEST
 ./perl/MANIFEST.SKIP
 ./perl/lib/WebAuth/KeyringEntry.pod
 ./perl/t/TODO
 ./perl/t/data/conf-password
 ./perl/t/data/pages/confirm/device-expiring
 ./perl/t/data/pages/confirm/expired-password
 ./perl/t/data/pages/confirm/no-pwexpiration
 ./perl/t/data/pages/confirm/pending-pwchange
 ./perl/t/data/pages/confirm/public-computer
 ./perl/t/data/pages/confirm/remote-user-checkbox
 ./perl/t/data/pages/error/return-url
 ./perl/t/data/pages/error/stale-token
 ./perl/t/data/pages/error/unrecoverable
 ./perl/t/data/pages/error/unrecoverable-webauth
 ./perl/t/data/pages/login/empty-username
 ./perl/t/data/pages/login/failed
 ./perl/t/data/pages/login/forced
 ./perl/t/data/pages/login/forced-no-wpt
 ./perl/t/data/pages/login/no-password
 ./perl/t/data/pages/login/no-username
 ./perl/t/data/pages/login/no-username-password
 ./perl/t/data/pages/login/pass-required
 ./perl/t/data/pages/login/pass-required-no-remuser
 ./perl/t/data/pages/login/pass-required-not-error
 ./perl/t/data/pages/login/rejected
 ./perl/t/data/pages/login/remote-user
 ./perl/t/data/pages/pwchange/bare
 ./perl/t/data/pages/pwchange/cpt
 ./perl/t/data/perl.conf
 ./perl/t/data/templates/confirm.tmpl
 ./perl/t/data/templates/error.tmpl
 ./perl/t/data/templates/login.tmpl
 ./perl/t/data/templates/logout.tmpl
 ./perl/t/data/templates/pwchange.tmpl
 ./perl/t/data/token.acl
 ./perl/t/data/webkdc.conf
 ./tests/TESTS
 ./tests/config/README
 ./tests/data/conf-webkdc.in
 ./tests/data/creds/README
 ./tests/data/creds/addresses
 ./tests/data/creds/renewable
 ./tests/data/id.acl
 ./tests/data/json/info/additional.json
 ./tests/data/json/info/factor.json
 ./tests/data/json/info/full.json
 ./tests/data/json/info/loginstate.json
 ./tests/data/json/info/message.json
 ./tests/data/json/info/mini.json
 ./tests/data/json/info/normal.json
 ./tests/data/json/info/random.json
 ./tests/data/json/info/restrict.json
 ./tests/data/json/validate/full.json
 ./tests/data/keyring
 ./tests/data/make-krb5-cred
 ./tests/data/make-tokens
 ./tests/data/perl.conf
 ./tests/data/perltidyrc
 ./tests/data/service-token
 ./tests/data/tokens/README
 ./tests/data/tokens/app-authz
 ./tests/data/tokens/app-bad-hmac
 ./tests/data/tokens/app-bad-session
 ./tests/data/tokens/app-expired
 ./tests/data/tokens/app-minimal
 ./tests/data/tokens/app-missing
 ./tests/data/tokens/app-ok
 ./tests/data/tokens/app-raw
 ./tests/data/tokens/app-session
 ./tests/data/tokens/cred-expired
 ./tests/data/tokens/cred-missing
 ./tests/data/tokens/cred-ok
 ./tests/data/tokens/cred-type
 ./tests/data/tokens/error-code
 ./tests/data/tokens/error-ok
 ./tests/data/tokens/id-authz
 ./tests/data/tokens/id-expired
 ./tests/data/tokens/id-krb5
 ./tests/data/tokens/id-krb5-authz
 ./tests/data/tokens/id-minimal
 ./tests/data/tokens/id-type
 ./tests/data/tokens/id-webkdc
 ./tests/data/tokens/login-both
 ./tests/data/tokens/login-dev-minimal
 ./tests/data/tokens/login-missing
 ./tests/data/tokens/login-neither
 ./tests/data/tokens/login-otp
 ./tests/data/tokens/login-otp-minimal
 ./tests/data/tokens/login-otp-type
 ./tests/data/tokens/login-pass
 ./tests/data/tokens/login-pass-device
 ./tests/data/tokens/proxy-authz
 ./tests/data/tokens/proxy-expired
 ./tests/data/tokens/proxy-missing
 ./tests/data/tokens/proxy-ok
 ./tests/data/tokens/proxy-type
 ./tests/data/tokens/req-auth-type
 ./tests/data/tokens/req-bad-command
 ./tests/data/tokens/req-command
 ./tests/data/tokens/req-id
 ./tests/data/tokens/req-id-krb5
 ./tests/data/tokens/req-minimal
 ./tests/data/tokens/req-proxy
 ./tests/data/tokens/req-proxy-type
 ./tests/data/tokens/req-type
 ./tests/data/tokens/wkfactor-expired
 ./tests/data/tokens/wkfactor-missing
 ./tests/data/tokens/wkfactor-none
 ./tests/data/tokens/wkfactor-ok
 ./tests/data/tokens/wkproxy-expired
 ./tests/data/tokens/wkproxy-min
 ./tests/data/tokens/wkproxy-ok
 ./tests/data/tokens/wkproxy-type
 ./tests/data/tokens/wkservice-expired
 ./tests/data/tokens/wkservice-ok
 ./tests/data/xml/info/additional.xml
 ./tests/data/xml/info/factor.xml
 ./tests/data/xml/info/full.xml
 ./tests/data/xml/info/loginstate.xml
 ./tests/data/xml/info/message.xml
 ./tests/data/xml/info/mini.xml
 ./tests/data/xml/info/normal.xml
 ./tests/data/xml/info/random.xml
 ./tests/data/xml/info/restrict.xml
 ./tests/data/xml/validate/fail.xml
 ./tests/data/xml/validate/full.xml
 ./tests/mod_webauth/htdocs/ldaptests/test3/placeholder
 ./tests/mod_webauth/htdocs/tests/index.html
 ./tests/perl/module-version-t
 ./tests/portable/asprintf.c
 ./tests/portable/mkstemp.c
 ./tests/portable/setenv.c
 ./tests/portable/snprintf.c
 ./tests/portable/strlcat.c
 ./tests/portable/strlcpy.c
 ./tests/portable/strndup.c
 ./tests/tap/perl/Test/RRA/Config.pm
 ./tools/wa_keyring.1
 ./tools/wa_keyring.pod
 ./tools/weblogin-passcheck
 ./util/xmalloc.c
 ./weblogin/images/error.xcf
 ./weblogin/images/help.xcf
 ./weblogin/images/login.xcf
 ./weblogin/images/logout.xcf
 ./weblogin/templates/help.html
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./Makefile.am
 ./autogen
 ./config-mod.h.in
 ./configure.ac
 ./include/mkheader
 ./lib/apr-buffer.c
 ./lib/attr-decode.c
 ./lib/attr-encode.c
 ./lib/context.c
 ./lib/errors.c
 ./lib/factors.c
 ./lib/file-io.c
 ./lib/hex.c
 ./lib/internal.h
 ./lib/keyring.c
 ./lib/keys.c
 ./lib/krb5-heimdal.c
 ./lib/krb5-mit.c
 ./lib/krb5.c
 ./lib/rules-cache.c
 ./lib/rules-keyring.c
 ./lib/rules-krb5.c
 ./lib/rules-tokens.c
 ./lib/token-crypto.c
 ./lib/token-encode.c
 ./lib/token-merge.c
 ./lib/userinfo-json.c
 ./lib/userinfo-remctl.c
 ./lib/userinfo-xml.c
 ./lib/userinfo.c
 ./lib/util.c
 ./lib/was-cache.c
 ./lib/webkdc-config.c
 ./lib/webkdc-logging.c
 ./lib/webkdc-login.c
 ./lib/xml.c
 ./m4/apache.m4
 ./m4/apr.m4
 ./m4/aprutil.m4
 ./m4/curl.m4
 ./m4/gssapi.m4
 ./m4/jansson.m4
 ./m4/krb5-config.m4
 ./m4/krb5.m4
 ./m4/ldap.m4
 ./m4/lib-depends.m4
 ./m4/lib-helper.m4
 ./m4/lib-pathname.m4
 ./m4/openssl.m4
 ./m4/remctl.m4
 ./m4/snprintf.m4
 ./m4/vamacros.m4
 ./modules/ldap/config.c
 ./modules/ldap/mod_webauthldap.c
 ./modules/ldap/mod_webauthldap.h
 ./modules/webauth/config.c
 ./modules/webauth/krb5.c
 ./modules/webauth/mod_webauth.c
 ./modules/webauth/mod_webauth.h
 ./modules/webauth/util.c
 ./modules/webauth/webkdc.c
 ./modules/webkdc/acl.c
 ./modules/webkdc/config.c
 ./modules/webkdc/logging.c
 ./modules/webkdc/mod_webkdc.c
 ./modules/webkdc/mod_webkdc.h
 ./modules/webkdc/util.c
 ./perl/Build.PL
 ./perl/lib/WebAuth.xs
 ./perl/t/cookies/cookies.t
 ./perl/t/cookies/factor-token.t
 ./perl/t/data/cmd-password
 ./perl/t/kerberos/changepw.t
 ./perl/t/kerberos/krb5.t
 ./perl/t/kerberos/webkdc.t
 ./perl/t/kerberos/weblogin.t
 ./perl/t/keyring/keyring.t
 ./perl/t/keyring/keys.t
 ./perl/t/keyring/token-decode.t
 ./perl/t/keyring/token-encode.t
 ./perl/t/keyring/token-errs.t
 ./perl/t/keyring/token-rights.t
 ./perl/t/lib/Util.pm
 ./perl/t/misc/config.t
 ./perl/t/misc/exception.t
 ./perl/t/misc/webkdcexception.t
 ./perl/t/misc/weblogin.t
 ./perl/t/pages/confirmation.t
 ./perl/t/pages/error.t
 ./perl/t/pages/global-errors.t
 ./perl/t/pages/login.t
 ./perl/t/pages/pwchange.t
 ./perl/t/token/misc.t
 ./perl/t/webkdc/web-request.t
 ./perl/t/webkdc/web-response.t
 ./perl/t/webkdc/xml.t
 ./perl/typemap
 ./tests/data/cmd-password
 ./tests/data/cmd-webkdc
 ./tests/data/tokens.conf
 ./tests/lib/apr-buffer-t.c
 ./tests/lib/errors-t.c
 ./tests/lib/factors-t.c
 ./tests/lib/hex-t.c
 ./tests/lib/interval-t.c
 ./tests/lib/keyring-t.c
 ./tests/lib/keys-t.c
 ./tests/lib/krb5-cred-t.c
 ./tests/lib/krb5-remctl-t.c
 ./tests/lib/krb5-t.c
 ./tests/lib/krb5-tgt-t.c
 ./tests/lib/token-crypto-t.c
 ./tests/lib/token-decode-t.c
 ./tests/lib/token-encode-t.c
 ./tests/lib/token-merge-t.c
 ./tests/lib/userinfo-t.c
 ./tests/lib/was-cache-t.c
 ./tests/lib/webkdc-krb-t.c
 ./tests/lib/webkdc-login-t.c
 ./tests/lib/webkdc-mf-t.c
 ./tests/mod_webauth/conf/ldaptests.conf
 ./tests/mod_webauth/conf/webauth-tests.conf
 ./tests/mod_webauth/htdocs/ldaptests/test1/test
 ./tests/mod_webauth/htdocs/ldaptests/test2/test
 ./tests/mod_webauth/htdocs/ldaptests/test4/test
 ./tests/mod_webauth/htdocs/ldaptests/test5/test
 ./tests/mod_webauth/htdocs/ldaptests/test6/test
 ./tests/mod_webauth/htdocs/ldaptests/test7/test
 ./tests/mod_webauth/htdocs/ldaptests/test8/test
 ./tests/mod_webauth/htdocs/ldaptests/util.pl
 ./tests/mod_webauth/htdocs/tests/auth/test1
 ./tests/mod_webauth/htdocs/tests/auth/test10
 ./tests/mod_webauth/htdocs/tests/auth/test11
 ./tests/mod_webauth/htdocs/tests/auth/test11return
 ./tests/mod_webauth/htdocs/tests/auth/test12
 ./tests/mod_webauth/htdocs/tests/auth/test12post
 ./tests/mod_webauth/htdocs/tests/auth/test12return
 ./tests/mod_webauth/htdocs/tests/auth/test13
 ./tests/mod_webauth/htdocs/tests/auth/test13login
 ./tests/mod_webauth/htdocs/tests/auth/test14unauth
 ./tests/mod_webauth/htdocs/tests/auth/test15
 ./tests/mod_webauth/htdocs/tests/auth/test2
 ./tests/mod_webauth/htdocs/tests/auth/test2return
 ./tests/mod_webauth/htdocs/tests/auth/test3
 ./tests/mod_webauth/htdocs/tests/auth/test4
 ./tests/mod_webauth/htdocs/tests/auth/test5
 ./tests/mod_webauth/htdocs/tests/auth/test5return
 ./tests/mod_webauth/htdocs/tests/auth/test6
 ./tests/mod_webauth/htdocs/tests/auth/test6return
 ./tests/mod_webauth/htdocs/tests/auth/test7
 ./tests/mod_webauth/htdocs/tests/auth/test7return
 ./tests/mod_webauth/htdocs/tests/auth/test8
 ./tests/mod_webauth/htdocs/tests/auth/test8return
 ./tests/mod_webauth/htdocs/tests/auth/test9
 ./tests/mod_webauth/htdocs/tests/logout
 ./tests/mod_webauth/htdocs/tests/multifactor/test1
 ./tests/mod_webauth/htdocs/tests/multifactor/test10
 ./tests/mod_webauth/htdocs/tests/multifactor/test11
 ./tests/mod_webauth/htdocs/tests/multifactor/test12
 ./tests/mod_webauth/htdocs/tests/multifactor/test13
 ./tests/mod_webauth/htdocs/tests/multifactor/test14
 ./tests/mod_webauth/htdocs/tests/multifactor/test2
 ./tests/mod_webauth/htdocs/tests/multifactor/test3
 ./tests/mod_webauth/htdocs/tests/multifactor/test4
 ./tests/mod_webauth/htdocs/tests/multifactor/test5
 ./tests/mod_webauth/htdocs/tests/multifactor/test6
 ./tests/mod_webauth/htdocs/tests/multifactor/test7
 ./tests/mod_webauth/htdocs/tests/multifactor/test8
 ./tests/mod_webauth/htdocs/tests/multifactor/test9
 ./tests/mod_webauth/htdocs/tests/path/test14
 ./tests/mod_webauth/htdocs/tests/path/test14logout
 ./tests/mod_webauth/htdocs/tests/unauth/test4
 ./tests/mod_webauth/lib/WebLogin/Tests.pm
 ./tests/mod_webauth/run-tests
 ./tests/mod_webauth/run-tests-multifactor
 ./tests/tap/webauth.c
 ./tests/tap/webauth.h
 ./tests/tools/wa_keyring-t
 ./tools/wa_keyring.c
 ./weblogin/login.fcgi
 ./weblogin/logout.fcgi
 ./weblogin/pwchange.fcgi
Copyright: 2002, 2009-2010, 2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005, 2009-2010, 2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005, 2009-2010, 2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005-2006, 2008-2009, 2011, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005-2006, 2008-2009, 2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005-2006, 2009-2010, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2008-2010, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2008-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2008-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2009, 2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2009-2010, 2012, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2009-2010, 2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2009-2010, 2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006, 2009-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2006-2007, 2009-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2004, 2006, 2008-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2004, 2006, 2009-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2004, 2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2006, 2008-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2006, 2008-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2006, 2009-2010, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2005, 2009, 2011-2012, The Board of Trustees of the Leland Stanford Junior University
  2003, 2005-2006, 2008-2014, The Board of Trustees of the Leland Stanford Junior University
  2003, 2005-2006, 2010-2012, The Board of Trustees of the Leland Stanford Junior University
  2003, 2005-2007, 2009-2010, 2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2006, 2009, 2014, The Board of Trustees of the Leland Stanford Junior University
  2003, 2006, 2009-2010, 2012, The Board of Trustees of the Leland Stanford Junior University
  2003, 2006, 2009-2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2009, 2011, The Board of Trustees of the Leland Stanford Junior University
  2003, 2010, The Board of Trustees of the Leland Stanford Junior University
  2003, 2011, 2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2003, The Board of Trustees of the Leland Stanford Junior University
  2003-2004, 2006, 2008-2013, The Board of Trustees of the Leland Stanford Junior University
  2003-2005, 2009, The Board of Trustees of the Leland Stanford Junior University
  2003-2006, 2009, 2013, The Board of Trustees of the Leland Stanford Junior University
  2003-2013, The Board of Trustees of the Leland Stanford Junior University
  2004, 2006, 2009, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2005, The Board of Trustees of the Leland Stanford Junior University
  2005-2007, The Board of Trustees of the Leland Stanford Junior University
  2005-2009, 2011-2012, The Board of Trustees of the Leland Stanford Junior University
  2005-2011, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2006, 2008-2009, The Board of Trustees of the Leland Stanford Junior University
  2008-2009, 2011, 2013, The Board of Trustees of the Leland Stanford Junior University
  2008-2009, The Board of Trustees of the Leland Stanford Junior University
  2009-2014, The Board of Trustees of the Leland Stanford Junior University
  2010, 2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2010, 2012, The Board of Trustees of the Leland Stanford Junior University
  2010, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2010, 2013, The Board of Trustees of the Leland Stanford Junior University
  2010, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2010, The Board of Trustees of the Leland Stanford Junior University
  2010-2011, The Board of Trustees of the Leland Stanford Junior University
  2010-2013, The Board of Trustees of the Leland Stanford Junior University
  2011, 2013, The Board of Trustees of the Leland Stanford Junior University
  2011, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2011-2012, The Board of Trustees of the Leland Stanford Junior University
  2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2012, The Board of Trustees of the Leland Stanford Junior University
  2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2013, The Board of Trustees of the Leland Stanford Junior University
  2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2014, The Board of Trustees of the Leland Stanford Junior University
License: UNKNOWN
 FIXME

Files: ./include/webauth/basic.h
 ./include/webauth/defines.h.in
 ./include/webauth/factors.h
 ./include/webauth/keys.h
 ./include/webauth/krb5.h
 ./include/webauth/tokens.h
 ./include/webauth/util.h
 ./include/webauth/was.h
 ./include/webauth/webkdc.h
 ./perl/lib/WebAuth.pm
 ./perl/lib/WebAuth/Exception.pm
 ./perl/lib/WebAuth/Key.pm
 ./perl/lib/WebAuth/Keyring.pm
 ./perl/lib/WebAuth/Krb5.pm
 ./perl/lib/WebAuth/Tests.pm
 ./perl/lib/WebAuth/Token.pm
 ./perl/lib/WebAuth/Token/App.pm
 ./perl/lib/WebAuth/Token/Cred.pm
 ./perl/lib/WebAuth/Token/Error.pm
 ./perl/lib/WebAuth/Token/Id.pm
 ./perl/lib/WebAuth/Token/Login.pm
 ./perl/lib/WebAuth/Token/Proxy.pm
 ./perl/lib/WebAuth/Token/Request.pm
 ./perl/lib/WebAuth/Token/WebKDCFactor.pm
 ./perl/lib/WebAuth/Token/WebKDCProxy.pm
 ./perl/lib/WebAuth/Token/WebKDCService.pm
 ./perl/lib/WebKDC.pm
 ./perl/lib/WebKDC/Config.pm
 ./perl/lib/WebKDC/WebKDCException.pm
 ./perl/lib/WebKDC/WebRequest.pm
 ./perl/lib/WebKDC/WebResponse.pm
 ./perl/lib/WebKDC/XmlDoc.pm
 ./perl/lib/WebKDC/XmlElement.pm
 ./perl/lib/WebLogin.pm
 ./perl/t/docs/pod-spelling.t
 ./perl/t/docs/pod.t
 ./perl/t/style/minimum-version.t
 ./perl/t/style/strict.t
 ./tests/data/generate-krb5-conf
 ./tests/data/perlcriticrc
 ./tests/data/valgrind.supp
 ./tests/docs/pod-spelling-t
 ./tests/docs/pod-t
 ./tests/perl/critic-t
 ./tests/perl/minimum-version-t
 ./tests/perl/strict-t
 ./tests/tap/kerberos.c
 ./tests/tap/kerberos.h
 ./tests/tap/perl/Test/RRA.pm
 ./tests/tap/perl/Test/RRA/Automake.pm
 ./tests/tap/process.h
 ./tests/tap/remctl.c
 ./tests/tap/remctl.h
Copyright: 2002, 2009, 2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005, 2009, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2005-2006, 2008-2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2008-2011, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2008-2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2003, 2009, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2006, 2008-2009, 2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2002-2014, The Board of Trustees of the Leland Stanford Junior University
  2003, 2005, 2008-2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2003, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2004-2009, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2006-2007, 2009, 2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2006-2007, 2009, 2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2006-2007, 2009-2014, The Board of Trustees of the Leland Stanford Junior University
  2006-2008, 2010-2011, The Board of Trustees of the Leland Stanford Junior University
  2009-2010, 2013, The Board of Trustees of the Leland Stanford Junior University
  2011, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2011, The Board of Trustees of the Leland Stanford Junior University
  2011-2012, The Board of Trustees of the Leland Stanford Junior University
  2011-2013, The Board of Trustees of the Leland Stanford Junior University
  2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2012, The Board of Trustees of the Leland Stanford Junior University
  2012-2013, The Board of Trustees of the Leland Stanford Junior University
  2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2013, The Board of Trustees of the Leland Stanford Junior University
  2013-2014, The Board of Trustees of the Leland Stanford Junior University
License: Expat
 FIXME

Files: ./portable/apache.h
 ./portable/apr.h
 ./portable/asprintf.c
 ./portable/dummy.c
 ./portable/krb5-extra.c
 ./portable/krb5.h
 ./portable/macros.h
 ./portable/mkstemp.c
 ./portable/reallocarray.c
 ./portable/setenv.c
 ./portable/stdbool.h
 ./portable/strlcat.c
 ./portable/strlcpy.c
 ./portable/strndup.c
 ./portable/system.h
 ./tests/portable/asprintf-t.c
 ./tests/portable/mkstemp-t.c
 ./tests/portable/setenv-t.c
 ./tests/portable/strlcat-t.c
 ./tests/portable/strlcpy-t.c
 ./tests/portable/strndup-t.c
 ./util/macros.h
Copyright: that they may have
License: UNKNOWN
 FIXME

Files: ./conf/README
 ./conf/sample-webauth.conf
 ./conf/stanford-ldap.conf
 ./conf/stanford-webauth.conf
 ./docs/weblogin-cookies
 ./docs/weblogin-testing
 ./perl/t/data/README
 ./tests/mod_webauth/htdocs/ldaptests/index.html
 ./tests/mod_webauth/htdocs/tests/php/test1.php
 ./tests/mod_webauth/htdocs/tests/test.tt2
Copyright: 2003, 2006, 2009-2010, The Board of Trustees of the Leland Stanford Junior University
  2003, The Board of Trustees of the Leland Stanford Junior University
  2003-2004, 2009, The Board of Trustees of the Leland Stanford Junior University
  2007, 2009, The Board of Trustees of the Leland Stanford Junior University
  2010, 2012, The Board of Trustees of the Leland Stanford Junior University
  2011, The Board of Trustees of the Leland Stanford Junior University
  2013-2014, The Board of Trustees of the Leland Stanford Junior University
License: FSFAP
 FIXME

Files: ./tests/tap/basic.c
 ./tests/tap/basic.h
 ./tests/tap/libtap.sh
 ./tests/tap/messages.c
 ./tests/tap/messages.h
 ./tests/tap/process.c
 ./tests/util/messages-t.c
 ./tests/util/xmalloc-t
 ./tests/util/xmalloc.c
Copyright: 2000-2001, 2006, 2014, Russ Allbery <eagle@eyrie.org>
  2000-2001, 2006, Russ Allbery <eagle@eyrie.org>
  2001-2002, 2004-2008, 2011-2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2001-2002, 2004-2008, 2011-2014, The Board of Trustees of the Leland Stanford Junior University
  2002, 2004-2005, 2013, Russ Allbery <eagle@eyrie.org>
  2002, 2004-2005, Russ Allbery <eagle@eyrie.org>
  2002, Russ Allbery <eagle@eyrie.org>
  2006-2007, 2009, 2012, 2014, The Board of Trustees of the Leland Stanford Junior University
  2006-2007, 2009, The Board of Trustees of the Leland Stanford Junior University
  2006-2008, 2013, The Board of Trustees of the Leland Stanford Junior University
  2008, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
  2008-2010, 2012, The Board of Trustees of the Leland Stanford Junior University
  2009-2011, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2009-2012, Russ Allbery <eagle@eyrie.org>
  2009-2012, The Board of Trustees of the Leland Stanford Junior University
  2009-2014, Russ Allbery <eagle@eyrie.org>
License: Expat
 FIXME

Files: ./weblogin/templates/confirm.tmpl
 ./weblogin/templates/error.tmpl
 ./weblogin/templates/login.tmpl
 ./weblogin/templates/logout.tmpl
 ./weblogin/templates/multifactor.tmpl
 ./weblogin/templates/pwchange.tmpl
Copyright: that they may
License: UNKNOWN
 FIXME

Files: ./build-aux/ar-lib
 ./build-aux/compile
 ./build-aux/depcomp
 ./build-aux/ltmain.sh
 ./build-aux/missing
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
  1996-2013, Free Software Foundation, Inc.
  1999-2013, Free Software Foundation, Inc.
  2010-2013, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./Makefile.in
 ./aclocal.m4
 ./m4/ltsugar.m4
 ./m4/ltversion.m4
 ./m4/lt~obsolete.m4
Copyright: 1994-2013, Free Software Foundation, Inc.
  1996-2013, Free Software Foundation, Inc.
  2004, Free Software Foundation, Inc.
  2004-2005, 2007, 2009, Free Software Foundation, Inc.
  2004-2005, 2007-2008, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./tests/tap/macros.h
 ./tests/tap/string.c
 ./tests/tap/string.h
Copyright: 2008, 2012-2013, Russ Allbery <eagle@eyrie.org>
  2011-2012, Russ Allbery <eagle@eyrie.org>
License: Expat
 FIXME

Files: ./build-aux/config.guess
 ./build-aux/config.sub
Copyright: 1992-2013, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./util/messages.h
 ./util/xmalloc.h
Copyright: 1991, 1994-2003, The Internet Software Consortium and Rich Salz
  2004-2006, Internet Systems Consortium, Inc. ("ISC")
  2008, 2010, 2013-2014, The Board of Trustees of the Leland Stanford Junior University
  2010, 2012-2014, The Board of Trustees of the Leland Stanford Junior University
License: ISC
 FIXME

Files: ./docs/scripts/clean-apache-manual
Copyright: word from build-license.
License: Apache
 FIXME

Files: ./build-aux/install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./LICENSE
Copyright: -format/1.0/
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: GPL-2+
 FIXME

Files: ./lib/encoding-rules
Copyright: 2012-2013
License: UNKNOWN
 FIXME

Files: ./weblogin/images/confirm.png
Copyright: l!}àUÃh²qp4¥Òxë×Åï¢åU;ð,éo½S¼X
  YízN<
  £®®âÔâPífôNÑrª¥VuÝûÃÀ»ï6­¬nPAÉ¦GÈæ¶üá+>ô'Tby(süx6´´Ï!üJÅõ(ýÊ
  ±tNØÀûo£º{±óæÞÞÿ
  µÉ¯¸8?ëmÍvvY*|)pðfà¼ÿí0Zß}í/ÙR­R$64­f
License: UNKNOWN
 FIXME

Files: ./weblogin/images/help.png
Copyright: ?ÞÀN­o
  P¢R ÃæðÑìÕNe)a³ÉVv*K
  (} v:µÕÂnU¿Ql~à-Ní5QPúp
  a+;=|Ä±Ý!à¿íåª´ãG%RÃú|{ï$öNì]@Ye
  cG³~Â·2{Ñ*ç²NcÀ½v»§PqÊ®4[#ì&ì#MYµ­I*NÙ
  !J<AU§Ï«EqG3Ö¤ Æ³¿ m<·g
  JR ÃjøÔÜ+uá
License: UNKNOWN
 FIXME

Files: ./weblogin/images/logout.png
Copyright: èDçÐHu?ª_)")ë
  <ËÒ ãú²ïA«¶ËW
  EiÅ&bÇQ¤ºþ2ºþ¯óÏ:ºMºEåÃa=V}äW^rÐmèh'H
  {øfi%ÛÞÀ]¯!ÝM4Rå)#lG5-e9u{ùCa|
  £ÔK-óðbÅçÜ|ìSæ0¡àíÃ±¿Yåt× ÿ|óV£q/½¶b÷= 7_Áh0H2@w§X
  ³N0
  ¹exæÔ[­_À¸RAjm"£ì^Dm2|¤ñqäZ0m^°UÈ¼Þâ
  »tâÚ¾áÚé1¡êxñQÝb.=4S
  È
  ÎA#õ Á¢y
  Ó(XtËÐTVìEò³=/z
  ×f_Íí ¿ûf«çÚYÔÍàÈR*jFÞÄ·uDL¡øäaT½¶+:öëøùH»	`GÑ
  ÙGÃ
  ïj8®iìzß¬*ºÅ ëR2ûÿidLO"¿ËÞðÍª¢[
  ø"·jåp}Ô,q5-×SÏÑÏá63|ôÏ
License: UNKNOWN
 FIXME

Files: ./tests/data/creds/service
Copyright: zëJG¦lÆ«ã°Õ<H7@^ÑÉ(Ï­ßi]%[ÃâV`hûëÝH$´×JAt³zh©Ì;
License: UNKNOWN
 FIXME

Files: ./docs/diagrams/weblogin-flow.dia
Copyright: ?ekK<zÆ=mCâ÷w!½ÕbxQ.W¨a|*Ï$.C@ãyËÀæLã®óålzóáã¢þÜP/vé­¸½Å t¡íLÈòöêwÔF~ÙÈ¨Fd8M¾8w)«¨h[O
  $}Xòo«IÝ!®ï¸×ÎÉ!¥ôLx'GZ®xQdKõ³i©.5®³½îMÜV¿.¦ËEvÔ®¢ÓÊaAZP®ó||üã¿^¸­;ä¡h.ÓÃéÇ­.<Kiß7BC_¡ Á¤
  N¿ÓCaäuçm§t
  O¯Æ-ÀúZo|.´xÕyszähÓWZì(Ö=­ÒbÈNuiq¤qIÜ:ÂIË¤3>q
  e>mtM£°LI%GgB0iM	)t#J	a
  w5l:ç>Påô±¤·*ïÅ^üú9	øvÕ±]
  N4 zÿþ÷;%gToÀ.zÅë"¾VÔdÒ
  ¦Ä1£eåph
  ×L&E$C1³ÆÀ%Hu#7Ë9äx²
  À´ï!
  ¡ wÜÞ¬{óRdêÇÆuî`·;GÛöÌ @Î©ëóËç?þüo`ÃÉÑ`]Óæ©U®ô4	&%Õ}/Æáß@G_;¤JÈ)RÙWhÞø:½H]AÌô¬j/nÔH¡)6whèîjJ×Y¡ÍY@/.qû#ÊLÎ{ndè.ª2&9L²WuÆö
  ·ÎÆÑåÀ'ÙçêÑçúáÔ:.!È¾P¬§°^èó$²ûÝ¯ãq¿â
  ¸=@
  ÇqfhFSßG44Ù(ç5YÂN?6¹f¡»Æ+GõÄl
  ÉÉ¸Ô"Øé¥-P²22°èà½OÖY%¢ÙµsÜgÀøª ãèãû FµM/ñÁ1ÿâLÍ1øcÛ#:ICq
  ÉAAÚhÚ!Î[S
  èÂÐ
  í	¢í¦,£+´EÓïî46
  íçñT48?ßPárÆó«YùE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2000-2002, 2004-2014, Russ Allbery <eagle@eyrie.org>
  2002-2014, The Board of Trustees of the Leland Stanford Junior University
License: UNKNOWN
 FIXME

Files: ./weblogin/images/error.png
Copyright: @ÛOØÏÅ5î6Ô>ÓX?FÉ$JfÔ¡é'@Núñùª¶$pÕH1~Âaä¬ÆÈ÷pÃçdxBÛO¨aXöÌÔKcþzÓÈ½H`AÒBbY¿Po »ÄnôÄÏRçNôÆâØ³ørÇjtÈ¢Zfòî­£)
  'Ð6Ã0
  7ÁZtÒgL#/ü*n5E	9ñqÞj,A1²ÿÓî
  º,´üU`¹enàYôÅ÷*ð%àJà-1Úo4@ÓOXEîÖFIÔJÞ=£Þs¡@á	-?aÙ¹¶RTg52zjoøpm
  Ùdí³ÁÚÏ$æ[Mj
  ñ&raé2Gíýþ`iÞÔ)ñóB§«.^|ÕS{«XÆU|ç{~
License: UNKNOWN
 FIXME

Files: ./tests/data/creds/ad-heimdal
Copyright: A6H:{£
  ­tzõKJÛò
  ¤yðÒ»6Q9ApyôéöN>4WQC	Æõõ§õ+µ´ï_ZÛG[0n'ÌÙu¼°«,üH ©Ù·Ñ¯_¯`Ãcb:a.:0ü<Pùâ[ÜÇj¿([:¬°¸¿hv8
  ³*J£O×ÏtWW¬0¨ÔIÍj¢SÚ&aæÃòP6G6&
  ÉÝ¬®J)9j»±©¤QÓëe >;
  ÞãÄåùCLj¶¢kÃ
  îòæ¸¶S¬j| z×ÔS`/ÝaWV°¿
License: UNKNOWN
 FIXME

Files: ./tests/data/creds/old-heimdal
Copyright: B]{+L
License: UNKNOWN
 FIXME

Files: ./m4/ltoptions.m4
Copyright: 2004-2005, 2007-2009, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./m4/ld-version.m4
Copyright: 2008-2010, Free Software Foundation, Inc.
  2010, The Board of Trustees of the Leland Stanford Junior University
License: UNKNOWN
 FIXME

Files: ./tests/portable/snprintf-t.c
Copyright: 1995, Patrick Powell
  2000-2006, Russ Allbery <eagle@eyrie.org>
  2001, Hrvoje Niksic
  2009-2010, The Board of Trustees of the Leland Stanford Junior University
License: UNKNOWN
 FIXME

Files: ./docs/protocol.txt
Copyright: 2014, IETF Trust and the persons identified as the
License: UNKNOWN
 FIXME

Files: ./util/messages.c
Copyright: 2004-2006, Internet Systems Consortium, Inc. ("ISC")
  2008-2010, 2013, The Board of Trustees of the Leland Stanford Junior University
License: UNKNOWN
 FIXME

Files: ./weblogin/images/login.png
Copyright: KÕ©6N|Kl¯õm÷^üÏhO7ÞÝ³Ï93³ó|¥÷¬gçÌùgç%Éd2L&Éd&ÈTMï;Øk?_­s:
  ¸2á±6$Ã¼V:êá×mñÍDÇê`Ì*
  À
  ÈHïï=ßÉ&°¿(
  ëÅH¹]<.¶Ö0kiatN¬ÒgÄgÁR"þògÄ¢p^Üo
License: UNKNOWN
 FIXME

Files: ./portable/snprintf.c
Copyright: Patrick Powell 1995
License: UNKNOWN
 FIXME

Files: ./tests/runtests.c
Copyright: 2000-2001, 2004, 2006-2014, Russ Allbery <eagle@eyrie.org>
License: UNKNOWN
 FIXME

Files: ./README
Copyright: 2002-2014, The Board of Trustees of the Leland Stanford Junior
License: UNKNOWN
 FIXME

Files: ./tests/data/creds/basic
Copyright: WxyÕäÿRîîdÜ¸êá?À0¶4(åWÜw±¾¨>¸
  âª3ó½3`®$]Õ.×àöúNB
License: UNKNOWN
 FIXME

Files: ./docs/diagrams/weblogin-flow.png
Copyright: pK aA
License: UNKNOWN
 FIXME

Files: ./weblogin/images/confirm.xcf
Copyright: ÿ	ÿ3 ýþÿÿ3 ýnøÿÿ3 ýSèÿÿ3 ý+Ôÿÿ3 ü
License: UNKNOWN
 FIXME

