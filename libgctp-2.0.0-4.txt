Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Makefile.am
 ./README
 ./announce.txt
 ./config.h.in
 ./configure.ac
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libgctp-dev.install
 ./debian/patches/pkg-config.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/GCTP.pdf
 ./doc/README
 ./doc/appendixA.txt
 ./doc/appendixB.txt
 ./doc/appendixC.txt
 ./doc/error_codes.txt
 ./doc/for_init.ps
 ./doc/gctp.ps
 ./doc/inv_init.ps
 ./doc/overview.ps
 ./doc/reference.txt
 ./doc/xxxfor.ps
 ./doc/xxxinv.ps
 ./gctp.pc.in
 ./source/Makefile.am
 ./source/README
 ./source/alberfor.c
 ./source/alberinv.c
 ./source/alconfor.c
 ./source/alconinv.c
 ./source/azimfor.c
 ./source/aziminv.c
 ./source/br_gctp.c
 ./source/cproj.c
 ./source/cproj.h
 ./source/eqconfor.c
 ./source/eqconinv.c
 ./source/equifor.c
 ./source/equiinv.c
 ./source/for_init.c
 ./source/gctp.c
 ./source/gnomfor.c
 ./source/gnominv.c
 ./source/goodfor.c
 ./source/goodinv.c
 ./source/gvnspfor.c
 ./source/gvnspinv.c
 ./source/hamfor.c
 ./source/haminv.c
 ./source/imolwfor.c
 ./source/imolwinv.c
 ./source/inv_init.c
 ./source/lamazfor.c
 ./source/lamazinv.c
 ./source/lamccfor.c
 ./source/lamccinv.c
 ./source/make.com
 ./source/merfor.c
 ./source/merinv.c
 ./source/millfor.c
 ./source/millinv.c
 ./source/molwfor.c
 ./source/molwinv.c
 ./source/nad1927
 ./source/nad1983
 ./source/obleqfor.c
 ./source/obleqinv.c
 ./source/omerfor.c
 ./source/omerinv.c
 ./source/options
 ./source/orthfor.c
 ./source/orthinv.c
 ./source/paksz.c
 ./source/polyfor.c
 ./source/polyinv.c
 ./source/proj.h
 ./source/psfor.c
 ./source/psinv.c
 ./source/report.c
 ./source/robfor.c
 ./source/robinv.c
 ./source/sinfor.c
 ./source/sininv.c
 ./source/somfor.c
 ./source/sominv.c
 ./source/sphdz.c
 ./source/spload.f
 ./source/sterfor.c
 ./source/sterinv.c
 ./source/stplnfor.c
 ./source/stplninv.c
 ./source/tmfor.c
 ./source/tminv.c
 ./source/untfz.c
 ./source/utmfor.c
 ./source/utminv.c
 ./source/vandgfor.c
 ./source/vandginv.c
 ./source/wivfor.c
 ./source/wivinv.c
 ./source/wviifor.c
 ./source/wviiinv.c
 ./test/README
 ./test/proj/alaska/alaska_test
 ./test/proj/alaska/data23
 ./test/proj/alaska/data23a
 ./test/proj/alaska/data23b
 ./test/proj/alaska/parm23
 ./test/proj/alaska/parm23a
 ./test/proj/alaska/parm23b
 ./test/proj/albers/data3
 ./test/proj/albers/data3a
 ./test/proj/albers/data3b
 ./test/proj/albers/parm3
 ./test/proj/albers/parm3a
 ./test/proj/albers/parm3b
 ./test/proj/azmeqd/data12
 ./test/proj/azmeqd/data12a
 ./test/proj/azmeqd/data12b
 ./test/proj/azmeqd/data12c
 ./test/proj/azmeqd/parm12
 ./test/proj/azmeqd/parm12a
 ./test/proj/azmeqd/parm12b
 ./test/proj/azmeqd/parm12c
 ./test/proj/eqrect/data17
 ./test/proj/eqrect/data17a
 ./test/proj/eqrect/data17b
 ./test/proj/eqrect/data17c
 ./test/proj/eqrect/data17d
 ./test/proj/eqrect/parm17
 ./test/proj/eqrect/parm17a
 ./test/proj/eqrect/parm17b
 ./test/proj/eqrect/parm17c
 ./test/proj/eqrect/parm17d
 ./test/proj/equidc/data8
 ./test/proj/equidc/data8b
 ./test/proj/equidc/data8c
 ./test/proj/equidc/data8d
 ./test/proj/equidc/parm8
 ./test/proj/equidc/parm8b
 ./test/proj/equidc/parm8c
 ./test/proj/equidc/parm8d
 ./test/proj/gnomon/data13
 ./test/proj/gnomon/data13a
 ./test/proj/gnomon/data13b
 ./test/proj/gnomon/data13c
 ./test/proj/gnomon/data13d
 ./test/proj/gnomon/parm13
 ./test/proj/gnomon/parm13a
 ./test/proj/gnomon/parm13b
 ./test/proj/gnomon/parm13c
 ./test/proj/gnomon/parm13d
 ./test/proj/good/data24a
 ./test/proj/good/data24b
 ./test/proj/good/data24c
 ./test/proj/good/parm24a
 ./test/proj/good/parm24b
 ./test/proj/good/parm24c
 ./test/proj/gvnsp/data15
 ./test/proj/gvnsp/data15a
 ./test/proj/gvnsp/data15b
 ./test/proj/gvnsp/data15c
 ./test/proj/gvnsp/data15d
 ./test/proj/gvnsp/parm15
 ./test/proj/gvnsp/parm15a
 ./test/proj/gvnsp/parm15b
 ./test/proj/gvnsp/parm15c
 ./test/proj/gvnsp/parm15d
 ./test/proj/hammer/data27a
 ./test/proj/hammer/data27b
 ./test/proj/hammer/parm27a
 ./test/proj/hammer/parm27b
 ./test/proj/hom/data20
 ./test/proj/hom/data20a
 ./test/proj/hom/data20b
 ./test/proj/hom/data20c
 ./test/proj/hom/data20d
 ./test/proj/hom/parm20
 ./test/proj/hom/parm20a
 ./test/proj/hom/parm20b
 ./test/proj/hom/parm20c
 ./test/proj/hom/parm20d
 ./test/proj/imoll/data26a
 ./test/proj/imoll/data26b
 ./test/proj/imoll/data26c
 ./test/proj/imoll/parm26a
 ./test/proj/imoll/parm26b
 ./test/proj/imoll/parm26c
 ./test/proj/lamaz/README
 ./test/proj/lamaz/data11
 ./test/proj/lamaz/data11a
 ./test/proj/lamaz/data11b
 ./test/proj/lamaz/data11c
 ./test/proj/lamaz/parm11
 ./test/proj/lamaz/parm11a
 ./test/proj/lamaz/parm11b
 ./test/proj/lamaz/parm11c
 ./test/proj/lamcc/README
 ./test/proj/lamcc/data4
 ./test/proj/lamcc/data4a
 ./test/proj/lamcc/data4b
 ./test/proj/lamcc/data4c
 ./test/proj/lamcc/parm4
 ./test/proj/lamcc/parm4a
 ./test/proj/lamcc/parm4b
 ./test/proj/lamcc/parm4c
 ./test/proj/mercat/data5
 ./test/proj/mercat/data5a
 ./test/proj/mercat/data5b
 ./test/proj/mercat/data5c
 ./test/proj/mercat/parm5
 ./test/proj/mercat/parm5a
 ./test/proj/mercat/parm5b
 ./test/proj/mercat/parm5c
 ./test/proj/miller/data18
 ./test/proj/miller/data18b
 ./test/proj/miller/data18c
 ./test/proj/miller/data18d
 ./test/proj/miller/data19a
 ./test/proj/miller/parm18
 ./test/proj/miller/parm18a
 ./test/proj/miller/parm18b
 ./test/proj/miller/parm18c
 ./test/proj/miller/parm18d
 ./test/proj/moll/data25a
 ./test/proj/moll/data25b
 ./test/proj/moll/data25c
 ./test/proj/moll/parm25a
 ./test/proj/moll/parm25b
 ./test/proj/moll/parm25c
 ./test/proj/obleqa/data30a
 ./test/proj/obleqa/data30b
 ./test/proj/obleqa/data30c
 ./test/proj/obleqa/parm30a
 ./test/proj/obleqa/parm30b
 ./test/proj/obleqa/parm30c
 ./test/proj/ortho/README
 ./test/proj/ortho/data14
 ./test/proj/ortho/data14a
 ./test/proj/ortho/data14b
 ./test/proj/ortho/data14c
 ./test/proj/ortho/data14d
 ./test/proj/ortho/parm14
 ./test/proj/ortho/parm14a
 ./test/proj/ortho/parm14b
 ./test/proj/ortho/parm14c
 ./test/proj/ortho/parm14d
 ./test/proj/polyc/data7a
 ./test/proj/polyc/data7b
 ./test/proj/polyc/data7c
 ./test/proj/polyc/data7d
 ./test/proj/polyc/parm7a
 ./test/proj/polyc/parm7b
 ./test/proj/polyc/parm7c
 ./test/proj/polyc/parm7d
 ./test/proj/ps/data6
 ./test/proj/ps/data6a
 ./test/proj/ps/data6b
 ./test/proj/ps/data6c
 ./test/proj/ps/data6d
 ./test/proj/ps/data6e
 ./test/proj/ps/parm6
 ./test/proj/ps/parm6a
 ./test/proj/ps/parm6b
 ./test/proj/ps/parm6c
 ./test/proj/ps/parm6d
 ./test/proj/ps/parm6e
 ./test/proj/robin/README
 ./test/proj/robin/data21
 ./test/proj/robin/data21a
 ./test/proj/robin/data21b
 ./test/proj/robin/data21c
 ./test/proj/robin/data21d
 ./test/proj/robin/parm21
 ./test/proj/robin/parm21a
 ./test/proj/robin/parm21b
 ./test/proj/robin/parm21c
 ./test/proj/robin/parm21d
 ./test/proj/snsoid/data16
 ./test/proj/snsoid/data16a
 ./test/proj/snsoid/data16c
 ./test/proj/snsoid/data16d
 ./test/proj/snsoid/parm16
 ./test/proj/snsoid/parm16a
 ./test/proj/snsoid/parm16c
 ./test/proj/snsoid/parm16d
 ./test/proj/som/README
 ./test/proj/som/data22
 ./test/proj/som/data22a
 ./test/proj/som/data22c
 ./test/proj/som/data22d
 ./test/proj/som/data22e
 ./test/proj/som/parm22
 ./test/proj/som/parm22a
 ./test/proj/som/parm22c
 ./test/proj/som/parm22d
 ./test/proj/som/parm22e
 ./test/proj/spcs/README
 ./test/proj/spcs/data0101
 ./test/proj/spcs/data0503
 ./test/proj/spcs/data1301
 ./test/proj/spcs/data1703
 ./test/proj/spcs/data2201
 ./test/proj/spcs/data2701
 ./test/proj/spcs/data2900
 ./test/proj/spcs/data3003
 ./test/proj/spcs/data3601
 ./test/proj/spcs/data3800
 ./test/proj/spcs/data4002
 ./test/proj/spcs/data4400
 ./test/proj/spcs/data4803
 ./test/proj/spcs/data5001
 ./test/proj/spcs/data5009
 ./test/proj/spcs/data5104
 ./test/proj/spcs/data5400
 ./test/proj/spcs/parm20101
 ./test/proj/spcs/parm20503
 ./test/proj/spcs/parm21301
 ./test/proj/spcs/parm21703
 ./test/proj/spcs/parm22201
 ./test/proj/spcs/parm22701
 ./test/proj/spcs/parm22900
 ./test/proj/spcs/parm23003
 ./test/proj/spcs/parm23601
 ./test/proj/spcs/parm23800
 ./test/proj/spcs/parm24002
 ./test/proj/spcs/parm24400
 ./test/proj/spcs/parm24803
 ./test/proj/spcs/parm25001
 ./test/proj/spcs/parm25009
 ./test/proj/spcs/parm25104
 ./test/proj/spcs/parm25400
 ./test/proj/stereo/data10
 ./test/proj/stereo/data10a
 ./test/proj/stereo/data10b
 ./test/proj/stereo/parm10
 ./test/proj/stereo/parm10a
 ./test/proj/stereo/parm10b
 ./test/proj/tm/data9
 ./test/proj/tm/data9a
 ./test/proj/tm/data9b
 ./test/proj/tm/data9c
 ./test/proj/tm/data9d
 ./test/proj/tm/data9e
 ./test/proj/tm/parm9
 ./test/proj/tm/parm9a
 ./test/proj/tm/parm9b
 ./test/proj/tm/parm9c
 ./test/proj/tm/parm9d
 ./test/proj/tm/parm9e
 ./test/proj/utm/README
 ./test/proj/utm/data1n13
 ./test/proj/utm/data1n31
 ./test/proj/utm/data1p13
 ./test/proj/utm/data1p31
 ./test/proj/utm/parm1n13
 ./test/proj/utm/parm1n31
 ./test/proj/utm/parm1p13
 ./test/proj/utm/parm1p31
 ./test/proj/vgrint/data19
 ./test/proj/vgrint/data19a
 ./test/proj/vgrint/data19b
 ./test/proj/vgrint/data19c
 ./test/proj/vgrint/data19d
 ./test/proj/vgrint/parm19
 ./test/proj/vgrint/parm19a
 ./test/proj/vgrint/parm19b
 ./test/proj/vgrint/parm19c
 ./test/proj/vgrint/parm19d
 ./test/proj/wagiv/data28
 ./test/proj/wagiv/data28a
 ./test/proj/wagiv/data28b
 ./test/proj/wagiv/data28c
 ./test/proj/wagiv/parm28
 ./test/proj/wagiv/parm28a
 ./test/proj/wagiv/parm28b
 ./test/proj/wagiv/parm28c
 ./test/proj/wagvii/data29
 ./test/proj/wagvii/data29a
 ./test/proj/wagvii/data29b
 ./test/proj/wagvii/data29c
 ./test/proj/wagvii/parm29
 ./test/proj/wagvii/parm29a
 ./test/proj/wagvii/parm29b
 ./test/proj/wagvii/parm29c
 ./test/test_proj/Makefile
 ./test/test_proj/alaska_test
 ./test/test_proj/albers_test
 ./test/test_proj/azmeqd_test
 ./test/test_proj/eqrect_test
 ./test/test_proj/equidc_test
 ./test/test_proj/gnomon_test
 ./test/test_proj/good_test
 ./test/test_proj/gvnsp_test
 ./test/test_proj/hammer_test
 ./test/test_proj/hom_test
 ./test/test_proj/imoll_test
 ./test/test_proj/lamaz_test
 ./test/test_proj/lamcc_test
 ./test/test_proj/mercat_test
 ./test/test_proj/miller_test
 ./test/test_proj/moll_test
 ./test/test_proj/obleqa_test
 ./test/test_proj/ortho_test
 ./test/test_proj/polyc_test
 ./test/test_proj/ps_test
 ./test/test_proj/robin_test
 ./test/test_proj/snsoid_test
 ./test/test_proj/som_test
 ./test/test_proj/spcs_test
 ./test/test_proj/stereo_test
 ./test/test_proj/testproj.c
 ./test/test_proj/tm_test
 ./test/test_proj/utm_test
 ./test/test_proj/vgrint_test
 ./test/test_proj/wagiv_test
 ./test/test_proj/wagvii_test
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./compile
 ./depcomp
 ./ltmain.sh
 ./missing
 ./test-driver
Copyright: 1996-2014, Free Software Foundation, Inc.
  1996-2015, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
  2011-2014, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2014, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./Makefile.in
 ./source/Makefile.in
Copyright: 1994-2014, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./aclocal.m4
Copyright: 1996-2014, Free Software Foundation, Inc.
License: GPL
 FIXME

Files: ./debian/copyright
Copyright: for NWS information -- see below), 2) use it in a manner that implies an endorsement
License: UNKNOWN
 FIXME

