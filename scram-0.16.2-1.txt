Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.appveyor.yml
 ./.clang-format
 ./.clang-tidy
 ./.codecov.yml
 ./.coveragerc
 ./.cppdep.yml
 ./.gitmodules
 ./.landscape.yml
 ./.style.yapf
 ./.travis.yml
 ./.travis/doxygen.conf
 ./.travis/gui_doxygen.conf
 ./.travis/install.sh
 ./.travis/lint.sh
 ./.travis/run_tests.sh
 ./.travis/script.sh
 ./CMakeLists.txt
 ./CODE_OF_CONDUCT.md
 ./CONTRIBUTING.md
 ./Dockerfile
 ./README.rst
 ./cmake/FindJeMalloc.cmake
 ./cmake/FindTcmalloc.cmake
 ./cmake/ScramBundle.cmake
 ./cmake/cmake_uninstall.cmake.in
 ./crowdin.yml
 ./debian/compat
 ./debian/control
 ./debian/rules
 ./debian/scram-gui.1
 ./debian/scram-gui.install
 ./debian/scram-gui.manpages
 ./debian/scram.1
 ./debian/scram.install
 ./debian/scram.manpages
 ./debian/source/format
 ./debian/watch
 ./doc/bugs.rst
 ./doc/citation.rst
 ./doc/coding_standards.rst
 ./doc/common_cause_analysis.rst
 ./doc/config_file.rst
 ./doc/description.rst
 ./doc/design_description.rst
 ./doc/event_tree_analysis.rst
 ./doc/example/aralia_input.txt
 ./doc/example/config.xml
 ./doc/example/input.xml
 ./doc/example/report.xml
 ./doc/fault_tree_analysis.rst
 ./doc/fault_tree_generator.rst
 ./doc/fta_algorithms.rst
 ./doc/fta_preprocessing.rst
 ./doc/gui.rst
 ./doc/input_file.rst
 ./doc/installation.rst
 ./doc/opsa_support.rst
 ./doc/performance.rst
 ./doc/probability_analysis.rst
 ./doc/references.rst
 ./doc/release/release_checklist.rst
 ./doc/release/v0.1.0.md
 ./doc/release/v0.1.1.md
 ./doc/release/v0.1.2.md
 ./doc/release/v0.11.0.md
 ./doc/release/v0.11.1.md
 ./doc/release/v0.11.2.md
 ./doc/release/v0.11.3.md
 ./doc/release/v0.11.4.md
 ./doc/release/v0.11.5.md
 ./doc/release/v0.11.6.md
 ./doc/release/v0.12.0.md
 ./doc/release/v0.13.0.md
 ./doc/release/v0.14.0.md
 ./doc/release/v0.15.0.md
 ./doc/release/v0.16.0.md
 ./doc/release/v0.16.1.md
 ./doc/release/v0.16.2.md
 ./doc/release/v0.2.0.md
 ./doc/release/v0.3.0.md
 ./doc/release/v0.3.1.md
 ./doc/release/v0.4.0.md
 ./doc/release/v0.5.0.md
 ./doc/release/v0.6.0.md
 ./doc/release/v0.7.0.md
 ./doc/release/v0.9.0.md
 ./doc/release/v0.9.1.md
 ./doc/release/v0.9.2.md
 ./doc/report_file.rst
 ./doc/scram-gui.h2m
 ./doc/scram.h2m
 ./doc/substitutions.rst
 ./doc/theory.rst
 ./doc/todo.rst
 ./doc/uncertainty_analysis.rst
 ./doc/xml_comments.rst
 ./gui/.clang-format
 ./gui/CMakeLists.txt
 ./gui/eventdialog.ui
 ./gui/images/scram_logo.svg
 ./gui/images/scram_solid.svg
 ./gui/images/scram_solid16x16.png
 ./gui/mainwindow.ui
 ./gui/namedialog.ui
 ./gui/preferencesdialog.ui
 ./gui/res.qrc
 ./gui/scram-gui.desktop
 ./gui/scram.ico
 ./gui/scram.rc
 ./gui/settingsdialog.ui
 ./gui/startpage.ui
 ./gui/tests/CMakeLists.txt
 ./gui/translations/CMakeLists.txt
 ./gui/translations/scramgui_af_ZA.ts
 ./gui/translations/scramgui_ar_SA.ts
 ./gui/translations/scramgui_ca_ES.ts
 ./gui/translations/scramgui_cs_CZ.ts
 ./gui/translations/scramgui_da_DK.ts
 ./gui/translations/scramgui_de_DE.ts
 ./gui/translations/scramgui_el_GR.ts
 ./gui/translations/scramgui_en.ts
 ./gui/translations/scramgui_es_ES.ts
 ./gui/translations/scramgui_fi_FI.ts
 ./gui/translations/scramgui_he_IL.ts
 ./gui/translations/scramgui_hu_HU.ts
 ./gui/translations/scramgui_id_ID.ts
 ./gui/translations/scramgui_it_IT.ts
 ./gui/translations/scramgui_ja_JP.ts
 ./gui/translations/scramgui_ko_KR.ts
 ./gui/translations/scramgui_nl_NL.ts
 ./gui/translations/scramgui_no_NO.ts
 ./gui/translations/scramgui_pl_PL.ts
 ./gui/translations/scramgui_pt_BR.ts
 ./gui/translations/scramgui_pt_PT.ts
 ./gui/translations/scramgui_ro_RO.ts
 ./gui/translations/scramgui_ru_RU.ts
 ./gui/translations/scramgui_sr_SP.ts
 ./gui/translations/scramgui_sv_SE.ts
 ./gui/translations/scramgui_tr_TR.ts
 ./gui/translations/scramgui_uk_UA.ts
 ./gui/translations/scramgui_vi_VN.ts
 ./gui/translations/scramgui_zh_CN.ts
 ./gui/translations/scramgui_zh_TW.ts
 ./input/Aralia/baobab1.xml
 ./input/Aralia/baobab2.xml
 ./input/Aralia/baobab3.xml
 ./input/Aralia/cea9601.xml
 ./input/Aralia/chinese.xml
 ./input/Aralia/das9201.xml
 ./input/Aralia/das9202.xml
 ./input/Aralia/das9203.xml
 ./input/Aralia/das9204.xml
 ./input/Aralia/das9205.xml
 ./input/Aralia/das9206.xml
 ./input/Aralia/das9207.xml
 ./input/Aralia/das9208.xml
 ./input/Aralia/das9209.xml
 ./input/Aralia/das9601.xml
 ./input/Aralia/das9701.xml
 ./input/Aralia/edf9201.xml
 ./input/Aralia/edf9202.xml
 ./input/Aralia/edf9203.xml
 ./input/Aralia/edf9204.xml
 ./input/Aralia/edf9205.xml
 ./input/Aralia/edf9206.xml
 ./input/Aralia/edfpa14b.xml
 ./input/Aralia/edfpa14o.xml
 ./input/Aralia/edfpa14p.xml
 ./input/Aralia/edfpa14q.xml
 ./input/Aralia/edfpa14r.xml
 ./input/Aralia/edfpa15b.xml
 ./input/Aralia/edfpa15o.xml
 ./input/Aralia/edfpa15p.xml
 ./input/Aralia/edfpa15q.xml
 ./input/Aralia/edfpa15r.xml
 ./input/Aralia/elf9601.xml
 ./input/Aralia/ftr10.xml
 ./input/Aralia/isp9601.xml
 ./input/Aralia/isp9602.xml
 ./input/Aralia/isp9603.xml
 ./input/Aralia/isp9604.xml
 ./input/Aralia/isp9605.xml
 ./input/Aralia/isp9606.xml
 ./input/Aralia/isp9607.xml
 ./input/Aralia/jbd9601.xml
 ./input/Aralia/nus9601.xml
 ./input/Autogenerated/200_event.xml
 ./input/BSCU/BSCU.xml
 ./input/Baobab/baobab1-basic-events.xml
 ./input/Baobab/baobab1.xml
 ./input/Baobab/baobab2-basic-events.xml
 ./input/Baobab/baobab2.xml
 ./input/Baobab/european1.txt
 ./input/CEA9601/CEA9601-basic-events.xml
 ./input/CEA9601/CEA9601.xml
 ./input/CMakeLists.txt
 ./input/Chinese/chinese-basic-events.xml
 ./input/Chinese/chinese.xml
 ./input/Chinese/high-prob.xml
 ./input/Chinese/mcs_prob.xml
 ./input/Chinese/mixed_mcs_prob.xml
 ./input/EventTrees/attack.xml
 ./input/EventTrees/attack_alignment.xml
 ./input/EventTrees/bcd.xml
 ./input/EventTrees/gas_leak/gas_leak.xml
 ./input/EventTrees/gas_leak/gas_leak_reactive.xml
 ./input/EventTrees/mef_example.xml
 ./input/HIPPS/HIPPS.xml
 ./input/Lift/OpenFTA_Report.txt
 ./input/Lift/lift.xml
 ./input/SmallTree/SmallTree.xml
 ./input/Theatre/OpenFTA_Report.txt
 ./input/Theatre/theatre.xml
 ./input/ThreeLevels/OpenFTA_Report.txt
 ./input/ThreeLevels/top.xml
 ./input/ThreeMotor/OpenFTA_Report.txt
 ./input/ThreeMotor/event_tree.xml
 ./input/ThreeMotor/three_motor.xml
 ./input/TransTest/OpenFTA_Report.txt
 ./input/TransTest/trans_model_data.xml
 ./input/TransTest/trans_one.xml
 ./input/TransTest/trans_two.xml
 ./input/TwoTrain/RiskMan_Reports/1.txt
 ./input/TwoTrain/RiskMan_Reports/2.txt
 ./input/TwoTrain/RiskMan_Reports/3.txt
 ./input/TwoTrain/RiskMan_Reports/4.txt
 ./input/TwoTrain/RiskMan_Reports/5.txt
 ./input/TwoTrain/RiskMan_Reports/6.txt
 ./input/TwoTrain/RiskMan_Reports/7.txt
 ./input/TwoTrain/RiskMan_Reports/8.txt
 ./input/TwoTrain/RiskMan_Reports/9.txt
 ./input/TwoTrain/common_cause.xml
 ./input/TwoTrain/config.xml
 ./input/TwoTrain/event_tree.xml
 ./input/TwoTrain/nondeclarative_substitutions.xml
 ./input/TwoTrain/substitutions.xml
 ./input/TwoTrain/two_train.xml
 ./input/TwoTrain/two_train_alignment.xml
 ./input/core/debug/forced_module.xml
 ./input/core/debug/linked_shared_events.xml
 ./input/core/debug/module_detection.xml
 ./input/core/lazy_module.xml
 ./input/core/module_cut_off.xml
 ./input/ne574/ne574.xml
 ./requirements-dev.txt
 ./requirements-tests.txt
 ./scripts/scram
 ./share/CMakeLists.txt
 ./share/config.rng
 ./share/gui.rng
 ./share/input.rng
 ./share/report.rng
 ./src/CMakeLists.txt
 ./tests/CMakeLists.txt
 ./tests/input/core/a_and_not_a.xml
 ./tests/input/core/a_and_not_b.xml
 ./tests/input/core/a_or_not_a.xml
 ./tests/input/core/a_or_not_ab.xml
 ./tests/input/core/a_or_not_b.xml
 ./tests/input/core/ab_bc.xml
 ./tests/input/core/ab_or_not_ac.xml
 ./tests/input/core/abc.xml
 ./tests/input/core/alpha_factor_ccf.xml
 ./tests/input/core/atleast.xml
 ./tests/input/core/beta_factor_ccf.xml
 ./tests/input/core/complement_module.xml
 ./tests/input/core/mgl_ccf.xml
 ./tests/input/core/multiple_parent_negative_gate.xml
 ./tests/input/core/nand.xml
 ./tests/input/core/nand_or_equality.xml
 ./tests/input/core/nor.xml
 ./tests/input/core/not_a.xml
 ./tests/input/core/not_and_or_equality.xml
 ./tests/input/core/null.xml
 ./tests/input/core/null_a.xml
 ./tests/input/core/one_prob.xml
 ./tests/input/core/phi_factor_ccf.xml
 ./tests/input/core/single_exponential.xml
 ./tests/input/core/subtle_null.xml
 ./tests/input/core/subtle_unity.xml
 ./tests/input/core/unity.xml
 ./tests/input/core/xor.xml
 ./tests/input/core/zero_prob.xml
 ./tests/input/custom_xmlns.xml
 ./tests/input/empty_attribute.xml
 ./tests/input/empty_element.xml
 ./tests/input/empty_model.xml
 ./tests/input/eta/block_instruction.xml
 ./tests/input/eta/collect_formula.xml
 ./tests/input/eta/cyclic_branches_fork.xml
 ./tests/input/eta/cyclic_branches_self.xml
 ./tests/input/eta/cyclic_branches_transitive.xml
 ./tests/input/eta/cyclic_link_self.xml
 ./tests/input/eta/cyclic_link_transitive.xml
 ./tests/input/eta/cyclic_rule_block.xml
 ./tests/input/eta/cyclic_rule_self.xml
 ./tests/input/eta/cyclic_rule_transitive.xml
 ./tests/input/eta/doubly_defined_branch.xml
 ./tests/input/eta/doubly_defined_event_tree.xml
 ./tests/input/eta/doubly_defined_functional_event.xml
 ./tests/input/eta/doubly_defined_initiating_event.xml
 ./tests/input/eta/doubly_defined_path_state.xml
 ./tests/input/eta/doubly_defined_rule.xml
 ./tests/input/eta/doubly_defined_sequence.xml
 ./tests/input/eta/empty.xml
 ./tests/input/eta/if_then_else_instruction.xml
 ./tests/input/eta/initiating_event.xml
 ./tests/input/eta/invalid_collect_formula.xml
 ./tests/input/eta/invalid_duplicate_event_in_forks.xml
 ./tests/input/eta/invalid_event_order_in_branch.xml
 ./tests/input/eta/invalid_event_order_in_initial_state.xml
 ./tests/input/eta/invalid_event_order_in_link.xml
 ./tests/input/eta/invalid_event_order_in_ref_branch.xml
 ./tests/input/eta/invalid_link_in_branch.xml
 ./tests/input/eta/invalid_link_in_rule.xml
 ./tests/input/eta/invalid_link_instruction.xml
 ./tests/input/eta/invalid_link_undefined_event_tree.xml
 ./tests/input/eta/link_in_rule.xml
 ./tests/input/eta/link_instruction.xml
 ./tests/input/eta/mixing_collect_instructions.xml
 ./tests/input/eta/mixing_collect_instructions_fork.xml
 ./tests/input/eta/mixing_collect_instructions_link.xml
 ./tests/input/eta/nested_formula.xml
 ./tests/input/eta/private_branch.xml
 ./tests/input/eta/private_functional_event.xml
 ./tests/input/eta/public_sequence.xml
 ./tests/input/eta/rule_instruction.xml
 ./tests/input/eta/set_house_event.xml
 ./tests/input/eta/simplest_correct.xml
 ./tests/input/eta/single_expression.xml
 ./tests/input/eta/test_event_default.xml
 ./tests/input/eta/test_functional_event.xml
 ./tests/input/eta/test_functional_event_link.xml
 ./tests/input/eta/test_initiating_event.xml
 ./tests/input/eta/undefined_arg_collect_formula.xml
 ./tests/input/eta/undefined_branch.xml
 ./tests/input/eta/undefined_event_tree.xml
 ./tests/input/eta/undefined_functional_event.xml
 ./tests/input/eta/undefined_house_in_set_house.xml
 ./tests/input/eta/undefined_rule.xml
 ./tests/input/eta/undefined_sequence.xml
 ./tests/input/eta/unused_elements.xml
 ./tests/input/fta/alpha_ccf_level_error.xml
 ./tests/input/fta/ambiguous_events_with_roles.xml
 ./tests/input/fta/atleast_gate.xml
 ./tests/input/fta/beta_ccf_level_error.xml
 ./tests/input/fta/case_sensitivity.xml
 ./tests/input/fta/ccf_more_factors_than_needed.xml
 ./tests/input/fta/ccf_negative_factor.xml
 ./tests/input/fta/ccf_unordered_factors.xml
 ./tests/input/fta/ccf_wrong_distribution.xml
 ./tests/input/fta/children_nand_nor.xml
 ./tests/input/fta/component_definition.xml
 ./tests/input/fta/constant_gates.xml
 ./tests/input/fta/constant_in_formulas.xml
 ./tests/input/fta/constant_propagation.xml
 ./tests/input/fta/correct_expressions.xml
 ./tests/input/fta/correct_formulas.xml
 ./tests/input/fta/correct_non_coherent.xml
 ./tests/input/fta/correct_tree_input.xml
 ./tests/input/fta/correct_tree_input_with_probs.xml
 ./tests/input/fta/cyclic_expression.xml
 ./tests/input/fta/cyclic_formula.xml
 ./tests/input/fta/cyclic_parameter.xml
 ./tests/input/fta/cyclic_tree.xml
 ./tests/input/fta/def_clash_basic_gate.xml
 ./tests/input/fta/def_clash_basic_house.xml
 ./tests/input/fta/def_clash_gate_primary.xml
 ./tests/input/fta/def_clash_house_basic.xml
 ./tests/input/fta/def_clash_house_gate.xml
 ./tests/input/fta/doubly_defined_basic.xml
 ./tests/input/fta/doubly_defined_ccf_group.xml
 ./tests/input/fta/doubly_defined_component.xml
 ./tests/input/fta/doubly_defined_gate.xml
 ./tests/input/fta/doubly_defined_house.xml
 ./tests/input/fta/doubly_defined_parameter.xml
 ./tests/input/fta/extra_ccf_level_beta_factor.xml
 ./tests/input/fta/flavored_types.xml
 ./tests/input/fta/full_configuration.xml
 ./tests/input/fta/graphing.xml
 ./tests/input/fta/importance_neg_test.xml
 ./tests/input/fta/importance_test.xml
 ./tests/input/fta/int_overflow.xml
 ./tests/input/fta/int_overflow_config.xml
 ./tests/input/fta/invalid_configuration.xml
 ./tests/input/fta/invalid_expression.xml
 ./tests/input/fta/invalid_periodic_test_num_args.xml
 ./tests/input/fta/invalid_probability.xml
 ./tests/input/fta/labels_and_attributes.xml
 ./tests/input/fta/mgl_ccf_level_error.xml
 ./tests/input/fta/missing_arg_expression.xml
 ./tests/input/fta/missing_bool_constant.xml
 ./tests/input/fta/missing_ccf_factor.xml
 ./tests/input/fta/missing_ccf_level_number.xml
 ./tests/input/fta/missing_ccf_members.xml
 ./tests/input/fta/missing_expression.xml
 ./tests/input/fta/missing_gate_definition.xml
 ./tests/input/fta/mixed_definitions.xml
 ./tests/input/fta/mixed_references.xml
 ./tests/input/fta/mixed_roles.xml
 ./tests/input/fta/model_data_mixed_definitions.xml
 ./tests/input/fta/name_clash_two_trees.xml
 ./tests/input/fta/nested_formula.xml
 ./tests/input/fta/non_top_gate.xml
 ./tests/input/fta/null_gate_with_label.xml
 ./tests/input/fta/orphan_primary_event.xml
 ./tests/input/fta/phi_ccf_wrong_sum.xml
 ./tests/input/fta/pi_configuration.xml
 ./tests/input/fta/private_at_model_scope.xml
 ./tests/input/fta/reference_missing_component.xml
 ./tests/input/fta/reference_missing_fault_tree.xml
 ./tests/input/fta/repeated_attribute.xml
 ./tests/input/fta/repeated_ccf_members.xml
 ./tests/input/fta/repeated_child.xml
 ./tests/input/fta/second_fault_tree.xml
 ./tests/input/fta/trailing_spaces.xml
 ./tests/input/fta/two_top_events.xml
 ./tests/input/fta/two_top_through_formula.xml
 ./tests/input/fta/two_trees.xml
 ./tests/input/fta/undefined_basic_event.xml
 ./tests/input/fta/undefined_event.xml
 ./tests/input/fta/undefined_gate.xml
 ./tests/input/fta/undefined_house_event.xml
 ./tests/input/fta/undefined_parameter.xml
 ./tests/input/fta/unordered_structure.xml
 ./tests/input/fta/unused_parameter.xml
 ./tests/input/fta/very_long_mcs.xml
 ./tests/input/fta/weibull_lnorm_deviate_2p.xml
 ./tests/input/fta/weibull_lnorm_deviate_3p.xml
 ./tests/input/fta/wrong_parameter_unit.xml
 ./tests/input/model/duplicate_alignment.xml
 ./tests/input/model/duplicate_extern_functions.xml
 ./tests/input/model/duplicate_extern_libraries.xml
 ./tests/input/model/duplicate_phases.xml
 ./tests/input/model/duplicate_substitution.xml
 ./tests/input/model/empty_alignment.xml
 ./tests/input/model/empty_extern_function.xml
 ./tests/input/model/excess_alignment.xml
 ./tests/input/model/extern_expression.xml
 ./tests/input/model/extern_full_check.xml
 ./tests/input/model/extern_function.xml
 ./tests/input/model/extern_library.xml
 ./tests/input/model/extern_library_invalid_path_format.xml
 ./tests/input/model/extern_library_ioerror.xml
 ./tests/input/model/incomplete_alignment.xml
 ./tests/input/model/invalid_num_args_extern_expression.xml
 ./tests/input/model/invalid_num_param_extern_function.xml
 ./tests/input/model/invalid_phase_fraction.xml
 ./tests/input/model/negative_phase_fraction.xml
 ./tests/input/model/private_phases.xml
 ./tests/input/model/substitution.xml
 ./tests/input/model/substitution_declarative_ccf.xml
 ./tests/input/model/substitution_declarative_noncoherent.xml
 ./tests/input/model/substitution_declarative_target_is_another_source.xml
 ./tests/input/model/substitution_duplicate_hypothesis_event.xml
 ./tests/input/model/substitution_duplicate_source_event.xml
 ./tests/input/model/substitution_nested_formula.xml
 ./tests/input/model/substitution_no_effect.xml
 ./tests/input/model/substitution_non_basic_event_formula.xml
 ./tests/input/model/substitution_nondeclarative_ccf_hypothesis.xml
 ./tests/input/model/substitution_nondeclarative_ccf_source.xml
 ./tests/input/model/substitution_nondeclarative_ccf_target.xml
 ./tests/input/model/substitution_nondeclarative_complex.xml
 ./tests/input/model/substitution_optional_source.xml
 ./tests/input/model/substitution_source_equal_target.xml
 ./tests/input/model/substitution_source_false_target.xml
 ./tests/input/model/substitution_source_is_another_hypothesis.xml
 ./tests/input/model/substitution_target_is_another_hypothesis.xml
 ./tests/input/model/substitution_target_is_another_source.xml
 ./tests/input/model/substitution_target_is_hypothesis.xml
 ./tests/input/model/substitution_type_mismatch.xml
 ./tests/input/model/substitution_types.xml
 ./tests/input/model/substitution_undefined_hypothesis_event.xml
 ./tests/input/model/substitution_undefined_source_event.xml
 ./tests/input/model/substitution_undefined_target_event.xml
 ./tests/input/model/system_extern_library.xml
 ./tests/input/model/undefined_extern_function.xml
 ./tests/input/model/undefined_extern_library.xml
 ./tests/input/model/undefined_symbol_extern_function.xml
 ./tests/input/model/undefined_target_set_house_event.xml
 ./tests/input/model/valid_alignment.xml
 ./tests/input/model/valid_sum_alignment.xml
 ./tests/input/model/zero_phase_fraction.xml
 ./tests/input/schema_fail.xml
 ./tests/input/undefined_xmlns.xml
 ./tests/input/unsupported_expression.xml
 ./tests/input/unsupported_feature.xml
 ./tests/input/unsupported_gate.xml
 ./tests/input/win_path_in_config.xml
 ./tests/input/xinclude.xml
 ./tests/input/xinclude_cycle.xml
 ./tests/input/xinclude_no_file.xml
 ./tests/input/xinclude_transitive.xml
 ./tests/input/xml_formatting_error.xml
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./gui/align.h
 ./gui/command.h
 ./gui/diagram.cpp
 ./gui/diagram.h
 ./gui/elementcontainermodel.cpp
 ./gui/elementcontainermodel.h
 ./gui/eventdialog.cpp
 ./gui/eventdialog.h
 ./gui/guiassert.h
 ./gui/importancetablemodel.cpp
 ./gui/importancetablemodel.h
 ./gui/language.cpp
 ./gui/language.h
 ./gui/main.cpp
 ./gui/mainwindow.cpp
 ./gui/mainwindow.h
 ./gui/model.cpp
 ./gui/model.h
 ./gui/modeltree.cpp
 ./gui/modeltree.h
 ./gui/overload.h
 ./gui/preferencesdialog.cpp
 ./gui/preferencesdialog.h
 ./gui/printable.cpp
 ./gui/printable.h
 ./gui/producttablemodel.cpp
 ./gui/producttablemodel.h
 ./gui/reporttree.cpp
 ./gui/reporttree.h
 ./gui/settingsdialog.cpp
 ./gui/settingsdialog.h
 ./gui/tests/data.h
 ./gui/tests/help.h
 ./gui/tests/testlanguage.cpp
 ./gui/tests/testmodel.cpp
 ./gui/tests/testvalidator.cpp
 ./gui/validator.cpp
 ./gui/validator.h
 ./gui/zoomableview.cpp
 ./gui/zoomableview.h
 ./scripts/fault_tree.py
 ./scripts/fault_tree_generator.py
 ./scripts/fuzz_tester.py
 ./scripts/nqueens.py
 ./scripts/test/test_fault_tree_generator.py
 ./src/alignment.cc
 ./src/alignment.h
 ./src/analysis.cc
 ./src/analysis.h
 ./src/bdd.cc
 ./src/bdd.h
 ./src/ccf_group.cc
 ./src/ccf_group.h
 ./src/config.cc
 ./src/config.h
 ./src/cycle.h
 ./src/element.cc
 ./src/element.h
 ./src/env.cc
 ./src/env.h
 ./src/error.h
 ./src/event.cc
 ./src/event.h
 ./src/event_tree.cc
 ./src/event_tree.h
 ./src/event_tree_analysis.cc
 ./src/event_tree_analysis.h
 ./src/expression.cc
 ./src/expression.h
 ./src/expression/boolean.h
 ./src/expression/conditional.cc
 ./src/expression/conditional.h
 ./src/expression/constant.cc
 ./src/expression/constant.h
 ./src/expression/exponential.cc
 ./src/expression/exponential.h
 ./src/expression/extern.cc
 ./src/expression/extern.h
 ./src/expression/numerical.cc
 ./src/expression/numerical.h
 ./src/expression/random_deviate.cc
 ./src/expression/random_deviate.h
 ./src/expression/test_event.cc
 ./src/expression/test_event.h
 ./src/ext/algorithm.h
 ./src/ext/bits.h
 ./src/ext/combination_iterator.h
 ./src/ext/find_iterator.h
 ./src/ext/float_compare.h
 ./src/ext/index_map.h
 ./src/ext/linear_map.h
 ./src/ext/multi_index.h
 ./src/ext/source_info.h
 ./src/ext/variant.h
 ./src/fault_tree.cc
 ./src/fault_tree.h
 ./src/fault_tree_analysis.cc
 ./src/fault_tree_analysis.h
 ./src/importance_analysis.cc
 ./src/importance_analysis.h
 ./src/initializer.cc
 ./src/initializer.h
 ./src/instruction.h
 ./src/logger.cc
 ./src/logger.h
 ./src/mocus.cc
 ./src/mocus.h
 ./src/model.cc
 ./src/model.h
 ./src/parameter.cc
 ./src/parameter.h
 ./src/pdag.cc
 ./src/pdag.h
 ./src/preprocessor.cc
 ./src/preprocessor.h
 ./src/probability_analysis.cc
 ./src/probability_analysis.h
 ./src/reporter.cc
 ./src/reporter.h
 ./src/risk_analysis.cc
 ./src/risk_analysis.h
 ./src/scram.cc
 ./src/serialization.cc
 ./src/serialization.h
 ./src/settings.cc
 ./src/settings.h
 ./src/substitution.cc
 ./src/substitution.h
 ./src/uncertainty_analysis.cc
 ./src/uncertainty_analysis.h
 ./src/version.cc.in
 ./src/version.h
 ./src/xml.cc
 ./src/xml.h
 ./src/xml_stream.h
 ./src/zbdd.cc
 ./src/zbdd.h
 ./tests/alignment_tests.cc
 ./tests/bench_200_event_tests.cc
 ./tests/bench_CEA9601_tests.cc
 ./tests/bench_attack.cc
 ./tests/bench_baobab1_tests.cc
 ./tests/bench_baobab2_tests.cc
 ./tests/bench_bscu_tests.cc
 ./tests/bench_chinese_tree_tests.cc
 ./tests/bench_core_tests.cc
 ./tests/bench_gas_leak.cc
 ./tests/bench_hipps_tests.cc
 ./tests/bench_lift_tests.cc
 ./tests/bench_ne574_tests.cc
 ./tests/bench_small_tree_tests.cc
 ./tests/bench_theatre_tests.cc
 ./tests/bench_three_motor_tests.cc
 ./tests/bench_two_train_tests.cc
 ./tests/ccf_group_tests.cc
 ./tests/config_tests.cc
 ./tests/element_tests.cc
 ./tests/event_tests.cc
 ./tests/expression_tests.cc
 ./tests/extern_function_tests.cc
 ./tests/fault_tree_tests.cc
 ./tests/initializer_tests.cc
 ./tests/linear_map_tests.cc
 ./tests/pdag_tests.cc
 ./tests/performance_tests.cc
 ./tests/performance_tests.h
 ./tests/risk_analysis_tests.cc
 ./tests/risk_analysis_tests.h
 ./tests/scram_dummy_extern.cc
 ./tests/scram_unit_test_driver.cc.in
 ./tests/serialization_tests.cc
 ./tests/settings_tests.cc
 ./tests/test_scram_call.py
 ./tests/utility.h
 ./tests/xml_stream_tests.cc
Copyright: 2014-2015, 2017-2018, Olzhas Rakhimov
  2014-2017, Olzhas Rakhimov
  2014-2018, Olzhas Rakhimov
  2015-2017, Olzhas Rakhimov
  2015-2018, Olzhas Rakhimov
  2016-2018, Olzhas Rakhimov
  2017, Olzhas Rakhimov
  2017-2018, Olzhas Rakhimov
  2018, Olzhas Rakhimov
License: GPL-3+
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  Copyright © 2014-2018 Olzhas Rakhimov <ol.rakhimov@gmail.com>
License: BSL and/or GPL-3+
 FIXME

Files: ./src/ext/scope_guard.h
Copyright: Yuri Kilochek 2017.
License: BSL-1.0
 FIXME

Files: ./doc/release/v0.8.0.md
Copyright: NONE
License: CC-BY-SA-4.0 and/or GPL
 FIXME

Files: ./gui/images/scram_solid64x64.png
Copyright: Pñx*Ç&£(X[[c!uLµZE2$ILuÑ(F£ÜÊ
  öÐÐ¼^/-®£@w&d2ø
License: UNKNOWN
 FIXME

Files: ./gui/images/scram32x32.png
Copyright: 1RS¡ð
  Ð"ü©)d£ÂvÚTffè´ZøA BgåJ
  Ðít¨..Ro6#B0[­Rk4x­^çÌò2ÕZ4IHÙjj½ÎÁÑÿúö[þðé§øA0±ìçkÀ0h..²õø1i¶ZGG
License: UNKNOWN
 FIXME

Files: ./gui/images/scram128x128.png
Copyright: B$02BF^¾ÁÁs,l!p8¤(@}qîÜEMôvgw[Ûê®Ó§¢ãäI
  «Ge%¼<t
  C"ÀÓÓÁjÅ)M
  I*:¼'b|xÃAÓìÙ8
  S
  SQP
  TVWó³ç§óôi~úÝïâ¯R1wÙ2þÇÓOsêÈ~ðàùÊT4ÍÍ¯^y
  ijÂf·+PÄã!
  jh
  kn&¿¨Õ*a99ÌYº{yóçO~I	¼¢"f-XÀýßü&s,Ao2)ûÿZ£Sv6
  ñ±1>Y¹¹Øl6Ò_|>>ö¬,Ìf³rÎX<NÇx&'qæçSQW§3HÐ×ÙÉÄè(9yy×ÖJË3O$è?{H$¢8¥é´t"d¤¿ßO^QYYYÊXÔ@ÀïÇëraÏÊ"Ãjeê
  /<ò®¼¢²2b±¡@ ½Á@MSw}õ«±j¥¥¤IÙÙÌ[¶û¿õ-êZZ(­¬$H
  ;¾üef-H$aÏ¶m¼òÇ?Òwö,¹EE¬¾ï>nºçÌÆÇqwwKÄÌÒRl¥¥dOw·ä±ÇãèL&UUX
  «?÷9ròóÉ°Ù°ge1géRVÞtFÂBtF#jµVÅéÄât^ôÃÿ
  ¡pñQZ>#µNC@
  4¾hÇC<TÆJ&z½Äb~¿D¢2Îx8L,þWf¥¹±)c¿X»¤T°æo|Ô
  xñý÷ÅÜeË#'GØQ?s¦øÏ>+Úo:$_w0ÍÙ¹¹â¡ï~WìNó.»Lè
  ÄÕ
  ×ÔµÍgÎòÚÚiXqE
  §Àd±°âÆyäG?¢¢®7}µÏ>ËÈÀ 6K®¾¾óìYY¼¿i/üæ7ô=KÍÆòë®ãÞGÁh6óÔO~ÂÖ×_grl¼¢"n¸ë.úÞ÷G£¼ðÛß²Q¾öl§ko¿/>þ8Ù¹¹¸;;ñöõDP«Õ²³q65¡3ñöõáêì$
  ªÂîpPZU¥|ÒVXVFN^U
  È-,$JQPLyM
  ÈÉÏgÞòå<ðÄ×ÖRQAÓH¥p0{É¾òä4¶¶'}×5òKJ³d	_xôQf/^,9BàózÑétTÖ×s÷Ã³üÚkÉÉËÃêpôûÑ
  Ñj©nl¤¸²ÆÖVçÍ¨Øj5:½ùó©kiÁ`µbq:¥ï¥|áz«sNæLÇ¹
  Ôj´ò2y>	Ro±
  Õ|´ckzþÎN
  Õ¸ÆÇ9¼gÝmmä3oùr
  ü=µNÞbv, JowFr* ê÷ÅÐLÓÇHõzG"èÌæiô9JIãôûÑ
  þ&J%áéß
License: UNKNOWN
 FIXME

Files: ./gui/images/scram64x64.png
Copyright: 6»] bõcuO<! ,yõU¡3$IbÃ³Ï
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_logo.png
Copyright: ä
  B©¤ù|@??NZ"Úrôè]WacìÄÈdÖ7"!%+
  S3u*>þ8
  tt³6~ó
  ââ-%
  ôV¤§£¶
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_solid32x32.png
Copyright: |>LÛ-Ð4­)çUMãÃì,G==ô
License: UNKNOWN
 FIXME

Files: ./gui/images/scram256x256.png
Copyright: ³SSLOLM§ñÔ44àq¸Ë©kz±Ï£åó
  ¹hëÅ"
  ½W¯rþÄ	¦ÇÇÉÉ .ã
  D|.ÕfÃåvã
  IþÔè(ã##DÃaó¤khm%
  O?%:?O<á÷Þ£¿«L*µ`±X°ù>zóMF(
  TUH9¤èDzxoHôîæõþó=ì}n$0ýM¬
  g·Ûq{<TÖÖòð³ÏòÐ³Ïtbn¹©)FÅ¼p4P dÝÖ­<õïY¨ë:S££Ä£QÕÇ«Í×ïgÛ½÷òèsÏÑ¹y³ïúýL1=1±`½Xä|z$ÂòÐO²ïÈÊü~FÐþ~s§ËEu}=O|ûÛxä¡N
  ihÀDÃaN}ü1¿úû¿çò©Sd2Óe°;ô¼¦ix¾ó*ª«QU|>Ïøð0'>ú·_zÞ-ÇbhÅ"ªÚZ®_ç¯u[·âHg³Lóáë¯óþ«¯rýòeñ8º®£(
  ihÀãõ¢g³dææÈÃ2á¡üð(
  ihàðSOñ'ÿæß°~ëV2§ïñùhyóH8LT¦qï^VYÉîùÖ_üûéXâm^»ÊÒ©YÉáøÝ<Àsÿò_òð³Ïª¨0]ë`y9k6lÀ
  kIaJ$Ì
  knfÇ¾}´oØ@ B÷ãÒñÂ³³
  ml¤¢¦§¤j^¬«äóy
  mlÄN&ùìwø»ÿðè¿vl:mê
  p5@N×l»ÃÝ²´ÐÆØD
  Õj¥¹£Ç¾ùMö9B(@©>M×¹ÑßO*X2¢(WWóÜÿ9>ñnÕjEu:ñúýÄc1&GFÈçr
  90~¬V+
  £]pÏª¨F@¾£¦ëÌÃ<y×_x+gÎFEÍB0ÈyèÙg9øøã
  £ë:ÑpÞ+WÈ¤R$b1ú¯]3¯l+I:dfrD4j6»¢®ëDææ¦(¹ééÛê1ÌMOßMÓÐ
  §hhmÅ¡ªhÀPo¯àÉü|Çóx½úïÐÐÒ¢ª&j-6?/`Çr<«Õ¢ª¸ÜnÖmÙÂ3ßû¶mÃãóM¡ë
  ©)
  ªs¹ñ
  ªèF,ÊfC±ÛE¾ßëEu»qx½8Aåå8+*ÌTõj,º®k,ñü¦¡e2dççÉÇãäÓiôb«ÍâtâQü~l.×ÂIÖB,Ff~b)ÀåÂî÷c±Wãe9ý±¡!N}ü1ç¾ø©±1,
  ­² ¸·Xt]ÊnÇ¢(â´Wl2Â¿¢ñPlªjþ}EUi_¿òª*¬+,B«ÍF¨¢Ú±Á|>jð·]¼¾@uÛ¶áñx½tlÜÇç[QÏåvÓ¹u+µØ
  ¯'J%UÂ£¨*uMMzòIÿË¿¤sóf3wít»éX¿ÍÆÌÄiÉdèUTWsøé§yöûßgãá§ª4¶µQ×Ü¦idR):é£êúz<ü0_ûþ÷Ù{èòrQÖkµ,+cíæÍ(ªJ<1	0ì³¿ë¾ûøî¿ù7Ü÷è£¥fllog«,,²Úl¸=B´tvòÀcñìýzóTs¸4¶¶Ò¾~=55ðär,+£¾¥
  ±1átnÙÂ¡§bÏRÛÔt3".=èü<OàêÙ³LR,Ó¾a[ï½5k¸×:âôOÄb
  ÉI²Ñ(
  ÔkU±X$H0?3¢ª´¬]Óí&27Ç¯ô#~úý_
  ä¢]ÓDqO"a¦QF¸|æÌØ|]Þ¡zz
  ñq2éôÍ»¼³äs9bóóô^¹Â
License: UNKNOWN
 FIXME

Files: ./gui/images/scram48x48.png
Copyright: 6ÜL
  I8.ñèsÏ	WjªXRY)n½óNa4Åý?ûÈ?_dÍ-ÿÝïÅf_ø¸ãÞ{
  â¥Kñfg
  ©8Ýn=fååát»ÑL ËSmþÖ ÿÄÿùàT¬ZEZf&/?÷ÿüíoÓ²gÝçÏsßý÷Æâr!Iáa¹¹QôzÌn7¾înì^/BÓà5
  ¬¯ç¶»î¢÷Âæ²|õjj©jh HðîáÃ¬ÍÊÂér¡3§Ù¼¼t=;w2zå
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_solid48x48.png
Copyright: (tÆ©[¡¥K«89X±`Ý¤~RLÌÇåÃäîºhjMÞ÷½3Qð7ÝÁÿyïyrï×Ý
  tD¶··LÑrd0`
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_solid128x128.png
Copyright: Gþbq>H}+Xv"N<D"ñÄÏ¨Vç
  i¢CÓ°vr:JÖü³cþÞ½:ÆF Àônßnk	¤ à¦á%I0jÓ«é ¦vÐüYÓP+Á>zÙË2!ÅIàt&Úc1üRRB%+~Äêy ­ ÀÔL°
  £¿iâl,	oÛê*`°®Î1å&¾'.°Ù
  ©Á
  ªªBCCJKK©£<-g ;DÐÒÒ1ÉNbY È(
  ¸2=
License: UNKNOWN
 FIXME

Files: ./input/Autogenerated/telecom.xml.gz
Copyright: IúÍa£ç-ÿúc¿B8Pïì.×ºA0P;·®oÞ ,Yßö
  xSò
  °°!2x®ªä×û
License: UNKNOWN
 FIXME

Files: ./doc/release/v0.10.0.md
Copyright: expansion for probability calculations
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_solid256x256.png
Copyright: eËôAO7Üàûäamüo*æâ²eèéÑÅåË}<,_" µJtwk|Éß§ «ãsV­Ò@OÆ[[}<,_" N%W®ÔÀîÝ_¼Ø÷)Èã®(Ò«¿ÄN~öYÆ_¥ÖDþj|üp*ÝÒâûäaý±& Sÿåø
  Þ}÷]ßg
  á|«y=ýÞµ4¦l6+ &Õù> ? 0   À0 F  Ã ` 
  öéñ/füp T¡OLè³gùZ5# UfÃÄ&ZÈ×ùQ|
  úz}²u+ý%  ¤/Ö@w·þwã¾OAÆ/ /RKj
License: UNKNOWN
 FIXME

Files: ./ICLA.md
Copyright: moral and neighboring rights, as
License: UNKNOWN
 FIXME

Files: ./gui/translations/scramgui_fr_FR.ts
Copyright: nement de Base</translation>
  nement maison</translation>
  nement non dÃ©veloppÃ©</translation>
License: UNKNOWN
 FIXME

Files: ./gui/images/scram24x24.png
Copyright: wª,±Ö¿Þ½ã/ òÑm
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: years w/ 2018
License: UNKNOWN
 FIXME

Files: ./gui/images/scram16x16.png
Copyright: ©««©©§§££¨¨¬¬Ã××ÙüüÙÿÿÛÿÿÊîîi||eww®®ÊÝÝÉÝÝÉÜÜÈÛÛÉÜÜÈÛÛÉÜÜÐããÒææØûûÙÿÿÛÿÿÊîîhzzfxxj{{k||qqnpk||n««ÊÝÝÙûûÙÿÿÚÿÿÓöö­­mduu_ppewwfxxduudvv^ooevvi{{ÑõõÚÿÿÚÿÿÙûûÎââ¨¼¼
  ª«¬­® ¯°±²³´µ¶·¸¹º»¼½¾  ¿ÀÁÂÃÄÅÆÇÈÉÊË
License: UNKNOWN
 FIXME

Files: ./gui/images/scram_solid24x24.png
Copyright: ³vDÈx½|?8èKìª×QøáÓ4|v ^.,°µ´Äî+4"U÷å2^Ý-ÐjµîÉ2sþô?ºÝ¤=¶À@uøPñÃÜ¸ÃÜ    IEND®B`
License: UNKNOWN
 FIXME

