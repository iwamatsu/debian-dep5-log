Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./Makefile.am
 ./NEWS
 ./README
 ./config.h.in
 ./configure.in
 ./debian/compat
 ./debian/control
 ./debian/liboobs-1-5.install
 ./debian/liboobs-1-dev.install
 ./debian/patches/01-return-error-code.patch
 ./debian/patches/02-proper-warning.patch
 ./debian/patches/03-migrate-gtk-doc.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/Makefile.am
 ./doc/reference/Makefile.am
 ./doc/reference/html/OobsGroup.html
 ./doc/reference/html/OobsGroupsConfig.html
 ./doc/reference/html/OobsHostsConfig.html
 ./doc/reference/html/OobsIfaceEthernet.html
 ./doc/reference/html/OobsIfaceIRLan.html
 ./doc/reference/html/OobsIfacePPP.html
 ./doc/reference/html/OobsIfacePlip.html
 ./doc/reference/html/OobsIfaceWireless.html
 ./doc/reference/html/OobsIfacesConfig.html
 ./doc/reference/html/OobsList.html
 ./doc/reference/html/OobsNFSConfig.html
 ./doc/reference/html/OobsNTPConfig.html
 ./doc/reference/html/OobsNTPServer.html
 ./doc/reference/html/OobsObject.html
 ./doc/reference/html/OobsSMBConfig.html
 ./doc/reference/html/OobsSelfConfig.html
 ./doc/reference/html/OobsService.html
 ./doc/reference/html/OobsServicesConfig.html
 ./doc/reference/html/OobsSession.html
 ./doc/reference/html/OobsShare.html
 ./doc/reference/html/OobsShareNFS.html
 ./doc/reference/html/OobsShareSMB.html
 ./doc/reference/html/OobsStaticHost.html
 ./doc/reference/html/OobsTimeConfig.html
 ./doc/reference/html/OobsUser.html
 ./doc/reference/html/OobsUsersConfig.html
 ./doc/reference/html/ch01.html
 ./doc/reference/html/ch02.html
 ./doc/reference/html/ch03.html
 ./doc/reference/html/ch04.html
 ./doc/reference/html/ch05.html
 ./doc/reference/html/ch06.html
 ./doc/reference/html/ch07.html
 ./doc/reference/html/index.html
 ./doc/reference/html/index.sgml
 ./doc/reference/html/left.png
 ./doc/reference/html/liboobs-Oobs.html
 ./doc/reference/html/liboobs-OobsError.html
 ./doc/reference/html/liboobs.devhelp
 ./doc/reference/html/liboobs.devhelp2
 ./doc/reference/html/style.css
 ./doc/reference/html/up.png
 ./doc/reference/liboobs-docs.sgml
 ./doc/reference/liboobs-sections.txt
 ./doc/reference/liboobs.types
 ./doc/reference/tmpl/iface-state-monitor.sgml
 ./doc/reference/tmpl/liboobs-unused.sgml
 ./doc/reference/tmpl/oobs-enum-types.sgml
 ./doc/reference/tmpl/oobs-error.sgml
 ./doc/reference/tmpl/oobs-group.sgml
 ./doc/reference/tmpl/oobs-groupsconfig.sgml
 ./doc/reference/tmpl/oobs-hostsconfig.sgml
 ./doc/reference/tmpl/oobs-iface-ethernet.sgml
 ./doc/reference/tmpl/oobs-iface-irlan.sgml
 ./doc/reference/tmpl/oobs-iface-plip.sgml
 ./doc/reference/tmpl/oobs-iface-ppp.sgml
 ./doc/reference/tmpl/oobs-iface-wireless.sgml
 ./doc/reference/tmpl/oobs-iface.sgml
 ./doc/reference/tmpl/oobs-ifacesconfig.sgml
 ./doc/reference/tmpl/oobs-list.sgml
 ./doc/reference/tmpl/oobs-nfsconfig.sgml
 ./doc/reference/tmpl/oobs-ntpconfig.sgml
 ./doc/reference/tmpl/oobs-ntpserver.sgml
 ./doc/reference/tmpl/oobs-object.sgml
 ./doc/reference/tmpl/oobs-result.sgml
 ./doc/reference/tmpl/oobs-selfconfig.sgml
 ./doc/reference/tmpl/oobs-service-private.sgml
 ./doc/reference/tmpl/oobs-service.sgml
 ./doc/reference/tmpl/oobs-servicesconfig-private.sgml
 ./doc/reference/tmpl/oobs-servicesconfig.sgml
 ./doc/reference/tmpl/oobs-session.sgml
 ./doc/reference/tmpl/oobs-share-nfs.sgml
 ./doc/reference/tmpl/oobs-share-smb.sgml
 ./doc/reference/tmpl/oobs-share.sgml
 ./doc/reference/tmpl/oobs-smbconfig.sgml
 ./doc/reference/tmpl/oobs-statichost.sgml
 ./doc/reference/tmpl/oobs-timeconfig.sgml
 ./doc/reference/tmpl/oobs-user.sgml
 ./doc/reference/tmpl/oobs-usersconfig.sgml
 ./doc/reference/tmpl/stamp-oobs-enum-types.sgml
 ./doc/reference/version.xml
 ./liboobs-1.pc
 ./liboobs-1.pc.in
 ./oobs/Makefile.am
 ./oobs/oobs-enum-types.c
 ./oobs/oobs-enum-types.c.in
 ./oobs/oobs-enum-types.h
 ./oobs/oobs-enum-types.h.in
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./oobs/iface-state-monitor-dummy.c
 ./oobs/iface-state-monitor-linux.c
 ./oobs/iface-state-monitor.h
 ./oobs/oobs-defines.h
 ./oobs/oobs-group.c
 ./oobs/oobs-group.h
 ./oobs/oobs-groupsconfig.c
 ./oobs/oobs-groupsconfig.h
 ./oobs/oobs-hostsconfig.c
 ./oobs/oobs-hostsconfig.h
 ./oobs/oobs-iface-ethernet.c
 ./oobs/oobs-iface-ethernet.h
 ./oobs/oobs-iface-irlan.c
 ./oobs/oobs-iface-irlan.h
 ./oobs/oobs-iface-wireless.c
 ./oobs/oobs-iface-wireless.h
 ./oobs/oobs-iface.c
 ./oobs/oobs-iface.h
 ./oobs/oobs-ifacesconfig.c
 ./oobs/oobs-ifacesconfig.h
 ./oobs/oobs-list-private.h
 ./oobs/oobs-list.c
 ./oobs/oobs-list.h
 ./oobs/oobs-nfsconfig.c
 ./oobs/oobs-nfsconfig.h
 ./oobs/oobs-ntpconfig.c
 ./oobs/oobs-ntpconfig.h
 ./oobs/oobs-ntpserver.c
 ./oobs/oobs-ntpserver.h
 ./oobs/oobs-object-private.h
 ./oobs/oobs-object.c
 ./oobs/oobs-object.h
 ./oobs/oobs-result.h
 ./oobs/oobs-selfconfig.c
 ./oobs/oobs-selfconfig.h
 ./oobs/oobs-service.c
 ./oobs/oobs-service.h
 ./oobs/oobs-servicesconfig.c
 ./oobs/oobs-servicesconfig.h
 ./oobs/oobs-session-private.h
 ./oobs/oobs-session.c
 ./oobs/oobs-session.h
 ./oobs/oobs-share-nfs.c
 ./oobs/oobs-share-nfs.h
 ./oobs/oobs-share-smb.c
 ./oobs/oobs-share-smb.h
 ./oobs/oobs-share.c
 ./oobs/oobs-share.h
 ./oobs/oobs-smbconfig.c
 ./oobs/oobs-smbconfig.h
 ./oobs/oobs-statichost.c
 ./oobs/oobs-statichost.h
 ./oobs/oobs-timeconfig.c
 ./oobs/oobs-timeconfig.h
 ./oobs/oobs-user.c
 ./oobs/oobs-user.h
 ./oobs/oobs-usersconfig.c
 ./oobs/oobs-usersconfig.h
 ./oobs/oobs.h
 ./oobs/utils.c
 ./oobs/utils.h
Copyright: 2004, Carlos Garnacho
  2004-2005, Carlos Garnacho
  2005, Carlos Garnacho
  2006, Carlos Garnacho
  2007, Carlos Garnacho
License: LGPL-2+
 FIXME

Files: ./oobs/oobs-error.c
 ./oobs/oobs-error.h
 ./oobs/oobs-group-private.h
 ./oobs/oobs-service-private.h
 ./oobs/oobs-servicesconfig-private.h
 ./oobs/oobs-user-private.h
Copyright: 2009, Milan Bouchet-Valat
  2010, Milan Bouchet-Valat
License: LGPL-2+
 FIXME

Files: ./oobs/oobs-iface-plip.c
 ./oobs/oobs-iface-plip.h
 ./oobs/oobs-iface-ppp.c
 ./oobs/oobs-iface-ppp.h
Copyright: 2004, Carlos Garnacho
  2005, Carlos Garnacho
License: GPL-2+
 FIXME

Files: ./config.guess
 ./config.sub
 ./ltmain.sh
 ./missing
Copyright: 1992-2010, Free Software Foundation, Inc.
  1996-1997, 1999-2000, 2002-2006, 2008-2009, Free Software Foundation, Inc.
  1996-2001, 2003-2008, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./Makefile.in
 ./doc/Makefile.in
 ./doc/reference/Makefile.in
 ./oobs/Makefile.in
Copyright: 1994-2009, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./COPYING
 ./INSTALL
 ./aclocal.m4
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-1996, 1999-2002, 2004-2009, Free Software Foundation, Inc.
  1996-2009, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2010, Free Software
License: FSFUL
 FIXME

Files: ./depcomp
Copyright: 1999-2000, 2003-2007, 2009, Free
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2004, Carlos Garnacho
  2004-2005, Carlos Garnacho <carlosg@gnome.org>
  This package was debianized by Daniel Holbach <daniel.holbach@ubuntu.com> on
License: GPL-2+ and/or LGPL-2+
 FIXME

Files: ./gtk-doc.make
Copyright: 2003, James Henstridge
License: GPL-3+
 FIXME

Files: ./debian/changelog
Copyright: - Change to a machine-readable format (for lintian) and fix errors.
License: UNKNOWN
 FIXME

Files: ./doc/reference/html/home.png
Copyright: FpÇÌÞÏ Yøz3kÃÐE»	üÿÁeâáá!SÕëõñ$½ëv»¿á«0KRó²oÃ0¼ßl6999jússsT*×
License: UNKNOWN
 FIXME

Files: ./doc/reference/html/right.png
Copyright: oc<´ÑA©¤×Xò</zq
License: UNKNOWN
 FIXME

