Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.gitattributes
 ./.travis.yml
 ./debian/README.source
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/libjackson2-datatype-joda-java-doc.doc-base
 ./debian/libjackson2-datatype-joda-java-doc.javadoc
 ./debian/libjackson2-datatype-joda-java.classpath
 ./debian/libjackson2-datatype-joda-java.poms
 ./debian/maven.properties
 ./debian/maven.rules
 ./debian/patches/java9.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./pom.xml
 ./release-notes/CREDITS
 ./release-notes/VERSION
 ./src/main/java/com/fasterxml/jackson/datatype/joda/JodaMapper.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/JodaModule.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/PackageVersion.java.in
 ./src/main/java/com/fasterxml/jackson/datatype/joda/cfg/FormatConfig.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/cfg/JacksonJodaDateFormat.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/cfg/JacksonJodaFormatBase.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/cfg/JacksonJodaPeriodFormat.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/DateMidnightDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/DateTimeDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/DateTimeZoneDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/DurationDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/InstantDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/IntervalDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/JodaDateDeserializerBase.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/JodaDeserializerBase.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/LocalDateDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/LocalDateTimeDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/LocalTimeDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/MonthDayDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/PeriodDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/YearMonthDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/DateTimeKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/DurationKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/JodaKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/LocalDateKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/LocalDateTimeKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/LocalTimeKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/deser/key/PeriodKeyDeserializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/DateMidnightSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/DateTimeSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/DateTimeZoneSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/DurationSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/InstantSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/IntervalSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/JodaDateSerializerBase.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/JodaSerializerBase.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/LocalDateSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/LocalDateTimeSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/LocalTimeSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/MonthDaySerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/PeriodSerializer.java
 ./src/main/java/com/fasterxml/jackson/datatype/joda/ser/YearMonthSerializer.java
 ./src/main/resources/META-INF/services/com.fasterxml.jackson.databind.Module
 ./src/test/java/com/fasterxml/jackson/datatype/joda/DateTimeTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/JodaMapperTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/JodaTestBase.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/MixedListTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/TestVersions.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/TimeZoneTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/depr/DateMidnightTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/DateTimeDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/DateTimeZoneDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/DurationDeserializationTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/InstantDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/IntervalDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/KeyDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/LocalDateDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/LocalDateTimeDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/LocalTimeDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/MonthDayDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/PeriodDeserializationTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/ReadablePeriodDeserializerTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/deser/YearMonthDeserTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/ser/InstantSerializationTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/ser/IntervalSerializationTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/ser/JodaSerializationTest.java
 ./src/test/java/com/fasterxml/jackson/datatype/joda/ser/WriteZoneIdTest.java
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./README.md
 ./src/main/resources/META-INF/LICENSE
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2014, Tim Potter <tpot@hp.com>
  2014-2016, FasterXML, LLC, Seattle, USA <info@fasterxml.com>
License: UNKNOWN
 FIXME

