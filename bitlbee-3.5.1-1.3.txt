Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.gdbinit
 ./.travis.yml
 ./.vimrc
 ./README.md
 ./auth.c
 ./auth.h
 ./auth_ldap.c
 ./auth_pam.c
 ./bitlbee.conf
 ./debian/NEWS
 ./debian/README.Debian
 ./debian/bitlbee-common.docs
 ./debian/bitlbee-common.examples
 ./debian/bitlbee-common.init
 ./debian/bitlbee-common.postinst
 ./debian/bitlbee-common.postrm
 ./debian/bitlbee-common.preinst
 ./debian/bitlbee-common.prerm
 ./debian/bitlbee-dev.postinst
 ./debian/bitlbee-libpurple.lintian-overrides
 ./debian/bitlbee-plugin-skype.docs
 ./debian/bitlbee.lintian-overrides
 ./debian/bitlbee.postinst
 ./debian/bitlbee.prerm
 ./debian/compat
 ./debian/control
 ./debian/patches/bitlbee.conf.diff
 ./debian/rules
 ./debian/skyped.README.Debian
 ./debian/skyped.docs
 ./debian/source/format
 ./debian/source/options
 ./debian/watch
 ./doc/AUTHORS
 ./doc/CHANGES
 ./doc/CREDITS
 ./doc/FAQ
 ./doc/HACKING
 ./doc/INSTALL
 ./doc/Makefile
 ./doc/README
 ./doc/bitlbee.conf.5
 ./doc/bitlbee.xinetd
 ./doc/example_plugin.c
 ./doc/git-bzr-rev-map
 ./doc/uncrustify.cfg
 ./doc/user-guide/Installation.xml
 ./doc/user-guide/Makefile
 ./doc/user-guide/Support.xml
 ./doc/user-guide/Usage.xml
 ./doc/user-guide/commands.xml
 ./doc/user-guide/help.txt
 ./doc/user-guide/help.xml
 ./doc/user-guide/misc.xml
 ./doc/user-guide/quickstart.xml
 ./doc/user-guide/user-guide.xml
 ./init/bitlbee.service.in
 ./init/bitlbee.socket
 ./init/bitlbee@.service.in
 ./lib/md5.c
 ./lib/md5.h
 ./lib/sha1.c
 ./lib/sha1.h
 ./motd.txt
 ./protocols/msn/gw.c
 ./protocols/oscar/AUTHORS
 ./protocols/oscar/admin.c
 ./protocols/oscar/admin.h
 ./protocols/oscar/aim.h
 ./protocols/oscar/aim_internal.h
 ./protocols/oscar/aim_prefixes.h
 ./protocols/oscar/auth.c
 ./protocols/oscar/bos.c
 ./protocols/oscar/bos.h
 ./protocols/oscar/buddylist.c
 ./protocols/oscar/buddylist.h
 ./protocols/oscar/chat.c
 ./protocols/oscar/chat.h
 ./protocols/oscar/chatnav.c
 ./protocols/oscar/chatnav.h
 ./protocols/oscar/conn.c
 ./protocols/oscar/icq.c
 ./protocols/oscar/icq.h
 ./protocols/oscar/im.c
 ./protocols/oscar/im.h
 ./protocols/oscar/info.c
 ./protocols/oscar/info.h
 ./protocols/oscar/misc.c
 ./protocols/oscar/msgcookie.c
 ./protocols/oscar/oscar_util.c
 ./protocols/oscar/rxhandlers.c
 ./protocols/oscar/rxqueue.c
 ./protocols/oscar/search.c
 ./protocols/oscar/search.h
 ./protocols/oscar/service.c
 ./protocols/oscar/snac.c
 ./protocols/oscar/ssi.c
 ./protocols/oscar/ssi.h
 ./protocols/oscar/stats.c
 ./protocols/oscar/tlv.c
 ./protocols/oscar/txqueue.c
 ./protocols/purple/bpurple.h
 ./protocols/skype/HACKING
 ./protocols/skype/Makefile
 ./protocols/skype/NEWS
 ./protocols/skype/README
 ./protocols/skype/asciidoc.conf
 ./protocols/skype/client.sh
 ./protocols/skype/skyped.1
 ./protocols/skype/skyped.cnf
 ./protocols/skype/skyped.conf.dist
 ./protocols/skype/skyped.txt
 ./protocols/skype/t/add-yes-bitlbee.mock
 ./protocols/skype/t/add-yes-skyped.mock
 ./protocols/skype/t/added-no-bitlbee.mock
 ./protocols/skype/t/added-no-skyped.mock
 ./protocols/skype/t/added-yes-bitlbee.mock
 ./protocols/skype/t/added-yes-skyped.mock
 ./protocols/skype/t/away-set-bitlbee.mock
 ./protocols/skype/t/away-set-skyped.mock
 ./protocols/skype/t/call-bitlbee.mock
 ./protocols/skype/t/call-failed-bitlbee.mock
 ./protocols/skype/t/call-failed-skyped.mock
 ./protocols/skype/t/call-skyped.mock
 ./protocols/skype/t/called-no-bitlbee.mock
 ./protocols/skype/t/called-no-skyped.mock
 ./protocols/skype/t/called-yes-bitlbee.mock
 ./protocols/skype/t/called-yes-skyped.mock
 ./protocols/skype/t/ctcp-help-bitlbee.mock
 ./protocols/skype/t/ctcp-help-skyped.mock
 ./protocols/skype/t/filetransfer-bitlbee.mock
 ./protocols/skype/t/filetransfer-skyped.mock
 ./protocols/skype/t/group-add-bitlbee.mock
 ./protocols/skype/t/group-add-skyped.mock
 ./protocols/skype/t/group-read-bitlbee.mock
 ./protocols/skype/t/group-read-skyped.mock
 ./protocols/skype/t/groupchat-invite-bitlbee.mock
 ./protocols/skype/t/groupchat-invite-skyped.mock
 ./protocols/skype/t/groupchat-invited-bitlbee.mock
 ./protocols/skype/t/groupchat-invited-skyped.mock
 ./protocols/skype/t/groupchat-leave-bitlbee.mock
 ./protocols/skype/t/groupchat-leave-skyped.mock
 ./protocols/skype/t/groupchat-msg-bitlbee.mock
 ./protocols/skype/t/groupchat-msg-skyped.mock
 ./protocols/skype/t/groupchat-topic-bitlbee.mock
 ./protocols/skype/t/groupchat-topic-skyped.mock
 ./protocols/skype/t/info-bitlbee.mock
 ./protocols/skype/t/info-skyped.mock
 ./protocols/skype/t/login-bitlbee.mock
 ./protocols/skype/t/login-skyped.mock
 ./protocols/skype/t/msg-bitlbee.mock
 ./protocols/skype/t/msg-skyped.mock
 ./protocols/skype/t/set-mood-text-bitlbee.mock
 ./protocols/skype/t/set-mood-text-skyped.mock
 ./protocols/skype/test.py
 ./sock.h
 ./tests/Makefile
 ./tests/check.c
 ./tests/check_arc.c
 ./tests/check_help.c
 ./tests/check_irc.c
 ./tests/check_jabber_sasl.c
 ./tests/check_jabber_util.c
 ./tests/check_md5.c
 ./tests/check_nick.c
 ./tests/check_set.c
 ./tests/check_user.c
 ./tests/check_util.c
 ./tests/testsuite.h
 ./utils/README
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./bitlbee.c
 ./bitlbee.h
 ./commands.h
 ./conf.c
 ./conf.h
 ./help.c
 ./help.h
 ./ipc.c
 ./ipc.h
 ./irc.c
 ./irc.h
 ./irc_cap.c
 ./irc_channel.c
 ./irc_commands.c
 ./irc_im.c
 ./irc_send.c
 ./irc_user.c
 ./irc_util.c
 ./lib/events.h
 ./lib/events_glib.c
 ./lib/events_libevent.c
 ./lib/http_client.c
 ./lib/http_client.h
 ./lib/ini.c
 ./lib/ini.h
 ./lib/misc.c
 ./lib/misc.h
 ./lib/ssl_client.h
 ./lib/ssl_gnutls.c
 ./lib/ssl_openssl.c
 ./lib/url.c
 ./lib/url.h
 ./log.c
 ./log.h
 ./nick.c
 ./nick.h
 ./otr.c
 ./otr.h
 ./protocols/account.c
 ./protocols/account.h
 ./protocols/bee.c
 ./protocols/bee.h
 ./protocols/bee_chat.c
 ./protocols/bee_user.c
 ./protocols/msn/msn.c
 ./protocols/msn/msn.h
 ./protocols/msn/msn_util.c
 ./protocols/msn/ns.c
 ./protocols/msn/soap.c
 ./protocols/msn/soap.h
 ./protocols/msn/tables.c
 ./protocols/nogaim.c
 ./protocols/nogaim.h
 ./query.c
 ./query.h
 ./root_commands.c
 ./set.c
 ./set.h
 ./storage.h
 ./storage_xml.c
 ./unix.c
Copyright: 2001-2004, Wilmer van der Gaast and others
  2001-2005, Wilmer van der Gaast and others
  2002-2004, Wilmer van der Gaast and others
  2002-2005, Wilmer van der Gaast and others
  2002-2006, Wilmer van der Gaast and others
  2002-2008, Wilmer van der Gaast and others
  2002-2010, Wilmer van der Gaast and others
  2002-2012, Wilmer van der Gaast and others
  2002-2013, Wilmer van der Gaast and others
  2006, Wilmer van der Gaast and others
License: GPL-2+
 FIXME

Files: ./lib/arc.c
 ./lib/arc.h
 ./lib/base64.c
 ./lib/base64.h
 ./lib/json_util.c
 ./lib/json_util.h
 ./lib/oauth.c
 ./lib/oauth.h
 ./lib/oauth2.c
 ./lib/oauth2.h
 ./lib/xmltree.c
 ./lib/xmltree.h
 ./protocols/bee_ft.c
 ./protocols/jabber/conference.c
 ./protocols/jabber/io.c
 ./protocols/jabber/iq.c
 ./protocols/jabber/jabber.c
 ./protocols/jabber/jabber.h
 ./protocols/jabber/jabber_util.c
 ./protocols/jabber/message.c
 ./protocols/jabber/presence.c
 ./protocols/jabber/sasl.c
 ./protocols/purple/ft-direct.c
 ./protocols/purple/ft.c
 ./protocols/purple/purple.c
Copyright: 2006, Wilmer van der Gaast <wilmer@gaast.net>
  2006-2010, Wilmer van der Gaast <wilmer@gaast.net>
  2006-2012, Wilmer van der Gaast <wilmer@gaast.net>
  2006-2013, Wilmer van der Gaast <wilmer@gaast.net>
  2007, Wilmer van der Gaast <wilmer@gaast.net>
  2007-2012, Wilmer van der Gaast <wilmer@gaast.net>
  2009-2010, Wilmer van der Gaast <wilmer@gaast.net>
  2009-2012, Wilmer van der Gaast <wilmer@gaast.net>
  2010, Wilmer van der Gaast <wilmer@gaast.net>
  2010-2013, Wilmer van der Gaast <wilmer@gaast.net>
  2012, Wilmer van der Gaast <wilmer@gaast.net>
License: GPL-2+
 FIXME

Files: ./Makefile
 ./lib/Makefile
 ./protocols/Makefile
 ./protocols/jabber/Makefile
 ./protocols/msn/Makefile
 ./protocols/oscar/Makefile
 ./protocols/purple/Makefile
 ./protocols/twitter/Makefile
Copyright: 2002, Lintux #
  2006, Lintux #
License: UNKNOWN
 FIXME

Files: ./dcc.c
 ./lib/ftutil.c
 ./lib/ftutil.h
 ./protocols/jabber/s5bytestream.c
 ./protocols/jabber/si.c
Copyright: 2007, Uli Meis <a.sporto+bee@gmail.com>
  2008, Uli Meis <a.sporto+bee@gmail.com>
License: GPL-2+
 FIXME

Files: ./protocols/twitter/twitter_http.c
 ./protocols/twitter/twitter_http.h
 ./protocols/twitter/twitter_lib.h
Copyright: 2009, Geert Mulders <g.c.w.m.mulders@gmail.com>
License: LGPL-2.1
 FIXME

Files: ./protocols/twitter/twitter.c
 ./protocols/twitter/twitter.h
 ./protocols/twitter/twitter_lib.c
Copyright: 2009-2010, Geert Mulders <g.c.w.m.mulders@gmail.com>
  2010-2012, Wilmer van der Gaast <wilmer@gaast.net>
  2010-2013, Wilmer van der Gaast <wilmer@gaast.net>
License: LGPL-2.1
 FIXME

Files: ./lib/json.c
 ./lib/json.h
Copyright: 2012-2014, James McLaughlin et al.
License: BSD-2-clause
 FIXME

Files: ./utils/bitlbeed.c
 ./utils/convert_purple.py
Copyright: 2002-2004, Wilmer van der Gaast <wilmer@gaast.net>
  2010, Wilmer van der Gaast <wilmer@gaast.net>
License: GPL
 FIXME

Files: ./doc/bitlbee.8
 ./doc/user-guide/genhelp.py
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./protocols/skype/skype.c
 ./protocols/skype/skyped.py
Copyright: 2007-2013, Miklos Vajna <vmiklos@vmiklos.hu>
License: GPL-2+
 FIXME

Files: ./doc/user-guide/docbook.xsl
 ./doc/user-guide/help.xsl
Copyright: 2004, Jelmer Vernooij
License: UNKNOWN
 FIXME

Files: ./protocols/oscar/oscar.c
Copyright: 1998-1999, Adam Fritzler <afritz@auk.cx>
  1998-1999, Mark Spencer <markster@marko.net>
  2002-2006, Jelmer Vernooij <jelmer@samba.org>
License: GPL-2+
 FIXME

Files: ./lib/ssl_nss.c
Copyright: 2002-2012, Wilmer van der Gaast and others
  2005, Jelmer Vernooij
License: GPL-2+
 FIXME

Files: ./storage.c
Copyright: 2002-2004, Wilmer van der Gaast and others
  2005, Jelmer Vernooij <jelmer@samba.org>
License: GPL-2+
 FIXME

Files: ./protocols/ft.h
Copyright: 2006, Marijn Kruisselbrink and others
License: GPL-2+
 FIXME

Files: ./dcc.h
Copyright: 2006, Marijn Kruisselbrink and others
  2007, Uli Meis <a.sporto+bee@gmail.com>
License: GPL-2+
 FIXME

Files: ./lib/proxy.h
Copyright: 1998-1999, Mark Spencer <markster@marko.net>
License: GPL-2+
 FIXME

Files: ./lib/proxy.c
Copyright: 1998-1999, Mark Spencer <markster@marko.net>
  2002-2004, Wilmer van der Gaast, Jelmer Vernooij
License: GPL-2+
 FIXME

Files: ./protocols/jabber/hipchat.c
Copyright: 2015, Xamarin Inc
License: GPL-2+
 FIXME

Files: ./lib/ns_parse.c
Copyright: 1996, 1999, Internet Software Consortium.
  2004, Internet Systems Consortium, Inc. ("ISC")
License: ISC
 FIXME

Files: ./doc/comic_3.0.png
Copyright: ¬I}"?À!»§/j.j	>_*ñÓ¾z×q£)gc0I$£"ª%¬
  ÄW¤ÁÍùÅ¶ Üöáq®í§·ÿÝy:8Êã@
  ±óÎÔy ú«]ûæïáÍý·¯ÿpæÑ~I©ÕÞÑ<ÔÖM¯©j8mÑ½q3TáÕÿKïiûLæ>8~©+£]K z,wÈ7
  EïµÅñÅ)Dæ¤Q7[Lå×ÿqÝbIGì8;¥Kº§KEöÁl¹jhùJgH§/Æ.;A¡ÛÛ;¯x(´¤z¢DÂ53»ßSÖÏmY¹^	6ýI/õ+L½»Â¡ÖEªt§mäH:,*Ù=rrS9ìW*Ì-ñ'õ*¶z
  7Ó92,23³æf_ÌeÏ³Ï¯(ØTø®X»$«tUÙ
  <ÏÉ0Ü ÏRÎºZe""¢a2¨[unååö¦|J<´
  S×¹A©""yyµø¢¢ÜïTû±îH*
  UÚRÚ°Æ~ü·j³nÀeê%
  XÁ4Gû³ÈW_¨ëlð4iVH|ICåÙ8ÂñdV9;§ijÂ=bT«Bo¨B.¾==jsÐÑMyÈÌ=
  aú·<ZöÜîÍSaµA÷ÎÿJ,>¸ÖÅÄ·ípÀkDÛ6Z:+
  eS]`OÌTìýß;
  jU
  nycL³u>a
  pÙ~ö+Þ5i!º<#Ò>/!P Ó/ßâÑãLNöô}#Üpòx¬Y
  u~!ælÈ¼2Z]²6/Aíål¬`ÇYñ*[#Àªb'ì#ÿRSDd3Ö3¥Z£¹zÈfØ?tê¦$)ñFìæ¥¹7o$Þ[-ÞÃ+·XfæYø1Êºç,
  w¿Ì|c{+"ò2/døñÑ<]è¹z|
  [}/èHO©nn1"²ZÏÈÞ²Á5e;Oºyç¨(êÖ­Ï|^nÕÕêw<Ø:Ñ3»]÷êWsGUÁ½É<z¢j³`æí×ü´@÷æ{ø WôûmEÂÑ.{]µ.©¿
  53[!ÆÜ{IWp2s8
  éØ=Õ¢µóÚgEDb÷þgpKò¦°±Nõ½õ'¸
  ,?Ü"M?©ª¼WêÏ¡¶ÂÌ]«°W~Ê·U3ÍKì§Vft·+ÐÀÉÌô$M®¾?_u>Àµ.Ç9+Jèêmk´ôxcòÈä×ÌtsrªãE¶fºÇÌÜé¹ß'ÛL}a¾I³?&ÏM]x/áÀS·Ù·ïIý´
  lÍ4OË
  ¤ùAëö$T]+=A½WúG
  ¬Y*wâUìR~íÒ»DäjoÍQS~0+­¯YJúB
  »6% ø}Û«Øª)É÷çt¢sóQ
  ¿±Ïd.ªZÏ~Í¢¦b]T9t§;T©N6i³Jv¨~yò>­»ñÒï±l¯	HÃßð»=aå
  ÀÝÃ
  Á
  Ç""¦²¬x	&ÈÓt_¡Î.³Èqwn<$¸é*¶Hc¥£9jødø{3-?Ë&üò_ùê
  ÖÝEû!Ü*÷®§|õUçgÛwTgëÒáÎêÚ=uñDÛÞû÷x*}ïáßÿ$°ÓÊyÊ´Gpµo$ðÇèBÏäîûé¯í
  Û¢IËÀÛçm½8<îN6Äé9 î*ÞáÃÐ3Ïö*T	ÇA×oy,¹Sa_YÊø®Mö¶5µÐ%ý¢:o½½î8þá÷8ö+SUêð[-¯.k8¡ÇqE?P¾;#ïP
  æÃ1¨ãe¨7xp~A¯¢ýMÄßoÈ·P#ìÖE­Êày
  êb,_ùPjc­ù,zá§ ðj³aÿë2Çv.4Ådþæì{!%Ð58µ6+D¼ã£¯é£bõÞ>>Y×S³R´®éME¾m@QsqfÞOù@¥ñï­½pKÐ¥Î$¼õnäøQic;ßõ«²Çì©Áò¿ÚìçìVª|@¯àº÷y#ÜßQÌ=o~¾ø- þºaÑ¾ÿtqÄåÇÐß
  ôÜÿàþ÷?¸ÿÁýn{ãPìá}Ñwõhbÿ:¸Úÿ¦u=kË¿|ôÍCõÿíp× *jmõþ DnOºS®}Û^bûûãºÇà¾* s|Ù¢JßÖ¡¿ÝÇtÓ è¯&Ed÷*óPðêøä§3ðØ	Ï4 äJöðqª|õÃ­yâ%Û§²M½yl§H"")o½p,¿°C5à4UD¶6nÐàñfÝÃ`°{úBp£`¸Èæjð½Ì
  ôÎÒàKBÚEÁ÷iªZJhµ;ÝOÊµø³êÁÉRAì1Ð ðO®¥K^è#ôYtNqEËD
  ü×^Bó0¢ ZD6¼éuåÃ8ÇÙÞ`@Ó'³Àæ)¥Ë}f9ðÎóSD?zê©E®8¸90KDäTàvk­÷³q9/"úYÃ_úüý=Õ>¬ëÔB=ÜüÇú8÷F½[Û®¤ÿx½JÇr
  ÿþù.çp;åúsÆ
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  1998-1999, Adam Fritzler <afritz@auk.cx>
License: UNKNOWN
 FIXME

Files: ./protocols/oscar/COPYING
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: UNKNOWN
 FIXME

Files: ./utils/bitlbee-ctl.pl
Copyright: 2006, Jelmer Vernooij <jelmer@samba.org>
License: UNKNOWN
 FIXME

Files: ./configure
Copyright: 2002, Lucumo #
  2004, Lintux #
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: provided by Jochen Sprickerhof
License: UNKNOWN
 FIXME

