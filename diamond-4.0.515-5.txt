Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./MANIFEST.in
 ./PKG-INFO
 ./bin/diamond
 ./bin/diamond-setup
 ./bin/init.d/diamond
 ./conf/diamond.conf.example
 ./conf/diamond.conf.example.windows
 ./conf/vagrant/collectors/RedisCollector.conf
 ./conf/vagrant/diamond.conf
 ./debian/compat
 ./debian/control
 ./debian/diamond.default
 ./debian/diamond.init
 ./debian/diamond.lintian-overrides
 ./debian/diamond.postinst
 ./debian/diamond.postrm
 ./debian/diamond.service
 ./debian/diamond.upstart
 ./debian/dirs
 ./debian/gbp.conf
 ./debian/postinst
 ./debian/postrm
 ./debian/preinst
 ./debian/prerm
 ./debian/pyversions
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./rpm/systemd/diamond.service
 ./rpm/upstart/diamond.conf
 ./setup.cfg
 ./setup.py
 ./src/collectors/amavis/amavis.py
 ./src/collectors/apcupsd/apcupsd.py
 ./src/collectors/aurora/aurora.py
 ./src/collectors/beanstalkd/beanstalkd.py
 ./src/collectors/bind/bind.py
 ./src/collectors/celerymon/celerymon.py
 ./src/collectors/ceph/ceph.py
 ./src/collectors/cephstats/cephstats.py
 ./src/collectors/chronyd/chronyd.py
 ./src/collectors/conntrack/conntrack.py
 ./src/collectors/cpu/cpu.py
 ./src/collectors/cpuacct_cgroup/cpuacct_cgroup.py
 ./src/collectors/darner/darner.py
 ./src/collectors/diskspace/diskspace.py
 ./src/collectors/disktemp/disktemp.py
 ./src/collectors/diskusage/diskusage.py
 ./src/collectors/docker_collector/docker_collector.py
 ./src/collectors/drbd/drbd.py
 ./src/collectors/dropwizard/dropwizard.py
 ./src/collectors/dseopscenter/dseopscenter.py
 ./src/collectors/elasticsearch/elasticsearch.py
 ./src/collectors/elb/elb.py
 ./src/collectors/endecadgraph/endecadgraph.py
 ./src/collectors/entropy/entropy.py
 ./src/collectors/eventstoreprojections/eventstoreprojections.py
 ./src/collectors/eventstoreprojections/tests/fixtures/projections
 ./src/collectors/example/example.py
 ./src/collectors/exim/exim.py
 ./src/collectors/files/files.py
 ./src/collectors/filestat/filestat.py
 ./src/collectors/fluentd/fluentd.py
 ./src/collectors/flume/flume.py
 ./src/collectors/gridengine/gridengine.py
 ./src/collectors/hadoop/hadoop.py
 ./src/collectors/haproxy/haproxy.py
 ./src/collectors/hbase/hbase.py
 ./src/collectors/http/http.py
 ./src/collectors/httpd/httpd.py
 ./src/collectors/httpjson/httpjson.py
 ./src/collectors/icinga_stats/icinga_stats.py
 ./src/collectors/interrupt/interrupt.py
 ./src/collectors/interrupt/soft.py
 ./src/collectors/iodrivesnmp/iodrivesnmp.py
 ./src/collectors/ip/ip.py
 ./src/collectors/ipmisensor/ipmisensor.py
 ./src/collectors/ipvs/ipvs.py
 ./src/collectors/jbossapi/jbossapi.py
 ./src/collectors/jcollectd/jcollectd.py
 ./src/collectors/jolokia/cassandra_jolokia.py
 ./src/collectors/jolokia/jolokia.py
 ./src/collectors/kafkastat/kafkastat.py
 ./src/collectors/ksm/ksm.py
 ./src/collectors/kvm/kvm.py
 ./src/collectors/libvirtkvm/libvirtkvm.py
 ./src/collectors/lmsensors/lmsensors.py
 ./src/collectors/loadavg/loadavg.py
 ./src/collectors/memcached/memcached.py
 ./src/collectors/memory/memory.py
 ./src/collectors/memory_cgroup/memory_cgroup.py
 ./src/collectors/memory_docker/memory_docker.py
 ./src/collectors/memory_lxc/memory_lxc.py
 ./src/collectors/mesos/mesos.py
 ./src/collectors/mesos_cgroup/mesos_cgroup.py
 ./src/collectors/mongodb/mongodb.py
 ./src/collectors/monit/monit.py
 ./src/collectors/mountstats/mountstats.py
 ./src/collectors/mysqlstat/mysql55.py
 ./src/collectors/mysqlstat/mysqlstat.py
 ./src/collectors/nagios/nagios.py
 ./src/collectors/nagiosperfdata/nagiosperfdata.py
 ./src/collectors/netapp/netapp.py
 ./src/collectors/netapp/netappDisk.py
 ./src/collectors/netapp/netapp_inode.py
 ./src/collectors/netscalersnmp/netscalersnmp.py
 ./src/collectors/network/network.py
 ./src/collectors/nfacct/nfacct.py
 ./src/collectors/nfs/nfs.py
 ./src/collectors/nfsd/nfsd.py
 ./src/collectors/nginx/nginx.py
 ./src/collectors/ntp/ntp.py
 ./src/collectors/ntpd/ntpd.py
 ./src/collectors/numa/numa.py
 ./src/collectors/onewire/onewire.py
 ./src/collectors/openldap/openldap.py
 ./src/collectors/openstackswift/openstackswift.py
 ./src/collectors/openstackswiftrecon/openstackswiftrecon.py
 ./src/collectors/openvpn/openvpn.py
 ./src/collectors/ossec/ossec.py
 ./src/collectors/passenger_stats/passenger_stats.py
 ./src/collectors/pgbouncer/pgbouncer.py
 ./src/collectors/pgq/pgq.py
 ./src/collectors/phpfpm/phpfpm.py
 ./src/collectors/ping/ping.py
 ./src/collectors/portstat/portstat.py
 ./src/collectors/postfix/postfix.py
 ./src/collectors/postgres/postgres.py
 ./src/collectors/postqueue/postqueue.py
 ./src/collectors/powerdns/powerdns.py
 ./src/collectors/proc/proc.py
 ./src/collectors/processresources/processresources.py
 ./src/collectors/puppetagent/puppetagent.py
 ./src/collectors/puppetdashboard/puppetdashboard.py
 ./src/collectors/puppetdb/puppetdb.py
 ./src/collectors/rabbitmq/rabbitmq.py
 ./src/collectors/redisstat/redisstat.py
 ./src/collectors/resqueweb/resqueweb.py
 ./src/collectors/s3/s3.py
 ./src/collectors/scribe/scribe.py
 ./src/collectors/servertechpdu/servertechpdu.py
 ./src/collectors/sidekiqweb/sidekiqweb.py
 ./src/collectors/slabinfo/slabinfo.py
 ./src/collectors/slony/slony.py
 ./src/collectors/smart/smart.py
 ./src/collectors/snmp/snmp.py
 ./src/collectors/snmpinterface/snmpinterface.py
 ./src/collectors/snmpraw/snmpraw.py
 ./src/collectors/sockstat/sockstat.py
 ./src/collectors/solr/solr.py
 ./src/collectors/sqs/sqs.py
 ./src/collectors/squid/squid.py
 ./src/collectors/supervisord/supervisord.py
 ./src/collectors/tcp/tcp.py
 ./src/collectors/tokumx/tokumx.py
 ./src/collectors/twemproxy/twemproxy.py
 ./src/collectors/udp/udp.py
 ./src/collectors/unbound/unbound.py
 ./src/collectors/ups/ups.py
 ./src/collectors/uptime/uptime.py
 ./src/collectors/users/users.py
 ./src/collectors/userscripts/userscripts.py
 ./src/collectors/varnish/varnish.py
 ./src/collectors/vmsdoms/vmsdoms.py
 ./src/collectors/vmsfs/vmsfs.py
 ./src/collectors/vmstat/vmstat.py
 ./src/collectors/websitemonitor/websitemonitor.py
 ./src/collectors/xen_collector/xen_collector.py
 ./src/collectors/xfs/xfs.py
 ./src/collectors/zookeeper/zookeeper.py
 ./src/diamond/__init__.py
 ./src/diamond/collector.py
 ./src/diamond/convertor.py
 ./src/diamond/error.py
 ./src/diamond/handler/Handler.py
 ./src/diamond/handler/__init__.py
 ./src/diamond/handler/archive.py
 ./src/diamond/handler/cloudwatch.py
 ./src/diamond/handler/datadog.py
 ./src/diamond/handler/g_metric.py
 ./src/diamond/handler/graphite.py
 ./src/diamond/handler/graphitepickle.py
 ./src/diamond/handler/hostedgraphite.py
 ./src/diamond/handler/httpHandler.py
 ./src/diamond/handler/influxdbHandler.py
 ./src/diamond/handler/libratohandler.py
 ./src/diamond/handler/logentries_diamond.py
 ./src/diamond/handler/mqtt.py
 ./src/diamond/handler/multigraphite.py
 ./src/diamond/handler/multigraphitepickle.py
 ./src/diamond/handler/mysql.py
 ./src/diamond/handler/null.py
 ./src/diamond/handler/queue.py
 ./src/diamond/handler/rabbitmq_pubsub.py
 ./src/diamond/handler/rabbitmq_topic.py
 ./src/diamond/handler/riemann.py
 ./src/diamond/handler/rrdtool.py
 ./src/diamond/handler/sentry.py
 ./src/diamond/handler/signalfx.py
 ./src/diamond/handler/stats_d.py
 ./src/diamond/handler/statsite.py
 ./src/diamond/handler/tsdb.py
 ./src/diamond/handler/zmq_pubsub.py
 ./src/diamond/metric.py
 ./src/diamond/server.py
 ./src/diamond/util.py
 ./src/diamond/utils/classes.py
 ./src/diamond/utils/config.py
 ./src/diamond/utils/log.py
 ./src/diamond/utils/scheduler.py
 ./src/diamond/utils/signals.py
 ./src/diamond/version.py
 ./version.txt
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: 2010-2012, Brightcove Inc. http:www.brightcove.com/
  2011-2012, Ivan Pouzyrevsky
  2011-2012, Rob Smith http:www.kormoc.com
  2012, Dennis Kaarsemaker <dennis@kaarsemaker.net>
  2012, Wijnand Modderman-Lenstra https:maze.io/
License: Expat
 FIXME

Files: ./src/diamond/gmetric.py
Copyright: 2007-2008, Nick Galbreath
License: Expat
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2011-2012, Ivan Pouzyrevsky
  2011-2012, Rob Smith http:www.kormoc.com
  2012, Dennis Kaarsemaker <dennis@kaarsemaker.net>
  2012, Wijnand Modderman-Lenstra https:maze.io/
  Copyright (C) 2010-2012 by Brightcove Inc. http:www.brightcove.com/
License: GPL
 FIXME

Files: ./debian/changelog
Copyright: - extend packaging copyright years
License: UNKNOWN
 FIXME

Files: ./src/collectors/jcollectd/collectd_network.py
Copyright: 2009, Adrian Perez <aperez@igalia.com>
License: UNKNOWN
 FIXME

