Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./src/calibrate/calibrate.cpp
 ./src/calibrate/calibrate.h
 ./src/cpu/abstract_cpu.cpp
 ./src/cpu/cpu.cpp
 ./src/cpu/cpu.h
 ./src/cpu/cpu_core.cpp
 ./src/cpu/cpu_linux.cpp
 ./src/cpu/cpu_package.cpp
 ./src/cpu/cpu_rapl_device.cpp
 ./src/cpu/cpu_rapl_device.h
 ./src/cpu/cpudevice.cpp
 ./src/cpu/cpudevice.h
 ./src/cpu/dram_rapl_device.cpp
 ./src/cpu/dram_rapl_device.h
 ./src/cpu/intel_cpus.cpp
 ./src/cpu/intel_cpus.h
 ./src/cpu/intel_gpu.cpp
 ./src/devices/ahci.cpp
 ./src/devices/ahci.h
 ./src/devices/alsa.cpp
 ./src/devices/alsa.h
 ./src/devices/backlight.cpp
 ./src/devices/backlight.h
 ./src/devices/device.cpp
 ./src/devices/device.h
 ./src/devices/gpu_rapl_device.cpp
 ./src/devices/gpu_rapl_device.h
 ./src/devices/i915-gpu.cpp
 ./src/devices/i915-gpu.h
 ./src/devices/network.cpp
 ./src/devices/network.h
 ./src/devices/rfkill.cpp
 ./src/devices/rfkill.h
 ./src/devices/runtime_pm.cpp
 ./src/devices/runtime_pm.h
 ./src/devices/thinkpad-fan.cpp
 ./src/devices/thinkpad-fan.h
 ./src/devices/thinkpad-light.cpp
 ./src/devices/thinkpad-light.h
 ./src/devices/usb.cpp
 ./src/devices/usb.h
 ./src/devlist.cpp
 ./src/display.cpp
 ./src/display.h
 ./src/lib.cpp
 ./src/lib.h
 ./src/main.cpp
 ./src/measurement/acpi.cpp
 ./src/measurement/acpi.h
 ./src/measurement/extech.h
 ./src/measurement/measurement.cpp
 ./src/measurement/measurement.h
 ./src/parameters/learn.cpp
 ./src/parameters/parameters.cpp
 ./src/parameters/parameters.h
 ./src/parameters/persistent.cpp
 ./src/perf/perf.cpp
 ./src/perf/perf.h
 ./src/perf/perf_bundle.cpp
 ./src/perf/perf_bundle.h
 ./src/perf/perf_event.h
 ./src/process/do_process.cpp
 ./src/process/interrupt.cpp
 ./src/process/interrupt.h
 ./src/process/powerconsumer.cpp
 ./src/process/powerconsumer.h
 ./src/process/process.cpp
 ./src/process/process.h
 ./src/process/processdevice.cpp
 ./src/process/processdevice.h
 ./src/process/timer.cpp
 ./src/process/timer.h
 ./src/process/work.cpp
 ./src/process/work.h
 ./src/report/report.cpp
 ./src/report/report.h
 ./src/tuning/bluetooth.cpp
 ./src/tuning/bluetooth.h
 ./src/tuning/ethernet.cpp
 ./src/tuning/ethernet.h
 ./src/tuning/runtime.cpp
 ./src/tuning/runtime.h
 ./src/tuning/tunable.cpp
 ./src/tuning/tunable.h
 ./src/tuning/tuning.cpp
 ./src/tuning/tuning.h
 ./src/tuning/tuningi2c.cpp
 ./src/tuning/tuningi2c.h
 ./src/tuning/tuningsysfs.cpp
 ./src/tuning/tuningsysfs.h
 ./src/tuning/tuningusb.cpp
 ./src/tuning/tuningusb.h
 ./src/tuning/wifi.cpp
 ./src/tuning/wifi.h
Copyright: 2010, Intel Corporation
  2011, Intel Corporation
  2012, Intel Corporation
  2015, Intel Corporation
License: GPL-2
 FIXME

Files: ./ABOUT-NLS
 ./Android.mk
 ./Makefile.am
 ./README
 ./TODO
 ./autogen.sh
 ./config.h.in
 ./configure.ac
 ./debian/compat
 ./debian/control
 ./debian/postrm
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/Makefile.am
 ./doc/powertop.8
 ./m4/gcc_fortify_source_cc.m4
 ./po/LINGUAS
 ./po/POTFILES.in
 ./po/Rules-quot
 ./po/boldquot.sed
 ./po/cs_CZ.gmo
 ./po/de_DE.gmo
 ./po/en@boldquot.header
 ./po/en@quot.header
 ./po/en_GB.gmo
 ./po/insert-header.sin
 ./po/nl_NL.gmo
 ./po/quot.sed
 ./po/remove-potcdate.sin
 ./po/stamp-po
 ./src/Makefile.am
 ./src/css.h
 ./src/devlist.h
 ./src/powertop.css
 ./src/report/report-data-html.cpp
 ./src/report/report-data-html.h
 ./traceevent/Makefile.am
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./config.rpath
 ./doc/Makefile.in
 ./m4/gettext.m4
 ./m4/iconv.m4
 ./m4/intlmacosx.m4
 ./m4/lib-ld.m4
 ./m4/lib-link.m4
 ./m4/lib-prefix.m4
 ./m4/ltsugar.m4
 ./m4/ltversion.m4
 ./m4/lt~obsolete.m4
 ./m4/po.m4
 ./m4/progtest.m4
 ./src/Makefile.in
 ./traceevent/Makefile.in
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2013, Free Software Foundation, Inc.
  1995-2010, Free Software Foundation, Inc.
  1996-2003, 2005, 2008-2010, Free Software Foundation, Inc.
  1996-2003, 2009-2010, Free Software Foundation, Inc.
  1996-2010, Free Software Foundation, Inc.
  2000-2002, 2007-2010, Free Software Foundation, Inc.
  2001-2005, 2008-2010, Free Software Foundation, Inc.
  2001-2010, Free Software Foundation, Inc.
  2004, Free Software Foundation, Inc.
  2004-2005, 2007, 2009, Free Software Foundation, Inc.
  2004-2005, 2007-2008, Free Software Foundation, Inc.
  2004-2010, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./po/ca.po
 ./po/cs_CZ.po
 ./po/de_DE.po
 ./po/en_GB.po
 ./po/en_US.po
 ./po/es_ES.po
 ./po/id_ID.po
 ./po/nl_NL.po
 ./po/powertop.pot
 ./po/zh_TW.po
Copyright: YEAR Intel Corporation
License: UNKNOWN
 FIXME

Files: ./src/report/report-formatter-base.cpp
 ./src/report/report-formatter-base.h
 ./src/report/report-formatter-csv.cpp
 ./src/report/report-formatter-csv.h
 ./src/report/report-formatter-html.cpp
 ./src/report/report-formatter-html.h
 ./src/report/report-formatter.h
 ./src/report/report-maker.cpp
 ./src/report/report-maker.h
Copyright: 2012, Samsung Electronics Co., Ltd.
License: GPL-2
 FIXME

Files: ./aclocal.m4
 ./ar-lib
 ./compile
 ./depcomp
 ./ltmain.sh
 ./missing
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
  1996-2013, Free Software Foundation, Inc.
  1999-2013, Free Software Foundation, Inc.
  2010-2013, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./traceevent/event-parse.c
 ./traceevent/event-parse.h
 ./traceevent/event-utils.h
 ./traceevent/parse-filter.c
 ./traceevent/parse-utils.c
 ./traceevent/trace-seq.c
Copyright: 2009, Red Hat Inc, Steven Rostedt <srostedt@redhat.com>
  2009-2010, Red Hat Inc, Steven Rostedt <srostedt@redhat.com>
  2010, Red Hat Inc, Steven Rostedt <srostedt@redhat.com>
License: LGPL-2.1
 FIXME

Files: ./src/measurement/sysfs.cpp
 ./src/measurement/sysfs.h
Copyright: 2011, Anssi Hannula
License: GPL-2
 FIXME

Files: ./src/cpu/rapl/rapl_interface.cpp
 ./src/cpu/rapl/rapl_interface.h
Copyright: 2012, Intel Corporation.
License: GPL-2
 FIXME

Files: ./src/devices/devfreq.cpp
 ./src/devices/devfreq.h
Copyright: 2012, Linaro
License: GPL-2
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2013, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./src/tuning/iw.c
 ./src/tuning/iw.h
Copyright: 2007-2008, Johannes Berg <johannes@sipsolutions.net>
License: ISC
 FIXME

Files: ./po/id_ID.gmo
 ./po/zh_TW.gmo
Copyright:   	   Î  =   Ø  /        F     f     w          °  
License: UNKNOWN
 FIXME

Files: ./m4/ltoptions.m4
 ./m4/nls.m4
Copyright: 1995-2003, 2005-2006, 2008-2010, Free Software Foundation,
  2004-2005, 2007-2009, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./m4/ax_cxx_compile_stdcxx_11.m4
Copyright: 2008, Benjamin Kosnik <bkoz@redhat.com>
  2012, Zack Weinberg <zackw@panix.com>
  2013, Roy Stogner <roystgnr@ices.utexas.edu>
  2014, Alexey Sokolov <sokolov@google.com>
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  © 1995-1997, 2000-2007, 2009-2010 Ulrich Drepper <drepper@gnu.ai.mit.edu>
  © 2010 Intel Corporation
License: GPL
 FIXME

Files: ./m4/ax_pthread.m4
Copyright: 2008, Steven G. Johnson <stevenj@alum.mit.edu>
  2011, Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL
 FIXME

Files: ./po/Makefile.in.in
Copyright: 1995-1997, 2000-2007, 2009-2010, Ulrich Drepper <drepper@gnu.ai.mit.edu>
License: GPL
 FIXME

Files: ./src/csstoh.sh
Copyright: NONE
License: GPL-2
 FIXME

Files: ./src/measurement/extech.cpp
Copyright: 2005, Patrick Mochel
  2006, Intel Corporation
  2011, Intel Corporation
License: GPL-2
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: GPL-2+
 FIXME

Files: ./src/tuning/nl80211.h
Copyright: 2006-2010, Johannes Berg <johannes@sipsolutions.net>
  2008, Colin McCabe <colin@cozybit.com>
  2008, Jouni Malinen <jouni.malinen@atheros.com>
  2008, Luis Carlos Cobo <luisca@cozybit.com>
  2008, Michael Buesch <mb@bu3sch.de>
  2008, Michael Wu <flamingice@sourmilk.net>
  2008-2009, Luis R. Rodriguez <lrodriguez@atheros.com>
License: ISC
 FIXME

Files: ./po/ca.gmo
Copyright:   $   Ã	  	   è	  =   ò	     0
License: UNKNOWN
 FIXME

Files: ./po/en_US.gmo
Copyright:      Â     Ù     ë  %     *   ,     W     q  
License: UNKNOWN
 FIXME

Files: ./po/hu_HU.gmo
Copyright:   !   ½     ß  .   ö     %  Ë  4  *         +  L   4       E        Ì  	   Þ     è     ù  7     E   G       #     	   ·     Á     Ú     ì  #   
License: UNKNOWN
 FIXME

Files: ./po/es_ES.gmo
Copyright:   ,   Ð  ;   ý  
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Add missing copyright notices.
License: UNKNOWN
 FIXME

Files: ./po/hu_HU.po
Copyright: YEAR Intel Corporation
  meth <nemeth.marton@gmail.com>, 2012
  meth <nm127@freemail.hu>, 2012
License: UNKNOWN
 FIXME

Files: ./po/Makevars
Copyright: for their translations to this person
License: UNKNOWN
 FIXME

