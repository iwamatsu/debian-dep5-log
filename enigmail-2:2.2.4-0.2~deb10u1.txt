Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.eslintrc.js
 ./.gitattributes
 ./.gitlab-ci.yml
 ./.jsbeautifyrc
 ./.travis.yml
 ./COMPILING
 ./config/autoconf.mk.in
 ./configure.ac
 ./debian/changelog
 ./debian/clean
 ./debian/control
 ./debian/enigmail.links
 ./debian/gbp.conf
 ./debian/patches/0002-Avoid-auto-download-of-pEpEngine-Closes-891882.patch
 ./debian/patches/0006-enable-the-use-of-extra-file-descriptors-and-test-th.patch
 ./debian/patches/0007-add-test-to-do-symmetric-encryption-decryption-with-.patch
 ./debian/patches/0012-Fix-parallel-build.patch
 ./debian/patches/0013-fix-eslint-errors.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./include/postbox.h
 ./include/tbird.h
 ./ipc/modules/.eslintrc.js
 ./ipc/readme.os2
 ./ipc/tests/.eslintrc.js
 ./lang/ar/am-enigprefs.properties
 ./lang/bg/am-enigprefs.properties
 ./lang/bg/enigmail.dtd
 ./lang/bg/enigmail.properties
 ./lang/ca/am-enigprefs.properties
 ./lang/cs/am-enigprefs.properties
 ./lang/current-languages.txt
 ./lang/da/am-enigprefs.properties
 ./lang/da/enigmail.properties
 ./lang/de/am-enigprefs.properties
 ./lang/de/enigmail.dtd
 ./lang/de/enigmail.properties
 ./lang/el/am-enigprefs.properties
 ./lang/el/enigmail.dtd
 ./lang/el/enigmail.properties
 ./lang/es-AR/enigmail.dtd
 ./lang/es-ES/am-enigprefs.properties
 ./lang/es-ES/enigmail.properties
 ./lang/eu/am-enigprefs.properties
 ./lang/eu/contents.rdf
 ./lang/eu/enigmail.dtd
 ./lang/eu/enigmail.properties
 ./lang/fa/am-enigprefs.properties
 ./lang/fi/am-enigprefs.properties
 ./lang/fi/enigmail.dtd
 ./lang/fi/enigmail.properties
 ./lang/fr/am-enigprefs.properties
 ./lang/gd/am-enigprefs.properties
 ./lang/gd/enigmail.dtd
 ./lang/gd/enigmail.properties
 ./lang/gl/am-enigprefs.properties
 ./lang/hr/am-enigprefs.properties
 ./lang/hr/enigmail.dtd
 ./lang/hr/enigmail.properties
 ./lang/hu/am-enigprefs.properties
 ./lang/hy/am-enigprefs.properties
 ./lang/it/am-enigprefs.properties
 ./lang/it/enigmail.dtd
 ./lang/it/enigmail.properties
 ./lang/ja/am-enigprefs.properties
 ./lang/ko/am-enigprefs.properties
 ./lang/lt/am-enigprefs.properties
 ./lang/lt/enigmail.dtd
 ./lang/lt/enigmail.properties
 ./lang/nb/am-enigprefs.properties
 ./lang/nb/enigmail.dtd
 ./lang/nb/enigmail.properties
 ./lang/nl/am-enigprefs.properties
 ./lang/nl/enigmail.properties
 ./lang/pl/am-enigprefs.properties
 ./lang/pl/enigmail.dtd
 ./lang/pl/enigmail.properties
 ./lang/pt-BR/am-enigprefs.properties
 ./lang/pt-BR/enigmail.dtd
 ./lang/pt-PT/am-enigprefs.properties
 ./lang/pt-PT/enigmail.dtd
 ./lang/ro/am-enigprefs.properties
 ./lang/ro/enigmail.dtd
 ./lang/ro/enigmail.properties
 ./lang/ru/am-enigprefs.properties
 ./lang/ru/enigmail.dtd
 ./lang/ru/enigmail.properties
 ./lang/sk/am-enigprefs.properties
 ./lang/sl/am-enigprefs.properties
 ./lang/sl/enigmail.dtd
 ./lang/sl/enigmail.properties
 ./lang/sq/am-enigprefs.properties
 ./lang/sq/enigmail.dtd
 ./lang/sq/enigmail.properties
 ./lang/sv/am-enigprefs.properties
 ./lang/sv/enigmail.dtd
 ./lang/sv/enigmail.properties
 ./lang/tr/am-enigprefs.properties
 ./lang/tr/enigmail.dtd
 ./lang/tr/enigmail.properties
 ./lang/vi/am-enigprefs.properties
 ./lang/vi/enigmail.dtd
 ./lang/zh-CN/am-enigprefs.properties
 ./lang/zh-CN/enigmail.dtd
 ./lang/zh-TW/am-enigprefs.properties
 ./package/.gitIgnore
 ./package/.gitattributes
 ./package/chrome.manifest
 ./package/cryptoAPI/README.txt
 ./package/install.rdf
 ./package/manifest.json
 ./package/prefs/.eslintrc.js
 ./package/schema.json
 ./package/webextension.js
 ./public/thunderbird-enigmail.metainfo.xml
 ./ui/locale/en-US/enigmail.dtd
 ./ui/locale/en-US/enigmail.properties
 ./ui/skin/images/check0.png
 ./ui/skin/images/check1.png
 ./ui/skin/images/check2.png
 ./ui/skin/images/ok-sign.svg
 ./util/checkFiles.py
 ./util/fixlang.pl
 ./util/plpp.pl
 ./util/prepPostbox
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./LICENSE
 ./Makefile
 ./ipc/Makefile
 ./ipc/modules/Makefile
 ./ipc/modules/enigmailprocess_common.jsm
 ./ipc/modules/enigmailprocess_main.jsm
 ./ipc/modules/enigmailprocess_shared.js
 ./ipc/modules/enigmailprocess_shared_unix.js
 ./ipc/modules/enigmailprocess_shared_win.js
 ./ipc/modules/enigmailprocess_unix.jsm
 ./ipc/modules/enigmailprocess_win.jsm
 ./ipc/modules/enigmailprocess_worker_common.js
 ./ipc/modules/enigmailprocess_worker_unix.js
 ./ipc/modules/enigmailprocess_worker_win.js
 ./ipc/modules/subprocess.jsm
 ./ipc/tests/IpcCat.pl
 ./ipc/tests/Makefile
 ./ipc/tests/main.js
 ./ipc/tests/subprocess-test.js
 ./lang/Makefile
 ./package/Makefile
 ./package/app.jsm
 ./package/armor.jsm
 ./package/clipboard.jsm
 ./package/compat.jsm
 ./package/constants.jsm
 ./package/core.jsm
 ./package/cryptoAPI.jsm
 ./package/cryptoAPI/Makefile
 ./package/cryptoAPI/gnupg-agent.jsm
 ./package/cryptoAPI/gnupg-core.jsm
 ./package/cryptoAPI/gnupg-key.jsm
 ./package/cryptoAPI/gnupg-keylist.jsm
 ./package/cryptoAPI/gnupg.js
 ./package/cryptoAPI/interface.js
 ./package/data.jsm
 ./package/dialog.jsm
 ./package/enigmailOverlays.jsm
 ./package/errorHandling.jsm
 ./package/execution.jsm
 ./package/files.jsm
 ./package/funcs.jsm
 ./package/key.jsm
 ./package/keyObj.jsm
 ./package/keyRing.jsm
 ./package/keyUsability.jsm
 ./package/lazy.jsm
 ./package/locale.jsm
 ./package/localizeHtml.jsm
 ./package/log.jsm
 ./package/os.jsm
 ./package/overlays.jsm
 ./package/passwords.jsm
 ./package/pipeConsole.jsm
 ./package/prefs.jsm
 ./package/prefs/defaultPrefs.js
 ./package/searchCallback.jsm
 ./package/singletons.jsm
 ./package/streams.jsm
 ./package/system.jsm
 ./package/time.jsm
 ./package/timer.jsm
 ./package/trust.jsm
 ./package/versioning.jsm
 ./package/windows.jsm
 ./static_analysis/eslint
 ./test.sh
 ./ui/Makefile
 ./ui/content/Makefile
 ./ui/content/aboutEnigmail.html
 ./ui/content/aboutEnigmail.js
 ./ui/content/enigmailKeySelection.js
 ./ui/content/enigmailKeySelection.xhtml
 ./ui/content/enigmailMessengerOverlay.js
 ./ui/content/enigmailMessengerOverlay.xhtml
 ./ui/content/enigmailMsgBox.js
 ./ui/content/enigmailMsgBox.xhtml
 ./ui/content/setupWizard2.js
 ./ui/content/setupWizard2.xhtml
 ./ui/content/upgradeInfo.html
 ./ui/content/upgradeInfo.js
 ./ui/locale/en-US/Makefile
 ./ui/skin/Makefile
 ./ui/skin/common/enigmail-common.css
 ./ui/skin/common/enigmail-html.css
 ./ui/skin/common/enigmail.css
 ./util/Expression.py
 ./util/JarMaker.py
 ./util/MozZipFile.py
 ./util/Preprocessor.py
 ./util/buildlist.py
 ./util/genxpi
 ./util/install
 ./util/make-lang-xpi.pl
 ./util/prepPackage
 ./util/utils.py
Copyright: NONE
License: MPL-2.0
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2013, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: MPL-2.0
 FIXME

Files: ./ui/skin/images/enigmail-logo.png
Copyright: ¥-ÄÒL
  $½yìI¨æÆê§»An]F}1 ³²
  *üå@É ÏìÎ`r¾}$®7c¶^¬ÃòÝP³Ø,Áâ7°Ëz¬¬¥ÇÒÓèµ8³áf'óP"ÔCmô¢â8
  4Zd]ïÂÜzÆl)3BÆT¤Ø¨ÿN<¿ç*>3P.`[
  J]×¥Àÿäâbi¦rý¨î#å
  Ru§*ñîñJÈT÷ÿy¯sêð·ûÂiÏõFÍ_ïF~½R¥ñ¾bÖ·ò
  Yç<VÚí9õ¬ÇçÄyu>a¨c±ã|Åòk´:||¦Ê¢ó¾²õX
  dÚg]
  lGú¶yÁ6ØTÀ[/Öá
  vy¸§3<îÃF©M
  B­íÏ)C
  @¤ÇûàTE;Zd*y1!TÒÄq+%Í2|tÚ²bþý
  ®
  °áÌ¡MXÓ£½pîùT«Ä
  À¢oòp¶òæM÷ºsN>7~.´ïOX%à$ÌO¤*jëQaÂ§z©¡
  Á-9(lìÖûüßg«±æ¾Æ*¥rµ&^ øälÖL	§U	ôvÄd2)õeV-¦Çxá¡úwEa°;5=½`°X@[îºÜåmø×ýÑx|4ÝÂÖ_ä7HñôîBÆ¥KA,à¢½Ç>>È±ÁîØùhÑï®hÇÝeY&W«Óáñÿæ³¦¥Èµ­lZ °ÿªáòDy^«±XÀE·Ò´êkJiâzCc«ÚåîC_º®=RÆ¸ß¾1æöå.KAÄ7}Nl.n¡	À;±æ_ùÓ8<DÛW4HXºó
  ÃÜìX$B>Ü
  ÓÁhË×9qÞ[K¦(`éè ä½4qæ
  òÞkËÁbt
License: UNKNOWN
 FIXME

Files: ./ui/skin/images/spinning-wheel.png
Copyright: fLuÌ$Û}ýãÄ)åÇÚÇÄ¾³+{o¿/~<Á1¡,aëÊ­ä¾MôJ¬INòO:4ÜON9Çæ%ñ®¦ª¦f¥öó
  -RURç¤¥¦¨4ª)ÕB-¢§Þ¤¾ÆIëH»Ks¤ó¤H_¥¡h4W¶vv6&ÑaÊ$ÊÊéµ
  TB!Ó|yyA±X¤®ìêÃÃCÓ¤¢^¯£Ûíf@&!kkkæÍD)E¡Pøöüüì,8
  i;à
  s°^Ì¿éÄÿfXÅóÂ³ÌÞÆOfmÿïHI.ÞCIrÀ+9"_ªOJõ3/fÿ"s9óýó ô
  býØ4÷ ±~,ÓW<Cò*1Çq=¶ÈÜÐUä³Ôã*®
  ®¬î¥.T?¬Þ«>£¡«¢«Ñ¬ñD¨ÉÐÓ,ÕìÒÔRÓòÓÊÑjÔz¨MÐfh'hÐîÖÖÑÕ	ÓÙ©Ó¦óRWA©­Û¨ûX¢ç¨·A¯Vï>V¡¤P¿Ï 6°4H0¨2¸cZr
  ´©·i®ié;33¶YÙ=s¹ùVóvó·±,î[Ò,ý,wZvY~³²¶X5Y[kYG[W[3d"Æ
  Âd3ÞÖÞ½¯ú}|*}ùø
  ämËýÁóÆ||AþðN»5?¢äþØ»Ë|WÅ®ï[
  øZP9XåRÕ­½«zú çàÀ!§CM5*5
  ü§ÙÝq»{­íÁîáíÚë¸·¾Z]2ºÏo_k)½´
  þ3ãs÷°f2¿â¾ÓÿÖñÝçûãÙÙY>KÀ·($á¸8 Þ ­ ¢Ä7hÁÏÏø;^ðÏóaÀN  ðEÛ ÐAR9p¯ÃææâüW¤Å-hÚkR6;ûñ8} ¾
License: UNKNOWN
 FIXME

Files: ./lang/sk/enigmail.properties
Copyright: GnuPG, ktorÃ© je umiestenÃ© v ceste %S %S
  heslo
  kÃ³dovanie 'quoted-printable', to mÃ´Å¾e spÃ´sobiÅ¥ nesprÃ¡vne deÅ¡ifrovanie a/lebo overenie vaÅ¡ej sprÃ¡vy.n Prajete si vypnÃºÅ¥ odosielanie sprÃ¡v v 'quoted-printable'?
  rozÅ¡Ã­renie Enigmail verzie %S
License: UNKNOWN
 FIXME

Files: ./lang/cs/enigmail.properties
Copyright: deÅ¡ifrovÃ¡nÃ­ a/nebo ovÄÅenÃ­ vaÅ¡Ã­ zprÃ¡vy.n PÅejete si vypnout odesÃ­lÃ¡nÃ­ zprÃ¡v v 'quoted-printable'?
  heslo
  je umÃ­stÄnÃ© v cestÄ %S %S
License: UNKNOWN
 FIXME

Files: ./lang/ar/enigmail.properties
Copyright: dlgNoPrompt=ÙØ§ ØªØ¸ÙØ± ÙØ°Ø§ Ø§ÙØ­ÙØ§Ø± Ø«Ø§ÙÙØ©
License: UNKNOWN
 FIXME

Files: ./lang/hu/enigmail.dtd
Copyright: ges">
  nti szabÃ¡lyok lÃ©trehozÃ¡sa">
  rlet a hiÃ¡nyzÃ³ kulcsok letÃ¶ltÃ©sÃ©re egy kulcskiszolgÃ¡lÃ³rÃ³l">
  rvÃ©nyes, nem megbÃ­zhatÃ³ vagy nem talÃ¡lhatÃ³">
  s alÃ¡Ã­rva">
  s titkosÃ­tva">
  s">
  se">
  sz">
License: UNKNOWN
 FIXME

Files: ./lang/hu/enigmail.properties
Copyright: ggÃ© megbÃ­zhatÃ³ kulcs: â%Sâ
  grehajtÃ¡sÃ¡hoz a(z) â%Sâ intelligens kÃ¡rtyÃ¡ra van szÃ¼ksÃ©g.nKÃ©rem, helyezze be az intelligens kÃ¡rtyÃ¡t Ã©s ismÃ©telje meg a mÅ±veletet.
  grehajtÃ¡sÃ¡hoz nincs szÃ¼ksÃ©g intelligens kÃ¡rtyÃ¡ra.nKÃ©rem, tÃ¡volÃ­tsa el az intelligens kÃ¡rtyÃ¡t Ã©s ismÃ©telje meg a mÅ±veletet.
  grehajtÃ¡sÃ¡hoz.nKÃ©rem, helyezze be a(z) â%2$Sâ intelligens kÃ¡rtyÃ¡t Ã©s ismÃ©telje meg a mÅ±veletet.
  gse
  kezzen a vÃ¡laszaimra, Ã©s ne kÃ©rdezze meg Ãºjra
  lbe Ã¡gyazott nyilvÃ¡nos kulcsokat?
  lyezte a âquoted-printableâ kÃ³dolÃ¡st a levÃ©lkÃ¼ldÃ©shez. Ez a levele tÃ©ves visszafejtÃ©sÃ©t, illetve ellenÅrzÃ©sÃ©t okozhatja.nSzeretnÃ© kikapcsolni a âquoted-printableâ alapÃº Ã¼zenetkÃ¼ldÃ©st most?
  nt a fÃ¡jl Ã­rÃ¡sakor: %S
  nyel. FrissÃ­tse a rendszerre telepÃ­tett GnuPG verziÃ³t. ellenkezÅ esetben az Enigmail nem fog mÅ±kÃ¶dni.
  pen olyan gpg-agent verziÃ³ talÃ¡lhatÃ³, amely nem mÅ±kÃ¶dik egyÃ¼tt a telepÃ­tett GnuPG verziÃ³val.
  pes kommunikÃ¡lni a dirmngr programmal (amely a GnuPG rÃ©sze).
  pes kommunikÃ¡lni a gpg-agent programmal (amely a GnuPG rÃ©sze).
  rdezni a jelmondatot a pinentry segÃ­tsÃ©gÃ©vel.
  rdÃ©s
  rem, helyezze be az Intelligens kÃ¡rtyÃ¡t Ã©s ismÃ©telje meg a mÅ±veletet.
  rhetÅ.nKÃ©rem, csatlakoztassa az Intelligens kÃ¡rtyaolvasÃ³t, helyezze be a kÃ¡rtyÃ¡t, majd ismÃ©telje meg a mÅ±veletet.
  rhetÅk el
  rtesÃ­tÃ©s ismÃ©tlÅdik mÃ©g %S
  rtesÃ­tÃ©s nem ismÃ©tlÅdik, amÃ­g nem frissÃ­ti az Enigmail programot.
  rvÃ©nyes vÃ©dett OpenPGP-adatblokk
  rÃ©s a felhasznÃ¡lÃ³ Ã¡ltal megszakÃ­tva
  rÃ©se.
  s
  s kimenet:
  s visszafejtÃ©shez a(z) %1$S program %2$S pÃ©ldÃ¡nyÃ¡t hasznÃ¡lja.
  se a kulcskiszolgÃ¡lÃ³ra: %S
  si vagy beÃ¡llÃ­tÃ¡si hiba, amely meggÃ¡tolja az Enigmail programot a helyes mÅ±kÃ¶dÃ©sben. Ez a hiba nem javÃ­thatÃ³ automatikusan.nnA hibaelhÃ¡rÃ­tÃ¡ssal kapcsolatos teendÅkÃ©rt lÃ¡togassa meg az Enigmail termÃ©ktÃ¡mogatÃ¡si weboldalÃ¡t:https:enigmail.net/faq.
  sÃ©hez a nem szabvÃ¡nyos programot (pÃ©ldÃ¡ul: gnome-keyring)hasznÃ¡lja, ezÃ©rt a kulcstÃ¡rolÃ³ Ã¼rÃ­tÃ©se nem lehetsÃ©ges az Enigmail programban.
  trehozva
License: UNKNOWN
 FIXME

Files: ./lang/sk/enigmail.dtd
Copyright: ho nÃ¡jsÅ¥">
License: UNKNOWN
 FIXME

Files: ./lang/fr/enigmail.dtd
Copyright: lection de clÃ©s dâEnigmail">
  lection">
License: UNKNOWN
 FIXME

Files: ./lang/pt-BR/enigmail.properties
Copyright: mais possÃ­vel de dentro do Enigmail.
  que vocÃª atualize o Enigmail.
License: UNKNOWN
 FIXME

Files: ./lang/fr/enigmail.properties
Copyright: moriser ma rÃ©ponse et ne plus me demander
License: UNKNOWN
 FIXME

Files: ./lang/vi/enigmail.properties
Copyright: n (nhÆ° lÃ
  u.
  u. Váº­y nÃªn khÃ´ng thá» xoÃ¡ máº­t kháº©u khi dÃ¹ng Enigmail.
License: UNKNOWN
 FIXME

Files: ./lang/es-ES/enigmail.dtd
Copyright: n de claves de Thunderbird.">
License: UNKNOWN
 FIXME

Files: ./lang/es-AR/enigmail.properties
Copyright: ntelo de nuevo.
License: UNKNOWN
 FIXME

Files: ./lang/pt-PT/enigmail.properties
Copyright: possÃ­vel apagar a frase de acesso a partir do Enigmail.
  Ã
License: UNKNOWN
 FIXME

Files: ./lang/gl/enigmail.properties
Copyright: posÃ­bel limpar a frase secreta desde o Enigmail.
  que actualice Enigmail.
License: UNKNOWN
 FIXME

Files: ./lang/gl/enigmail.dtd
Copyright: posÃ­bel">
License: UNKNOWN
 FIXME

Files: ./lang/da/enigmail.dtd
Copyright: r regler pr. modtager(e)">
License: UNKNOWN
 FIXME

Files: ./lang/ca/enigmail.dtd
Copyright: s possible xifrar">
License: UNKNOWN
 FIXME

Files: ./lang/ca/enigmail.properties
Copyright: s.
License: UNKNOWN
 FIXME

Files: ./lang/nl/enigmail.dtd
Copyright: sleutels exporteren van GnuPG en importeren in Thunderbird.
  sleutels selecteren â¦">
License: UNKNOWN
 FIXME

Files: ./lang/zh-TW/enigmail.properties.big5
Copyright: w
  w Enigmail¡H
  w±zªº¥D­n¶l§}¥H¥Î©óÃ±ÃÒ±H¥X¶l¥ó®É¨Ï¥Î¡Cn ¦pªG±z­n¥Î¡§±H¥X¡¨ªº¶l§}§@¬°Ã±ÃÒ½Ð§â¦¹Äæ¸mªÅ¡Cnnª`·N¡GEnigmail 0.60 ¥H¤W¤w§â default signing key ³o§t½kªº¿ï¶µ¥h°£.
  w¶Ü¡H
  Ò¥H§Ú­Ì«ØÄ³§â¦¹¼¶¶µÃö¤W¡Cnn±z­n Enigmail ¬°±z°õ¦æÃö³¬¡@Allow flowed text (RFC 2646) ³o­Ó¿ï¶µ¶Ü?
  óÃ±ÃÒ±H¥Xªº¶l¥ó
License: UNKNOWN
 FIXME

Files: ./lang/cs/enigmail.dtd
Copyright: zt nebo je nedÅ¯vÄryhodnÃ½">
License: UNKNOWN
 FIXME

Files: ./lang/ko/enigmail.properties
Copyright: ì¼
  ì¼ ê²½ê³
  ì¼ íë¡¬íí¸
License: UNKNOWN
 FIXME

Files: ./lang/zh-TW/enigmail.dtd
Copyright: ¦å¾å¯é°ä¼ºæå¨å¯å
  ±æ¡ã">
License: UNKNOWN
 FIXME

Files: ./lang/zh-TW/enigmail.properties
Copyright: ±æ¡
  ²è­¦åéè¤ %Sã
License: UNKNOWN
 FIXME

Files: ./lang/zh-CN/enigmail.properties.big5
Copyright: ÃûÈÏÖ¤Ê±·¢ÉúÎÊÌâ¡£nËùÒÔÎÒÃÇ½¨Òé°Ñ´Ë×«Ïî¹ØÉÏ¡£nnÄúÒª Enigmail ÎªÄúÖ´ÐÐ¹Ø±Õ¡¡Allow flowed text (RFC 2646) Õâ¸öÑ¡ÏîÂð?
  Ö¤¼Ä³öµÄÓÊ¼þ
  Ö¤¼Ä³öÓÊ¼þÊ±Ê¹ÓÃ¡£n Èç¹ûÄúÒªÓÃ¡°¼Ä³ö¡±µÄÓÊÖ·×÷ÎªÇ©Ö¤Çë°Ñ´ËÀ¸ÖÃ¿Õ¡£nn×¢Òâ£ºEnigmail 0.60 ÒÔÉÏÒÑ°Ñ default signing key Õâº¬ºýµÄÑ¡ÏîÈ¥³ý.
License: UNKNOWN
 FIXME

Files: ./lang/hy/enigmail.dtd
Copyright: ÕµÕ¸ÖÕ¶">
License: UNKNOWN
 FIXME

Files: ./lang/hy/enigmail.properties
Copyright: ÕµÕ¸ÖÕ¶Õ¶Õ¥Ö
License: UNKNOWN
 FIXME

Files: ./lang/fa/enigmail.properties
Copyright: Ø§Ø¨Ø²Ø§Ø± ØºÛØ±-Ø§Ø³ØªØ§ÙØ¯Ø§Ø±Ø¯ (Ù
  Ø§Ø± Ø§ØµÙØ§Ø­ Ø´ÙØ¯.nnØªÙØµÛÙ Ø§Ú©ÛØ¯ Ù
  Ø§Ø±Øª %1$S ÛØ§ÙØªÙ Ø´Ø¯Ù Ø¯Ø± Ø®ÙØ§ÙÙØ¯Ù Ø´Ù
  Ø§Ø±Øª %2$S Ø®ÙØ¯ Ø±Ø§ ÙØ§Ø±Ø¯ Ú©ÙÛØ¯ Ù Ø¹Ù
  Ø§Ø±Øª S% Ø´Ù
  Ø§Ø±Øª Ø®ÙØ§Ø³ØªÙ Ø´Ø¯Ù Ø±Ø§ ÙØ§Ø±Ø¯ Ú©ÙÛØ¯ Ù Ø¹Ù
  Ø§Ø±Øª Ø®ÙØ¯ Ø±Ø§ Ø­Ø°Ù Ú©Ø±Ø¯Ù Ù Ø¹Ù
  Ø§Ø±Øª Ø®ÙØ¯ Ø±Ø§ ÙØ§Ø±Ø¯ Ú©Ø±Ø¯Ù Ù Ø¹Ù
  Ø§Ø±Øª Ø®ÙØ¯ Ø±Ø§ Ù¾ÛÙØ³Øª Ú©Ø±Ø¯ÙØ Ú©Ø§Ø±Øª Ø®ÙØ¯ Ø±Ø§ ÙØ§Ø±Ø¯ Ú©ÙÛØ¯Ø Ù Ø¹Ù
  Ø§Ø±Øª Ø¯Ø± Ø®ÙØ§ÙÙØ¯Ù ÙØ¯Ø§Ø±Ø¯.nÙØ·ÙØ§ Ø§Ø³Ù
  Ø§Ø±Øª Ø´Ù
  Ø§Ø±ØªÛ Ø¯Ø± Ø®ÙØ§ÙÙØ¯Ù Ø´Ù
  Ø§ÙÛ Ø¨Ø±Ø§Û Ú©ÙÛØ¯ 'S%' ÙØ¬ÙØ¯ ÙØ¯Ø§Ø±Ø¯
  Ø§Ù Ø¨Ø±ÙØ§Ù
  Ø®Ø·Ø§ Ø¯Ø± Ø§Ø±ØªØ¨Ø§Ø· Ø´Ù
  Ø±Ø§Ø± Ø®ÙØ§ÙØ¯ Ú©Ø±Ø¯
  Ø±Ø§Ø± ÙØ®ÙØ§ÙØ¯ Ø´Ø¯.
  Ø±Ø§Ø± Ú©ÙÛØ¯.
  Ø±Ø¨ÙØ¯Û Ù
  Ø±Ø¯Ù Ø§Ø±Ø³Ø§Ù Ù¾ÛØ§Ù
  Ø±Ø¯Ù Ú©ÙÛØ¯ Ø¹Ù
  Ø±Ø¯Ù Ø§ÛØ¯.
  Ø±Ø¯Ù Ø§ÛØ¯. Ø§ÛÙ Ø¨Ø§Ø¹Ø« Ø±Ù
  Ø±Ø¯ÙØ Ø¯Ø± ØºÛØ± Ø§ÛÙØµÙØ±Øª Enigmail Ú©Ø§Ø± ÙØ®ÙØ§ÙØ¯ Ú©Ø±Ø¯.
  Ø³Ø§Ø²Û Ø¹Ø¨Ø§Ø±Øª Ø¹Ø¨ÙØ± Ø§Ø² Ø·Ø±ÛÙ Enigmail Ù
  Ø´Ø¯Ù Ø§Ø³Øª.
  ÙÛØ¯
  ÙÛØ¯ ØªÙØ³Ø· Ú©Ø§Ø±Ø¨Ø±
  ÙÛØ¯ Ø¯Ø± Ø­ÙÙÙ Ú©ÙÛØ¯ Ø´Ù
  ÙÛØ¯ Ø³Ø±ÙØ±:
  ÙÛØ¯ Ø¹Ù
  ÙÛØ¯ ÙØ§Ù
  ÙÛØ¯ Ù¾ÛÚ©Ø±Ø¨ÙØ¯Û Ø´Ø¯Ù 'S%' Ø¯Ø± Ø­ÙÙÙ Ú©ÙÛØ¯ Ø´Ù
  ÙÛØ¯(ÙØ§Û) OpenPGP Ø¨Ù Ù
  ÙÛØ¯(ÙØ§Û) Ø¹Ù
  Ù ÙÛØ³Øª.
  ÙØªØ±Ù Ø¹Ø¨Ø§Ø±Øª Ø¹Ø¨ÙØ± Ù
  ÙØ¯ Ù ÙÙ
  ÙØ¯ Ú©Ù Ù
  ÙØ¯.
  ÙØ³Ø®Ù Ú¯Ø±Ø§ÙÛÚ©Û Ø§Ø² pinetry Ø¯Ø§Ø±ÛØ¯.
  ÙØ³ÙÙ Ø§Ø² pinetry Ù¾ÛÚ©Ø±Ø¨ÙØ¯Û Ø´Ø¯Ù Ø§Ø³Øª. ÙØ±ÚÙØ¯Ø Ø²Ù
  ÙØµØ¨ Ø³ÛØ³ØªÙ
  Ù Ø§Ø² Ú©Ø§Ø± Ú©Ø±Ø¯Ù Enigmail Ø¨Ù Ø¯Ø±Ø³ØªÛ Ø¬ÙÙÚ¯ÛØ±Û Ù
  Ù Ø¯ÛÚ¯Ø± Ù¾Ø´ØªÛØ¨Ø§ÙÛ ÙÙ
  Ù Ø´Ù
  Ù Ù
  Ù¾Ø§Ø±ÚÚ¯Û (MDC) ÙØ§ÙØµ ÛØ§ Ø®Ø±Ø§Ø¨ Ù
  Û
  Û Ø§Ø² Ø§Ø¬Ø²Ø§ GnuPG) Ú¯Ø²Ø§Ø±Ø´ Ú©Ø±Ø¯.
  Û Ø§Ø² Ø§Ø¬Ø²Ø§ GnuPG) ÛÚ© Ø®Ø·Ø§ Ú¯Ø²Ø§Ø±Ø´ Ú©Ø±Ø¯.
License: UNKNOWN
 FIXME

Files: ./lang/fa/enigmail.dtd
Copyright: Ø§Ø±Ø¨Ø±Û / Ø¢ÛØ¯Û Ú©Ø§Ø±Ø¨Ø±">
  ÙÛØ¯ Enigmail">
  ÙÛØ¯">
License: UNKNOWN
 FIXME

Files: ./lang/ar/enigmail.dtd
Copyright: Ø§ÙÙ
License: UNKNOWN
 FIXME

Files: ./lang/ja/enigmail.properties
Copyright: ã®éæ¨æºã®ãã¼ã«ãä½¿ç¨ãã¦ãããããEnigmail ã¯ãã¹ãã¬ã¼ãºãæ¶å»ã§ãã¾ããã
  ã¼: Enigmail ã³ã¢ãµã¼ãã¹ã«ã¢ã¯ã»ã¹ã§ãã¾ããã§ããï¼
  å·ããã¦ãã¾ã
License: UNKNOWN
 FIXME

Files: ./lang/zh-CN/enigmail.properties
Copyright: äºå¨åéé®ä»¶æ¶è¿è¡âquoted-printableâç¼ç
  ç¨äº %S ç OpenPGP å¯é¥
License: UNKNOWN
 FIXME

Files: ./lang/ja/enigmail.dtd
Copyright: ç¨ããããã«ã¯ãé©åã«è¨­å®ãè¡ãå¿
License: UNKNOWN
 FIXME

Files: ./lang/ko/enigmail.dtd
Copyright: ì ID">
License: UNKNOWN
 FIXME

