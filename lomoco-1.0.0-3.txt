Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./Makefile.am
 ./autogen.sh
 ./configure.ac
 ./configure.scan
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/dirs
 ./debian/docs
 ./debian/lomoco.default
 ./debian/lomoco.install
 ./debian/lomoco.metainfo.xml
 ./debian/patches/00_no_uninitialized_buffers.patch
 ./debian/patches/01_Support_for_COM_for_Notebooks.patch
 ./debian/patches/02_M500.patch
 ./debian/patches/02_MX518.patch
 ./debian/patches/03_fix_udev_scripts.patch
 ./debian/patches/series
 ./debian/postinst
 ./debian/preinst
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./src/.splintrc
 ./src/Makefile.am
 ./src/lomoco.1
 ./src/lomoco.xml
 ./udev/README
 ./udev/toudev.awk
 ./udev/udev.lomoco
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/cmdline.c
 ./src/cmdline.h
 ./src/lomoco.c
 ./src/lomoco.h
Copyright: 2002, Alexios Chouchoulas
  2004, Alexios Chouchoulas
  2005, Andreas Schneider <andreas.schneider@linux-gamers.net>
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./README
Copyright: 2004, Alexios Chouchoulas
  2005, Andreas Schneider, Peter Feuerer, Tobias Schleuss
License: GPL-2+
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, Free Software
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

