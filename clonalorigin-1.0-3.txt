Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./README.md
 ./debian/changelog
 ./debian/clonalorigin-gui.1
 ./debian/clonalorigin-gui.manpages
 ./debian/clonalorigin.1
 ./debian/clonalorigin.README.Debian
 ./debian/clonalorigin.install
 ./debian/clonalorigin.links
 ./debian/clonalorigin.manpages
 ./debian/compat
 ./debian/control
 ./debian/createmanpages
 ./debian/patches/portability.patch
 ./debian/patches/qt5.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/metadata
 ./debian/warg.1
 ./debian/watch
 ./gui/coloured.ui
 ./gui/colouredimpl.cpp
 ./gui/colouredimpl.h
 ./gui/densityontree.cpp
 ./gui/densityontree.h
 ./gui/gelmanrubin.ui
 ./gui/gelmanrubinimpl.cpp
 ./gui/gelmanrubinimpl.h
 ./gui/gui.cpp
 ./gui/gui.pro
 ./gui/heat.ui
 ./gui/heatimpl.cpp
 ./gui/heatimpl.h
 ./gui/mainapplicationdbusadaptor.h
 ./gui/mainwindow.ui
 ./gui/mainwindowimpl.cpp
 ./gui/mainwindowimpl.h
 ./gui/outputfile.cpp
 ./gui/outputfile.h
 ./gui/paramcons.cpp
 ./gui/paramcons.h
 ./gui/paramconsmult.cpp
 ./gui/paramconsmult.h
 ./gui/parammr.cpp
 ./gui/parammr.h
 ./gui/paramqt.cpp
 ./gui/paramqt.h
 ./gui/paramtreecons.cpp
 ./gui/paramtreecons.h
 ./gui/pd.ui
 ./gui/pdimpl.cpp
 ./gui/pdimpl.h
 ./gui/pheat.ui
 ./gui/pheatimpl.cpp
 ./gui/pheatimpl.h
 ./gui/plot.ui
 ./gui/plotimpl.cpp
 ./gui/plotimpl.h
 ./warg/AUTHORS
 ./warg/ChangeLog
 ./warg/Makefile.am
 ./warg/NEWS
 ./warg/README
 ./warg/autogen.sh
 ./warg/configure.ac
 ./warg/data/Pestis_warg.nwk
 ./warg/data/Pestis_warg.xmfa
 ./warg/data/bac2.nwk
 ./warg/data/data.xmfa
 ./warg/data/ecoli-names.txt
 ./warg/data/ecoli.nwk
 ./warg/data/ecoli_2outgroups.xmfa
 ./warg/data/ecoli_2outgroups1Sample100.nwk
 ./warg/data/ecoli_2outgroupsSample100.nwk
 ./warg/data/feil.nwk
 ./warg/data/feil.xmfa
 ./warg/data/french.xmfa
 ./warg/data/german.xmfa
 ./warg/data/tree.nwk
 ./warg/m4/gsl.m4
 ./warg/scripts/Makefile.am
 ./warg/scripts/blocksplit.pl
 ./warg/scripts/computeMedians.pl
 ./warg/scripts/convert.pl
 ./warg/scripts/genomeplots_log.R
 ./warg/scripts/getClonalTree
 ./warg/scripts/makeMauveWargFile.pl
 ./warg/scripts/rescale.pl
 ./warg/scripts/rhomulus.pl
 ./warg/src/Makefile.am
 ./warg/src/data.cpp
 ./warg/src/data.h
 ./warg/src/exponential.h
 ./warg/src/metropolis.cpp
 ./warg/src/metropolis.h
 ./warg/src/move.cpp
 ./warg/src/move.h
 ./warg/src/moveaddedge.cpp
 ./warg/src/moveaddedge.h
 ./warg/src/moveageclonal.cpp
 ./warg/src/moveageclonal.h
 ./warg/src/movedelta.cpp
 ./warg/src/movedelta.h
 ./warg/src/moveedgechange.cpp
 ./warg/src/moveedgechange.h
 ./warg/src/movegreedytree.cpp
 ./warg/src/movegreedytree.h
 ./warg/src/moveregraftclonal.cpp
 ./warg/src/moveregraftclonal.h
 ./warg/src/moveremedge.cpp
 ./warg/src/moveremedge.h
 ./warg/src/moverho.cpp
 ./warg/src/moverho.h
 ./warg/src/movescaletree.cpp
 ./warg/src/movescaletree.h
 ./warg/src/movesitechange.cpp
 ./warg/src/movesitechange.h
 ./warg/src/movetheta.cpp
 ./warg/src/movetheta.h
 ./warg/src/movetimechange.cpp
 ./warg/src/movetimechange.h
 ./warg/src/mpiutils.cpp
 ./warg/src/mpiutils.h
 ./warg/src/mydata.cpp
 ./warg/src/node.cpp
 ./warg/src/node.h
 ./warg/src/param.cpp
 ./warg/src/param.h
 ./warg/src/profilerun.sh
 ./warg/src/recedge.cpp
 ./warg/src/recedge.h
 ./warg/src/rectree.cpp
 ./warg/src/rectree.h
 ./warg/src/rng.cpp
 ./warg/src/rng.h
 ./warg/src/slotallocator.h
 ./warg/src/tree.cpp
 ./warg/src/tree.h
 ./warg/src/truth.xml
 ./warg/src/wargxml.cpp
 ./warg/src/wargxml.h
 ./warg/src/weakarg.cpp
 ./warg/src/weakarg.h
 ./warg/weakarg.pro
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  © 2008-2010 Xavier Didelot, Aaron Darling, et. al.
  © 2017 Andreas Tille <tille@debian.org>
License: UNKNOWN
 FIXME

Files: ./warg/COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

