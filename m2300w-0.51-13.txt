Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./INSTALL
 ./aclocal.m4
 ./debian/.git-dpm
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/patches/0001-Include-CPPFLAGS-in-CFLAGS-as-well-as-LDFLAGS-in-LIB.patch
 ./debian/patches/0002-PPD-for-the-Minolta-magicolor-2400W-was-not-passing-.patch
 ./debian/patches/0003-Adds-dNOINTERPOLATE-to-the-Ghostscript-command-line..patch
 ./debian/patches/0004-Search-foomatic-rip-in-usr-lib-cups-filter-as-this-i.patch
 ./debian/patches/0005-Replace-let-bashism-with-arithmetic-expansion.patch
 ./debian/patches/series
 ./debian/printer-driver-m2300w.docs
 ./debian/printer-driver-m2300w.install
 ./debian/printer-driver-m2300w.ppd-updater
 ./debian/rules
 ./debian/source.lintian-overrides
 ./debian/source/format
 ./debian/watch
 ./foomatic/Makefile.in
 ./foomatic/driver/m2300w.xml
 ./foomatic/driver/m2400w.xml
 ./foomatic/foomatic-rip
 ./foomatic/opt/m2300w-ColorMode.xml
 ./foomatic/opt/m2300w-ColorProfile.xml
 ./foomatic/opt/m2300w-Debug.xml
 ./foomatic/opt/m2300w-DefaultRGB.xml
 ./foomatic/opt/m2300w-MediaType.xml
 ./foomatic/opt/m2300w-PageSize.xml
 ./foomatic/opt/m2300w-PrintoutMode.xml
 ./foomatic/opt/m2300w-Quality.xml
 ./foomatic/opt/m2300w-Resolution.xml
 ./foomatic/opt/m2300w-psnup.xml
 ./foomatic/printer/Minolta-magicolor_2300W.xml
 ./foomatic/printer/Minolta-magicolor_2400W.xml
 ./psfiles/CHP410-1200-AutoColor.crd
 ./psfiles/CHP410-1200-Draft.crd
 ./psfiles/CHP410-1200-Photo-kh.crd
 ./psfiles/CHP410-1200-Photo.crd
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./Makefile.in
 ./configure.in
 ./m2300w.spec.in
 ./ppd/Makefile.in
 ./psfiles/Makefile.in
 ./psfiles/defrgb-builtin.csa
 ./psfiles/epilogue.ps
 ./psfiles/prolog.ps
 ./src/Makefile.in
Copyright: 2004, Gerhard Fuernkranz
License: GPL-2+
 FIXME

Files: ./README
 ./psfiles/defrgb-AdobeRGB.csa
 ./psfiles/defrgb-gimp-print.csa
 ./psfiles/defrgb-sRGB.csa
 ./psfiles/screen1200.ps
 ./psfiles/screen2400.ps
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./ppd/magicolor_2300W-m2300w.ppd
 ./ppd/magicolor_2400W-m2400w.ppd
Copyright: NONE
License: GPL
 FIXME

Files: ./src/m2300w.c
 ./src/m2400w.c
Copyright: 2004, Leif Birkenfeld (leif@birkenfeld-home.de)
License: GPL-2+
 FIXME

Files: ./README.ghostscript
Copyright: NONE
License: Aladdin and/or GPL
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2002, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: Copyright 2004 Leif Birkenfeld
License: GPL-2+
 FIXME

Files: ./src/m2300w-wrapper.in
Copyright: 2003, Rick Richardson
License: GPL-2+
 FIXME

Files: ./debian/local/apport-hook.py
Copyright: 2009, Canonical Ltd.
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

