Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./00api
 ./00changes
 ./00changes.0
 ./00changes.1
 ./00changes.2
 ./00changes.3
 ./00changes.4.1
 ./00changes.4.2
 ./00changes.5.1
 ./00changes.5.2
 ./00changes.6.1
 ./00changes.6.2
 ./00changes.6.3
 ./00changes.6.4
 ./00changes.6.5
 ./00changes.6.6
 ./00changes.6.7
 ./00changes.6.8
 ./00diff
 ./00readme
 ./Makefile.in
 ./README.md
 ./bin/Makefile.in
 ./bin/Makefile.w32
 ./bin/cmew
 ./bin/cmew.1
 ./bin/config.h.in
 ./bin/configure.in
 ./bin/hs/00readme
 ./bin/hs/Index.hs
 ./bin/hs/Mail.hs
 ./bin/hs/Makefile
 ./bin/hs/Msg.hs
 ./bin/hs/Param.hs
 ./bin/hs/Search.hs
 ./bin/hs/Setup.hs
 ./bin/hs/Sql.hs
 ./bin/hs/Util.hs
 ./bin/hs/cmew.hs
 ./bin/hs/mew.cabal
 ./bin/hs/smew.hs
 ./bin/incm.c
 ./bin/mew-pinentry.1
 ./bin/mew.h
 ./bin/mewencode.c
 ./bin/mewest
 ./bin/mewest.1
 ./bin/mewl.c
 ./bin/pattern.c
 ./bin/smew
 ./bin/smew.1
 ./bin/utils.c
 ./bin/w32/config.h
 ./bin/w32/dirent.c
 ./bin/w32/w32.h
 ./configure.in
 ./contrib/mew-absfilter.el
 ./contrib/mew-browse.el
 ./contrib/mew-caesar.el
 ./contrib/mew-edebug.el
 ./contrib/mew-fancy-summary.el
 ./contrib/mew-nmz-fixer.el
 ./contrib/mew-nmz.el
 ./contrib/mew-refile-view.el
 ./contrib/mew-toolbar-frame.el
 ./debian/README.Debian.in
 ./debian/control
 ./debian/dirs.in
 ./debian/emacsen-compat
 ./debian/emacsen-install.in
 ./debian/emacsen-remove.in
 ./debian/emacsen-startup.in
 ./debian/mew-beta-bin.lintian-overrides
 ./debian/mew-beta-bin.postinst
 ./debian/mew-beta-bin.prerm
 ./debian/mew-beta.postinst
 ./debian/mew-beta.prerm
 ./debian/patches/010_contrib.patch
 ./debian/patches/020_netpbm.patch
 ./debian/patches/030_cache-long-scans.patch
 ./debian/patches/040_incm-lock.patch
 ./debian/patches/070_checkhost.patch
 ./debian/patches/900_changes.patch
 ./debian/patches/series
 ./debian/source/format
 ./debian/watch
 ./dot.emacs
 ./dot.mew
 ./dot.theme
 ./etc/Mew.img
 ./etc/Mew.xbm
 ./etc/Mew.xpm
 ./info/Makefile
 ./mew-addrbook.el
 ./mew-attach.el
 ./mew-auth.el
 ./mew-blvs.el
 ./mew-bq.el
 ./mew-cache.el
 ./mew-complete.el
 ./mew-config.el
 ./mew-const.el
 ./mew-darwin.el
 ./mew-decode.el
 ./mew-draft.el
 ./mew-edit.el
 ./mew-encode.el
 ./mew-env.el
 ./mew-exec.el
 ./mew-ext.el
 ./mew-fib.el
 ./mew-func.el
 ./mew-gemacs.el
 ./mew-header.el
 ./mew-highlight.el
 ./mew-imap.el
 ./mew-imap2.el
 ./mew-key.el
 ./mew-lang-jp.el
 ./mew-local.el
 ./mew-mark.el
 ./mew-message.el
 ./mew-mime.el
 ./mew-minibuf.el
 ./mew-mule.el
 ./mew-mule3.el
 ./mew-net.el
 ./mew-nntp.el
 ./mew-nntp2.el
 ./mew-passwd.el
 ./mew-pgp.el
 ./mew-pick.el
 ./mew-pop.el
 ./mew-refile.el
 ./mew-scan.el
 ./mew-search.el
 ./mew-smime.el
 ./mew-smtp.el
 ./mew-sort.el
 ./mew-ssh.el
 ./mew-ssl.el
 ./mew-summary.el
 ./mew-summary2.el
 ./mew-summary3.el
 ./mew-summary4.el
 ./mew-syntax.el
 ./mew-thread.el
 ./mew-unix.el
 ./mew-vars.el
 ./mew-vars2.el
 ./mew-vars3.el
 ./mew-varsx.el
 ./mew-virtual.el
 ./mew-win32.el
 ./mew.el
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./etc/mew-Folder.xpm
 ./etc/mew-attach.xpm
 ./etc/mew-cite.xpm
 ./etc/mew-forward.xpm
 ./etc/mew-next.xpm
 ./etc/mew-pgp-enc.xpm
 ./etc/mew-pgp-sigenc.xpm
 ./etc/mew-pgp-sign.xpm
 ./etc/mew-prev.xpm
 ./etc/mew-queue.xpm
 ./etc/mew-refile.xpm
 ./etc/mew-reply.xpm
 ./etc/mew-send.xpm
 ./etc/mew-show.xpm
 ./etc/mew-write.xpm
 ./etc/mew-yank.xpm
Copyright: 1997, Kazu Yamamoto <kazu@Mew.org>
  1997, Yuuichi Teranishi <teranisi@isl.ntt.co.jp>
License: UNKNOWN
 FIXME

Files: ./etc/mew-Audio.xpm
 ./etc/mew-Blank.xpm
 ./etc/mew-External.xpm
 ./etc/mew-Image.xpm
 ./etc/mew-Octet-Stream.xpm
 ./etc/mew-Postscript.xpm
 ./etc/mew-Rfc822.xpm
 ./etc/mew-Text.xpm
 ./etc/mew-Unknown.xpm
 ./etc/mew-Video.xpm
Copyright: 1997, Kazu Yamamoto <kazu@Mew.org>
  1997, Yoshiaki Kasahara <kasahara@nc.kyushu-u.ac.jp>
License: UNKNOWN
 FIXME

Files: ./00copyright
 ./bin/incm.1
 ./bin/mewencode.1
 ./bin/mewl.1
 ./mew-lang-kr.el
Copyright: 1994-2015, Mew developing team.
  1997-2015, Mew developing team.
  2001-2003, Mew developing team.
License: BSD-3-clause
 FIXME

Files: ./bin/mew-pinentry.in
 ./bin/w32/dirent.h
 ./mew-lang-latin.el
Copyright: 2001-2003, Mew developing team.
  2002-2015, Mew developing team.
  2006-2007, Mew developing team.
License: UNKNOWN
 FIXME

Files: ./bin/configure
 ./configure
Copyright: 1992-1996, 1998-2009, Free Software Foundation,
License: FSFUL
 FIXME

Files: ./debian/mewstunnel
 ./debian/mewstunnel.1
Copyright: 2005-2014, Tatsuya Kinoshita
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  1994-2018, Mew developing team
License: BSD-3-clause
 FIXME

Files: ./install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: NTP
 FIXME

Files: ./gitlog2mewchanges
Copyright: 
  Copyright (c) 2013 Tatsuya Kinoshita
License: UNKNOWN
 FIXME

Files: ./etc/Mew.png
Copyright: #Ð£Ëä_÷÷Ï<Cö@Ývå%Té§ºlbéV}Ý°¹ë0ed÷éöIOõç­¡ÄLl+d?
  (ëØ°Á®W#ZN)~£åó{Õãrø7'};½¤môòXWÑÊ
  Cô¬'ÒÅ©îj²¶ßËjy
  Gª±HÂä®õÞ#ÖýCMÎHFõl?±ò-í=uj;gt¦D¾p
  UXfZýb;éÜ7m÷ÐÞË.gA¤¦Û=gmtyKu5L8]éNîf pÃH¨ùÓùÝZå:µû§u*Éq	¼tJMäÿÏ°¿p"fÈ¦k4N'45'+$ºîé­4]E¤Âã1ÏÖ·TÁÙÅ9@é}à'TEïá%ÙÃ;h§ÕÖêxÓî°Û-8kmuØÝüýpì4x~¬ióïâx¾ÏëõÎkLu¿ ¾~AáEuûÉ±ÏM[·²W
  UË,wUo5ë½D8Ë|BætÎ²Jm^¡®KI¹
  j¸'ÖìÒ< do¿¬
  o¶öÏX
  1ãdÜØ?1Pac/Q:ÈïîUK^lDWt²ò xFëÛ^f{Bè²_ùWÆcNH)
  §RôÚ
  H
  Ã»ú ÚTYËx[Ý©·%¶Ìtac¹¡+èQÿõÊzEoßàaF
  B°>N`Å¿DNÍmíÝÇ£Zï±Ê
  à§Âþé'û¸Ü×ï9¡Ûx
  ©[$¹]qËÌ<Ö
  ¥¿þÖx|}æ«§A©¶QÊÌäîïï×hbÀ®×r-ÂR<£snc§Â/ÎpAØ@Q­xÿK¯k½¶UþòÑFr(°ú©CT©[	]Ë{ùâV>ÿ¼XÌo¦¸õ×þËå{µáûÌÔWX=ûª>NkÐ$Oô¾ÃÉÉÐÄ¨üýÊIíÑ1©a{¤ÛÃrÝõk:Íò%ö¿kø¢0¥µs%ppÝ 9ØÙ(mèUM®³ë¸VêÊäÑÖ×
  ©x|lh¤Ýg39eR"ê6°-UuÛ@¨í'ÔÀ­°Ö±u@1cørõI
  ­,Ëêj(®æT9§ªi
  ­ÕbiÔµ»n7­G£ËÉSéãí6Îfmoãníw¢ù5-»F¥ål6!Ë¡àb»$´ef&P_¼´þ9ÐeÝÖB´§¾I~÷
  ¯#QªÌ0
  ²â1kQ=fdÁm*aûäÁPÈvPËÖ°%}¤}×uððR
  ³²C#x»]ªl¶·_dq
  ºª¶îÁ­xæÖ1Äõ]0ÄS:4ì©¤BZ&ÃÀ3Ñ
  Àõ 6§D¥èJÞ$ÚÆCøÛÂT B¨ÆÉÛ2q]Ñ
  ÑT4®Ê¾«ª¬N§éeKø>±Y	Ø=
  Ö4¥Ö'ÓY]ÕËÞÉ1kÔTËTaÃ*çÈévv ®ÐKÂrÃÈ²q­{Ö-Õew|ÌL²²!
  éë¿ÖJ~I¿Ì'(Îø[êÚîTNT¥t*¢Çuò%o£´n4ÍIÛ([Á:ÕÖ³_ß u©T-ÀskóêW"SÔ ERÞ­çbkï¨|OßBtç+RýÿEoÎ´(YÕgU³|¶:Õ´÷=3þ§8WuQØ©VöuH÷Én27¯dü0Öâ°Þ7eu 7jáªbdòé=µ°Q¿ÜÐýnøØÇLIz¥ëpìÔðeªG¤Z=È3ó@¬U
  îTªtY>¡¯yT¥ßsõ³:ôìbGGWðjì{³/ ÔñÓ/"Á}|«ecú)³VÖ¬CP?}EÈ2l÷÷R%|øÒa	
License: UNKNOWN
 FIXME

Files: ./debian/rules
Copyright: $$|^00changes)' > debian/docs
License: UNKNOWN
 FIXME

Files: ./info/mew.ja.info
Copyright: Convention:: ã¡ãã»ã¼ã¸ã®ä½æ³
  ã«ã¤ãã¦
  ã«ã
License: UNKNOWN
 FIXME

Files: ./mew-demo.el
Copyright: 1994-2018, Kazu Yamamoto
License: UNKNOWN
 FIXME

Files: ./etc/mew-sep.xpm
Copyright: 1997, Kazu Yamamoto <kazu@Mew.org>
License: UNKNOWN
 FIXME

Files: ./etc/mew-inc.xpm
Copyright: 1997, Kazu Yamamoto <kazu@Mew.org>
  1997, Yoshiaki Kasahara <kasahara@nc.kyushu-u.ac.jp>
  1997, Yuuichi Teranishi <teranisi@isl.ntt.co.jp>
License: UNKNOWN
 FIXME

Files: ./etc/mew-lock.xpm
Copyright: 2003, Kazu Yamamoto <kazu@Mew.org>
  2003, Yuuichi Teranishi <teranisi@gohome.org>
  2004, Yoshifumi Nishida <nishida@csl.sony.co.jp>
License: UNKNOWN
 FIXME

Files: ./info/mew.texi
Copyright: 1996-2018, Kazuhiko Yamamoto
  1996-2018, å±±æ¬åå½¦
License: UNKNOWN
 FIXME

Files: ./00copyright.ja
Copyright: 1994-2015, Mew developing team.
  ã®ãããªå½¢ã«ãããã®ã½ããã¦ã§ã¢ã®å©ç¨ããçºçããåé¡ã«
  ã¯ãè±èªã®åæ¬ã«å¾ãã
  å£²ä¿é²ã®ããã«ããã®ãã¼ã
  ç¨ããã³ç¹å®ç®çã¸ã®é©åã«å¯¾ããè¨å¤ã®ä¿è¨¼ãå«ã¿ãã¾ãããã
  ç¨ãã¦ã¯ãªããª
  ç¨ã¯ã
  ç¨æ¨©ããã¼
  çã®æå¤±; ãããã¯å¶æ¥­ã®ä¸­æ­ãå«ããã¾ããããã
  è¡¨ç¤ºããã®æ¡ä»¶é
  è¡¨ç¤ºãè¨³ã
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Update Standards-Version to 4.3.0
License: UNKNOWN
 FIXME

Files: ./info/mew.info
Copyright: Variable Index:: Variable Index
License: UNKNOWN
 FIXME

