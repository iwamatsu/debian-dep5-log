Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./MANIFEST.in
 ./README.rst
 ./debian/.git-dpm
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/h2m/genkrb5conf.h2m
 ./debian/h2m/typeperf.h2m
 ./debian/h2m/wecutil.h2m
 ./debian/h2m/winrm.h2m
 ./debian/h2m/winrs.h2m
 ./debian/man/genkrb5conf.1
 ./debian/man/typeperf.1
 ./debian/man/wecutil.1
 ./debian/man/winrm.1
 ./debian/man/winrs.1
 ./debian/patches/0001-Upstream-calls-pykerberos-kerberos-we-want-the-defau.patch
 ./debian/patches/0002-Fix-tests.patch
 ./debian/patches/0003-Wecutil-Argument-Bug.patch
 ./debian/patches/series
 ./debian/python-txwinrm.install
 ./debian/rules
 ./debian/source/format
 ./debian/tests/control
 ./debian/txwinrm.install
 ./debian/txwinrm.lintian-overrides
 ./debian/txwinrm.manpages
 ./debian/watch
 ./docs/ms_tools.md
 ./examples/config.ini
 ./examples/jenkins.sh
 ./examples/krb5.conf
 ./txwinrm/_zenclient.py
 ./txwinrm/request/command.xml
 ./txwinrm/request/create.xml
 ./txwinrm/request/delete.xml
 ./txwinrm/request/enum_shells.xml
 ./txwinrm/request/enumerate.xml
 ./txwinrm/request/event_pull.xml
 ./txwinrm/request/pull.xml
 ./txwinrm/request/pull_shells.xml
 ./txwinrm/request/receive.xml
 ./txwinrm/request/send.xml
 ./txwinrm/request/signal.xml
 ./txwinrm/request/subscribe.xml
 ./txwinrm/request/unsubscribe.xml
 ./txwinrm/test/data/server_2003/Win32_ComputerSystem.properties
 ./txwinrm/test/data/server_2003/Win32_ComputerSystem_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_ComputerSystem_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_IP4RouteTable.properties
 ./txwinrm/test/data/server_2003/Win32_IP4RouteTable_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_IP4RouteTable_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_LogicalDisk.properties
 ./txwinrm/test/data/server_2003/Win32_LogicalDisk_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_LogicalDisk_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_NetworkAdapterConfiguration.properties
 ./txwinrm/test/data/server_2003/Win32_NetworkAdapterConfiguration_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_NetworkAdapterConfiguration_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_OperatingSystem.properties
 ./txwinrm/test/data/server_2003/Win32_OperatingSystem_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_OperatingSystem_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfDisk_PhysicalDisk.properties
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfDisk_PhysicalDisk_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfDisk_PhysicalDisk_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfProc_Process.properties
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfProc_Process_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_PerfProc_Process_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_Tcpip_NetworkInterface.properties
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_Tcpip_NetworkInterface_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_PerfRawData_Tcpip_NetworkInterface_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_Process.properties
 ./txwinrm/test/data/server_2003/Win32_Process_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_Process_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_Processor.properties
 ./txwinrm/test/data/server_2003/Win32_Processor_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_Processor_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_Service.properties
 ./txwinrm/test/data/server_2003/Win32_Service_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_Service_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_SystemEnclosure.properties
 ./txwinrm/test/data/server_2003/Win32_SystemEnclosure_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_SystemEnclosure_star_000.xml
 ./txwinrm/test/data/server_2003/Win32_Volume.properties
 ./txwinrm/test/data/server_2003/Win32_Volume_all_000.xml
 ./txwinrm/test/data/server_2003/Win32_Volume_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_ComputerSystem.properties
 ./txwinrm/test/data/server_2008/Win32_ComputerSystem_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_ComputerSystem_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_IP4RouteTable.properties
 ./txwinrm/test/data/server_2008/Win32_IP4RouteTable_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_IP4RouteTable_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_IP4RouteTable_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_IP4RouteTable_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_LogicalDisk.properties
 ./txwinrm/test/data/server_2008/Win32_LogicalDisk_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_LogicalDisk_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_LogicalDisk_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_LogicalDisk_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_NetworkAdapterConfiguration.properties
 ./txwinrm/test/data/server_2008/Win32_NetworkAdapterConfiguration_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_NetworkAdapterConfiguration_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_NetworkAdapterConfiguration_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_NetworkAdapterConfiguration_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_OperatingSystem.properties
 ./txwinrm/test/data/server_2008/Win32_OperatingSystem_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_OperatingSystem_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfDisk_PhysicalDisk.properties
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfDisk_PhysicalDisk_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfDisk_PhysicalDisk_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfDisk_PhysicalDisk_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfDisk_PhysicalDisk_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process.properties
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_PerfProc_Process_star_002.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_Tcpip_NetworkInterface.properties
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_Tcpip_NetworkInterface_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_Tcpip_NetworkInterface_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_Tcpip_NetworkInterface_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_PerfRawData_Tcpip_NetworkInterface_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_Process.properties
 ./txwinrm/test/data/server_2008/Win32_Process_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_Process_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_Process_all_002.xml
 ./txwinrm/test/data/server_2008/Win32_Process_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Process_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_Process_star_002.xml
 ./txwinrm/test/data/server_2008/Win32_Processor.properties
 ./txwinrm/test/data/server_2008/Win32_Processor_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_Processor_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Product.properties
 ./txwinrm/test/data/server_2008/Win32_Product_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_Product_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_Product_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Product_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_Service.properties
 ./txwinrm/test/data/server_2008/Win32_Service_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_Service_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_Service_all_002.xml
 ./txwinrm/test/data/server_2008/Win32_Service_all_003.xml
 ./txwinrm/test/data/server_2008/Win32_Service_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Service_star_001.xml
 ./txwinrm/test/data/server_2008/Win32_Service_star_002.xml
 ./txwinrm/test/data/server_2008/Win32_Service_star_003.xml
 ./txwinrm/test/data/server_2008/Win32_SystemEnclosure.properties
 ./txwinrm/test/data/server_2008/Win32_SystemEnclosure_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_SystemEnclosure_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Volume.properties
 ./txwinrm/test/data/server_2008/Win32_Volume_all_000.xml
 ./txwinrm/test/data/server_2008/Win32_Volume_all_001.xml
 ./txwinrm/test/data/server_2008/Win32_Volume_star_000.xml
 ./txwinrm/test/data/server_2008/Win32_Volume_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_ComputerSystem.properties
 ./txwinrm/test/data/server_2012/Win32_ComputerSystem_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_ComputerSystem_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_IP4RouteTable.properties
 ./txwinrm/test/data/server_2012/Win32_IP4RouteTable_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_IP4RouteTable_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_IP4RouteTable_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_IP4RouteTable_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_LogicalDisk.properties
 ./txwinrm/test/data/server_2012/Win32_LogicalDisk_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_LogicalDisk_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_NetworkAdapterConfiguration.properties
 ./txwinrm/test/data/server_2012/Win32_NetworkAdapterConfiguration_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_NetworkAdapterConfiguration_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_NetworkAdapterConfiguration_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_NetworkAdapterConfiguration_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_OperatingSystem.properties
 ./txwinrm/test/data/server_2012/Win32_OperatingSystem_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_OperatingSystem_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfDisk_PhysicalDisk.properties
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfDisk_PhysicalDisk_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfDisk_PhysicalDisk_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfDisk_PhysicalDisk_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfDisk_PhysicalDisk_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process.properties
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_all_002.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_PerfProc_Process_star_002.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_Tcpip_NetworkInterface.properties
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_Tcpip_NetworkInterface_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_Tcpip_NetworkInterface_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_Tcpip_NetworkInterface_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_PerfRawData_Tcpip_NetworkInterface_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_Process.properties
 ./txwinrm/test/data/server_2012/Win32_Process_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_Process_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_Process_all_002.xml
 ./txwinrm/test/data/server_2012/Win32_Process_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Process_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_Process_star_002.xml
 ./txwinrm/test/data/server_2012/Win32_Processor.properties
 ./txwinrm/test/data/server_2012/Win32_Processor_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_Processor_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Product.properties
 ./txwinrm/test/data/server_2012/Win32_Product_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_Product_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_Product_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Product_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_Service.properties
 ./txwinrm/test/data/server_2012/Win32_Service_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_Service_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_Service_all_002.xml
 ./txwinrm/test/data/server_2012/Win32_Service_all_003.xml
 ./txwinrm/test/data/server_2012/Win32_Service_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Service_star_001.xml
 ./txwinrm/test/data/server_2012/Win32_Service_star_002.xml
 ./txwinrm/test/data/server_2012/Win32_Service_star_003.xml
 ./txwinrm/test/data/server_2012/Win32_SystemEnclosure.properties
 ./txwinrm/test/data/server_2012/Win32_SystemEnclosure_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_SystemEnclosure_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Volume.properties
 ./txwinrm/test/data/server_2012/Win32_Volume_all_000.xml
 ./txwinrm/test/data/server_2012/Win32_Volume_all_001.xml
 ./txwinrm/test/data/server_2012/Win32_Volume_star_000.xml
 ./txwinrm/test/data/server_2012/Win32_Volume_star_001.xml
 ./txwinrm/test/data_error/max_concurrent.xml
 ./txwinrm/test/data_shell/command_resp.xml
 ./txwinrm/test/data_shell/create_resp.xml
 ./txwinrm/test/data_shell/receive_resp_01.xml
 ./txwinrm/test/data_shell/receive_resp_02.xml
 ./txwinrm/test/data_subscribe/pull_resp_01.xml
 ./txwinrm/test/data_subscribe/pull_resp_02.xml
 ./txwinrm/test/data_subscribe/subscribe_resp.xml
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./scripts/genkrb5conf
 ./scripts/typeperf
 ./scripts/wecutil
 ./scripts/winrm
 ./scripts/winrs
 ./txwinrm/__init__.py
 ./txwinrm/app.py
 ./txwinrm/associate.py
 ./txwinrm/collect.py
 ./txwinrm/constants.py
 ./txwinrm/enumerate.py
 ./txwinrm/genkrb5conf.py
 ./txwinrm/krb5.py
 ./txwinrm/shell.py
 ./txwinrm/subscribe.py
 ./txwinrm/test/__init__.py
 ./txwinrm/test/complex
 ./txwinrm/test/cover
 ./txwinrm/test/fetch.py
 ./txwinrm/test/missing
 ./txwinrm/test/precommit
 ./txwinrm/test/test_app.py
 ./txwinrm/test/test_enumerate.py
 ./txwinrm/test/test_shell.py
 ./txwinrm/test/test_subscribe.py
 ./txwinrm/test/test_util.py
 ./txwinrm/test/tools.py
 ./txwinrm/typeperf.py
 ./txwinrm/util.py
 ./txwinrm/wecutil.py
 ./txwinrm/winrm.py
 ./txwinrm/winrs.py
Copyright: Zenoss, Inc. 2013
License: UNKNOWN
 FIXME

Files: ./txwinrm/SessionManager.py
 ./txwinrm/WinRMClient.py
Copyright: Zenoss, Inc. 2016-2017
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: Zenoss, Inc. 2013
License: GPL-2
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2013-2017, Zenoss, Inc. <dbouchillon@zenoss.com>
  2017, Christopher Hoskin <mans0954@debian.org>
License: GPL-2+
 FIXME

Files: ./gpl-2.0.txt
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Update Standards-Version from 4.0.0 to 4.1.1
License: UNKNOWN
 FIXME

Files: ./setup.py
Copyright: Zenoss, Inc. 2013-2017
License: UNKNOWN
 FIXME

Files: ./txwinrm/twisted_utils.py
Copyright: Zenoss, Inc. 2016
License: UNKNOWN
 FIXME

Files: ./txwinrm/test/test_connection_info.py
Copyright: Zenoss, Inc. 2017
License: UNKNOWN
 FIXME

