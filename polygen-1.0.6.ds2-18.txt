Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Changes
 ./README
 ./README.win32
 ./debian/NEWS
 ./debian/README.Debian
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/install-grammars
 ./debian/man.grm
 ./debian/patches/01-dont-regenerate-existing-ofiles.diff
 ./debian/patches/02-grammar-typos.patch
 ./debian/patches/03-makefile.diff
 ./debian/patches/04-create-mode.diff
 ./debian/patches/05-8bit-strings.diff
 ./debian/patches/06-typo-fix.diff
 ./debian/patches/07-fix-ocaml-4.05.patch
 ./debian/patches/series
 ./debian/polygen-data.dirs
 ./debian/polygen-data.docs
 ./debian/polygen-data.install
 ./debian/polygen-data.links
 ./debian/polygen-data.manpages
 ./debian/polygen-data.postinst
 ./debian/polygen.docs
 ./debian/polygen.install
 ./debian/polygen.manpages
 ./debian/polyrun
 ./debian/polyrun.1
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./eng/HOWTO-Refman.html
 ./grm/eng/artex.grm
 ./grm/eng/bio.grm
 ./grm/eng/boyband.grm
 ./grm/eng/chiname.grm
 ./grm/eng/debian/autopolygen.grm
 ./grm/eng/debian/cappuccino.grm
 ./grm/eng/debian/compileline.grm
 ./grm/eng/debian/complaints.grm
 ./grm/eng/debian/dplquestion.grm
 ./grm/eng/debian/flame.grm
 ./grm/eng/debian/gr.grm
 ./grm/eng/debian/mao.grm
 ./grm/eng/debian/nm.grm
 ./grm/eng/debian/rcstalk.grm
 ./grm/eng/debian/sorry.grm
 ./grm/eng/debian/test.grm
 ./grm/eng/debian/thanks.grm
 ./grm/eng/debian/thanksforpackaging.grm
 ./grm/eng/debian/unixmeme.grm
 ./grm/eng/debian/you-know.grm
 ./grm/eng/designpatterns.grm
 ./grm/eng/fernanda.grm
 ./grm/eng/genius.grm
 ./grm/eng/man.grm
 ./grm/eng/manager.grm
 ./grm/eng/metal.grm
 ./grm/eng/ms.grm
 ./grm/eng/nipponame.grm
 ./grm/eng/paper.grm
 ./grm/eng/paper2.grm
 ./grm/eng/payoff.grm
 ./grm/eng/pornsite.grm
 ./grm/eng/pythoniser.grm
 ./grm/eng/rappaz.grm
 ./grm/eng/reviews.grm
 ./grm/eng/videogames.grm
 ./grm/fra/grandeur.grm
 ./grm/ita/GATA.grm
 ./grm/ita/action.grm
 ./grm/ita/aldo.grm
 ./grm/ita/allmusic.grm
 ./grm/ita/amiG.grm
 ./grm/ita/amore.grm
 ./grm/ita/annunci.grm
 ./grm/ita/antani.grm
 ./grm/ita/autobus.grm
 ./grm/ita/b-film.grm
 ./grm/ita/basket.grm
 ./grm/ita/beautiful.grm
 ./grm/ita/beghelli.grm
 ./grm/ita/bio.grm
 ./grm/ita/bloccotraffico.grm
 ./grm/ita/blues.grm
 ./grm/ita/bofh.grm
 ./grm/ita/bruce.grm
 ./grm/ita/calciatori.grm
 ./grm/ita/canipericolosi.grm
 ./grm/ita/cartoons.grm
 ./grm/ita/cavallogoloso.grm
 ./grm/ita/chat.grm
 ./grm/ita/chicoypaco.grm
 ./grm/ita/clerasil.grm
 ./grm/ita/coatti.grm
 ./grm/ita/cocktail.grm
 ./grm/ita/comunilombardi.grm
 ./grm/ita/concerto.grm
 ./grm/ita/contursi.grm
 ./grm/ita/covo.grm
 ./grm/ita/cug.grm
 ./grm/ita/delonghi.grm
 ./grm/ita/dezan.grm
 ./grm/ita/diablo.grm
 ./grm/ita/disclaimer.grm
 ./grm/ita/diz.grm
 ./grm/ita/djfrancesco.grm
 ./grm/ita/duplo.grm
 ./grm/ita/enrica.grm
 ./grm/ita/epigrafi.grm
 ./grm/ita/fantasyfanfiction.grm
 ./grm/ita/farmaci.grm
 ./grm/ita/farmagen.grm
 ./grm/ita/fiaba.grm
 ./grm/ita/fiera.grm
 ./grm/ita/filippi.grm
 ./grm/ita/flavia.grm
 ./grm/ita/gamesrece.grm
 ./grm/ita/genio.grm
 ./grm/ita/genitoriamici.grm
 ./grm/ita/gestionale.grm
 ./grm/ita/ghezzi.grm
 ./grm/ita/girotondi.grm
 ./grm/ita/gleba.grm
 ./grm/ita/gourmet.grm
 ./grm/ita/governatore.grm
 ./grm/ita/gusto.grm
 ./grm/ita/haiku.grm
 ./grm/ita/hollywood.grm
 ./grm/ita/housemusic.grm
 ./grm/ita/idag.grm
 ./grm/ita/if.grm
 ./grm/ita/infocamere.grm
 ./grm/ita/insulti.grm
 ./grm/ita/kamasutra.grm
 ./grm/ita/kassander.grm
 ./grm/ita/koan.grm
 ./grm/ita/levissima.grm
 ./grm/ita/lovecraft.grm
 ./grm/ita/maildisclaimer.grm
 ./grm/ita/man.grm
 ./grm/ita/manager.grm
 ./grm/ita/manback.grm
 ./grm/ita/maremmamaiala.grm
 ./grm/ita/meliconi.grm
 ./grm/ita/melissap.grm
 ./grm/ita/mike.grm
 ./grm/ita/misssarajevo.grm
 ./grm/ita/monte.grm
 ./grm/ita/nico.grm
 ./grm/ita/nomiemiliani.grm
 ./grm/ita/ogm.grm
 ./grm/ita/oop.grm
 ./grm/ita/ordinazione.grm
 ./grm/ita/oroscopo.grm
 ./grm/ita/panino.grm
 ./grm/ita/papertitle.grm
 ./grm/ita/payoff.grm
 ./grm/ita/piani.grm
 ./grm/ita/pieroa.grm
 ./grm/ita/pizzul.grm
 ./grm/ita/png.grm
 ./grm/ita/polygen.grm
 ./grm/ita/ponti.grm
 ./grm/ita/poster.grm
 ./grm/ita/professionisti.grm
 ./grm/ita/prov.grm
 ./grm/ita/recensioni.grm
 ./grm/ita/recensionidischi.grm
 ./grm/ita/recensioniindie.grm
 ./grm/ita/recensionilibri.grm
 ./grm/ita/recensionivg.grm
 ./grm/ita/ricette.grm
 ./grm/ita/ristoranticinesi.grm
 ./grm/ita/sagra.grm
 ./grm/ita/secante.grm
 ./grm/ita/silvio.grm
 ./grm/ita/snakeoil.grm
 ./grm/ita/startrek.grm
 ./grm/ita/sttng.grm
 ./grm/ita/studioaperto.grm
 ./grm/ita/supereroi.grm
 ./grm/ita/teen.grm
 ./grm/ita/teen2.grm
 ./grm/ita/telefonia.grm
 ./grm/ita/tormentoni.grm
 ./grm/ita/trameromantiche.grm
 ./grm/ita/trial.grm
 ./grm/ita/trolleggi.grm
 ./grm/ita/uforobot.grm
 ./grm/ita/unieuro.grm
 ./grm/ita/verdena.grm
 ./grm/ita/vision-x.grm
 ./grm/ita/visionbello.grm
 ./grm/ita/zen.grm
 ./ita/HOWTO-Refman.html
 ./src/DATE
 ./src/README
 ./src/VERSION
 ./src/absyn.ml
 ./src/check.ml
 ./src/env.ml
 ./src/err.ml
 ./src/fake.ml
 ./src/gen.ml
 ./src/io.ml
 ./src/lexer.mll
 ./src/load.ml
 ./src/load.mli
 ./src/main.ml
 ./src/makefile
 ./src/parser.mly
 ./src/pre.ml
 ./src/prelude.ml
 ./src/test.grm
 ./src/ver.ml.template
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./grm/eng/photoshop.grm
 ./grm/ita/lotto.grm
Copyright: NONE
License: Beerware
 FIXME

Files: ./Credits
Copyright: NONE
License: GPL
 FIXME

Files: ./debian/make_polygen-data_manpage
Copyright: (C) 2005 Enrico Zini <enrico@debian.org>
License: GPL-2+
 FIXME

Files: ./License
Copyright: 2002, -03 Alvise Spano'
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: 2002, -03 Alvise SpanÃ²
  of their respective authors, as indicated
License: GPL-2+
 FIXME

Files: ./debian/changelog
Copyright: bastien Villemot <sebastien@debian.org> Wed, 27 Sep 2017 14:19:22 +0200
  phane Glondu.
License: UNKNOWN
 FIXME

Files: ./grm/eng/debian/evan.grm
Copyright: miaire"
License: UNKNOWN
 FIXME

