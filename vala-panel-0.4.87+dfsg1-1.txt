Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.clang-format
 ./.configure-custom.sh
 ./CMakeLists.txt
 ./README.md
 ./app/CMakeLists.txt
 ./app/app-about.ui
 ./app/app-pref.ui
 ./app/application.h
 ./app/vala-panel.gresource.xml
 ./applets/CMakeLists.txt
 ./applets/core/CMakeLists.txt
 ./applets/core/clock/CMakeLists.txt
 ./applets/core/clock/org.valapanel.clock.desktop.plugin.in
 ./applets/core/dirmenu/CMakeLists.txt
 ./applets/core/dirmenu/org.valapanel.dirmenu.desktop.plugin.in
 ./applets/core/kbled/CMakeLists.txt
 ./applets/core/kbled/kbled.gresource.xml
 ./applets/core/kbled/org.valapanel.kbled.desktop.plugin.in
 ./applets/core/launchbar/CMakeLists.txt
 ./applets/core/launchbar/config.ui
 ./applets/core/launchbar/launchbar.gresource.xml
 ./applets/core/launchbar/org.valapanel.launchbar.desktop.plugin.in
 ./applets/core/menumodel/CMakeLists.txt
 ./applets/core/menumodel/menumodel.gresource.xml
 ./applets/core/menumodel/org.valapanel.menumodel.desktop.plugin.in
 ./applets/core/menumodel/system-menus.ui
 ./applets/core/separator/CMakeLists.txt
 ./applets/core/separator/org.valapanel.separator.desktop.plugin.in
 ./applets/drawing/CMakeLists.txt
 ./applets/drawing/cpu/CMakeLists.txt
 ./applets/drawing/cpu/org.valapanel.cpu.desktop.plugin.in
 ./applets/drawing/monitors/CMakeLists.txt
 ./applets/drawing/monitors/org.valapanel.monitors.desktop.plugin.in
 ./applets/drawing/netmon/CMakeLists.txt
 ./applets/drawing/netmon/org.valapanel.netmon.desktop.plugin.in
 ./applets/wnck/CMakeLists.txt
 ./applets/wnck/buttons/CMakeLists.txt
 ./applets/wnck/buttons/org.valapanel.buttons.desktop.plugin.in
 ./applets/wnck/deskno/CMakeLists.txt
 ./applets/wnck/deskno/org.valapanel.deskno.desktop.plugin.in
 ./applets/wnck/pager/CMakeLists.txt
 ./applets/wnck/pager/org.valapanel.pager.desktop.plugin.in
 ./applets/wnck/tasklist/CMakeLists.txt
 ./applets/wnck/tasklist/config.ui
 ./applets/wnck/tasklist/org.xfce.tasklist.desktop.plugin.in
 ./applets/wnck/tasklist/tasklist-xfce.vapi
 ./applets/wnck/wincmd/CMakeLists.txt
 ./applets/wnck/wincmd/org.valapanel.wincmd.desktop.plugin.in
 ./cmake/FallbackVersion.cmake.in
 ./configure
 ./data/CMakeLists.txt
 ./data/css/vala-panel.css
 ./data/desktop/org.valapanel.application.appdata.xml.in
 ./data/desktop/org.valapanel.application.desktop.in
 ./data/gschemas/CMakeLists.txt
 ./data/gschemas/org.valapanel.X.gschema.xml
 ./data/gschemas/org.valapanel.builtin.gschema.xml
 ./data/gschemas/org.valapanel.gschema.xml
 ./data/gschemas/org.valapanel.plugins.gschema.xml
 ./data/gschemas/org.valapanel.toplevel.gschema.xml
 ./data/images/CMakeLists.txt
 ./data/images/panel.svg
 ./data/man/CMakeLists.txt
 ./data/man/vala-panel-runner.1
 ./data/man/vala-panel.1
 ./data/profiles/CMakeLists.txt
 ./data/profiles/appmenu
 ./data/profiles/default.in
 ./data/profiles/gnome
 ./data/profiles/two_panels
 ./data/ui/new-plugin.ui
 ./data/ui/panel-menus.ui
 ./data/ui/plugin-about.ui
 ./data/ui/pref.ui
 ./debian/compat
 ./debian/control
 ./debian/libvalapanel-dev.install
 ./debian/libvalapanel0.install
 ./debian/libvalapanel0.symbols
 ./debian/patches/2001_no-GitVersion-for-Debian-packaging.patch
 ./debian/patches/README
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/vala-panel-common.install
 ./debian/vala-panel-plugins-base.install
 ./debian/vala-panel-plugins-wnck.install
 ./debian/vala-panel.install
 ./debian/watch
 ./po/CMakeLists.txt
 ./po/LINGUAS.in
 ./po/aa.po
 ./po/ab.po
 ./po/ae.po
 ./po/ak.po
 ./po/am.po
 ./po/an.po
 ./po/as.po
 ./po/ast.po
 ./po/av.po
 ./po/ay.po
 ./po/az.po
 ./po/ba.po
 ./po/bh.po
 ./po/bi.po
 ./po/bm.po
 ./po/bo.po
 ./po/br.po
 ./po/bs.po
 ./po/ce.po
 ./po/ch.po
 ./po/ckb.po
 ./po/co.po
 ./po/cr.po
 ./po/cu.po
 ./po/cv.po
 ./po/cy.po
 ./po/dv.po
 ./po/dz.po
 ./po/ee.po
 ./po/en_AU.po
 ./po/en_CA.po
 ./po/en_GB.po
 ./po/eo.po
 ./po/ff.po
 ./po/fj.po
 ./po/fr_CA.po
 ./po/fy.po
 ./po/ga.po
 ./po/gd.po
 ./po/gn.po
 ./po/gu.po
 ./po/gv.po
 ./po/ha.po
 ./po/hi.po
 ./po/ho.po
 ./po/ht.po
 ./po/hy.po
 ./po/hz.po
 ./po/ia.po
 ./po/ie.po
 ./po/ig.po
 ./po/ii.po
 ./po/ik.po
 ./po/io.po
 ./po/iu.po
 ./po/jv.po
 ./po/ka.po
 ./po/kg.po
 ./po/ki.po
 ./po/kj.po
 ./po/kl.po
 ./po/kn.po
 ./po/kr.po
 ./po/ks.po
 ./po/ku.po
 ./po/kv.po
 ./po/kw.po
 ./po/ky.po
 ./po/la.po
 ./po/lb.po
 ./po/li.po
 ./po/ln.po
 ./po/lo.po
 ./po/lu.po
 ./po/lv.po
 ./po/mg.po
 ./po/mh.po
 ./po/mi.po
 ./po/mk.po
 ./po/mn.po
 ./po/mo.po
 ./po/mr.po
 ./po/mt.po
 ./po/my.po
 ./po/na.po
 ./po/nd.po
 ./po/ne.po
 ./po/ng.po
 ./po/nr.po
 ./po/nv.po
 ./po/ny.po
 ./po/oc.po
 ./po/oj.po
 ./po/om.po
 ./po/or.po
 ./po/os.po
 ./po/pi.po
 ./po/qu.po
 ./po/rm.po
 ./po/rn.po
 ./po/rue.po
 ./po/rw.po
 ./po/sa.po
 ./po/sc.po
 ./po/sd.po
 ./po/se.po
 ./po/sg.po
 ./po/si.po
 ./po/sm.po
 ./po/sma.po
 ./po/sn.po
 ./po/so.po
 ./po/sq.po
 ./po/ss.po
 ./po/st.po
 ./po/su.po
 ./po/sw.po
 ./po/ta.po
 ./po/tg.po
 ./po/ti.po
 ./po/tk.po
 ./po/tl.po
 ./po/tn.po
 ./po/to.po
 ./po/ts.po
 ./po/tt.po
 ./po/tw.po
 ./po/ty.po
 ./po/uz.po
 ./po/ve.po
 ./po/vo.po
 ./po/wa.po
 ./po/wo.po
 ./po/xh.po
 ./po/yi.po
 ./po/yo.po
 ./po/za.po
 ./po/zh.po
 ./po/zu.po
 ./rpm/vala-panel.spec.in
 ./runner/CMakeLists.txt
 ./runner/app-runner.ui
 ./runner/runner.gresource.xml
 ./ui/CMakeLists.txt
 ./ui/applet-widget-api.h
 ./ui/applet-widget.c
 ./ui/applet-widget.h
 ./ui/libvalapanel.gresource.xml
 ./ui/private.h
 ./ui/vala-panel-enums.c.template
 ./ui/vala-panel-enums.h.template
 ./ui/vala-panel.deps
 ./ui/vala-panel.vapi
 ./util/CMakeLists.txt
 ./util/config.h.in
 ./util/gtk/CMakeLists.txt
 ./util/gtk/vala-panel-util-enums.c.template
 ./util/gtk/vala-panel-util-enums.h.template
 ./vala-panel.pc.cmake
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./app/application.c
 ./app/vala-panel-platform-standalone-x11.c
 ./app/vala-panel-platform-standalone-x11.h
 ./applets/core/clock/clock.vala
 ./applets/core/dirmenu/dirmenu.vala
 ./applets/core/kbled/kbled.vala
 ./applets/core/launchbar/launchbar-button.vala
 ./applets/core/launchbar/launchbar-config.vala
 ./applets/core/launchbar/launchbar.vala
 ./applets/core/menumodel/menu-maker.vala
 ./applets/core/menumodel/menumodel.vala
 ./applets/core/separator/separator.vala
 ./applets/drawing/cpu/cpu.c
 ./applets/drawing/cpu/cpu.h
 ./applets/drawing/monitors/cpu.c
 ./applets/drawing/monitors/cpu.h
 ./applets/drawing/monitors/mem.c
 ./applets/drawing/monitors/mem.h
 ./applets/drawing/monitors/monitor.c
 ./applets/drawing/monitors/monitor.h
 ./applets/drawing/monitors/monitors.c
 ./applets/drawing/monitors/monitors.h
 ./applets/drawing/monitors/swap.c
 ./applets/drawing/monitors/swap.h
 ./applets/drawing/netmon/monitor.c
 ./applets/drawing/netmon/monitor.h
 ./applets/drawing/netmon/monitors.c
 ./applets/drawing/netmon/monitors.h
 ./applets/drawing/netmon/net.c
 ./applets/drawing/netmon/net.h
 ./applets/wnck/buttons/buttons.vala
 ./applets/wnck/deskno/deskno.vala
 ./applets/wnck/pager/pager.vala
 ./applets/wnck/tasklist/tasklist-xfce.vala
 ./applets/wnck/wincmd/wincmd.vala
 ./runner/runner-app.c
 ./runner/runner-app.h
 ./runner/runner.c
 ./runner/runner.h
 ./ui/applet-holder.vala
 ./ui/applet-info.c
 ./ui/applet-info.h
 ./ui/applet-manager.c
 ./ui/applet-manager.h
 ./ui/applet-plugin.c
 ./ui/applet-plugin.h
 ./ui/client.h
 ./ui/definitions.h
 ./ui/panel-layout.c
 ./ui/panel-layout.h
 ./ui/panel-layout.vala
 ./ui/panel-platform.c
 ./ui/panel-platform.h
 ./ui/server.h
 ./ui/settings-manager.c
 ./ui/settings-manager.h
 ./ui/toplevel-config.c
 ./ui/toplevel-config.h
 ./ui/toplevel.c
 ./ui/toplevel.h
 ./util/boxed-wrapper.c
 ./util/boxed-wrapper.h
 ./util/constants.h
 ./util/glistmodel-filter.c
 ./util/glistmodel-filter.h
 ./util/gtk/css.c
 ./util/gtk/css.h
 ./util/gtk/generic-config-dialog.c
 ./util/gtk/generic-config-dialog.h
 ./util/gtk/launcher-gtk.c
 ./util/gtk/launcher-gtk.h
 ./util/gtk/menu-maker.c
 ./util/gtk/menu-maker.h
 ./util/gtk/misc-gtk.c
 ./util/gtk/misc-gtk.h
 ./util/gtk/util-gtk.h
 ./util/info-data.c
 ./util/info-data.h
 ./util/launcher.c
 ./util/launcher.h
 ./util/misc.c
 ./util/misc.h
 ./util/util.h
 ./vapi/gio-addons-2.0.vapi
Copyright: 2015, Konstantin Pugin <ria.freelander@gmail.com>
  2015-2016, Konstantin Pugin <ria.freelander@gmail.com>
  2015-2017, Konstantin Pugin <ria.freelander@gmail.com>
  2015-2018, Konstantin Pugin <ria.freelander@gmail.com>
  2017, Konstantin Pugin <ria.freelander@gmail.com>
  2018, Konstantin Pugin <ria.freelander@gmail.com>
License: LGPL-3+
 FIXME

Files: ./po/af.po
 ./po/be.po
 ./po/bg.po
 ./po/bn.po
 ./po/bn_IN.po
 ./po/ca.po
 ./po/de.po
 ./po/el.po
 ./po/fa.po
 ./po/fi.po
 ./po/fo.po
 ./po/gl.po
 ./po/he.po
 ./po/hr.po
 ./po/is.po
 ./po/km.po
 ./po/lg.po
 ./po/lt.po
 ./po/ml.po
 ./po/ms.po
 ./po/nb.po
 ./po/pa.po
 ./po/ps.po
 ./po/ru.po
 ./po/te.po
 ./po/th.po
 ./po/tr.po
 ./po/tt_RU.po
 ./po/ug.po
 ./po/uk.po
 ./po/ur.po
 ./po/ur_PK.po
 ./po/vala-panel.pot
 ./po/vi.po
 ./po/zh_CN.po
 ./po/zh_HK.po
 ./po/zh_TW.po
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/cs.po
 ./po/da.po
 ./po/ja.po
Copyright: 2008-2009, THE lxpanel'S COPYRIGHT HOLDER
  2009, THE lxpanel'S COPYRIGHT HOLDER
  2010, THE lxpanel'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/es.po
 ./po/es_VE.po
 ./po/frp.po
Copyright: 2006, the LXDE team
  2006-2008, the LXDE team
License: UNKNOWN
 FIXME

Files: ./applets/wnck/tasklist/tasklist-widget.c
 ./applets/wnck/tasklist/tasklist-widget.h
Copyright: 2008-2010, Nick Schermer <nick@xfce.org>
License: GPL-2+
 FIXME

Files: ./applets/wnck/tasklist/xfce-arrow-button.c
 ./applets/wnck/tasklist/xfce-arrow-button.h
Copyright: 2006-2007, Jasper Huijsmans <jasper@xfce.org>
  2008-2010, Nick Schermer <nick@xfce.org>
License: LGPL-2.1+
 FIXME

Files: ./debian/copyright
 ./debian/copyright.in
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./po/sr.po
 ./po/sr@latin.po
Copyright: TheÂ
License: UNKNOWN
 FIXME

Files: ./data/images/panel.png
Copyright: :Ñ:]Wqý}ÆøÂuuk¿^±¢JÑ§HÕ
  cg6FOeü§!åÓå
  1A1×ë#.^fF¾Ëý»v}+ßº¹Q°  uuëPSSÍhÎ_gÂQ:½=½Z¿ßÏt«öØëNìiÍG@8Æë¿ûcVFe­@ Þ³ç2J£ÃÑ¥OÁ´¹²·.=)" ónÐØØ8=Sï*>i¦+4
  O¤¢7
  ZQ
  ¦¨%
  õ¢Æ¥!ëxåÕñö;o¡¬LùuÙL¬É«!Çó]`4sµ9¸áù}¹@ØfÙÓ÷a6¯TÀ$yT×QW¿Ñ.]¸ñqéªñïøÙNS5³(Ö
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/scrllock-on.png
Copyright: »÷ãÀAÀ[Ò­PJÅ¥
  BàÇÄÆáä.@
  é!_L ??»éþv½Ç[ÛÆÀØÈÄR}ý£Oçþj"²ÛíÅYãÂ0u¶rYR+I1÷ÏÍä6Kaù
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ­­Ee._¾L[["N§I§Óøý~ZZZenqÌæö&0
  ¯¯'
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/numlock-off.png
Copyright: Î;Ð~õ¯Î|8&&&&eYÂqa¦Èårb}}]är9a¦pGX%&&&Äû§Ç¦*Àà³æ,Úsè!õâÑÑ7>Ií?¤þà4ÃÃÃ$IÂá0
  BàÇÄÆáä.@
  EUOUz(ýÁC!¨æRSê$-ã°$Mlg½Þµ½¯lq*NÌéIï½ùÎÌ÷;3>!/Ó$^²)ÍÃùóç;.«»
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/numlock-on.png
Copyright: 5ÓP,lfMÓ¤»»]×)EdYF$$Ijq^£ë:ÝÝÝ¦É£ òÀvÀ¦i~3y}x<N4%
  BàÇÄÆáä.@
  DÔ®ëø|>
  T	^ÖH 
  ¼L
  BÓ|Eàð%ÐñÊq=66Ö
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ªK¥i9pè!$Ð$©)
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./data/images/cpufreq-icon.png
Copyright: 99V::Z;;Y;;_<<^==`>>_??e??fAAbEEdGGlNNnPPyUUw}~]]~^^~^^bbbbcceeffhhjjllllmmnnqqssssvvyyzz||~~
  §ª«­¯¯°°³·
  ã6mõUÍ6"pa%
License: UNKNOWN
 FIXME

Files: ./po/eu.po
Copyright: 2009, Alain Mendizabal <alainmendi@gmail.com>
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/scrllock-off.png
Copyright: BàÇÄÆáä.@
  D @UUª5¦ÀJ@µ¾ãÊ²L8F$LÓD$üÆN¹üT&5ËËË¼ÉÇ÷l#Ë2;ff£9 v¥7
  EUWUlº(ý`Á¢(¡IÈ¢)
  V«!Ú>ÞØàÑú:Ö×166F£!¨V«Äãq4M#uhHé.£ÜP÷¾=zdË²
  x4ÙëñQs-ªõ]×±6<^Ï~1{/;Æó÷
  [EUU@ÛB6
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  ÌÀ	Ïèè(CCCÔj5jµ®ëâñxðz½x½^æïóÌÝùãÊÚ£âÀÃÖkO)jU.´Á©Ão÷J¥(J{H Fjµ$I¤R)Z
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/capslock-on.png
Copyright: BàÇÄÆáä.@
  DQ@f¤Ê«ïn©öïk§³³¿ß¦i¬­­H$bdôWgÓÃ7ÆO®>^¿<x)@65ú¶Pv6îutttÐÜÜL.#ËaYË
  TÊ¨ªªB$,Ë¢··~<Ç·F0ðµby³T¬Ü¾1þ©â¾ô³ªª<xD"ÁÊÊ­7ç©le2X
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ©mzA4ÚôÎ¦1½ñhÓ¤5¦MzÑ°1i´Òb$Ñ^¬ü
  ©É¦innn£ù2Ün7¦Q!oð Uå5°²(Çã±[óêÕ«6Àùóç9räcccö»/¢(
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
  ØTUºº:²Ù,BËÜ¼y¿ßl6K]]ªªÚÖ,ê¼'ÀYwy¿ß½«Ã0ðz½X
License: UNKNOWN
 FIXME

Files: ./applets/core/kbled/images/capslock-off.png
Copyright: BàÇÄÆáä.@
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ©)2
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  ¹U«ñ°(rm¸ÓÚmÃò3ðc{øö¿mff&ØÄiàmà(0lóÖl?Å|ÐàwàÉÓ«³
  Ó¥mÛÃa4M£Gî ñ®WÔôê(B$Á4M¦iú	¶¶¶ØÚÚê"»P(1MH$¢(Ô.må®êÍª
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./data/images/background.png
Copyright: BàÇÄÆáä.@
  ¼L
  4H#ÉÚdk²9, +È
  ¡9C3J3W³Róf?ãqøtN	ç(§ó~Þï)â)¦4L¹1ekªX«H«Q«Gë½6®í§¦½E»YûAÇJ''GgÎçSÙSÝ§
  ª¦ªÞª
  «Ä¹dÏfÒféæÞ-[ªæn
  ·°åØÛ®¶m¶}agbg·Å®Ãî½}º}ý=
  Ô«UªPëSSg©;¨ªg¨oT?¤~YýYÃLÃOC¤Q
License: UNKNOWN
 FIXME

Files: ./po/it.po
Copyright: 2009, Fabio Barone <phonky at gmx.net>
License: UNKNOWN
 FIXME

Files: ./po/sl.po
Copyright: 2011-2013, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

Files: ./po/ko.po
Copyright: 2009-2012, Frer software foundation Inc.
License: UNKNOWN
 FIXME

Files: ./po/nn.po
Copyright: 2009, Karl Ove Hufthammer <karl@huftis.org>
License: UNKNOWN
 FIXME

Files: ./po/et.po
Copyright: 2008, LXDE Desktop
License: UNKNOWN
 FIXME

Files: ./po/sv.po
Copyright: 2009, Martin Bagge <brother@bsnet.se>
License: UNKNOWN
 FIXME

Files: ./po/fr.po
Copyright: 2006-2008, the LXDE team
  Mauchin <zebob.m@pengzone.org>, 2008.
License: UNKNOWN
 FIXME

Files: ./po/ro.po
Copyright: 2012, THE LXPanel'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/ar.po
Copyright: 2009, THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/pt.po
Copyright: 2009, THE lxpanel'S COPYRIGHT HOLDER
  rgio Marques <smarquespt@gmail.com> 2010-2014
License: UNKNOWN
 FIXME

Files: ./po/hu.po
Copyright: 2006-2010, The LXDE Team
License: UNKNOWN
 FIXME

Files: ./po/pt_BR.po
Copyright: 2006, The LXDE Team.
  rgio Cipolla <secipolla@gmail.com>, 2010-2011.
License: UNKNOWN
 FIXME

Files: ./po/kk.po
Copyright: 2012, The LXDE team
License: UNKNOWN
 FIXME

Files: ./po/id.po
Copyright: YEAR THE lxpanel'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: attributions.
License: UNKNOWN
 FIXME

Files: ./po/sk.po
Copyright: 2006, lxde team
License: UNKNOWN
 FIXME

Files: ./po/nl.po
Copyright: the author of the lxpanel package.
License: UNKNOWN
 FIXME

Files: ./po/pl.po
Copyright: 2006, the lxde team
License: UNKNOWN
 FIXME

