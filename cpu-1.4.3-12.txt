Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./Makefile.am
 ./NEWS
 ./README
 ./TODO
 ./configure.ac
 ./contrib/postaddscript.sh
 ./contrib/report.sh
 ./debian/NEWS
 ./debian/README.Debian
 ./debian/compat
 ./debian/config
 ./debian/control
 ./debian/cpu.docs
 ./debian/dirs
 ./debian/docs
 ./debian/patches/02_dontmesswithetc.patch
 ./debian/patches/04_lvalue.patch
 ./debian/patches/05_allowbadpass.patch
 ./debian/patches/06_manpage.patch
 ./debian/patches/07_kfreebsd-build.patch
 ./debian/patches/08_add-flag-for-O_CREAT.patch
 ./debian/patches/09_build-system.patch
 ./debian/patches/10_support-inetOrgPerson-Schema.patch
 ./debian/patches/11_md5crypt-password-support.patch
 ./debian/patches/12_yes-unimplemented.patch
 ./debian/patches/13_ldap-object-class-violation.patch
 ./debian/patches/series
 ./debian/patches/spelling.patch
 ./debian/patches/unused/01_ldapv3please.patch
 ./debian/patches/unused/03_strlenzero.patch
 ./debian/po/POTFILES.in
 ./debian/po/cs.po
 ./debian/po/de.po
 ./debian/po/fr.po
 ./debian/po/nl.po
 ./debian/po/pt_BR.po
 ./debian/postinst
 ./debian/postrm
 ./debian/rules
 ./debian/source/format
 ./debian/templates
 ./debian/watch
 ./doc/Makefile.am
 ./doc/cpu.conf
 ./doc/cpu.conf.doc
 ./doc/man/Makefile.am
 ./doc/man/cpu-ldap.8
 ./doc/man/cpu.8
 ./doc/man/cpu.conf.5
 ./doc/test.ldif
 ./mkinstalldirs
 ./src/Makefile.am
 ./src/include/config.h.in
 ./src/include/util/bitvector.h
 ./src/main/Makefile.am
 ./src/plugins/Makefile.am
 ./src/plugins/ldap/Makefile.am
 ./src/plugins/passwd/Makefile.am
 ./src/util/Makefile.am
 ./src/util/bitvector.c
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/include/conf.h
 ./src/include/debugging.h
 ./src/include/main/cpu.h
 ./src/include/plugins/ldap/ldap.h
 ./src/include/plugins/passwd/passwd.h
 ./src/include/util/cputil.h
 ./src/include/util/dll.h
 ./src/include/util/fileaction.h
 ./src/include/util/hash.h
 ./src/include/util/helper.h
 ./src/include/util/parser.h
 ./src/include/util/password.h
 ./src/include/util/xmem.h
 ./src/main/cpu.c
 ./src/plugins/ldap/cat.c
 ./src/plugins/ldap/commandline.c
 ./src/plugins/ldap/group.c
 ./src/plugins/ldap/ld.c
 ./src/plugins/ldap/ldap.c
 ./src/plugins/ldap/ldap_errors.c
 ./src/plugins/ldap/user.c
 ./src/plugins/passwd/commandline.c
 ./src/plugins/passwd/data.c
 ./src/plugins/passwd/getid.c
 ./src/plugins/passwd/passwd.c
 ./src/util/base64.c
 ./src/util/cgetpwent.c
 ./src/util/dll.c
 ./src/util/hash.c
 ./src/util/helper.c
 ./src/util/parser.c
 ./src/util/xmem.c
Copyright: 2003, Blake Matheny (and other contributing authors)
License: GPL-2+
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./debian/po/vi.po
 ./doc/Makefile.in
 ./doc/man/Makefile.in
 ./src/Makefile.in
 ./src/main/Makefile.in
 ./src/plugins/Makefile.in
 ./src/plugins/ldap/Makefile.in
 ./src/plugins/passwd/Makefile.in
 ./src/util/Makefile.in
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2003, Free Software Foundation, Inc.
  2005, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./aclocal.m4
 ./config.guess
 ./config.sub
 ./depcomp
 ./ltmain.sh
 ./missing
 ./src/include/util/cmd5.h
 ./src/util/md5.c
Copyright: 1992-2003, Free Software Foundation, Inc.
  1995-1996, 1999, Free Software Foundation, Inc.
  1995-1996, Free Software Foundation, Inc.
  1996-1997, 1999-2000, 2002, Free Software Foundation, Inc.
  1996-2001, Free Software Foundation, Inc.
  1996-2002, Free Software Foundation, Inc.
  1999-2000, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./src/include/util/parseconfig.h
 ./src/util/parseconfig.c
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./src/include/util/csha1.h
 ./src/util/sha1.c
Copyright: 1999, Scott G. Miller
  2000, Scott G. Miller
License: UNKNOWN
 FIXME

Files: ./debian/po/fi.po
 ./debian/po/pl.po
Copyright: 2009, This file is distributed under the same license as the cpu package.
License: UNKNOWN
 FIXME

Files: ./debian/po/ru.po
 ./debian/po/templates.pot
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 2003, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: FSFUL and/or GPL-2
 FIXME

Files: ./src/util/fileaction.c
Copyright: 2003, Blake Matheny
License: GPL-2+
 FIXME

Files: ./src/util/getopt.c
Copyright: 1987, 88, 89, 90, 91, 92, 93, 94, 95, 96, 97
License: LGPL-2+
 FIXME

Files: ./src/util/getopt1.c
Copyright: 1987, 88,89,90,91,92,93,94,96,97 Free Software Foundation, Inc.
License: LGPL-2+
 FIXME

Files: ./src/include/util/cgetopt.h
Copyright: 1989, 90,91,92,93,94,96,97 Free Software Foundation, Inc.
License: LGPL-2+
 FIXME

Files: ./debian/changelog
Copyright: -with-old-dh-make-debian-copyright (Lintian).
License: UNKNOWN
 FIXME

Files: ./debian/po/gl.po
Copyright: 2009, Debian
License: UNKNOWN
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, Free Software
License: UNKNOWN
 FIXME

Files: ./debian/po/sv.po
Copyright: 2009, Martin Bagge <brother@bsnet.se>
License: UNKNOWN
 FIXME

Files: ./debian/po/es.po
Copyright: 2003, 2009, Software in the Public Interest
License: UNKNOWN
 FIXME

Files: ./debian/po/sk.po
Copyright: 2012, THE cpu'S COPYRIGHT HOLDER
  preklady pre balÃ­k cpu.
License: UNKNOWN
 FIXME

Files: ./debian/po/it.po
Copyright: 2004, Valentina Commissari <ayor@quaqua.net>.
License: UNKNOWN
 FIXME

Files: ./debian/po/da.po
Copyright: 2012, cpu & nedenstÃ¥ende oversÃ¦ttere.
License: UNKNOWN
 FIXME

Files: ./debian/po/pt.po
Copyright: s do debconf?"
License: UNKNOWN
 FIXME

Files: ./debian/po/ja.po
Copyright: ¥ë¥È¥Ý¡¼¥ÈÃÍ¤Ï 389 ¤Ç¤¹¡£"
License: UNKNOWN
 FIXME

