Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./BUGS
 ./ChangeLog
 ./FAQ.TXT
 ./Makefile.am
 ./NEWS
 ./README.FIRST
 ./THANKS
 ./TODO
 ./VERSION
 ./autogen.sh
 ./config.h.in
 ./configure.ac
 ./debian/README.Debian
 ./debian/clean
 ./debian/control
 ./debian/docs
 ./debian/fim.doc-base
 ./debian/rules
 ./debian/source/format
 ./debian/source/options
 ./debian/tests/control
 ./debian/tests/run-tests
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./distros/Makefile.am
 ./doc/FIM.TXT
 ./doc/FIM.html
 ./doc/Makefile.am
 ./doc/doctags.c
 ./doc/fim-stylesheet.css
 ./doc/fim.man
 ./doc/fim.man.html
 ./doc/fimgs.man
 ./doc/fimgs.man.html
 ./doc/fimrc.man
 ./doc/fimrc.man.html
 ./doc/vim2html.pl
 ./scripts/Makefile.am
 ./scripts/example/oneline.fim
 ./scripts/maintenance/configure-brute-check.sh
 ./scripts/maintenance/configure-live.sh
 ./scripts/maintenance/configure-minimal.sh
 ./scripts/maintenance/configure-noscripting.sh
 ./scripts/maintenance/configure-only-fb-mini.sh
 ./scripts/maintenance/cron-build.sh.in
 ./scripts/maintenance/cron-rsync.sh.in
 ./scripts/maintenance/cron-svndump.pl.in
 ./scripts/maintenance/htmlbody.awk
 ./scripts/maintenance/remote-build.sh.in
 ./scripts/maintenance/yacc2grammar.awk
 ./scripts/maintenance/yacc2grammar.h
 ./scripts/rc/fimrc.eog
 ./scripts/rc/fimrc.gqview
 ./scripts/rc/fimrc.kuickshow
 ./scripts/rc/fimrc.lazy
 ./scripts/tests/font.sh
 ./scripts/tests/maxlenscript.fim
 ./scripts/tests/sanity.fim
 ./scripts/tests/version.sh
 ./scripts/utilities/fimscan.sh
 ./scripts/utilities/screenshot.sh
 ./src/Makefile.am
 ./src/default_font_byte_array.h
 ./src/default_icon_byte_array.h
 ./src/examples.h
 ./src/fimrc
 ./src/grammar.h
 ./src/help-acm.cpp
 ./src/help.cpp
 ./src/testdir/Makefile
 ./src/testdir/test1.in
 ./src/testdir/test1.ok
 ./src/testdir/test10.in
 ./src/testdir/test10.ok
 ./src/testdir/test11.in
 ./src/testdir/test11.ok
 ./src/testdir/test12.in
 ./src/testdir/test12.ok
 ./src/testdir/test13.in
 ./src/testdir/test13.ok
 ./src/testdir/test14.in
 ./src/testdir/test14.ok
 ./src/testdir/test15.in
 ./src/testdir/test15.ok
 ./src/testdir/test16.in
 ./src/testdir/test16.ok
 ./src/testdir/test2.in
 ./src/testdir/test2.ok
 ./src/testdir/test3.in
 ./src/testdir/test3.ok
 ./src/testdir/test4.in
 ./src/testdir/test4.ok
 ./src/testdir/test5.in
 ./src/testdir/test5.ok
 ./src/testdir/test6.in
 ./src/testdir/test6.ok
 ./src/testdir/test7.in
 ./src/testdir/test7.ok
 ./src/testdir/test8.in
 ./src/testdir/test8.ok
 ./src/testdir/test9.in
 ./src/testdir/test9.ok
 ./src/testsuite/config/unix.exp
 ./src/testsuite/fim.test/fim.exp
 ./src/version.h
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/AADevice.cpp
 ./src/AADevice.h
 ./src/Arg.cpp
 ./src/Arg.h
 ./src/Benchmarkable.h
 ./src/Browser.cpp
 ./src/Browser.h
 ./src/CACADevice.cpp
 ./src/CACADevice.h
 ./src/Cache.cpp
 ./src/Cache.h
 ./src/Command.cpp
 ./src/Command.h
 ./src/CommandConsole-cmd.cpp
 ./src/CommandConsole-help.cpp
 ./src/CommandConsole-init.cpp
 ./src/CommandConsole-var.cpp
 ./src/CommandConsole.cpp
 ./src/CommandConsole.h
 ./src/DebugConsole.cpp
 ./src/DebugConsole.h
 ./src/DisplayDevice.cpp
 ./src/DisplayDevice.h
 ./src/DummyDisplayDevice.h
 ./src/FbiStuffMagick.cpp
 ./src/FbiStuffText.cpp
 ./src/FimWindow.cpp
 ./src/FimWindow.h
 ./src/Image.cpp
 ./src/Image.h
 ./src/Imlib2Device.cpp
 ./src/Imlib2Device.h
 ./src/Namespace.cpp
 ./src/Namespace.h
 ./src/SDLDevice.h
 ./src/Var.cpp
 ./src/Var.h
 ./src/Viewport.cpp
 ./src/Viewport.h
 ./src/b2ba.c
 ./src/common.cpp
 ./src/common.h
 ./src/defaultConfiguration.cpp
 ./src/fim.cpp
 ./src/fim.h
 ./src/fim_limits.h
 ./src/fim_plugin.cpp
 ./src/fim_plugin.h
 ./src/fim_stream.cpp
 ./src/fim_stream.h
 ./src/fim_string.cpp
 ./src/fim_string.h
 ./src/fim_types.h
 ./src/fim_wrappers.h
 ./src/interpreter.cpp
 ./src/lex.h
 ./src/lex.lex
 ./src/readline.cpp
 ./src/readline.h
 ./src/yacc.ypp
Copyright: 2007-2013, Michele Martone
  2007-2015, Michele Martone
  2007-2016, Michele Martone
  2007-2017, Michele Martone
  2008-2013, Michele Martone
  2008-2015, Michele Martone
  2008-2016, Michele Martone
  2009-2015, Michele Martone
  2010-2016, Michele Martone
  2011-2013, Michele Martone
  2011-2015, Michele Martone
  2011-2016, Michele Martone
  2013-2015, Michele Martone
  2013-2016, Michele Martone
License: GPL-2+
 FIXME

Files: ./src/FbiStuff.cpp
 ./src/FbiStuff.h
 ./src/FbiStuffBit1.cpp
 ./src/FbiStuffBit24.cpp
 ./src/FbiStuffBmp.cpp
 ./src/FbiStuffDjvu.cpp
 ./src/FbiStuffFbtools.cpp
 ./src/FbiStuffFbtools.h
 ./src/FbiStuffGif.cpp
 ./src/FbiStuffJasPer.cpp
 ./src/FbiStuffJpeg.cpp
 ./src/FbiStuffList.h
 ./src/FbiStuffLoader.cpp
 ./src/FbiStuffLoader.h
 ./src/FbiStuffMatrixMarket.cpp
 ./src/FbiStuffPdf.cpp
 ./src/FbiStuffPng.cpp
 ./src/FbiStuffPpm.cpp
 ./src/FbiStuffPs.cpp
 ./src/FbiStuffTiff.cpp
 ./src/FbiStuffXyz.cpp
 ./src/FontServer.cpp
 ./src/FontServer.h
 ./src/FramebufferDevice.cpp
 ./src/FramebufferDevice.h
 ./src/SDLDevice.cpp
Copyright: 1998-2006, Gerd Knorr <kraxel@bytesex.org>
  2007-2015, Michele Martone
  2007-2017, Michele Martone
  2008-2013, Michele Martone
  2008-2015, Michele Martone
  2008-2016, Michele Martone
  2008-2017, Michele Martone
  2009-2015, Michele Martone
  2014-2015, Michele Martone
License: GPL-2+
 FIXME

Files: ./Makefile.in
 ./distros/Makefile.in
 ./doc/Makefile.in
 ./scripts/Makefile.in
 ./src/Makefile.in
Copyright: 1994-2011, Free Software
License: UNKNOWN
 FIXME

Files: ./depcomp
 ./missing
Copyright: 1996-1997, 1999-2000, 2002-2006, 2008-2012, Free Software Foundation, Inc.
  1999-2000, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2011, Free Software Foundation,
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./README
Copyright: NONE
License: GPL
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2008-2016, Michele Martone <michelemartone@users.sourceforge.net>
  2009, Michele Martone <michelemartone@users.sourceforge.net>
License: GPL-2+
 FIXME

Files: ./aclocal.m4
Copyright: 1996-2011, Free Software Foundation,
License: GPL-2+
 FIXME

Files: ./src/FbiStuffPcx.cpp
Copyright: 1998-2006, Gerd Knorr <kraxel@bytesex.org>
  2014, Mohammed Isam.
  2014-2016, Michele Martone
License: GPL-2+
 FIXME

Files: ./src/regex.c
Copyright: 1993-1997, Free Software Foundation, Inc.
License: LGPL-2+
 FIXME

Files: ./var/fonts/Lat15-Terminus16.psf
Copyright:  ÿÿ® ÿÿ"!ÿÿ£ ÿÿ!¼%ÿÿ!À%ÿÿ!¶%ÿÿ!²%ÿÿ%ÿÿ
  ¥~      ûUUQ            $ x "~      T8          @þ@            þ        8T    """"""""     <<<<<<      $$               8|þ|8           |         ~BBBBBB         >    ð             | |    ð       ð          ||      D8DDD8D       D(||             8D@0HDD$D8   $$               8<D< |             $HH$         ~       <                                       $$$              $$$~$$~$$$     |||    dh ,RL      $$0JDDD:                                     $~$           |                           ~                         @@      <BBFJRbBB<      (>      <BB @~      <BBBB<      
License: UNKNOWN
 FIXME

Files: ./media/image.jpg
Copyright: QS´S6-!b¸i2Â.­xR¹6ëÇ£_n»¦T½MQÿÙ
  Á"üVfbu×ÆQum
  ãÊÜ*ÒyZ:î¾VÓ«ÿ 3Mb.øñý%¾Û_ã+¥­}JÖcGËjß	f´Çãþ«z¶-!G,ëÞÿ Ïùî=ùo®«à©ºãºÝ|U¤tm¬¾Ùüçêæ£>ÜaÅnYq86ÄZkã;ÜEl°ã´ºQY_Äâi
  bõJéáiâ"ÔH¶"p1ªa$ÂÒ*#&VÂ8ªxU)À
  
  Ó4OÍzT½.Î(à´®O8×WÌáËÆ§tq8Âj­þkM
License: UNKNOWN
 FIXME

Files: ./media/image.png
Copyright: 8_2Af=d#ñ
  Bõü!p¥ñÿ7ovCsEX<©K7ës$rB´æ{X]¯t=
  Lø   	pHYs  .#  .#x¥?v   tIMEÖ
  e
  ¬àVé¸Ã9WpÎ-rÎuºôßzMÈÎ¹=ê¿Ï9÷ÕÔ²sn«óÃ>çÜ;+ßsn§úv:ç®ì*»{6Ø"²Z>G^
  ÀÑEàõ]^cêàÚx´F3~½ßÌNéÀ
  Ìë_Öî2îWar²>÷p26"G *eQ+Í+Dß¶µIMiÞa6íu>ÿÙ¤&=Aî îFN2®ñP£¼CQ¯øü7`¯­Ãd¯Yg±ÿÑÙóý<§ÝÓnÏ³óm@]cÖÐh-ãå3AFáßß jõ2×?OÜ|F
  þôÌTÝv
License: UNKNOWN
 FIXME

Files: ./media/fim.png
Copyright: <ºà§~º JØùõ¡+Ãk.¢þþþêêêÎÎÎÜ¿ÔT*ÕÚÚZ¿c¶«1+..^µjU^êêÕ«
  Kºï¾û
  S½½½
  SmWìòÀÔ©Szê©Ð«9rdçÎ±=¨7ß|3té¢(zê©§²[:Ä§ð¾áÎGÄ0Æ
  Tª··7è*sçÎ=räHmW';.÷ÍzÃÝÏµaÃÐ¥|jÂÉnDìÚÚÚfÍ500ô~ì
  Tê»ï¾
  }Zwww"¸páBÐUR©TkkkII1ÖÉ.o¯Zµ*ô*ýýýëÖ­s·cðÚk¯
  ¥¥¥ººÚvu²#
  ©iîÜ¹¡W7oÞáÃmÐáuß}÷}öÙg¡Wijj3gNlWì®®®nñâÅ¡W9räÈßþö7w{xCéî»ï¾-bß|/@>ç~z{1¶pÆØ(.^¼H$ººº.:qâÄL&S^^n^¿x^Y7n&?~|NmWìnÂ	K.
  ©©¶¶ÖÞý1L¦ªªª¯¯/è*
  ©éèèz©eeeétzÊ)
  ªª²]ììFêêêz(ô*}öYSSÓ5þß}÷ÝÐ¥¢èáÎñÒ!väå4wí/Sa1Æ
  Ô3g
  ÔÎ;kkkCFmÚ´i¥¥¥¶«1ÖÉSZZúÂ
  ×É; ±; ±; ±; ±Ä@ì Ä@ì Ä@ì Ä@ì Ä; ±; ±; ±; ±;@ì Ä@ì Ä@ì âRâ
  òg;tèÐM7Ýz~øý÷ß½JCCCccc
  òÔ©S½½½AW©¯¯?tèP}}}'O
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./src/fimgs
Copyright: 2007-2013, Michele Martone
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Use secure URL in Format
License: UNKNOWN
 FIXME

