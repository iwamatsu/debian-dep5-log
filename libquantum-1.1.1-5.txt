Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Makefile.in
 ./classic.c
 ./classic.h
 ./complex.c
 ./complex.h
 ./config.h.in
 ./configure.in
 ./decoherence.c
 ./decoherence.h
 ./defs.h
 ./density.c
 ./density.h
 ./error.c
 ./error.h
 ./expn.c
 ./expn.h
 ./gates.c
 ./gates.h
 ./grover.c
 ./ising.c
 ./matrix.c
 ./matrix.h
 ./measure.c
 ./measure.h
 ./oaddn.c
 ./oaddn.h
 ./objcode.c
 ./objcode.h
 ./omuln.c
 ./omuln.h
 ./qec.c
 ./qec.h
 ./qft.c
 ./qft.h
 ./quantum.h.in
 ./quobdump.c
 ./quobprint.c
 ./qureg.c
 ./qureg.h
 ./shor.c
 ./types.h.in
 ./version.c
 ./version.h
Copyright: 2003, 2005, Bjoern Butscher, Hendrik Weimer
  2003, Bjoern Butscher, Hendrik Weimer
  2003-2004, Bjoern Butscher, Hendrik Weimer
  2003-2008, Bjoern Butscher, Hendrik Weimer
  2003-2013, Bjoern Butscher, Hendrik Weimer
  2004, Bjoern Butscher, Hendrik Weimer
  2004-2005, Bjoern Butscher, Hendrik Weimer
  2005, Bjoern Butscher, Hendrik Weimer
  2013, Bjoern Butscher, Hendrik Weimer
License: GPL-3+
 FIXME

Files: ./CHANGES
 ./INSTALL
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libquantum-dev.install
 ./debian/libquantum8.install
 ./debian/patches/add-hardening-flags-to-compiler-options.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./energy.c
 ./energy.h
 ./lapack.c
 ./lapack.h
 ./qtime.c
 ./qtime.h
Copyright: 2006-2013, Hendrik Weimer
  2008-2013, Hendrik Weimer
  2013, Hendrik Weimer
License: GPL-3+
 FIXME

Files: ./aclocal.m4
 ./config.guess
 ./config.sub
 ./ltmain.sh
Copyright: 1992-2004, Free Software Foundation, Inc.
  1996-2001, 2003-2004, Free Software Foundation, Inc.
  1996-2001, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2010, Free Software
License: FSFUL
 FIXME

Files: ./README
Copyright: NONE
License: GPL-3
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-3+
 FIXME

Files: ./install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: NTP
 FIXME

Files: ./COPYING
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

