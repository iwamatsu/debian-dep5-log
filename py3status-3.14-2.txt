Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.git-hooks/pre-push
 ./.travis.yml
 ./CHANGELOG
 ./MANIFEST.in
 ./README.rst
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/python3-py3status.docs
 ./debian/rules
 ./debian/source/format
 ./debian/source/options
 ./debian/watch
 ./doc/Makefile
 ./doc/README.md
 ./doc/conf.py
 ./doc/configuration.rst
 ./doc/contributing.rst
 ./doc/doc-requirements.txt
 ./doc/index.rst
 ./doc/intro.rst
 ./doc/modules.rst
 ./doc/py3-cmd.rst
 ./doc/py3.rst
 ./doc/py3status.html
 ./doc/writing_modules.rst
 ./py3status.egg-info/SOURCES.txt
 ./py3status.egg-info/dependency_links.txt
 ./py3status.egg-info/entry_points.txt
 ./py3status.egg-info/requires.txt
 ./py3status.egg-info/top_level.txt
 ./py3status/__init__.py
 ./py3status/autodoc.py
 ./py3status/cli.py
 ./py3status/command.py
 ./py3status/composite.py
 ./py3status/constants.py
 ./py3status/core.py
 ./py3status/docstrings.py
 ./py3status/events.py
 ./py3status/exceptions.py
 ./py3status/formatter.py
 ./py3status/helpers.py
 ./py3status/i3status.py
 ./py3status/module.py
 ./py3status/module_test.py
 ./py3status/modules/README.md
 ./py3status/modules/air_quality.py
 ./py3status/modules/aws_bill.py
 ./py3status/modules/battery_level.py
 ./py3status/modules/bitcoin_price.py
 ./py3status/modules/bluetooth.py
 ./py3status/modules/check_tcp.py
 ./py3status/modules/clementine.py
 ./py3status/modules/clock.py
 ./py3status/modules/cmus.py
 ./py3status/modules/coin_balance.py
 ./py3status/modules/coin_market.py
 ./py3status/modules/deadbeef.py
 ./py3status/modules/dpms.py
 ./py3status/modules/external_script.py
 ./py3status/modules/file_status.py
 ./py3status/modules/frame.py
 ./py3status/modules/getjson.py
 ./py3status/modules/github.py
 ./py3status/modules/gitlab.py
 ./py3status/modules/glpi.py
 ./py3status/modules/google_calendar.py
 ./py3status/modules/graphite.py
 ./py3status/modules/group.py
 ./py3status/modules/hddtemp.py
 ./py3status/modules/i3block.py
 ./py3status/modules/i3pystatus.py
 ./py3status/modules/imap.py
 ./py3status/modules/kdeconnector.py
 ./py3status/modules/keyboard_locks.py
 ./py3status/modules/lm_sensors.py
 ./py3status/modules/loadavg.py
 ./py3status/modules/mail.py
 ./py3status/modules/moc.py
 ./py3status/modules/mpris.py
 ./py3status/modules/net_iplist.py
 ./py3status/modules/netdata.py
 ./py3status/modules/ns_checker.py
 ./py3status/modules/nvidia_smi.py
 ./py3status/modules/online_status.py
 ./py3status/modules/pingdom.py
 ./py3status/modules/pomodoro.py
 ./py3status/modules/process_status.py
 ./py3status/modules/rainbow.py
 ./py3status/modules/rate_counter.py
 ./py3status/modules/rss_aggregator.py
 ./py3status/modules/rt.py
 ./py3status/modules/screenshot.py
 ./py3status/modules/spaceapi.py
 ./py3status/modules/spotify.py
 ./py3status/modules/sql.py
 ./py3status/modules/static_string.py
 ./py3status/modules/sysdata.py
 ./py3status/modules/thunderbird_todos.py
 ./py3status/modules/timer.py
 ./py3status/modules/timewarrior.py
 ./py3status/modules/tor_rate.py
 ./py3status/modules/transmission.py
 ./py3status/modules/uname.py
 ./py3status/modules/volume_status.py
 ./py3status/modules/vpn_status.py
 ./py3status/modules/wanda_the_fish.py
 ./py3status/modules/weather_owm.py
 ./py3status/modules/weather_yahoo.py
 ./py3status/modules/whatismyip.py
 ./py3status/modules/whoami.py
 ./py3status/modules/wwan.py
 ./py3status/modules/wwan_status.py
 ./py3status/modules/xrandr.py
 ./py3status/modules/xscreensaver.py
 ./py3status/parse_config.py
 ./py3status/private.py
 ./py3status/profiling.py
 ./py3status/py3.py
 ./py3status/request.py
 ./py3status/screenshots.py
 ./py3status/storage.py
 ./py3status/udev_monitor.py
 ./py3status/util.py
 ./py3status/version.py
 ./setup.cfg
 ./tests/test_composite.py
 ./tests/test_formatter.py
 ./tests/test_module_doc.py
 ./tests/test_py3.py
 ./tox.ini
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./PKG-INFO
 ./doc/example_module.py
 ./py3status.egg-info/PKG-INFO
 ./py3status/modules/apt_updates.py
 ./py3status/modules/arch_updates.py
 ./py3status/modules/backlight.py
 ./py3status/modules/diskdata.py
 ./py3status/modules/do_not_disturb.py
 ./py3status/modules/dropboxd_status.py
 ./py3status/modules/exchange_rate.py
 ./py3status/modules/fedora_updates.py
 ./py3status/modules/gpmdp.py
 ./py3status/modules/hamster.py
 ./py3status/modules/icinga2.py
 ./py3status/modules/insync.py
 ./py3status/modules/nvidia_temp.py
 ./py3status/modules/player_control.py
 ./py3status/modules/scratchpad_async.py
 ./py3status/modules/selinux.py
 ./py3status/modules/systemd.py
 ./py3status/modules/taskwarrior.py
 ./py3status/modules/twitch.py
 ./py3status/modules/uptime.py
 ./py3status/modules/wifi.py
 ./py3status/modules/window_title_async.py
 ./py3status/modules/xrandr_rotate.py
 ./py3status/modules/xsel.py
 ./py3status/modules/yandexdisk_status.py
 ./py3status/modules/yubikey.py
 ./setup.py
Copyright: NONE
License: BSD~unspecified
 FIXME

Files: ./py3status/modules/keyboard_layout.py
 ./py3status/modules/mpd_status.py
 ./py3status/modules/net_rate.py
 ./py3status/modules/scratchpad_counter.py
 ./py3status/modules/vnstat.py
 ./py3status/modules/window_title.py
Copyright: NONE
License: EPL
 FIXME

Files: ./fastentrypoints.py
Copyright: 2016, Aaron Christianson
License: BSD-2-clause
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: BSD-3-clause
 FIXME

Files: ./LICENSE
Copyright: 2013-2015, Ultrabug and contributors
License: BSD-3-clause
 FIXME

Files: ./debian/changelog
Copyright: bastien Delafond ]
License: UNKNOWN
 FIXME

