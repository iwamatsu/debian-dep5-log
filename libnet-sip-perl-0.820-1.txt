Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./BUGS
 ./Changes
 ./HOWTO
 ./INSTALL
 ./META.json
 ./META.yml
 ./Makefile.PL
 ./README
 ./THANKS
 ./TODO
 ./bin/README
 ./bin/answer_machine.pl
 ./bin/nathelper.pl
 ./bin/stateless_proxy.pl
 ./debian/NEWS
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libnet-sip-perl.docs
 ./debian/libnet-sip-perl.examples
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/metadata
 ./debian/watch
 ./lib/Net/SIP.pm
 ./lib/Net/SIP.pod
 ./lib/Net/SIP/Authorize.pm
 ./lib/Net/SIP/Authorize.pod
 ./lib/Net/SIP/Blocker.pm
 ./lib/Net/SIP/Blocker.pod
 ./lib/Net/SIP/DTMF.pm
 ./lib/Net/SIP/DTMF.pod
 ./lib/Net/SIP/Debug.pm
 ./lib/Net/SIP/Debug.pod
 ./lib/Net/SIP/Dispatcher.pm
 ./lib/Net/SIP/Dispatcher.pod
 ./lib/Net/SIP/Dispatcher/Eventloop.pm
 ./lib/Net/SIP/Dispatcher/Eventloop.pod
 ./lib/Net/SIP/Dropper.pm
 ./lib/Net/SIP/Dropper/ByField.pm
 ./lib/Net/SIP/Dropper/ByIPPort.pm
 ./lib/Net/SIP/Endpoint.pm
 ./lib/Net/SIP/Endpoint.pod
 ./lib/Net/SIP/Endpoint/Context.pm
 ./lib/Net/SIP/Endpoint/Context.pod
 ./lib/Net/SIP/Leg.pm
 ./lib/Net/SIP/Leg.pod
 ./lib/Net/SIP/NATHelper/Base.pm
 ./lib/Net/SIP/NATHelper/Base.pod
 ./lib/Net/SIP/NATHelper/Client.pm
 ./lib/Net/SIP/NATHelper/Client.pod
 ./lib/Net/SIP/NATHelper/Local.pm
 ./lib/Net/SIP/NATHelper/Local.pod
 ./lib/Net/SIP/NATHelper/Server.pm
 ./lib/Net/SIP/NATHelper/Server.pod
 ./lib/Net/SIP/Packet.pm
 ./lib/Net/SIP/Packet.pod
 ./lib/Net/SIP/ReceiveChain.pm
 ./lib/Net/SIP/ReceiveChain.pod
 ./lib/Net/SIP/Redirect.pm
 ./lib/Net/SIP/Redirect.pod
 ./lib/Net/SIP/Registrar.pm
 ./lib/Net/SIP/Registrar.pod
 ./lib/Net/SIP/Request.pm
 ./lib/Net/SIP/Request.pod
 ./lib/Net/SIP/Response.pm
 ./lib/Net/SIP/Response.pod
 ./lib/Net/SIP/SDP.pm
 ./lib/Net/SIP/SDP.pod
 ./lib/Net/SIP/Simple.pm
 ./lib/Net/SIP/Simple.pod
 ./lib/Net/SIP/Simple/Call.pm
 ./lib/Net/SIP/Simple/Call.pod
 ./lib/Net/SIP/Simple/RTP.pm
 ./lib/Net/SIP/Simple/RTP.pod
 ./lib/Net/SIP/SocketPool.pm
 ./lib/Net/SIP/SocketPool.pod
 ./lib/Net/SIP/StatelessProxy.pm
 ./lib/Net/SIP/StatelessProxy.pod
 ./lib/Net/SIP/Util.pm
 ./lib/Net/SIP/Util.pod
 ./samples/3pcc.pl
 ./samples/README
 ./samples/bench/README
 ./samples/bench/call.pl
 ./samples/bench/listen.pl
 ./samples/dtmf.pl
 ./samples/invite_and_recv.pl
 ./samples/invite_and_send.pl
 ./samples/register_and_redirect.pl
 ./samples/test_registrar_and_proxy.pl
 ./t/01_load.t
 ./t/02_listen_and_invite.t
 ./t/03_forward_stateless.t
 ./t/04_call_with_rtp.t
 ./t/05_call_with_stateless_proxy.t
 ./t/06_call_with_reinvite.t
 ./t/07_call_on_hold.t
 ./t/08_register_with_auth.t
 ./t/11_invite_timeout.t
 ./t/12_maddr.t
 ./t/13_maddr_proxy.t
 ./t/14_bugfix_0.51.t
 ./t/15_block_invite.t
 ./t/16_drop_invite.t
 ./t/17_call_with_reinvite_and_auth.t
 ./t/18_register_with_auth_step_by_step.t
 ./t/19_call_with_dtmf.t
 ./t/20_channel_on_hold.t
 ./t/21_channel_on_hold_stateless_proxy.t
 ./t/22_stateless_proxy_ack_on_error.t
 ./t/23_valid_message.t
 ./t/certs/ca.pem
 ./t/certs/caller.sip.test.pem
 ./t/certs/listen.sip.test.pem
 ./t/certs/proxy.sip.test.pem
 ./t/testlib.pl
 ./tools/generate-dtmf.pl
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: Artistic and/or GPL-1
 FIXME

Files: ./COPYRIGHT
Copyright: 2006-2008, Steffen Ullrich.
License: Artistic or GPL-1+
 FIXME

Files: ./MANIFEST
Copyright: README
License: UNKNOWN
 FIXME

