Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.rspec
 ./.travis.yml
 ./Changelog.md
 ./Gemfile
 ./Gemfile.devtools
 ./Guardfile
 ./README.md
 ./Rakefile
 ./coercible.gemspec
 ./config/flay.yml
 ./config/flog.yml
 ./config/mutant.yml
 ./config/roodi.yml
 ./config/site.reek
 ./debian/compat
 ./debian/patches/series
 ./debian/ruby-coercible.docs
 ./debian/ruby-tests.rake
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./lib/coercible.rb
 ./lib/coercible/coercer.rb
 ./lib/coercible/coercer/array.rb
 ./lib/coercible/coercer/configurable.rb
 ./lib/coercible/coercer/date.rb
 ./lib/coercible/coercer/date_time.rb
 ./lib/coercible/coercer/decimal.rb
 ./lib/coercible/coercer/false_class.rb
 ./lib/coercible/coercer/float.rb
 ./lib/coercible/coercer/hash.rb
 ./lib/coercible/coercer/integer.rb
 ./lib/coercible/coercer/numeric.rb
 ./lib/coercible/coercer/object.rb
 ./lib/coercible/coercer/string.rb
 ./lib/coercible/coercer/symbol.rb
 ./lib/coercible/coercer/time.rb
 ./lib/coercible/coercer/time_coercions.rb
 ./lib/coercible/coercer/true_class.rb
 ./lib/coercible/configuration.rb
 ./lib/coercible/version.rb
 ./lib/support/options.rb
 ./lib/support/type_lookup.rb
 ./metadata.yml
 ./spec/integration/configuring_coercers_spec.rb
 ./spec/shared/unit/coerced_predicate.rb
 ./spec/shared/unit/configurable.rb
 ./spec/spec_helper.rb
 ./spec/unit/coercible/coercer/array/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/array/to_set_spec.rb
 ./spec/unit/coercible/coercer/class_methods/new_spec.rb
 ./spec/unit/coercible/coercer/configurable/config_name_spec.rb
 ./spec/unit/coercible/coercer/configurable/config_spec.rb
 ./spec/unit/coercible/coercer/date/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/date/to_date_spec.rb
 ./spec/unit/coercible/coercer/date/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/date/to_string_spec.rb
 ./spec/unit/coercible/coercer/date/to_time_spec.rb
 ./spec/unit/coercible/coercer/date_time/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/date_time/to_date_spec.rb
 ./spec/unit/coercible/coercer/date_time/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/date_time/to_string_spec.rb
 ./spec/unit/coercible/coercer/date_time/to_time_spec.rb
 ./spec/unit/coercible/coercer/decimal/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/decimal/to_decimal_spec.rb
 ./spec/unit/coercible/coercer/decimal/to_float_spec.rb
 ./spec/unit/coercible/coercer/decimal/to_integer_spec.rb
 ./spec/unit/coercible/coercer/decimal/to_string_spec.rb
 ./spec/unit/coercible/coercer/element_reader_spec.rb
 ./spec/unit/coercible/coercer/false_class/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/false_class/to_string_spec.rb
 ./spec/unit/coercible/coercer/float/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/float/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/float/to_decimal_spec.rb
 ./spec/unit/coercible/coercer/float/to_float_spec.rb
 ./spec/unit/coercible/coercer/float/to_integer_spec.rb
 ./spec/unit/coercible/coercer/float/to_string_spec.rb
 ./spec/unit/coercible/coercer/hash/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/hash/to_date_spec.rb
 ./spec/unit/coercible/coercer/hash/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/hash/to_time_spec.rb
 ./spec/unit/coercible/coercer/integer/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/integer/datetime_format_spec.rb
 ./spec/unit/coercible/coercer/integer/datetime_proc_spec.rb
 ./spec/unit/coercible/coercer/integer/to_boolean_spec.rb
 ./spec/unit/coercible/coercer/integer/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/integer/to_decimal_spec.rb
 ./spec/unit/coercible/coercer/integer/to_float_spec.rb
 ./spec/unit/coercible/coercer/integer/to_integer_spec.rb
 ./spec/unit/coercible/coercer/integer/to_string_spec.rb
 ./spec/unit/coercible/coercer/integer_spec.rb
 ./spec/unit/coercible/coercer/numeric/to_decimal_spec.rb
 ./spec/unit/coercible/coercer/numeric/to_float_spec.rb
 ./spec/unit/coercible/coercer/numeric/to_integer_spec.rb
 ./spec/unit/coercible/coercer/numeric/to_string_spec.rb
 ./spec/unit/coercible/coercer/object/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/object/inspect_spec.rb
 ./spec/unit/coercible/coercer/object/method_missing_spec.rb
 ./spec/unit/coercible/coercer/object/to_array_spec.rb
 ./spec/unit/coercible/coercer/object/to_hash_spec.rb
 ./spec/unit/coercible/coercer/object/to_integer_spec.rb
 ./spec/unit/coercible/coercer/object/to_string_spec.rb
 ./spec/unit/coercible/coercer/string/class_methods/config_spec.rb
 ./spec/unit/coercible/coercer/string/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/string/to_boolean_spec.rb
 ./spec/unit/coercible/coercer/string/to_constant_spec.rb
 ./spec/unit/coercible/coercer/string/to_date_spec.rb
 ./spec/unit/coercible/coercer/string/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/string/to_decimal_spec.rb
 ./spec/unit/coercible/coercer/string/to_float_spec.rb
 ./spec/unit/coercible/coercer/string/to_integer_spec.rb
 ./spec/unit/coercible/coercer/string/to_symbol_spec.rb
 ./spec/unit/coercible/coercer/string/to_time_spec.rb
 ./spec/unit/coercible/coercer/string_spec.rb
 ./spec/unit/coercible/coercer/symbol/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/symbol/to_string_spec.rb
 ./spec/unit/coercible/coercer/time/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/time/to_integer_spec.rb
 ./spec/unit/coercible/coercer/time/to_time_spec.rb
 ./spec/unit/coercible/coercer/time_coercions/to_date_spec.rb
 ./spec/unit/coercible/coercer/time_coercions/to_datetime_spec.rb
 ./spec/unit/coercible/coercer/time_coercions/to_string_spec.rb
 ./spec/unit/coercible/coercer/time_coercions/to_time_spec.rb
 ./spec/unit/coercible/coercer/true_class/coerced_predicate_spec.rb
 ./spec/unit/coercible/coercer/true_class/to_string_spec.rb
 ./spec/unit/coercible/configuration/class_methods/build_spec.rb
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/control
 ./debian/patches/remove_coverage_from_spec_helper.patch
 ./debian/patches/rspec3.patch
Copyright: dric Boutillier <boutil@debian.org>
License: UNKNOWN
 FIXME

Files: ./LICENSE.txt
Copyright: 2012, Piotr Solnica
License: Expat
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2012, Piotr Solnica <piotr.solnica@gmail.com>
  2014, CÃ©dric Boutillier <boutil@debian.org>
License: UNKNOWN
 FIXME

Files: ./debian/patches/move_support_inside_coercible_subdir.patch
Copyright: dric Boutil <boutil@debian.org>
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: dric Boutillier <boutil@debian.org> Wed, 12 Aug 2015 16:59:51 +0200
License: UNKNOWN
 FIXME

Files: ./checksums.yaml.gz
Copyright: À*¡,:FÇ6¢{Éh§£Å'Ê·2
License: UNKNOWN
 FIXME

