Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.arcconfig
 ./CMakeLists.txt
 ./ExtraDesktop.sh
 ./HOWTO_MakeNewtemplate.txt
 ./Mainpage.dox
 ./Messages.sh
 ./TODO
 ./debian/compat
 ./debian/control
 ./debian/kapptemplate.manpages
 ./debian/man/kapptemplate.1
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/metadata
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./doc/CMakeLists.txt
 ./doc/index.docbook
 ./icons/CMakeLists.txt
 ./icons/sc-apps-kapptemplate.svg
 ./po/de/docs/kapptemplate/index.docbook
 ./po/de/kapptemplate.po
 ./po/eo/kapptemplate.po
 ./po/es/docs/kapptemplate/index.docbook
 ./po/et/docs/kapptemplate/index.docbook
 ./po/fi/kapptemplate.po
 ./po/it/docs/kapptemplate/index.docbook
 ./po/ja/kapptemplate.po
 ./po/lt/kapptemplate.po
 ./po/nb/kapptemplate.po
 ./po/nl/docs/kapptemplate/index.docbook
 ./po/nn/kapptemplate.po
 ./po/pt_BR/docs/kapptemplate/index.docbook
 ./po/sk/kapptemplate.po
 ./po/sr/kapptemplate.po
 ./po/sv/docs/kapptemplate/index.docbook
 ./po/uk/docs/kapptemplate/index.docbook
 ./src/CMakeLists.txt
 ./src/application/CMakeLists.txt
 ./src/application/choice.ui
 ./src/application/generate.ui
 ./src/application/introduction.ui
 ./src/application/kapptemplate.kcfg
 ./src/application/logging.h
 ./src/application/prefs.kcfgc
 ./src/application/properties.ui
 ./src/templates/C++/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5-simple/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5-simple/Messages.sh
 ./src/templates/C++/kde-frameworks5-simple/README
 ./src/templates/C++/kde-frameworks5-simple/icons/22-apps-%{APPNAMELC}.png
 ./src/templates/C++/kde-frameworks5-simple/icons/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5-simple/kde-frameworks5-simple.kdevtemplate
 ./src/templates/C++/kde-frameworks5-simple/src/%{APPNAMELC}.ui
 ./src/templates/C++/kde-frameworks5-simple/src/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5-simple/src/org.example.%{APPNAMELC}.appdata.xml
 ./src/templates/C++/kde-frameworks5-simple/src/org.example.%{APPNAMELC}.desktop
 ./src/templates/C++/kde-frameworks5/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5/Messages.sh
 ./src/templates/C++/kde-frameworks5/README
 ./src/templates/C++/kde-frameworks5/doc/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5/icons/22-apps-%{APPNAMELC}.png
 ./src/templates/C++/kde-frameworks5/icons/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5/kde-frameworks5.kdevtemplate
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}ui.rc
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}view.ui
 ./src/templates/C++/kde-frameworks5/src/%{APPNAME}Settings.kcfg
 ./src/templates/C++/kde-frameworks5/src/%{APPNAME}Settings.kcfgc
 ./src/templates/C++/kde-frameworks5/src/CMakeLists.txt
 ./src/templates/C++/kde-frameworks5/src/org.example.%{APPNAMELC}.appdata.xml
 ./src/templates/C++/kde-frameworks5/src/org.example.%{APPNAMELC}.desktop
 ./src/templates/C++/kde-frameworks5/src/settings.ui
 ./src/templates/CMakeLists.txt
 ./tests/CMakeLists.txt
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./po/ast/kapptemplate.po
 ./po/da/kapptemplate.po
 ./po/el/kapptemplate.po
 ./po/en_GB/kapptemplate.po
 ./po/es/kapptemplate.po
 ./po/et/kapptemplate.po
 ./po/gl/kapptemplate.po
 ./po/hu/kapptemplate.po
 ./po/it/kapptemplate.po
 ./po/kk/kapptemplate.po
 ./po/ko/kapptemplate.po
 ./po/lv/kapptemplate.po
 ./po/mr/kapptemplate.po
 ./po/nds/kapptemplate.po
 ./po/nl/kapptemplate.po
 ./po/pl/kapptemplate.po
 ./po/pt/kapptemplate.po
 ./po/ru/kapptemplate.po
 ./po/sl/kapptemplate.po
 ./po/sv/kapptemplate.po
 ./po/tr/kapptemplate.po
 ./po/ug/kapptemplate.po
 ./po/zh_CN/kapptemplate.po
 ./po/zh_TW/kapptemplate.po
Copyright: YEAR This_file_is_part_of_KDE
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5-simple/src/%{APPNAMELC}window.cpp
 ./src/templates/C++/kde-frameworks5-simple/src/%{APPNAMELC}window.h
 ./src/templates/C++/kde-frameworks5-simple/src/main.cpp
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}view.cpp
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}view.h
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}window.cpp
 ./src/templates/C++/kde-frameworks5/src/%{APPNAMELC}window.h
 ./src/templates/C++/kde-frameworks5/src/main.cpp
Copyright: %{CURRENT_YEAR} by %{AUTHOR} <%{EMAIL}>
License: GPL
 FIXME

Files: ./src/application/apptemplateitem.cpp
 ./src/application/apptemplateitem.h
 ./src/application/apptemplatesmodel.cpp
 ./src/application/apptemplatesmodel.h
Copyright: 2007, Alexander Dymo <adymo@kdevelop.org>
  2008, Anne-Marie Mahfouf <annma@kde.org>
License: GPL-2+
 FIXME

Files: ./src/application/choicepage.cpp
 ./src/application/choicepage.h
 ./src/application/generatepage.cpp
 ./src/application/generatepage.h
Copyright: 2001, Bernd Gehrmann <bernd@kdevelop.org>
  2004-2005, Sascha Cunz <sascha@kdevelop.org>
  2007, Alexander Dymo <adymo@kdevelop.org>
  2008, Anne-Marie Mahfouf <annma@kde.org>
License: GPL-2+
 FIXME

Files: ./tests/macrosubstitutiontest.cpp
 ./tests/macrosubstitutiontest.h
 ./tests/namevalidatortest.cpp
 ./tests/namevalidatortest.h
Copyright: 2008, Anne-Marie Mahfouf
License: GPL-2+
 FIXME

Files: ./po/ca/kapptemplate.po
 ./po/ca@valencia/kapptemplate.po
 ./po/ga/kapptemplate.po
 ./po/uk/kapptemplate.po
Copyright: 2008-2017, This_file_is_part_of_KDE
  2009, This_file_is_part_of_KDE
License: UNKNOWN
 FIXME

Files: ./src/application/kapptemplate.cpp
 ./src/application/kapptemplate.h
Copyright: 2008, Anne-Marie Mahfouf <annma@kde.org>
License: GPL-2+
 FIXME

Files: ./doc/third-page.png
 ./po/ca/docs/kapptemplate/third-page.png
Copyright: ?uZ¥¦¢¼ç¯?B¶ìhG¾ÑTW3ÐÓ!fõï7{úÔ3.!8ó
  9xÔ¯±±±õÚ­)(`ì:pÔ{­×K÷ú@mX¹TAA~×£QÛFMUÅÏ4>'Ü
  TüqKýBééérÎ2vMJêût]åxü#ñãøÔÖÖÖü¸$0
  aJKKõ63=jDóïö13Íúú­ªªZTTÔÐÀ`Ìèª**ãð¯â·±ÒûñÆ#étBhÚÔÉââb!-MÍ¡L&³¢¢¢®®^DDDC]}¬íÈñßÈ    IDATáÖø`À?L
  aùù¯¢clGncÅcÝ-mwçTZZvóöD²4Â{scíùúc¶ÁûÏqÎµµµ/Q6ÖÎ¶}êêêhki}ËÉa0
  l
  y´H8x_u-ÅÉ¬ÌxG»>¦=åådkëê22³#<ÿÔþ(Û«}ñ
  mZ³|×}G|ç	qñu^KäOm÷'°òÆÕËKJÊöñE­_±dãêå»¶-ßhq{<Ãoð	®!¤©®ÎgüøO
  ÄÈ@D"}IÏèì@~¤/=µ5!¤¤¨@äæ8ÐÈ'Iüeü,ôo.#3kÉªõ]¿¸NÐÏ<q¿ÒEÒÓÄB¥|IKïì@~ª³áÕT{õâ³~|Â§s¯´éÄìIdr»Â  ð
  ¡®©¡¾sÿFGZ;~åï½ÌÚ*0µï=t&ü
  ¡±hÇÙ3i4Bè|ø¥îòá¡#ÇðLÓ³G÷¥º»ÍêÑÝÄÎv4ÿP[[+..¾gçv{
  §I72
  ©éì@  
  ««÷
  ¬¬,))=èsäýûzo H_ÒÒñjËV¬n²"qGP)*ª¥¥TÁf³««kdeeÒ~p2$($Æ¿àRTÌ+
  ¹|ífï^=uO
  ¿tùÒÒÒMë×rÖyûî}ä£'t:BHLT
  Ó]§8sjÎìøçàgÏ#?AUWÿ²^Âjd	* ¢1-BýO*­®®Æ'êëë0Øl6BÅjäÑ&þ
  ÔWÑ1UUÕ¡®Sð{5sçÌ~öüå×oß¢c^Ï=³¥½£P(ë×¬Âosåäæ
  Ý!vL§ü·â~¡H¤ÖCÏ_¾j7jÄ½GíûªÄoppõ:öõÛwêÑÍB!§|ùÒ
  éIðÑÀ|Fb»¾G}
  õíÝSK]]NN!TVVWPÐîgü$;W¤@C  Ð%u762a¾®6×¥Ùß.]»ò%­­ÍÖTWQºREE;!  .B¡Ì2aö´Éòr²-Õf>HJJòÓçTv[º8¬zQwSA
  ÷/>7A£&ç4è¿Ïo®^ZÎçqÆ_Ä	Ù¦O_FUTV­êróK^MÝp+ËÄ¤¢°Õ»ÔsÞµ[wãükLÿá}-­Ï+++ææå#T¹nâSòç¡¯Þ¼C
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5-simple/icons/48-apps-%{APPNAMELC}.png
 ./src/templates/C++/kde-frameworks5/icons/48-apps-%{APPNAMELC}.png
Copyright: qðW©<."U9ÞÓÓsÄ/888Ø399ù#Ó4G×Ý
  T*øz
  J©÷'°»L­ûT*¹îüarr2ÑDÅ-¡ÍmÆ·ãZ ¿·mû. /øªÜÃå@&¹ÀÇ#äÆ=Ï»îÀ¯Åðqä. _ðîw)
  §]u`ß¾}/È¿ýí"òÿá  !é¿&¾vöìÙ.ë¹
  ü²SÅ>6ÂU%2Çq*"²×§ô­õvîÜYïT©$/N¥R|3cgbiû|Rk½¹C]ÿèKØ=88xQÉ(
License: UNKNOWN
 FIXME

Files: ./doc/second-page.png
 ./po/ca/docs/kapptemplate/second-page.png
Copyright: XdtÌñÓçSJçEÒÞBí8¬¦â+­<?+ÆØÈh`_ænMLøÆEryLl¼ÿí{a/#ªïeu©ÿ5¨æ
  D*wî
  D´;÷3bÈÔ'äåç_¾àÙªEÅ-s®9lù¢?(º|=À­kéúJMO_¶zÃÈ!~CüúéðxEEEV¬MIKc^¼zý¯¹3õôtK;S"-ë7o3
  P
  TPÚ¸ñQÆ
  ©¡á)iiÕ§Ç|üFèéÔ¨«¨7¸:56h@C{Û2ÆÆ'8{ñí»¨ªF+®n$ûÉ"¢AÄ7Ã9Ôo	ß¸c;OýWo"éª4eTJáèêÆápjÂU¨èðxÖ"¾¼H®R©êÚÏA¿þüC;ÏÚ;4°s°³{ÂÐ´ñ«J4M@üGiÙÜ½OnªC¢¢c¯Ü
  ªÓ¢¹[VW45F­.¥5-=ÜSS´Iµ`Ê
  ¬^<
  ²Àà8 t
  ¶ïÙ?sê¤WoÞ+?¡@WÔNyô9+îºHbö.:¦®ù|D¼}wãæ^Ý»©P
  Î©pÑ¼Y§Î_R*P~®YJ,õè"Ñ
  Üf
  öñ°ïê©1l_Yê |¾±' 
License: UNKNOWN
 FIXME

Files: ./doc/fourth-page.png
 ./po/ca/docs/kapptemplate/fourth-page.png
Copyright: û¹ïûÜW¹rñð8»ä*¹é@GÇv§s·ùý1¬Õ±>ïçõìÎ|ç;ßùî·ùì;!nõi(B>Êª³ß¾èáNk{Bõ£qºINù0kîYY%
  ·î,]0gÏÌ
  CöUkkÝfËÎóÇ?~ºpÎ
  I/(`&&%·µ±þ±M¨AÒmP¦BahXDhp
  Rø.t¶·Fc*7kÚ4óãGÑìûML
  TM:ÝØØ()9Eâ¹
  TêÆu«û
  TòM×~ÎÏmÚ°öòë¡a_Éáäì÷ß}äÐ>yyêÂ
  UùÙÄF
  cn^Þë7©¥¥¥WUU¥¥¥åàAE?jP²ªjPhÑÂ|Â¸ÑÖVV¿8à_N§«ªªðxUýûöñôø»#BÉ@!$[8@ !Ð¯É!Ìa²A!$slBÉ&B2É!Ìa²A!$slBÉ&B2É!Ìa²A!$slBÉ&B2É!Ìa²A!$slBÉÜlCv;qêwGB@?
  i¬ÂÂêêêYÞÓ>~òúõOØNÝ»N2¬£A×øðáCqq@ <q<ÙE$êÐÈãþ_'
  oI*@FF¦¨¸X$[B¨Dól´.­¶nÛùòÕë¤·o[4oN¥wNGç{þÇm}O{MÞÆ´µÏ´ÉÔãâ)Âo²¿§yQZJÊrA)**®õóÃBh
  oÔ®lÂËn*c¦A!©«¬¡Ý,UO,«U.ï;{fÿ~}Ä
  {7Ö¯»²Ê£ÇOàVìíñcG«««kiiðvíú
  {7QýûN2ÉÀ@F£1!Ö¯±ø2ìÇlZ¿Æ¢
  {×´ôÅKu¬gñ
  IvnîëT	#¬Þ¤½j7kûÝÉB¡ @MO5Sçôuu6¯Y ~+×ÿ@¾12Ð7kÚD4k×¡ýØÃ8
  ÉX¯¡N]ÿºx¹Ö«
  ]ß>î×¢n @ÈîOØ­,-**+3?fÝ¼»iýZ--ÍúÔfh?>yúLEEåÂÅK¬ÂÂZÕtttÈ=ûvëê;h@?ÑÈðj±ïÔ¾lå!=*++?ùÍ¥Z7kjúþCæÓgÏÀÄØØ¢Åçÿ}¡khtqt¸wÿA^^¾ÿ®]>|øxåÚu PVVîÞ½+ P©T}½ìÜÊÊOë6néd×ñùó?l `åuûþ}ù
  ¢¢6 ¢¢cJKË¿l}[ÑÑ¶ÃÉSgº88aÞÜ_Í¿{ï¾]ÇÏÊÏÛ×ÖÖnÖÔtÆô©ë6l.ø¾¸}¼£}èÈñºISSSt8$öÆooüÃÔÔ4¡PxóÖíÌk
  'IJJÖmñ»ö
  'ÊËË++)M41:æVUUUTWWÏõeld¨¢¢Rë%cc#òD¦©Iwsu¹{÷þçè)EóçÒéte%¥¸
  ªªªÖmØ¼'<D]]]KKSbØõõ«[7®6&ÎW¯·61 ñáÔßQßV0Z·ïÞ+-+³jÓZIIiõº
  ÆMZ2dð
  ª* DÇÜz÷þxºûüäé³d124ô:yüØÑÊÊÊ püäéäWWÄáp¶îØ)
  ©2B¡ElÝ¾3hÇVÑ&×zfSUU%º9&Ô ZIIéÓgÏàEÂKòÃ, <}ö¼´´¼³$
  ©6CK, ÉHb¹Ä­ûuÆPSUYé7¿¤¬|ñÊuWnÄTWWÿX;B¡0¯¹ñK¾QTP
  ©IHL~õ&ÍÈÐPK.±NÆ»!ûbbïüÀî^é!  AF;´562Ô¤Ó
  ©YTT$ª_V^!qqmm
  ©i
  ©y÷îýõ1=ºw¯[§}SSSwúÌ_½ÝþñÀFWW§][Ýáä Ôü& .Î=;Ád±AzÆ[ ÐÔÔ,**.))mä°ÝÒÒ2==]VRR*ºµÒõ
  ©øÅ¨j
  ©©®cFB¡(**f³sr}æ-,--%gKJJoÅÞ~øðQPàÖæff¢j1·b/]¹ÆápÈÙÔ´ô«Öî	mÖÔ,Ù¾çì¹ó¢úoßíÚþ&-méâ
  ©©uÉb>rüñÓ§¥¥ejjªíÛµ?v±1ùê¼
  ©©µkk.Åà¥l¼m4èlU
  ©©ÉÍÍ«UMtºäW×¾üú^jjjä}§¬¬ì¢¢Ï·øÞ¤¦6¸ »
  ©©î
  ª¡¬¬Ü¡};*ª¢¢bÕ¦u^~~áEÝqwëÕÂ¼9AíÛµu°ïC¾dhh`ß¹ (**¶nÙ2ócVÝÅçÌ]0Ðs¸ÿÊÕÍ7ç;,¬®®ë3ËØÈPEE%¿
  ª¨p8¯aCÚÚXs¹Üi3çÄÞ¾ÛËÕy÷´Ð°£÷Õ
  ªÖ
  °¶n3bøÐï]êZÔ
  µ.RÔÍX¡°fî¬©:ÚÚùùÌ3þn8Âº³É¯ßlX¹J¥Æ?~zñÊõZ×·jÅÙ@"¬¢¢U·Øwéuu5ó&-}õ¦mdÍïÝFÖodüç/_9tðº~ ððÉó
  µÏÙN^^>  :Ûu$'ÒÒÓ%®(ãË!Ø»Ïþä4ùð¯°¨ Þ¾{OÚwê(zhú×N²QWÿ<´É*d0ª*ªãÇÇO¾~*ª&ºÃÃçóÉ#ÏÏj|iR'}úÄ&'¸níFÿhV__¯V¦¹|õÚÎàPò	´B
  ¸´`hi}úÄfs8Ê_~G ´45¬Âæff¢í'o?==CâïxjjÒ
  »¨MñÅ,Öó	Æ£R©úÝº8ÞG£Ñ
  Ô;ê«cmÕfïìÀàÐ¼¼<9y9G{{r@]ó}çL9çäé³#½Õv#ûDöìn·µ5n¢»kGF.ØÀVØÙÚþ}éÊ´)?¯Â¾Ó«×E×Ï
  ßÂÝ»»8Ú@MMÍ_ç.¯&%§ÆÚÊ*b÷®ý{Âì;@tÌ­âí=~2¿
  ávò
  á{öf
  ç/z:u722ÔÔÔLMM;ñ Ðh4{Ißcö1++!!1'7¬¦®®.zñçIJNÎÈx_ù 
  éßÛ,ûS%Õ¼¥M^þ§#D!ôF¥RG
  éé?°¢ÑÃ755ÙÆár%VPRTè3ãCfÖSg¿;è/ôuuÌMîÌ½yÇ®ïÍ7íÛZÕ*!sx¾eº;ÚYåäå6fExeÓ(B!Xµj9}ÒðýS^§ [õácöÙlgÈÀ~áû¿Ú }=Ýaý[[ZP¨W¯Óö=QQù	 G<vºoW
  ëQÑL¦§Ç@555Qá»÷x¼ªW¯ßäå61ùáýyd±o%DÐo!µd#
  îhqjjj6ÖV÷Ä»º8gçä;:_¬£ÒQ¶    IDAT÷éívàðÑØÛw
  ïXHìä´¨Q(eeeÑ©B¡±Ú×S&½øý¶3x ß<î©%yee%uµÏûHÁøôÍf³ÉwBA mmÃu ())o¤¾¿
  òòò&ÆÆ^Ã¶µ¶"+´o×vWà¶®ê
  ùµýË'2*
  úzza!¢úíÚÚÌ1mÃæRCýU-6Âhq«VøíÚ=lähEûÎvÞS' ÁX½bYø}¶lmÖ´}çO>I|
License: UNKNOWN
 FIXME

Files: ./doc/first-page.png
 ./po/ca/docs/kapptemplate/first-page.png
Copyright: B±gï>bªG	@SSsüØÑS'O
  ¿g*ÞQ!ý{2 ðÊõwï«Ü¾ôÀZ×I¤ÙXY%&'ñ]±_D¢²úõê&&W8Ðh4PÂ3æÊS.3îö
  *Z`®Üz$
  UZªvÓ<{Lìvi4¶6ËÆÆÆÐ
  UhX8Bèyps»¶u,""£BadTt©|ßÞÙÙÈÈ!Ô´Ic»vÁ/^Ã
  UË¾}W98,Y4ßÆÚÉdêëëµií$ïÛGåÆ
  YÇÜ£§'(
  iiK¯^à6gç>qÂïÐËW¯ûNOO§ÑèÃý×øøïkÿ·15-mÌÉcc/]9¹¹^>ÑÑ£F4°¿ÄZ¹IÊGhdh8dÄh÷­4n5vb&×¯YIÌ""2J|E¹ïÜ£ÏÑËÊÊDÓ¦L"öÎî;÷4kÖ4<"òÉÓgK.èÑ½kÐgÎÏÌÌ433ñ÷§V-XçG}úüÙËû@bR	;wö?ÍBÄæ
  <3Ã
  CSâA»6­}$wpq151AÕµ±æèë'&%s8»A÷çÌüÇÂÜ!Df¼}÷^¯=l¬­BÃ
  ÛµÕÔÔÄ0¬m'ñ-*ÝIºWâÊ­3emmõþýÇà85È¤)S.M^ òÊ¨IW
  ¥WâKòÕ	ªlYPPxÄïøÊe;uì@
  ¥«EDÚµ-,*Úíu@$?vyRSÓ·îòZµÄmù9î>¡å
  ¥âã+×oM=rûÕ"èÆ &¤WiJZÚÖ]£>d¦fqqñ÷=É©©Ä«ÿÞºó¿K´µYÒÁHÌpçþC/[0g:×Ø855=ðÊ5ÅJ?úðqËºt:=äÕ«7nKL.o
  ©)È¬ûß&ÐQ#§OB
  ©1eâr¸¼8-,ÌÛµEiii5iÔ(þ{Bèö{®={ØØXc6bØÐÈ¨èÜÜ<Piié·9u,tttB6ÖVumlBõmmRÓÒ$IçñÞ
  ©Ù´qcD|7Gt½]DT®®Ú#SÏêøÑHà¢ö¦L&â?½Î¡.úZt,½¸Ì;²@AÓµ$8Wã7¿·7Ól¯}äCaÀ¢%ÍÙ=êúPø0éGe«b)yTé@]¨äN`æôi¦&&çwïõ¢ÓhkW-6dÐ6÷]#FÓeëN2éÑ§?öEèÇé×üqÇññãF
  ©éiÍ?àÂF÷=|~±¼øüâî{ü.(ÓÚ»°Hµt ¯
  ¬¬LSS!¼¦©&Z
  °ðp
  ³_¾|Ã0k`ÀQÐùR¹u¯$[gª[ÎLó.zù0nÌÀþ
  ¶Èp= Ôv|«ËSïÌÅ·*¨x=jYÔZÚÚ¶
  ¼&³¯K^Ý:×ØxèàÛÜw
  ÁlïìL|WXsÛ6;8UKS³·kÏí]áöv
  ÄjW° 2Wµñý.kXÉø]Ú¶öÌÌú±áÚ·k#³µJ×Q¹ÆF¶66Á¯Þà8òê­­±¡X ²7Ìá2®ueè²uÖ-_·lÝ¦wJKKUkÇñ´ô­?Ó¦&1PKóGØºÛ+%-]µMM¸+»eee»ïõ.âóE|¾û^ï¬¬ìÝLM¸J6¥à»r
  ÉÀ>½$æ"s
  ÒÒÒwnßâÔªeÀù
  Ô*Í/(4Ð×OçeüJ_?/?ÿ×ån±
  Õ´)Ä_U{a5P¥T¨[7~ìèñcGWClç  ¢à  Pä   
  ÕÝ{÷R:ul¯¡¡Ñ²
  ÜEÍJÇ,³ÀºBUD¡¨rÇWÎI¢
  ãGÉùÊ[]²a±>sêÖ¥sLLlAAáEó![¶í°²²<wúdz:oÎüEuZµlºuûîÁýû
  æüÂRb«Òì7<aþojuØ k«ßK
  é<¦Å0ì¯¡È§ßâãw{0æ¯þ½{JyãNÿÙóÝfCF
  ò
License: UNKNOWN
 FIXME

Files: ./COPYING.DOC
 ./src/templates/C++/kde-frameworks5/COPYING.DOC
Copyright: 2000-2002, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5-simple/icons/32-apps-%{APPNAMELC}.png
 ./src/templates/C++/kde-frameworks5/icons/32-apps-%{APPNAMELC}.png
Copyright: K¾ïïMûõõõuéº~GOOÏñËkñb±hax 4M À] ÖLLLT-Ë:Lòô}ÿËm® Y"Ù°;Ë²6X¯w	à
  Lxs%ÙArÉåMòÌàf?R(67+	  Õjõ9JväÉÉÉi+ ,MÏ
  T:àÝ</¦9Ø Kú  
  Ïfmª0::z*|ß_çyÞí ¾²¨Û)c$G2­Õj[22´¤¦z­M½
  ÔÐí öx"¦¯íyÞX3`ú±¡`YlÚÏçw¶ú?ÈpgXDö$mñI|,"ïAðcÊ¿S)õ,­$oI¥ÛïyÞ×
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5-simple/icons/16-apps-%{APPNAMELC}.png
 ./src/templates/C++/kde-frameworks5/icons/16-apps-%{APPNAMELC}.png
Copyright: ÓPAaw&(D6?ÑØhÒ$ìCÖ¹6³Ëlb*O÷Þ½÷{÷°Åb>Ï$Iâ
  ä 7_í>g;@ooïD$ßÇí;kí¤µö§ïûoGFF z¶d¤ Æ"rU]{×(Jý¹îLÕëõiU-ËÒáÅ8/l±à8Î
  ê+
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5/doc/index.docbook
Copyright: NONE
License: CC-BY-SA-4.0
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-2+
 FIXME

Files: ./src/application/main.cpp
Copyright: 2008, Anne-Marie Mahfouf <annma@kde.org>
  2008, Beat Wolf <asraniel@fryx.ch>
License: GPL-2+
 FIXME

Files: ./po/ca/docs/kapptemplate/kapp4.png
Copyright:  
  ÂÄDG§¥¦×^¬f¼Lrø8xàÍñÀºÕ
  Óµk»yºçûá°!ñ.È*ÀÙnØàÀªWÏ
  %)eËBÔì,¶'±u³+ß	¤ÄO;ìôäÞ';}~aÂLÀÑ©Gôóíûq3j<>Çm°ªX	Yx`EØXLEÉXåíÛ¾==azQ}~]óÀª¬mÈiOÇº×x|Û`µÉc¥u
  %å¶XU6
  4Ký©Ù
  5Å1ùù
  B
  LeÇ²¢ááaø
  ct§Xì;8¬ø¿ñìÿÊv²O>RA°H+ï	ÏÃÛ°Hçmx
  kñÙ^z)ÉóZø§V
  ysHsA~Þ ïþO>ùÄSO}A8wøç
   X|}7nÜ Âøû©`1äÌÌËTM78<Ö°¡Cz{ï
  O:{iXô·¹ÅËBCæt¿Ån
  çâÀÓ£ûÛ=lÛÖ­'Nà*øE.ÄHEHYò=jddd$à®Æm5xð`¸®wÄEá]Ö®
  ¨úJJaÝú&J 6û[ÖñcÇ~ü£4o?@xöìY¸:·|Ù²'¾÷øyzçjÍhAJ®Û
  ÁØV=s&#=pyYÙÓ§SÏPÎÂþ|u.9tðÑR9°î·XXIIqrrI"leKl
  Èr5c:óhøªï;ïóØ|}|HZÓ9.ê¦¯S  Hô·»
  Ï£ÂluÏ$ÖÀ¢=9ëÐ¬~ñýRÎÌHÑ;²÷Ìý5ÝÒÁª7!æÓË	[Ä4,&à^
  ×æ_y¥5mJU¢þþ;QYàën{öPÃ=c2!CôXØ3ÎÌü§ÈÁâ¿aý9=`q
  Ü^Ê&ã
  Ý~;ßªCedØ7ß1"õ`Ò¼w/dÿÜâ'L0Ô§}÷)`.§­[;WèÝÛí/,0;p4{­mÙâö±T¶tu¥
  à"¢yàÌpY³¸BöRüÿn8b°Ò/Àb8Ä'$ÒÊÓ6±UG
  âÕÆ­ê§4%ÂÂcrÍtÖSaÎ /§
  æ³ÿcEXXÏþÆµïê¸VÏîÝÇû£*2y2þËÿì§?Á½ØñÈxäáº¼ñ:ÍÀzö¿ßòêÖö`y÷ïk]´0ßæçç÷¶»!Ð|ü±GÇEËxUlQ=ºÃVÐÂZÔ
  ñññÄü×³¿Ü³GNwò$x½þÚkaaa_y°ÈYSKÁ
  ø¨DÓ
License: UNKNOWN
 FIXME

Files: ./icons/64-apps-kapptemplate.png
Copyright: `Ì¼y/ÿòËÝ/âÑ)÷}R«¥ j}C­bN"[ø°¡O>Iûözç	Ê@5¯¹®D)D&'§õÞ¿ÿðç³fÍÚÖµkÚï¼ó¸nôC,0­V§m+ 5PkP´,~ãë:Ñ-YÞ½{÷Ç³³
  *D[·nmÄ>Rî½´S¦LQuM-8À`Çð)qã¦L6'<<X}öôI<xÉÄºÝ¤¶ù);'Ñ)·ãØ¥Ý`wáBmÙò%S Ü«Hh'ØWbýúâm4jÔp<ùf
License: UNKNOWN
 FIXME

Files: ./po/ca/docs/kapptemplate/kde-frameworks5-simple.png
Copyright: ¢òx¢ÕUW^ÆcëÃÜ°d) ~âãü÷aY¶£õ£MÓ4òB!²YìÝ»Y³O§»;@Ø0Ø³ÿíý>oíy`0@^(öà,ö@F{îÙgÑuZ¶ p´µ'	pþù4·´:­|þ²«ìk¿|ùðýtuu±ß^Á .
  éÓÇÁSÄg¾q=ç*Ù|ër÷ò?§x|?!ÞÓS
  qT©qHC45)«¦iL"-5¹©æÒ¤Æ"E-¹©¦H:ZGÔ¬&Y5!ÒQMKjªÕÔÖIG4lÛ¦º¦ µ¤¨ê©qHCQªªªTRúÐªJyyå±Þ'!
  qH'ù&ÓÂ!>7Ï17B8:äFáL+bxìÄ?ñ¬$ÕpÄN©qT¹y®N(b¸ìÔrP!J£(RëáDê´©qH§gÔ@R#uÎ¨J¿ÑÑRëNÊèh9*CÉ5L+Â!:-Äð¥T´¥^s@áDÊÈ©q¹ ¡%Ýå£wä"i!ÒPèsÍUSeNi(æê¹k¡
  xÍÿµG4VÍUx1Ëobõíw3þÔÌæ&,¾ËOÆÂScõ}­,ºê>UÜgyÒEÜòÙM¬ùÎ
  7Ï:Ãa2³² 8r¤ó, àÕ-['yqâF£457ÊÉ¦¤h"¢`YªªbÛ6MMÔ74Çã9¡bkÇí¦±¹ed¯möïßGA~>ååeh.§LL4àÉÙ¿nüü|Ê+Ê5kö°>8~èºN}Cñù|DcFÒÚ,l(7ßIyEãÆÅëõP±³²¦
  Efár×i)4B8ÚÇ¦Îô»]»T;B¤8;"ÄqDI¹];ry(!èsG
  ª-Zªbm­µZA±T+B
  ªôrp"å*7rTäòPB¼2GaH¹Ê4Ôp*)9 4!I9*ý!J-5!Ò±é;EiÔÔÖbYÉ}i©	Öoì$ËïÀÒÇÂóÏ%''9 DzÝeå@ò	P);B
  ­«c÷îÝ(ªÙ³g¡¨*ibY6ªªâÒT°möìÞÅgÉñãÓ¶?^bëz·öîãÇ?6cöwÏ9óÃÛoC·ÛMCCº®ÓÔÔD{{Ïëóù hjj¢¦¦¦¦Þ·Bqqñ0¿CD×µlÄOöéztÀìNºõ£MUU|>´¶´pàÀEÁáC×u
  ®©aÚ©§¦ý@'#»Ïÿsßå^ùÔÔÖ%«ªk3¦ÀñúÑær¹ðù}44ÔcÇûµ]]ÝÔidÍ÷sÆé³xôÒÒÚN8ÜaX¶Å|~ßÙ©bqñ1yòdê4aÍ-ää¸æê+Gûç¿ø%Û·ï`ãú1n!¥k¦²ªMO<>¶M¢¬$õqR¿Ä)S¦²èâÿ½`0ÈøÂBòB!rrr3f
  ±t]Ç0
  ïo©ø=+ÜMhf	F¯=û6c>:¹;©9öþäÛ,Ûð:FÒë}1Ç¹sã³g2Ö«æÀ¶P­K+¦(J"!ÏªBTÛ
  ø¼èza
License: UNKNOWN
 FIXME

Files: ./po/ca/docs/kapptemplate/kpartapp.png
Copyright: N#
  Tåå!zdff2§¢
  Â"ø
  Ü0G}
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5/icons/sc-apps-%{APPNAMELC}.svgz
Copyright: Æ¶ï6ä"cMWõuÛm7Ùþüµ§²«Ë]ß5¬ë³ïnn(
  mNÿê7`AÈ¶Þd(Å fÅ+Sä¬p²¾Ü%¤öÊ&¥uÝWÞ&ûRìúmÏ	¢;hÜÖÍÃHs8:!ÊØ*Ý^ìÉ¸¦L¯º÷å8×ÇØ¡Ü
  u^®´u°U<jÔFvºè
  äQ++
  «ÓÙíÐÌëç;¸®ö-­þ=µ»Ýoäñ+§í´kîRÌùq©bu.ãäêU·«tÚ¾kÓ®¼ovìwb.ï¸úãaÚ¹]Á}Ãõi(»` vâq7Õ"/¢2;¥~Zà?Óã¥¹KuQaød%w2*ë²ÿ­fÆr,1ãîP8ë¢úKýUü»z#z0¯¹ÖÒøGíR¢ÒsOÖçHÁ&r/¹BK+)>/·^*ZÀÒÛÀâ1j|®5wÞiìõÀ
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5/kde-frameworks5.png
Copyright: Èú
  ~Æù
  @ÿÖ`õjU=ó
  ARìpxIÉfßYßéú!ûâåéA½:µññöæ©Aöé
  Aôwe èhàQÍ¤>Û®YÍèIñèVÝ¥ãÈqúo×^­ZÇK/>ÇøÑ£hÝ¢nnî×'ÝµØ´5cÇO¢ë°'d?W®F±'t?Ã"°Zu?Éñ'©Y½Ãv·säØq¬V§Î°jÝºtlÃ]<)ëW(@Ëfw4§ÏãÊÕhæ/_©4¬_?Ósº»M+ÍÉìÜBÄÑã=ÎÚg8'WÊr4ò¶nÇl±rýz
  U3ãcå¨P>öíÚ~ôßý´ó/åô^¨0³ÖofKX¶ÍL{ZNjfº®®£L¶5;tKÜutêÏ3yxaò,¶¾¢È«S«&/zooÖôÒ¾õ
  W¯.k×ÿ/Ç×4F¤xñâhÆ#÷&11£Ç"Õ¤7öEªV©BíÀZL|i<f³9uII øú
  [§vË¹½¦Ç"#©n7ÂðÐaªV­ßÅ3¬ß[êD·n](^Ü7ÞzýráÂEöí?ÀÒßZ¸¿'?-ü«ÕJÛ6­®S½ZUî¹»|öûöàâ¥K,_±õþdÈ§®×ñ¾öÄÄÄ0oþÜÓ®­Csm`ÿ~
  oÊÐuM·`²$¢é°XÑMÞ¤vÙ­×Õ-£{ÄÆÆrîüE©±}èÅÞ}°XÒ}9gúçØÙsîl»em[µ üÈQçÕèÕ½3ëþüÝ1O<ÍÞÐý
  ÉäÉ	èîÅT@¹S¤ÒRîy³Z0%Ç£ÜÁÓ«æFê
  -ÛÆßßÏá±ul
  «gÞ.¼3[ÕaYú*¿»»Ã¼»)Ãz¶ÿ{zy¢ë:ßûd>5OpúÚº³~]*V(O
  ©ªù®9«»I¾-#4
  ÓþÔàlkgÃ#3ïloÎÙYÞvnÕÜA7cÑMéö£oEHQºn®¥(×HÎ»ÀiÐ¨A=Z·jAý:.íÀÕ«Q
  Ó§ùjÖlBB÷áë[¶­[1|ØÓøú/°òÖ¨^OÖ¬[ÏãõwxlÅÊÕtéÜï¾wüÓeqññÌúÍ[¶rãF
  ã$à2kfj¦£KÓRQØ* Å OÒ*W¶þ2
License: UNKNOWN
 FIXME

Files: ./po/ca/docs/kapptemplate/kde-frameworks5.png
Copyright: W"®ëJÏ+EE	¥µZwÑ¨'µÖ(Ê=/ÆX^$DR&·R;RÊTWG´eUÛ¶MFeWÖXÝÀi0¸·**rëFT4êIÏ
  S.Ã5oÑÒåqç]Í.1æ+y0Î=ã´&+ÓÐ!qî§ V¬z¿ÁõÈR ZD$-@HAm
  vì
  ,ËÑñ¸el;¡««CÚ¶wè|¬nØH ²¼¼Äb1ïÓoÈÕ²BÜíeúÈqK>]¿vv<áÔÇ!¿ç%Ç1JYÚ²HØ:WÚA|ÓG±T¯KvëÖM$	éytÝbUT´ú?dRÖÅB
  } 2ÆBÔ]-
  ïði!c¬yH,ûpáº!!RJØ®@HJRJÚg©PÈVZT$â*×¨¢¢ò<Oj­
  ûK3?Ý+]~ÆÌÔ-ÅÅÅ2HÈhÔDt°
License: UNKNOWN
 FIXME

Files: ./icons/128-apps-kapptemplate.png
Copyright: âÙó ld­÷Ùî³ñßß/#6®Çgka*fR-STóR<«ax¾8eN<¢.ÍÍuz
  ¨¨" »{ú
  ­àTRIi	W`Î¸pgLË½ùæîçÒWB¡³N
  Ñô_øyÊ L|ËÛoÏ»fäÈÓÝn·Þãqë<YaY½Öé$Yt½^ïÎË;»á¦ÆÍå?hMó
  Öû:lÝ0áº}$%-ÀP^Æ.ÿè8¦ÕíwÞ;NvðH¾úêÕúæonõx<·Xã(ÄB&¨v
  ï)f «¢Vl;Ì èÆ¡ÿügTc+«OÃ*$ÿä.pß E#4!]Fo¿ý.Ýxãõ0
  ú1cÆâÞÃÒ'!/¯Ä0cÆ#OkJn¿kük ß}|=ôÛQVkXVBBPQ9ß~ÃjÕ¾ðöU¨H@<K«YL´K¼
License: UNKNOWN
 FIXME

Files: ./icons/48-apps-kapptemplate.png
Copyright: 4#&õúuÿzÚõ®Å'©³µ';[;òòô&o/_rs=EmmÔÞÞ%U§Ë(--rrò©¢â.¾ï½LãrdÌäd)µ¶¶àZ;Éd$MJ ÿ ÿÖñãÇOç*7PaÃ±÷ß©©¹jî×RyYõtw9oo_2Ð7
  Y_Ï±ÒÒjº{·NcäááKÒ¸x(POµwkÉûØÏZ¦
  ¨¸
  ¬©¤¤¢QPp§NIIéSVî5h½~àláÑÀ{À;,´ÜýaÀsó#¡$÷ïÞ½zúðÃòóLCÖ`
  é1©©móÁý9ÀGýÏÎK©®³ó)_¾Üèï­À4Ý;:
  öþ}êìè¼-ÝØØJååwiß>Ap
License: UNKNOWN
 FIXME

Files: ./icons/32-apps-kapptemplate.png
Copyright: 5'YÉ8ùª;9¹eÆÄ>pàpÁÑ£¦¦2ÎÌõ4êa(--%ß!v×C¡PPÅ´
  òfml¾<BzWzÌ²×áÀ
  ´âC2??¾º­Í8mòÖ©ñèÑ4´ÚA´´´¹Æ{FéãHËH½>þäc°8,jÃÂBïÚµkãkuBZåå5:ág´µýôðöí®'7*â0T¸³[Ð#ù-8nðô<6Ûîî<°X~HOÏBdd¾°úbÑø¡ÃPRZjÔë'qõj~ò×`gg÷k©T>52b
  Í~DbéåÐ¥
  îDdD|½}NôCÎÎÎ«ÿãÑ1ÑÕÝÝ]0
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5/COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: UNKNOWN
 FIXME

Files: ./po/pt_BR/kapptemplate.po
Copyright: 2008-2016, This_file_is_part_of_KDE
  Marcelo Alvarenga <alvarenga@kde.org>, 2008, 2011, 2012, 2013, 2014, 2015, 2016.
License: UNKNOWN
 FIXME

Files: ./po/bs/kapptemplate.po
Copyright: 2010, Rosetta Contributors and Canonical Ltd 2010
License: UNKNOWN
 FIXME

Files: ./po/cs/kapptemplate.po
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/fr/kapptemplate.po
Copyright: YEAR This_file_is_part_of_KDE
  bastien Renard <Sebastien.Renard@digitalfox.org>, 2008.
License: UNKNOWN
 FIXME

Files: ./src/application/org.kde.kapptemplate.desktop
Copyright: ad KDE
  nÃ©rateur de modÃ¨les pour KDE
  szÃ­tÅ
License: UNKNOWN
 FIXME

Files: ./src/templates/C++/kde-frameworks5-simple/kde-frameworks5-simple.png
Copyright: cãÞãµ L?ÎÆü=lß¹«Á
  ¾&¿ÖÕ£{76geóÓ¡Cþm;v`ã¦ø|ËWþm55µQcYÙa>ßEeåÑV¿&Ñ_S¤÷L#ÆfmôÅ9
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: ed
  ed by the Free Software Foundation, but
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: format uri.
License: UNKNOWN
 FIXME

Files: ./src/application/org.kde.kapptemplate.appdata.xml
Copyright: nÃ©rateur de modÃ¨les pour KDE</summary>
License: UNKNOWN
 FIXME

Files: ./po/ca/docs/kapptemplate/index.docbook
Copyright: s un generador de projectes de programari nous utilitzant plantilles. </para>
License: UNKNOWN
 FIXME

Files: ./po/pt/docs/kapptemplate/index.docbook
Copyright: um gerador de modelos do &kde;. </para>
License: UNKNOWN
 FIXME

Files: ./src/application/kapptemplate.knsrc
Copyright: veloppement)
License: UNKNOWN
 FIXME

Files: ./icons/16-apps-kapptemplate.png
Copyright: ­¹¹KQ¹cÇóÜP(XÿC*tbëkk ÉêÎûa[ÝÉÊdð8·©T¥äävg(­V[zêôiÊë
  ¯¯ßsüø·
License: UNKNOWN
 FIXME

Files: ./icons/22-apps-kapptemplate.png
Copyright: âÚj­£ÕËL©{VÖÚVÑ¦TPYÊ"aÂ@ DAÀB% Ø°$@J^¸° 
License: UNKNOWN
 FIXME

