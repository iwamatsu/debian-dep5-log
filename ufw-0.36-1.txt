Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./ChangeLog
 ./ChangeLog.pre-0.25
 ./Makefile
 ./README
 ./conf/after.rules
 ./conf/after6.rules
 ./conf/before.rules
 ./conf/before6.rules
 ./conf/sysctl.conf
 ./conf/ufw.conf
 ./conf/ufw.defaults
 ./conf/user.rules
 ./conf/user6.rules
 ./debian/20-ufw.conf
 ./debian/README.Debian
 ./debian/after.rules.md5sum
 ./debian/after6.rules.md5sum
 ./debian/before.rules.md5sum
 ./debian/before6.rules.md5sum
 ./debian/changelog
 ./debian/changelog.Debian.pre-0.27.1
 ./debian/compat
 ./debian/config
 ./debian/control
 ./debian/patches/0001-optimize-boot.patch
 ./debian/patches/series
 ./debian/po/POTFILES.in
 ./debian/python-ufw.install
 ./debian/rules
 ./debian/source/format
 ./debian/sysctl.conf
 ./debian/templates
 ./debian/triggers
 ./debian/ufw.dirs
 ./debian/ufw.init
 ./debian/ufw.install
 ./debian/ufw.links
 ./debian/ufw.lintian-overrides
 ./debian/ufw.logrotate
 ./debian/ufw.maintscript
 ./debian/ufw.postinst
 ./debian/ufw.postrm
 ./debian/ufw.preinst
 ./debian/ufw.prerm
 ./debian/ufw.service
 ./debian/user.rules.md5sum
 ./debian/user6.rules.md5sum
 ./debian/watch
 ./doc/initscript.example
 ./doc/rsyslog.example
 ./doc/systemd.example
 ./doc/ufw-framework.8
 ./doc/ufw-on-snappy.8
 ./doc/ufw.8
 ./doc/upstart.example
 ./examples/apache
 ./examples/bind9
 ./examples/dovecot
 ./examples/samba
 ./examples/webapp
 ./locales/Makefile
 ./profiles/ufw-bittorent
 ./profiles/ufw-chat
 ./profiles/ufw-directoryserver
 ./profiles/ufw-dnsserver
 ./profiles/ufw-fileserver
 ./profiles/ufw-loginserver
 ./profiles/ufw-mailserver
 ./profiles/ufw-printserver
 ./profiles/ufw-proxyserver
 ./profiles/ufw-webserver
 ./pylintrc
 ./setup.cfg
 ./snap-files/bin/cli
 ./snap-files/bin/doc
 ./snap-files/bin/init
 ./snap-files/bin/srv
 ./snapcraft.yaml
 ./tests/bad/apps/result
 ./tests/bad/args/result
 ./tests/bad/netmasks/result
 ./tests/bad/policy/result
 ./tests/bugs/apps/result
 ./tests/bugs/apps/result.1.3
 ./tests/bugs/misc/result
 ./tests/bugs/rules/result
 ./tests/defaults/profiles.bad/bad_description
 ./tests/defaults/profiles.bad/bad_name
 ./tests/defaults/profiles.bad/bad_name2
 ./tests/defaults/profiles.bad/bad_no_proto
 ./tests/defaults/profiles.bad/bad_ports
 ./tests/defaults/profiles.bad/bad_title
 ./tests/defaults/profiles/0verkill
 ./tests/defaults/profiles/apache
 ./tests/defaults/profiles/apache.dpkg-dist
 ./tests/defaults/profiles/apache.dpkg-new
 ./tests/defaults/profiles/apache.dpkg-old
 ./tests/defaults/profiles/apache.rpmnew
 ./tests/defaults/profiles/apache.rpmsave
 ./tests/defaults/profiles/bind9
 ./tests/defaults/profiles/dovecot
 ./tests/defaults/profiles/good_multi
 ./tests/defaults/profiles/good_name
 ./tests/defaults/profiles/good_no_proto
 ./tests/defaults/profiles/openntpd
 ./tests/defaults/profiles/samba
 ./tests/defaults/profiles/webapp
 ./tests/destructive/bugs/result
 ./tests/good/apps/result
 ./tests/good/args/result
 ./tests/good/logging/result
 ./tests/good/netmasks/result
 ./tests/good/policy/result
 ./tests/good/reports/bin/show_listening_debug
 ./tests/good/reports/netstat.enlp
 ./tests/good/reports/proc_net_dev
 ./tests/good/reports/proc_net_if_inet6
 ./tests/good/reports/result
 ./tests/good/route/result
 ./tests/good/rules/result
 ./tests/installation/check_help/result
 ./tests/installation/check_root/result
 ./tests/ipv6/bad_args6/result
 ./tests/ipv6/good_args6/result
 ./tests/ipv6/logging/result
 ./tests/ipv6/logging/result.1.3
 ./tests/ipv6/rules6/result
 ./tests/ipv6/rules64/result
 ./tests/root/bugs/result
 ./tests/root/live/result
 ./tests/root/live_apps/result
 ./tests/root/live_route/result
 ./tests/root/logging/result
 ./tests/root/normalization/result
 ./tests/root/requirements/result
 ./tests/root/valid/result
 ./tests/root/valid6/result
 ./tests/root_kern/limit6/result
 ./tests/test-srv-upgrades.sh
 ./tests/test-srv-upgrades.sh.expected
 ./tests/unit/fake-binaries/ip6tables
 ./tests/unit/fake-binaries/ip6tables-restore
 ./tests/unit/fake-binaries/iptables
 ./tests/unit/fake-binaries/iptables-restore
 ./tests/unit/fake-binaries/sysctl-forward-no
 ./tests/unit/fake-binaries/sysctl-forward-yes
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./doc/skel-ui.example
 ./run_tests.sh
 ./setup.py
 ./shell-completion/bash
 ./snap-files/ufw.completer.bash
 ./src/after.init
 ./src/applications.py
 ./src/backend.py
 ./src/backend_iptables.py
 ./src/before.init
 ./src/common.py
 ./src/frontend.py
 ./src/parser.py
 ./src/ufw
 ./src/ufw-init
 ./src/ufw-init-functions
 ./src/util.py
 ./tests/bad/apps/runtest.sh
 ./tests/bad/args/runtest.sh
 ./tests/bad/netmasks/runtest.sh
 ./tests/bad/policy/runtest.sh
 ./tests/bugs/apps/runtest.sh
 ./tests/bugs/misc/runtest.sh
 ./tests/bugs/rules/runtest.sh
 ./tests/check-locales
 ./tests/check-requirements
 ./tests/debug/test_boot.sh
 ./tests/debug/wrap_command.sh
 ./tests/destructive/bugs/runtest.sh
 ./tests/good/apps/runtest.sh
 ./tests/good/args/runtest.sh
 ./tests/good/logging/runtest.sh
 ./tests/good/netmasks/runtest.sh
 ./tests/good/policy/runtest.sh
 ./tests/good/reports/runtest.sh
 ./tests/good/route/runtest.sh
 ./tests/good/rules/runtest.sh
 ./tests/installation/check_help/runtest.sh
 ./tests/installation/check_root/runtest.sh
 ./tests/ipv6/bad_args6/runtest.sh
 ./tests/ipv6/good_args6/runtest.sh
 ./tests/ipv6/logging/runtest.sh
 ./tests/ipv6/rules6/runtest.sh
 ./tests/ipv6/rules64/runtest.sh
 ./tests/root/bugs/runtest.sh
 ./tests/root/live/runtest.sh
 ./tests/root/live_apps/runtest.sh
 ./tests/root/live_route/runtest.sh
 ./tests/root/logging/runtest.sh
 ./tests/root/normalization/runtest.sh
 ./tests/root/requirements/runtest.sh
 ./tests/root/valid/runtest.sh
 ./tests/root/valid6/runtest.sh
 ./tests/root_kern/limit6/runtest.sh
 ./tests/runtest_ex.sh
 ./tests/testlib.sh
 ./tests/unit/runner.py
 ./tests/unit/support.py
 ./tests/unit/test_applications.py
 ./tests/unit/test_backend.py
 ./tests/unit/test_backend_iptables.py
 ./tests/unit/test_common.py
 ./tests/unit/test_frontend.py
 ./tests/unit/test_parser.py
 ./tests/unit/test_skeleton.py
 ./tests/unit/test_util.py
Copyright: 2008, Canonical Ltd.
  2008-2009, Canonical Ltd.
  2008-2011, Canonical Ltd.
  2008-2012, Canonical Ltd.
  2008-2013, Canonical Ltd.
  2008-2014, Canonical Ltd.
  2008-2015, Canonical Ltd.
  2008-2016, Canonical Ltd.
  2008-2018, Canonical Ltd.
  2009, Canonical Ltd.
  2009-2012, Canonical Ltd.
  2009-2018, Canonical Ltd.
  2010, Canonical Ltd.
  2012, Canonical Ltd.
  2012-2015, Canonical Ltd.
  2012-2018, Canonical Ltd.
  2013, Canonical Ltd.
  2013-2018, Canonical Ltd.
  2014, Canonical Ltd.
License: GPL-3
 FIXME

Files: ./locales/po/ar.po
 ./locales/po/bg.po
 ./locales/po/ca.po
 ./locales/po/de.po
 ./locales/po/en_AU.po
 ./locales/po/en_GB.po
 ./locales/po/es.po
 ./locales/po/fi.po
 ./locales/po/fr.po
 ./locales/po/he.po
 ./locales/po/it.po
 ./locales/po/nl.po
 ./locales/po/pl.po
 ./locales/po/pt.po
 ./locales/po/pt_BR.po
 ./locales/po/ru.po
 ./locales/po/sk.po
 ./locales/po/sl.po
 ./locales/po/sv.po
 ./locales/po/zh_CN.po
 ./locales/po/zh_TW.po
Copyright: 2008, Rosetta Contributors and Canonical Ltd 2008
License: UNKNOWN
 FIXME

Files: ./locales/po/cs.po
 ./locales/po/da.po
 ./locales/po/el.po
 ./locales/po/hu.po
 ./locales/po/id.po
 ./locales/po/nb.po
 ./locales/po/sr.po
 ./locales/po/tl.po
Copyright: 2009, Rosetta Contributors and Canonical Ltd 2009
License: UNKNOWN
 FIXME

Files: ./debian/po/eu.po
 ./debian/po/ru.po
 ./debian/po/sk.po
 ./debian/po/sv.po
 ./debian/po/templates.pot
 ./locales/po/ufw.pot
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./locales/po/ce.po
 ./locales/po/et.po
 ./locales/po/lv.po
 ./locales/po/tr.po
Copyright: 2014, Rosetta Contributors and Canonical Ltd 2014
License: UNKNOWN
 FIXME

Files: ./README.design
 ./README.translations
 ./TODO
Copyright: 2008-2012, Canonical Ltd.
  2008-2015, Canonical Ltd.
  2011, Canonical Ltd.
License: UNKNOWN
 FIXME

Files: ./debian/po/fi.po
 ./debian/po/gl.po
 ./debian/po/pl.po
Copyright: 2009, This file is distributed under the same license as the ufw package.
License: UNKNOWN
 FIXME

Files: ./locales/po/ast.po
 ./locales/po/ur.po
Copyright: 2010, Rosetta Contributors and Canonical Ltd 2010
License: UNKNOWN
 FIXME

Files: ./locales/po/bs.po
 ./locales/po/ja.po
Copyright: 2011, Rosetta Contributors and Canonical Ltd 2011
License: UNKNOWN
 FIXME

Files: ./debian/po/es.po
 ./debian/po/it.po
Copyright: 2009, Software in the Public Interest
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: BSD-3-clause and/or GPL-3+
 FIXME

Files: ./tests/test-srv-upgrades-data.tar.gz
Copyright: 9Þ¹¶=±d´6 '?ø&+FoÂ!xÍ:øaÆóh³¢;Å~á-±¨Jùí6D¥~bgè!±"¼nßÑxÝ½j£-Ëqð&ßc]VUýEäz>ËÃ,õ·Vh»8§UYSÊêY1t0cMVyj
  ?ú >ø7HêÌ}²ÝKÒ¢Þuã®C]mÔÓNü_XÐr<öÆøå´ÛÝ6âË/Úâ«ðg­j7¡ÐÇö
  É+°ÍrÀ´
  ¸}pÊ¢ÂN¨2)×!`uÿ û¦Ó%>Æ^ìbâzkjY×ÅáBÕó
  MdHâ²pA<éö[Ó}¸)Ý+*aüy÷lÃM©×äò»êz«¦©'@c·«ÉÍ·%(ÌDvrºõN.ë¸íXÄZÝ|( "?J¬À	£ùõjv¾GÅ¥VYäS
  MD­#þNÏ&bÎ­`Ú3%Z¿Ou/i´S¯¦"FG>úý""¨Ø
  ZÙ3)
  ai
  mC§t»£TPGz;ÞË70j¥ûñkøå¦É¤g[|ÐÄöMá7êC]iL­7õ æõÐÝÆp-·kCõ)ÑÚP£²þp}µ¶&Ôc©ë|ûêë´{(ì^B·ÖØ=¶SÙÛ!éÔÙ=(¨´{°ï¡øölàu`g³£_o÷´ø»·
  vn
  |®ü+ç?#­¼þMÛ{·õ¯Ì­ÿby¿þw³í¾Údìúvî1äG72çK¥¦+âh³]÷ÁÀÇ$[:íÛ	åý:ÿÞiÕý?þê9Ù iç?ðÿØú´÷ÿw?S§GæÏõZ`Uûy/eÌNZÍ
  6'yÑ]8ímâ&âUVvÑ« sÍ1
  ü¯¨Êÿ»Hsünö©ëå¦ÚÈÚåþ_©Á¾¢ü/ÔÒÞÿÛE²µhS´åCK¢äª£gÜÉh¤9S^Hy(da2u÷QÚ}FQSsÑì
  a#vOTÅ5½F¦SÊ±­SC/O¦ vñK«Ï*Jßrñ}CPÀ©
  º$¯á9 d³ºJûfµ{S?½é]Ðlt>ÝÔSé};¤ñ16&ÖoÕf£
  ?þùØmì°þÕ±ªÃøëº©ãÿOÓøãç?¦L·<J}ùÁ®Ûÿ:äÿzçï,Í³#xàÈ+H*GÊ¿Ë<-UCHÿ$ª¤
  ±Æ
   òæÊÀ»ôìt²üýYîç7be+,ð©
  £ÃCßÊC{[×Ó~ýä¤Jå¾(.÷{Aß:ÔF°r§ÏBÂXÑzØ¿÷µtÃóDÏÆý
  ¤*GÆähdJ×W¯0iÝDºMÆXÿÿûýýõ!mã9Tó®ÆX,AØ:5Nñª!tV¡SCèÔ:5N
  ¥y×¯ÇÆ84Ë/í[w
  ¨â¼9ÆVdUtCýª²RB]b#4­È&Ó×õ¬N=Dc@$ dÁ16¦xQey¬_8GÄ/Ö
  ª¢¡)ÞôæsÁTOðÚ	@¸äñ±v¬ZÔbò© o@:ªãÞ¶T»Ú<õ½Î»àÖëÂz¼ÊªàÞµSL^ÌR°oºb( Utk«=":õ
  °táB¼¨ÒxQ×s®V'F:ë1íCÙ
  ±ªî&Ñ:¸ÄÐ)¼'é
  ³HWö^[WÂpXÉ
  ´çÿ.RÿÔÓ¿#ÿbA¾+ÁÿÂÿ»Hqþ¿)°>ÿÅÒ^ÿï$%ñâRGÍ9º[Á
  µ~{ÏÑ,ýK§ÉúíÇú)°¶³0á¡
  µõ:^¥KùÒ%º¥£s¾¯«	R7sý
  µûûø¥qïm×EA²ûûÅù]A=p
  ¸£ÇÌv
  ÆnéiªÙîÜO&?ZHaä¥+!ýRüðÃqçVî
  ËSQQzµÃÜz!3"ê¥Üa_Gáó
  Ì¶jUE/Æ
  Ó(Â­0©¬Ì
  ÞÆ·´ÿmù_¾âòkþÊù_ÿ¥§[ÿ÷OeÛþ¥5ûKsòßåh.¹§Ñ3/£Êdï
  ÞßW[Ý¯¿@uol#ô
  à'ÞìtVÚM46W´££ùìQN=·ll8ºÜíiÿþãÉ¾ÿ¿Gõï?
  åDÙ
  êïíGì¶é-
  ëi ¹8RLAùy`év'ÉÓæÞb
  ìÕ,ÕÙ) Þ¨´S
  ìïï$=~ùòÅ&_h/é¸³£bÍ÷ü
  ï´±R/¢A¦*Ô¿·%yz
  ñ[l{}Ý
  ñ_"÷eü¥¸·ÿv"~îð/
  ô
  ö@á¦Yi²Rô¼«ÑTZ¡Ö=Å(T¨	pÒ
  ûf@âBnÔj´ç¦P¹[ØiJÂ
License: UNKNOWN
 FIXME

Files: ./screenshot1.png
Copyright: ;ÀûÁ"aDdÔZ#¯IÍ¼x¬ (Ôt¿çBû8p:¼¡®I0x)nê1JjàBðÞ£èô2§²qPo
  -V#ÞkW ZÕû`mµIé°½CÁ&tûf»mRýÉò0+8¡±¾Jã¡ D§xcÇS«v	ý^÷°Î{`§m·1Gò§äæXüØ~b¾¹[Z|_
  Cõ£%
  Qð¡N@l´Tç2ÄjûÁÀk¼ã"> Ó{£õ};xèV@+K°tþÖ81P
  e
  åï¿»z!s,&¨pÈHZFM@ÁIÚÁ£%R¯ÚñÌ{³'àvÔOyAõðyA_£¶¸	d®/¿áU=´^)ó &¶)öä/2*
  Fò-aÃECDóã®PHun×à;À
  8¡ì{ñ(ô ºÝÈþä=@ÆRa5,ÖZÔL¬åÃÎøv£²³
  $
  Ôbè|¦Tô
  öÁà>${¨qYÙ<NáedjD,õw(0(Þ¡ØMìð/dèÍ Dræk2¶ÀTõa`Ð£M¸ùÀ'¶:#X¢ðc¦ÁU§ÊXÙ2@ìn;z
License: UNKNOWN
 FIXME

Files: ./AUTHORS
Copyright: Canonical Ltd. 2007-2012.
License: UNKNOWN
 FIXME

Files: ./debian/po/fr.po
Copyright: 2009, Debian French l10n team <debian-l10n-french@lists.debian.org>
License: UNKNOWN
 FIXME

Files: ./debian/po/vi.po
Copyright: 2009, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 2007, Free Software Foundation, Inc. <http:fsf.org/>
License: UNKNOWN
 FIXME

Files: ./debian/po/de.po
Copyright: Helge Kreutzmann <debian@helgefjell.de>, 2009.
License: UNKNOWN
 FIXME

Files: ./debian/po/ja.po
Copyright: 2009, Jamie Strandboge <jamie@ubuntu.com>
License: UNKNOWN
 FIXME

Files: ./debian/po/cs.po
Copyright: 2009, Michal Simunek <michal.simunek@gmail.com>
License: UNKNOWN
 FIXME

Files: ./locales/po/se.po
Copyright: 2012, Rosetta Contributors and Canonical Ltd 2012
License: UNKNOWN
 FIXME

Files: ./locales/po/uk.po
Copyright: 2013, Rosetta Contributors and Canonical Ltd 2013
License: UNKNOWN
 FIXME

Files: ./locales/po/ko.po
Copyright: 2015, Rosetta Contributors and Canonical Ltd 2015
License: UNKNOWN
 FIXME

Files: ./debian/po/nl.po
Copyright: 2012, THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./debian/po/pt_BR.po
Copyright: 2012, THE ufw'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./debian/po/pt.po
Copyright: 2009, the ufw's copyright holder
  rico Monteiro <a_monteiro@netcabo.pt>, 2009.
License: UNKNOWN
 FIXME

Files: ./debian/po/da.po
Copyright: 2012, ufw & nedenstÃ¥ende oversÃ¦ttere.
License: UNKNOWN
 FIXME

