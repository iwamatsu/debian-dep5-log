Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./README.txt
 ./b
 ./b.bat
 ./bh.bat
 ./build
 ./bw.bat
 ./c.bat
 ./cavesax.py
 ./darkside_build_tunnel.bat
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/javabuild
 ./debian/javadoc
 ./debian/manpage.sgml
 ./debian/manpages
 ./debian/patches/angle-units.patch
 ./debian/patches/jdk9fix-893427.patch
 ./debian/patches/series
 ./debian/patches/svg-dashed-lines.patch
 ./debian/patches/tunnel-version-details.patch
 ./debian/patches/xml-encoding.patch
 ./debian/rules
 ./debian/source/format
 ./debian/tunnelx.desktop
 ./debian/tunnelx.install
 ./debian/tunnelx.links
 ./debian/tunnelx.manifest
 ./debian/tunnelx.png.uu
 ./debian/tunnelx.svg
 ./debian/tunnelx.xpm
 ./debian/watch
 ./j.bat
 ./r
 ./r.bat
 ./rerw.bat
 ./ri.bat
 ./runtunnel.bat
 ./src/PrintScaleDialog.java
 ./src/SqliteInterface.java
 ./src/stripcntlm.py
 ./symbols/bats.xml
 ./symbols/bedrock.xml
 ./symbols/bigboulder1.xml
 ./symbols/bigboulder2.xml
 ./symbols/boulder.xml
 ./symbols/breeze.xml
 ./symbols/column.xml
 ./symbols/curtain.xml
 ./symbols/flowstone.xml
 ./symbols/fontcolours.xml
 ./symbols/fontcolours_a.xml
 ./symbols/fontcolours_b.xml
 ./symbols/helictite.xml
 ./symbols/helpfile.html
 ./symbols/helpfile.md
 ./symbols/hexagons.xml
 ./symbols/mud.xml
 ./symbols/northarrowbig.xml
 ./symbols/northarrowsmall.xml
 ./symbols/npit.xml
 ./symbols/pants.xml
 ./symbols/pebble.xml
 ./symbols/pebble2.xml
 ./symbols/popcorn.xml
 ./symbols/puddle.xml
 ./symbols/rats.xml
 ./symbols/sand.xml
 ./symbols/slope.xml
 ./symbols/snow.xml
 ./symbols/stalactite.xml
 ./symbols/stalagmite.xml
 ./symbols/stalagtite.xml
 ./symbols/stalboss.xml
 ./symbols/straws.xml
 ./symbols/stream.xml
 ./symbols/sump.xml
 ./symbols/triangleboulder.xml
 ./t.bat
 ./tunnelmanifest.txt
 ./tutorials/010_lesson_one.xml
 ./tutorials/020_lesson_two.xml
 ./tutorials/030_lesson_three.xml
 ./tutorials/040_joining_nodes.xml
 ./tutorials/050_building_areas.xml
 ./tutorials/060_symbols_areas.xml
 ./tutorials/810_scalebars.xml
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./AATunnelTopParser.java
 ./src/AtlasGenerator.java
 ./src/ConnectiveAreaSigTabPane.java
 ./src/ConnectiveCentrelineTabPane.java
 ./src/ConnectiveComponentAreas.java
 ./src/ConnectiveLabelTabPane.java
 ./src/DelTriangulation.java
 ./src/DepthCol.java
 ./src/ElevWarp.java
 ./src/FileAbstraction.java
 ./src/Graphics2Dadapter.java
 ./src/ImageWarp.java
 ./src/InstantHelp.java
 ./src/LabelFontAttr.java
 ./src/LegLineFormat.java
 ./src/LineInputStream.java
 ./src/LineOutputStream.java
 ./src/LineStyleAttr.java
 ./src/MainBox.java
 ./src/MainBox.java-applet
 ./src/MatchSketchCentrelines.java
 ./src/Matrix3D.java
 ./src/MutualComponentArea.java
 ./src/NetConnection.java
 ./src/OneLeg.java
 ./src/OnePath.java
 ./src/OnePathNode.java
 ./src/OneSArea.java
 ./src/OneSSymbol.java
 ./src/OneSketch.java
 ./src/OneStation.java
 ./src/PathLabelDecode.java
 ./src/PocketTopoLoader.java
 ./src/ProximityDerivation.java
 ./src/PtrelLn.java
 ./src/Quaternion.java
 ./src/RefPathO.java
 ./src/SSymbScratch.java
 ./src/SSymbScratchPath.java
 ./src/SSymbolBase.java
 ./src/SVGnew.java
 ./src/SelectedSubsetsStructure.java
 ./src/SketchBackgroundPanel.java
 ./src/SketchDisplay.java
 ./src/SketchFrameDef.java
 ./src/SketchGraphics.java
 ./src/SketchGrid.java
 ./src/SketchInfoPanel.java
 ./src/SketchLineStyle.java
 ./src/SketchPrintPanel.java
 ./src/SketchSecondRenderPanel.java
 ./src/SketchSubsetPanel.java
 ./src/SketchSymbolAreas.java
 ./src/SketchZTiltPanel.java
 ./src/SubsetAttr.java
 ./src/SubsetAttrStyle.java
 ./src/SurvexLoaderNew.java
 ./src/SvgGraphics2D.java
 ./src/SvxFileDialog.java
 ./src/SymbolStyleAttr.java
 ./src/SymbolsDisplay.java
 ./src/TN.java
 ./src/TNXML.java
 ./src/TodeNode.java
 ./src/TunnelFileList.java
 ./src/TunnelLoader.java
 ./src/TunnelTopParser.java
 ./src/TunnelXML.java
 ./src/TunnelXML.javawithjaxp
 ./src/TunnelXMLparse.java
 ./src/TunnelXMLparsebase.java
 ./src/Vec3.java
 ./src/Vec3d.java
 ./src/WarpPiece.java
 ./src/WireAxes.java
 ./src/WireframeDisplay.java
 ./src/WireframeGraphics.java
Copyright: 2002, Julian Todd.
  2004, Julian Todd.
  2005, Julian Todd.
  2007, Julian Todd.
  2008, Julian Todd.
  2009, Julian Todd.
  2010, Julian Todd.
  2011, Julian Todd.
  2012, Julian Todd.
License: GPL-2+
 FIXME

Files: ./src/SVGAreas.java
 ./src/SVGPaths.java
 ./src/SVGSymbols.java
Copyright: 2006, Martin Green, Julian Todd
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: <Copyright (C) 2001-2012 Julian Todd>
License: BSD-3-clause and/or GPL-2+
 FIXME

Files: ./src/GraphicsAbstraction.java
Copyright: 2006, Martin Green.
License: GPL-2+
 FIXME

Files: ./src/PathLabelXMLparse.java
Copyright: 2001, Pavel Kouznetsov.
License: UNKNOWN
 FIXME

