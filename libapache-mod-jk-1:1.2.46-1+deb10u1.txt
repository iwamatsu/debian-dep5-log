Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./debian/changelog
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/jk.load
 ./debian/libapache-mod-jk-doc.doc-base
 ./debian/libapache-mod-jk-doc.docs
 ./debian/libapache-mod-jk-doc.examples
 ./debian/libapache2-mod-jk.README.Debian
 ./debian/libapache2-mod-jk.apache2
 ./debian/libapache2-mod-jk.install
 ./debian/libapache2-mod-jk.links
 ./debian/patches/0001-disable-logo.patch
 ./debian/patches/0002-debianize-log-directory.patch
 ./debian/patches/0003-upgrade-info-to-error-message.patch
 ./debian/patches/fix-privacy-breach.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./debian/workers.properties
 ./docs/ajp/ajpv13a.html
 ./docs/ajp/ajpv13ext.html
 ./docs/ajp/printer/ajpv13a.html
 ./docs/ajp/printer/ajpv13ext.html
 ./docs/common_howto/loadbalancers.html
 ./docs/common_howto/printer/loadbalancers.html
 ./docs/common_howto/printer/proxy.html
 ./docs/common_howto/printer/quick.html
 ./docs/common_howto/printer/timeouts.html
 ./docs/common_howto/printer/workers.html
 ./docs/common_howto/proxy.html
 ./docs/common_howto/quick.html
 ./docs/common_howto/timeouts.html
 ./docs/common_howto/workers.html
 ./docs/images/code.gif
 ./docs/images/docs.gif
 ./docs/images/fix.gif
 ./docs/images/void.gif
 ./docs/index.html
 ./docs/miscellaneous/changelog.html
 ./docs/miscellaneous/doccontrib.html
 ./docs/miscellaneous/faq.html
 ./docs/miscellaneous/jkstatustasks.html
 ./docs/miscellaneous/printer/changelog.html
 ./docs/miscellaneous/printer/doccontrib.html
 ./docs/miscellaneous/printer/faq.html
 ./docs/miscellaneous/printer/jkstatustasks.html
 ./docs/miscellaneous/reporttools.html
 ./docs/news/20041100.html
 ./docs/news/20050101.html
 ./docs/news/20060101.html
 ./docs/news/20070301.html
 ./docs/news/20081001.html
 ./docs/news/20090301.html
 ./docs/news/20100101.html
 ./docs/news/20120301.html
 ./docs/news/printer/20041100.html
 ./docs/news/printer/20050101.html
 ./docs/news/printer/20060101.html
 ./docs/news/printer/20070301.html
 ./docs/news/printer/20081001.html
 ./docs/news/printer/20090301.html
 ./docs/news/printer/20120301.html
 ./docs/printer/index.html
 ./docs/reference/apache.html
 ./docs/reference/iis.html
 ./docs/reference/printer/apache.html
 ./docs/reference/printer/iis.html
 ./docs/reference/printer/status.html
 ./docs/reference/printer/uriworkermap.html
 ./docs/reference/printer/workers.html
 ./docs/reference/status.html
 ./docs/reference/uriworkermap.html
 ./docs/reference/workers.html
 ./docs/webserver_howto/apache.html
 ./docs/webserver_howto/iis.html
 ./docs/webserver_howto/nes.html
 ./docs/webserver_howto/printer/apache.html
 ./docs/webserver_howto/printer/iis.html
 ./docs/webserver_howto/printer/nes.html
 ./jkstatus/build.properties.default
 ./native/apache-2.0/bldjk.qclsrc
 ./native/apache-2.0/bldjk54.qclsrc
 ./native/common/.indent.pro
 ./native/common/config.h.in
 ./native/iis/README
 ./native/iis/isapi.dsp
 ./native/iis/isapi.dsw
 ./native/iis/isapi_redirect.reg
 ./native/iis/pcre/132html
 ./native/iis/pcre/COPYING
 ./native/iis/pcre/ChangeLog
 ./native/iis/pcre/CheckMan
 ./native/iis/pcre/CleanTxt
 ./native/iis/pcre/Detrail
 ./native/iis/pcre/HACKING
 ./native/iis/pcre/Makefile.am
 ./native/iis/pcre/NEWS
 ./native/iis/pcre/NON-AUTOTOOLS-BUILD
 ./native/iis/pcre/NON-UNIX-USE
 ./native/iis/pcre/PrepareRelease
 ./native/iis/pcre/README
 ./native/iis/pcre/RunGrepTest
 ./native/iis/pcre/RunTest
 ./native/iis/pcre/RunTest.bat
 ./native/iis/pcre/cmake/FindEditline.cmake
 ./native/iis/pcre/cmake/FindPackageHandleStandardArgs.cmake
 ./native/iis/pcre/cmake/FindReadline.cmake
 ./native/iis/pcre/config-cmake.h.in
 ./native/iis/pcre/config.h.generic
 ./native/iis/pcre/config.h.in
 ./native/iis/pcre/config.hw
 ./native/iis/pcre/configure.ac
 ./native/iis/pcre/doc/html/NON-AUTOTOOLS-BUILD.txt
 ./native/iis/pcre/doc/html/README.txt
 ./native/iis/pcre/doc/html/index.html
 ./native/iis/pcre/doc/html/pcre-config.html
 ./native/iis/pcre/doc/html/pcre.html
 ./native/iis/pcre/doc/html/pcre16.html
 ./native/iis/pcre/doc/html/pcre32.html
 ./native/iis/pcre/doc/html/pcre_assign_jit_stack.html
 ./native/iis/pcre/doc/html/pcre_compile.html
 ./native/iis/pcre/doc/html/pcre_compile2.html
 ./native/iis/pcre/doc/html/pcre_config.html
 ./native/iis/pcre/doc/html/pcre_copy_named_substring.html
 ./native/iis/pcre/doc/html/pcre_copy_substring.html
 ./native/iis/pcre/doc/html/pcre_dfa_exec.html
 ./native/iis/pcre/doc/html/pcre_exec.html
 ./native/iis/pcre/doc/html/pcre_free_study.html
 ./native/iis/pcre/doc/html/pcre_free_substring.html
 ./native/iis/pcre/doc/html/pcre_free_substring_list.html
 ./native/iis/pcre/doc/html/pcre_fullinfo.html
 ./native/iis/pcre/doc/html/pcre_get_named_substring.html
 ./native/iis/pcre/doc/html/pcre_get_stringnumber.html
 ./native/iis/pcre/doc/html/pcre_get_stringtable_entries.html
 ./native/iis/pcre/doc/html/pcre_get_substring.html
 ./native/iis/pcre/doc/html/pcre_get_substring_list.html
 ./native/iis/pcre/doc/html/pcre_jit_exec.html
 ./native/iis/pcre/doc/html/pcre_jit_stack_alloc.html
 ./native/iis/pcre/doc/html/pcre_jit_stack_free.html
 ./native/iis/pcre/doc/html/pcre_maketables.html
 ./native/iis/pcre/doc/html/pcre_pattern_to_host_byte_order.html
 ./native/iis/pcre/doc/html/pcre_refcount.html
 ./native/iis/pcre/doc/html/pcre_study.html
 ./native/iis/pcre/doc/html/pcre_utf16_to_host_byte_order.html
 ./native/iis/pcre/doc/html/pcre_utf32_to_host_byte_order.html
 ./native/iis/pcre/doc/html/pcre_version.html
 ./native/iis/pcre/doc/html/pcreapi.html
 ./native/iis/pcre/doc/html/pcrebuild.html
 ./native/iis/pcre/doc/html/pcrecallout.html
 ./native/iis/pcre/doc/html/pcrecompat.html
 ./native/iis/pcre/doc/html/pcrecpp.html
 ./native/iis/pcre/doc/html/pcredemo.html
 ./native/iis/pcre/doc/html/pcregrep.html
 ./native/iis/pcre/doc/html/pcrejit.html
 ./native/iis/pcre/doc/html/pcrelimits.html
 ./native/iis/pcre/doc/html/pcrematching.html
 ./native/iis/pcre/doc/html/pcrepartial.html
 ./native/iis/pcre/doc/html/pcrepattern.html
 ./native/iis/pcre/doc/html/pcreperform.html
 ./native/iis/pcre/doc/html/pcreposix.html
 ./native/iis/pcre/doc/html/pcreprecompile.html
 ./native/iis/pcre/doc/html/pcresample.html
 ./native/iis/pcre/doc/html/pcrestack.html
 ./native/iis/pcre/doc/html/pcresyntax.html
 ./native/iis/pcre/doc/html/pcretest.html
 ./native/iis/pcre/doc/html/pcreunicode.html
 ./native/iis/pcre/doc/index.html.src
 ./native/iis/pcre/doc/pcre-config.1
 ./native/iis/pcre/doc/pcre-config.txt
 ./native/iis/pcre/doc/pcre.3
 ./native/iis/pcre/doc/pcre.txt
 ./native/iis/pcre/doc/pcre16.3
 ./native/iis/pcre/doc/pcre32.3
 ./native/iis/pcre/doc/pcre_assign_jit_stack.3
 ./native/iis/pcre/doc/pcre_compile.3
 ./native/iis/pcre/doc/pcre_compile2.3
 ./native/iis/pcre/doc/pcre_config.3
 ./native/iis/pcre/doc/pcre_copy_named_substring.3
 ./native/iis/pcre/doc/pcre_copy_substring.3
 ./native/iis/pcre/doc/pcre_dfa_exec.3
 ./native/iis/pcre/doc/pcre_exec.3
 ./native/iis/pcre/doc/pcre_free_study.3
 ./native/iis/pcre/doc/pcre_free_substring.3
 ./native/iis/pcre/doc/pcre_free_substring_list.3
 ./native/iis/pcre/doc/pcre_fullinfo.3
 ./native/iis/pcre/doc/pcre_get_named_substring.3
 ./native/iis/pcre/doc/pcre_get_stringnumber.3
 ./native/iis/pcre/doc/pcre_get_stringtable_entries.3
 ./native/iis/pcre/doc/pcre_get_substring.3
 ./native/iis/pcre/doc/pcre_get_substring_list.3
 ./native/iis/pcre/doc/pcre_jit_exec.3
 ./native/iis/pcre/doc/pcre_jit_stack_alloc.3
 ./native/iis/pcre/doc/pcre_jit_stack_free.3
 ./native/iis/pcre/doc/pcre_maketables.3
 ./native/iis/pcre/doc/pcre_pattern_to_host_byte_order.3
 ./native/iis/pcre/doc/pcre_refcount.3
 ./native/iis/pcre/doc/pcre_study.3
 ./native/iis/pcre/doc/pcre_utf16_to_host_byte_order.3
 ./native/iis/pcre/doc/pcre_utf32_to_host_byte_order.3
 ./native/iis/pcre/doc/pcre_version.3
 ./native/iis/pcre/doc/pcreapi.3
 ./native/iis/pcre/doc/pcrebuild.3
 ./native/iis/pcre/doc/pcrecallout.3
 ./native/iis/pcre/doc/pcrecompat.3
 ./native/iis/pcre/doc/pcrecpp.3
 ./native/iis/pcre/doc/pcredemo.3
 ./native/iis/pcre/doc/pcregrep.1
 ./native/iis/pcre/doc/pcregrep.txt
 ./native/iis/pcre/doc/pcrejit.3
 ./native/iis/pcre/doc/pcrelimits.3
 ./native/iis/pcre/doc/pcrematching.3
 ./native/iis/pcre/doc/pcrepartial.3
 ./native/iis/pcre/doc/pcrepattern.3
 ./native/iis/pcre/doc/pcreperform.3
 ./native/iis/pcre/doc/pcreposix.3
 ./native/iis/pcre/doc/pcreprecompile.3
 ./native/iis/pcre/doc/pcresample.3
 ./native/iis/pcre/doc/pcrestack.3
 ./native/iis/pcre/doc/pcresyntax.3
 ./native/iis/pcre/doc/pcretest.1
 ./native/iis/pcre/doc/pcretest.txt
 ./native/iis/pcre/doc/pcreunicode.3
 ./native/iis/pcre/doc/perltest.txt
 ./native/iis/pcre/libpcre.pc.in
 ./native/iis/pcre/libpcre16.pc.in
 ./native/iis/pcre/libpcre32.pc.in
 ./native/iis/pcre/libpcrecpp.pc.in
 ./native/iis/pcre/libpcreposix.pc.in
 ./native/iis/pcre/makevp.bat
 ./native/iis/pcre/makevp_c.txt
 ./native/iis/pcre/makevp_l.txt
 ./native/iis/pcre/pcre-config.in
 ./native/iis/pcre/pcre.dsp
 ./native/iis/pcre/pcre_chartables.c.dist
 ./native/iis/pcre/pcre_ucd.c
 ./native/iis/pcre/pcredemo.c
 ./native/iis/pcre/perltest.pl
 ./native/iis/pcre/testdata/grepbinary
 ./native/iis/pcre/testdata/grepfilelist
 ./native/iis/pcre/testdata/grepinput
 ./native/iis/pcre/testdata/grepinput3
 ./native/iis/pcre/testdata/grepinputv
 ./native/iis/pcre/testdata/grepinputx
 ./native/iis/pcre/testdata/greplist
 ./native/iis/pcre/testdata/grepoutput
 ./native/iis/pcre/testdata/grepoutputN
 ./native/iis/pcre/testdata/greppatN4
 ./native/iis/pcre/testdata/saved16
 ./native/iis/pcre/testdata/saved16BE-1
 ./native/iis/pcre/testdata/saved16BE-2
 ./native/iis/pcre/testdata/saved16LE-1
 ./native/iis/pcre/testdata/saved16LE-2
 ./native/iis/pcre/testdata/saved32
 ./native/iis/pcre/testdata/saved32BE-1
 ./native/iis/pcre/testdata/saved32BE-2
 ./native/iis/pcre/testdata/saved32LE-1
 ./native/iis/pcre/testdata/saved32LE-2
 ./native/iis/pcre/testdata/saved8
 ./native/iis/pcre/testdata/testinput1
 ./native/iis/pcre/testdata/testinput10
 ./native/iis/pcre/testdata/testinput11
 ./native/iis/pcre/testdata/testinput12
 ./native/iis/pcre/testdata/testinput13
 ./native/iis/pcre/testdata/testinput14
 ./native/iis/pcre/testdata/testinput15
 ./native/iis/pcre/testdata/testinput16
 ./native/iis/pcre/testdata/testinput17
 ./native/iis/pcre/testdata/testinput18
 ./native/iis/pcre/testdata/testinput19
 ./native/iis/pcre/testdata/testinput2
 ./native/iis/pcre/testdata/testinput20
 ./native/iis/pcre/testdata/testinput21
 ./native/iis/pcre/testdata/testinput22
 ./native/iis/pcre/testdata/testinput23
 ./native/iis/pcre/testdata/testinput24
 ./native/iis/pcre/testdata/testinput25
 ./native/iis/pcre/testdata/testinput26
 ./native/iis/pcre/testdata/testinput3
 ./native/iis/pcre/testdata/testinput4
 ./native/iis/pcre/testdata/testinput5
 ./native/iis/pcre/testdata/testinput6
 ./native/iis/pcre/testdata/testinput7
 ./native/iis/pcre/testdata/testinput8
 ./native/iis/pcre/testdata/testinput9
 ./native/iis/pcre/testdata/testinputEBC
 ./native/iis/pcre/testdata/testoutput1
 ./native/iis/pcre/testdata/testoutput10
 ./native/iis/pcre/testdata/testoutput11-16
 ./native/iis/pcre/testdata/testoutput11-32
 ./native/iis/pcre/testdata/testoutput11-8
 ./native/iis/pcre/testdata/testoutput12
 ./native/iis/pcre/testdata/testoutput13
 ./native/iis/pcre/testdata/testoutput14
 ./native/iis/pcre/testdata/testoutput15
 ./native/iis/pcre/testdata/testoutput16
 ./native/iis/pcre/testdata/testoutput17
 ./native/iis/pcre/testdata/testoutput18-16
 ./native/iis/pcre/testdata/testoutput18-32
 ./native/iis/pcre/testdata/testoutput19
 ./native/iis/pcre/testdata/testoutput2
 ./native/iis/pcre/testdata/testoutput20
 ./native/iis/pcre/testdata/testoutput21-16
 ./native/iis/pcre/testdata/testoutput21-32
 ./native/iis/pcre/testdata/testoutput22-16
 ./native/iis/pcre/testdata/testoutput22-32
 ./native/iis/pcre/testdata/testoutput23
 ./native/iis/pcre/testdata/testoutput24
 ./native/iis/pcre/testdata/testoutput25
 ./native/iis/pcre/testdata/testoutput26
 ./native/iis/pcre/testdata/testoutput3
 ./native/iis/pcre/testdata/testoutput3A
 ./native/iis/pcre/testdata/testoutput3B
 ./native/iis/pcre/testdata/testoutput4
 ./native/iis/pcre/testdata/testoutput5
 ./native/iis/pcre/testdata/testoutput6
 ./native/iis/pcre/testdata/testoutput7
 ./native/iis/pcre/testdata/testoutput8
 ./native/iis/pcre/testdata/testoutput9
 ./native/iis/pcre/testdata/testoutputEBC
 ./native/iis/pcre/testdata/valgrind-jit.supp
 ./native/iis/pcre/testdata/wintestinput3
 ./native/iis/pcre/testdata/wintestoutput3
 ./native/iis/pcre/ucp.h
 ./native/netscape/Makefile.vc
 ./native/netscape/README
 ./native/netscape/nsapi.dsp
 ./tools/dist/.htaccess
 ./tools/dist/HEADER.html
 ./tools/dist/README.html
 ./tools/dist/binaries/windows/HEADER.html
 ./tools/dist/binaries/windows/README.html
 ./tools/dist/binaries/windows/symbols/HEADER.html
 ./tools/dist/binaries/windows/symbols/README.html
 ./xdocs/images/code.gif
 ./xdocs/images/docs.gif
 ./xdocs/images/fix.gif
 ./xdocs/images/void.gif
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./HOWTO-RELEASE.txt
 ./LICENSE
 ./README.txt
 ./conf/httpd-jk.conf
 ./conf/uriworkermap.properties
 ./conf/workers.properties
 ./docs/style.css
 ./jkstatus/build.xml
 ./jkstatus/conf/jkstatus-tasks.xml
 ./jkstatus/example/jkstatus.properties.default
 ./jkstatus/example/jkstatus.xml
 ./jkstatus/example/show.txt
 ./jkstatus/example/show.xml
 ./jkstatus/src/share/org/apache/jk/status/AbstractJkStatusTask.java
 ./jkstatus/src/share/org/apache/jk/status/JkBalancer.java
 ./jkstatus/src/share/org/apache/jk/status/JkBalancerMapping.java
 ./jkstatus/src/share/org/apache/jk/status/JkBalancerMember.java
 ./jkstatus/src/share/org/apache/jk/status/JkResult.java
 ./jkstatus/src/share/org/apache/jk/status/JkServer.java
 ./jkstatus/src/share/org/apache/jk/status/JkSoftware.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatus.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusAccessor.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusParser.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusResetTask.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusTask.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusUpdateLoadbalancerTask.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusUpdateTask.java
 ./jkstatus/src/share/org/apache/jk/status/JkStatusUpdateWorkerTask.java
 ./jkstatus/src/share/org/apache/jk/status/LocalStrings.properties
 ./jkstatus/src/share/org/apache/jk/status/antlib.xml
 ./jkstatus/src/share/org/apache/jk/status/jkstatus.tasks
 ./jkstatus/src/share/org/apache/jk/status/mbeans-descriptors.xml
 ./jkstatus/src/share/org/apache/jk/status/package.html
 ./jkstatus/test/build.xml
 ./jkstatus/test/conf/jkstatus.xml
 ./jkstatus/test/conf/log4j.xml
 ./jkstatus/test/src/share/org/apache/jk/status/JkStatusParserTest.java
 ./native/BUILDING.txt
 ./native/Makefile.am
 ./native/README.txt
 ./native/STATUS.txt
 ./native/TODO.txt
 ./native/apache-2.0/Makefile.apxs.in
 ./native/apache-2.0/Makefile.in
 ./native/apache-2.0/Makefile.vc
 ./native/apache-2.0/config.m4
 ./native/apache-2.0/mod_jk.c
 ./native/buildconf.sh
 ./native/common/Makefile.in
 ./native/common/ap_snprintf.c
 ./native/common/ap_snprintf.h
 ./native/common/jk.rc
 ./native/common/jk_ajp12_worker.c
 ./native/common/jk_ajp12_worker.h
 ./native/common/jk_ajp13.c
 ./native/common/jk_ajp13.h
 ./native/common/jk_ajp13_worker.c
 ./native/common/jk_ajp13_worker.h
 ./native/common/jk_ajp14.c
 ./native/common/jk_ajp14.h
 ./native/common/jk_ajp14_worker.c
 ./native/common/jk_ajp14_worker.h
 ./native/common/jk_ajp_common.c
 ./native/common/jk_ajp_common.h
 ./native/common/jk_connect.c
 ./native/common/jk_connect.h
 ./native/common/jk_context.c
 ./native/common/jk_context.h
 ./native/common/jk_global.h
 ./native/common/jk_lb_worker.c
 ./native/common/jk_lb_worker.h
 ./native/common/jk_logger.h
 ./native/common/jk_map.c
 ./native/common/jk_map.h
 ./native/common/jk_msg_buff.c
 ./native/common/jk_msg_buff.h
 ./native/common/jk_mt.h
 ./native/common/jk_pool.c
 ./native/common/jk_pool.h
 ./native/common/jk_service.h
 ./native/common/jk_shm.c
 ./native/common/jk_shm.h
 ./native/common/jk_sockbuf.c
 ./native/common/jk_sockbuf.h
 ./native/common/jk_status.c
 ./native/common/jk_status.h
 ./native/common/jk_types.h.in
 ./native/common/jk_uri_worker_map.c
 ./native/common/jk_uri_worker_map.h
 ./native/common/jk_url.c
 ./native/common/jk_url.h
 ./native/common/jk_util.c
 ./native/common/jk_util.h
 ./native/common/jk_version.h
 ./native/common/jk_worker.c
 ./native/common/jk_worker.h
 ./native/common/jk_worker_list.h
 ./native/common/list.mk.in
 ./native/configure.ac
 ./native/docs/api/README.txt
 ./native/iis/Makefile.amd64
 ./native/iis/Makefile.x86
 ./native/iis/isapi.def
 ./native/iis/isapi_install.vbs
 ./native/iis/jk_isapi_plugin.c
 ./native/iis/pcre/pcre.amd64
 ./native/iis/pcre/pcre.ia64
 ./native/iis/pcre/pcre.x86
 ./native/netscape/Makefile.linux
 ./native/netscape/Makefile.solaris
 ./native/netscape/jk_nsapi_plugin.c
 ./native/scripts/build/config_vars.mk
 ./native/scripts/build/instdso.sh
 ./native/scripts/build/jk_common.m4
 ./native/scripts/build/rules.mk
 ./native/scripts/build/unix/buildcheck.sh
 ./support/apache.m4
 ./support/get_ver.awk
 ./support/jk_apache_static.m4
 ./support/jk_apr.m4
 ./support/jk_apxs.m4
 ./support/jk_dominohome.m4
 ./support/jk_exec.m4
 ./support/jk_java.m4
 ./support/jk_pcre.m4
 ./support/jk_tchome.m4
 ./support/jk_ws.m4
 ./support/os_apache.m4
 ./tools/jkbindist.sh
 ./tools/jkrelease.sh
 ./tools/lineends.pl
 ./tools/reports/README.txt
 ./tools/reports/tomcat_reports.pl
 ./tools/reports/tomcat_trend.pl
 ./tools/signfile.sh
 ./xdocs/ajp/ajpv13a.xml
 ./xdocs/ajp/ajpv13ext.xml
 ./xdocs/ajp/project.xml
 ./xdocs/build.xml
 ./xdocs/common_howto/loadbalancers.xml
 ./xdocs/common_howto/project.xml
 ./xdocs/common_howto/proxy.xml
 ./xdocs/common_howto/quick.xml
 ./xdocs/common_howto/timeouts.xml
 ./xdocs/common_howto/workers.xml
 ./xdocs/empty.xml
 ./xdocs/index.xml
 ./xdocs/miscellaneous/changelog.xml
 ./xdocs/miscellaneous/doccontrib.xml
 ./xdocs/miscellaneous/faq.xml
 ./xdocs/miscellaneous/jkstatustasks.xml
 ./xdocs/miscellaneous/project.xml
 ./xdocs/miscellaneous/reporttools.xml
 ./xdocs/news/20041100.xml
 ./xdocs/news/20050101.xml
 ./xdocs/news/20060101.xml
 ./xdocs/news/20070301.xml
 ./xdocs/news/20081001.xml
 ./xdocs/news/20090301.xml
 ./xdocs/news/20100101.xml
 ./xdocs/news/20110701.xml
 ./xdocs/news/20120301.xml
 ./xdocs/news/20140201.xml
 ./xdocs/news/20150101.xml
 ./xdocs/news/20160901.xml
 ./xdocs/news/20180301.xml
 ./xdocs/news/project.xml
 ./xdocs/project.xml
 ./xdocs/reference/apache.xml
 ./xdocs/reference/iis.xml
 ./xdocs/reference/project.xml
 ./xdocs/reference/status.xml
 ./xdocs/reference/uriworkermap.xml
 ./xdocs/reference/workers.xml
 ./xdocs/style.css
 ./xdocs/style.xsl
 ./xdocs/webserver_howto/apache.xml
 ./xdocs/webserver_howto/iis.xml
 ./xdocs/webserver_howto/nes.xml
 ./xdocs/webserver_howto/project.xml
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./native/iis/pcre/dftables.c
 ./native/iis/pcre/pcre.h.generic
 ./native/iis/pcre/pcre.h.in
 ./native/iis/pcre/pcre16_byte_order.c
 ./native/iis/pcre/pcre16_chartables.c
 ./native/iis/pcre/pcre16_compile.c
 ./native/iis/pcre/pcre16_config.c
 ./native/iis/pcre/pcre16_dfa_exec.c
 ./native/iis/pcre/pcre16_exec.c
 ./native/iis/pcre/pcre16_fullinfo.c
 ./native/iis/pcre/pcre16_get.c
 ./native/iis/pcre/pcre16_globals.c
 ./native/iis/pcre/pcre16_jit_compile.c
 ./native/iis/pcre/pcre16_maketables.c
 ./native/iis/pcre/pcre16_newline.c
 ./native/iis/pcre/pcre16_ord2utf16.c
 ./native/iis/pcre/pcre16_printint.c
 ./native/iis/pcre/pcre16_refcount.c
 ./native/iis/pcre/pcre16_string_utils.c
 ./native/iis/pcre/pcre16_study.c
 ./native/iis/pcre/pcre16_tables.c
 ./native/iis/pcre/pcre16_ucd.c
 ./native/iis/pcre/pcre16_utf16_utils.c
 ./native/iis/pcre/pcre16_valid_utf16.c
 ./native/iis/pcre/pcre16_version.c
 ./native/iis/pcre/pcre16_xclass.c
 ./native/iis/pcre/pcre32_byte_order.c
 ./native/iis/pcre/pcre32_chartables.c
 ./native/iis/pcre/pcre32_compile.c
 ./native/iis/pcre/pcre32_config.c
 ./native/iis/pcre/pcre32_dfa_exec.c
 ./native/iis/pcre/pcre32_exec.c
 ./native/iis/pcre/pcre32_fullinfo.c
 ./native/iis/pcre/pcre32_get.c
 ./native/iis/pcre/pcre32_globals.c
 ./native/iis/pcre/pcre32_jit_compile.c
 ./native/iis/pcre/pcre32_maketables.c
 ./native/iis/pcre/pcre32_newline.c
 ./native/iis/pcre/pcre32_ord2utf32.c
 ./native/iis/pcre/pcre32_printint.c
 ./native/iis/pcre/pcre32_refcount.c
 ./native/iis/pcre/pcre32_string_utils.c
 ./native/iis/pcre/pcre32_study.c
 ./native/iis/pcre/pcre32_tables.c
 ./native/iis/pcre/pcre32_ucd.c
 ./native/iis/pcre/pcre32_utf32_utils.c
 ./native/iis/pcre/pcre32_valid_utf32.c
 ./native/iis/pcre/pcre32_version.c
 ./native/iis/pcre/pcre32_xclass.c
 ./native/iis/pcre/pcre_byte_order.c
 ./native/iis/pcre/pcre_compile.c
 ./native/iis/pcre/pcre_config.c
 ./native/iis/pcre/pcre_dfa_exec.c
 ./native/iis/pcre/pcre_exec.c
 ./native/iis/pcre/pcre_fullinfo.c
 ./native/iis/pcre/pcre_get.c
 ./native/iis/pcre/pcre_globals.c
 ./native/iis/pcre/pcre_internal.h
 ./native/iis/pcre/pcre_maketables.c
 ./native/iis/pcre/pcre_newline.c
 ./native/iis/pcre/pcre_ord2utf8.c
 ./native/iis/pcre/pcre_printint.c
 ./native/iis/pcre/pcre_refcount.c
 ./native/iis/pcre/pcre_string_utils.c
 ./native/iis/pcre/pcre_study.c
 ./native/iis/pcre/pcre_tables.c
 ./native/iis/pcre/pcre_valid_utf8.c
 ./native/iis/pcre/pcre_version.c
 ./native/iis/pcre/pcre_xclass.c
 ./native/iis/pcre/pcregrep.c
 ./native/iis/pcre/pcreposix.c
 ./native/iis/pcre/pcreposix.h
Copyright: 1997-2012, University of Cambridge
  1997-2013, University of Cambridge
  1997-2014, University of Cambridge
  1997-2016, University of Cambridge
  1997-2017, University of Cambridge
  1997-2018, University of Cambridge
License: BSD-3-clause
 FIXME

Files: ./native/iis/pcre/sljit/sljitConfig.h
 ./native/iis/pcre/sljit/sljitConfigInternal.h
 ./native/iis/pcre/sljit/sljitExecAllocator.c
 ./native/iis/pcre/sljit/sljitLir.c
 ./native/iis/pcre/sljit/sljitLir.h
 ./native/iis/pcre/sljit/sljitNativeARM_32.c
 ./native/iis/pcre/sljit/sljitNativeARM_64.c
 ./native/iis/pcre/sljit/sljitNativeARM_T2_32.c
 ./native/iis/pcre/sljit/sljitNativeMIPS_32.c
 ./native/iis/pcre/sljit/sljitNativeMIPS_64.c
 ./native/iis/pcre/sljit/sljitNativeMIPS_common.c
 ./native/iis/pcre/sljit/sljitNativePPC_32.c
 ./native/iis/pcre/sljit/sljitNativePPC_64.c
 ./native/iis/pcre/sljit/sljitNativePPC_common.c
 ./native/iis/pcre/sljit/sljitNativeSPARC_32.c
 ./native/iis/pcre/sljit/sljitNativeSPARC_common.c
 ./native/iis/pcre/sljit/sljitNativeX86_32.c
 ./native/iis/pcre/sljit/sljitNativeX86_64.c
 ./native/iis/pcre/sljit/sljitNativeX86_common.c
 ./native/iis/pcre/sljit/sljitUtils.c
Copyright: Zoltan Herczeg (hzmester@freemail.hu).
License: BSD-2-clause
 FIXME

Files: ./docs/miscellaneous/printer/reporttools.html
 ./docs/news/20110701.html
 ./docs/news/20140201.html
 ./docs/news/20150101.html
 ./docs/news/20160901.html
 ./docs/news/20180301.html
 ./docs/news/printer/20100101.html
 ./docs/news/printer/20110701.html
 ./docs/news/printer/20140201.html
 ./docs/news/printer/20150101.html
 ./docs/news/printer/20160901.html
 ./docs/news/printer/20180301.html
Copyright: 1999-2018, Apache Software Foundation
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/pcre_scanner.cc
 ./native/iis/pcre/pcre_scanner.h
 ./native/iis/pcre/pcre_scanner_unittest.cc
 ./native/iis/pcre/pcre_stringpiece.cc
 ./native/iis/pcre/pcre_stringpiece.h.in
 ./native/iis/pcre/pcrecpp.cc
 ./native/iis/pcre/pcrecpp.h
 ./native/iis/pcre/pcrecpp_internal.h
 ./native/iis/pcre/pcrecpp_unittest.cc
 ./native/iis/pcre/pcrecpparg.h.in
Copyright: 2005, Google Inc.
  2005-2010, Google Inc.
  2010, Google Inc.
License: BSD-3-clause
 FIXME

Files: ./native/iis/pcre/ar-lib
 ./native/iis/pcre/compile
 ./native/iis/pcre/depcomp
 ./native/iis/pcre/ltmain.sh
 ./native/iis/pcre/missing
 ./native/iis/pcre/test-driver
 ./native/scripts/build/unix/compile
 ./native/scripts/build/unix/ltmain.sh
 ./native/scripts/build/unix/missing
Copyright: 1996-2014, Free Software Foundation, Inc.
  1996-2015, Free Software Foundation, Inc.
  1996-2017, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
  1999-2017, Free Software Foundation, Inc.
  2010-2017, Free Software Foundation, Inc.
  2011-2017, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./native/iis/pcre/config.guess
 ./native/iis/pcre/config.sub
 ./native/scripts/build/unix/config.guess
 ./native/scripts/build/unix/config.sub
Copyright: 1992-2017, Free Software Foundation, Inc.
  1992-2018, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./native/aclocal.m4
 ./native/iis/pcre/aclocal.m4
 ./native/iis/pcre/m4/libtool.m4
Copyright: 1996-2001, 2003-2017, Free Software Foundation, Inc.
  1996-2017, Free Software Foundation, Inc.
License: GPL
 FIXME

Files: ./native/iis/pcre/m4/ltoptions.m4
 ./native/iis/pcre/m4/ltsugar.m4
 ./native/iis/pcre/m4/lt~obsolete.m4
Copyright: 2004-2005, 2007, 2009, 2011-2017, Free Software
  2004-2005, 2007-2008, 2011-2017, Free Software
  2004-2005, 2007-2009, 2011-2017, Free Software
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/Makefile.in
 ./native/iis/pcre/m4/ltversion.m4
 ./native/iis/pcre/m4/pcre_visibility.m4
Copyright: 1994-2017, Free Software Foundation, Inc.
  2004, 2011-2017, Free Software Foundation, Inc.
  2005, 2008, 2010-2011, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/sljit/sljitNativeTILEGX-encoder.c
 ./native/iis/pcre/sljit/sljitNativeTILEGX_64.c
Copyright: 2013, Tilera Corporation(jiwang@tilera.com).
  Zoltan Herczeg (hzmester@freemail.hu).
License: BSD-2-clause
 FIXME

Files: ./native/iis/pcre/cmake/COPYING-CMAKE-SCRIPTS
 ./native/iis/pcre/pcretest.c
Copyright: NONE
License: BSD-3-clause
 FIXME

Files: ./native/iis/pcre/pcre_jit_compile.c
 ./native/iis/pcre/pcre_jit_test.c
Copyright: 1997-2012, University of Cambridge
  1997-2013, University of Cambridge
  2010-2012
  2010-2013
License: BSD-3-clause
 FIXME

Files: ./native/iis/pcre/install-sh
 ./native/scripts/build/unix/install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./native/configure
 ./native/iis/pcre/configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./docs/images/add.gif
 ./xdocs/images/add.gif
Copyright: 5/#äÒ®,-IÚÿ«Êù ÿ÷ÿ JKR)£:1!zohÅxwìõ  7«»Ô 	ÿúÿ EP*»OÜdÿk2ýwNòhÔ 2ÿüÿ    
License: UNKNOWN
 FIXME

Files: ./docs/images/jakarta-logo.gif
 ./xdocs/images/jakarta-logo.gif
Copyright: {tC"ÉPDF÷©TaM4P!¤Ð¥ADC	F2j¡=pAfãÂêÁ¶0 O8¯ðà"
  Ñ©1@øð½XxàìïRþù]eÑQþ'Åõ£YBò2{ÑÀCzí>¤ýôloêHÁÐ]å ¢Ä#* çÉP³CsÈàa!¡pÓ	7ÜÒWÝI!âhàcé¤CA/ÄóÉöÝgDý|ßÕ6|¤­4f vdß}!£D$wÙØ]<½a&@1º´F<ìÍ#{4@ùÔ
  ßåDöØ]:QbVÆ  b
License: UNKNOWN
 FIXME

Files: ./docs/images/design.gif
 ./xdocs/images/design.gif
Copyright: «®¼½¾ÅÆ !ÂÄ"#$%%&&Ì''ÃÅ()*++*,-Ø./012345667çÃ(89:;<=>>?ê
  Â¬¥®®®ÏÑÐ»Àº¡¯¢­~
License: UNKNOWN
 FIXME

Files: ./docs/images/update.gif
 ./xdocs/images/update.gif
Copyright: «®¼½¾ÅÆ !ÂÄ"#$%Ô&Ö''ÃÅ()*++*,-./0123456789:;<=>é(?@ABC1rI%K
  Â¬2
License: UNKNOWN
 FIXME

Files: ./docs/images/printer.gif
 ./xdocs/images/printer.gif
Copyright: ®ê2¦lº3-³4Õw[ê"C$Ò$aHØ$HåRÒäv#i±:0<Ú×Ë8@±¢ê©/áA  G¬ä~rt^
License: UNKNOWN
 FIXME

Files: ./docs/images/tomcat.gif
 ./xdocs/images/tomcat.gif
Copyright: ÐV
License: UNKNOWN
 FIXME

Files: ./native/Makefile.in
Copyright: 1994-2017, Free Software Foundation, Inc.
License: Apache-2.0
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: Apache-2.0 and/or BSD-3-clause
 FIXME

Files: ./native/common/jk_md5.c
Copyright: RSA Data Security, Inc.
License: Apache-2.0 and/or Beerware and/or NTP
 FIXME

Files: ./native/common/jk_md5.h
Copyright: RSA Data Security, Inc.
License: Apache-2.0 and/or NTP
 FIXME

Files: ./native/iis/pcre/pcregexp.pas
Copyright: 2001, Alexander Tokarev
  2001, Alexander Tokarev <dwalin@dwalin.ru>
  2001, Peter S. Voronov aka Chem O'Dun <petervrn@yahoo.com>
License: BSD-3-clause
 FIXME

Files: ./native/iis/pcre/INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2016, Free Software
License: FSFAP
 FIXME

Files: ./native/iis/pcre/m4/ax_pthread.m4
Copyright: 2008, Steven G. Johnson <stevenj@alum.mit.edu>
  2011, Daniel Richard G. <skunk@iSKUNK.ORG>
License: GPL
 FIXME

Files: ./NOTICE
Copyright: (C)
  2002-2018, The Apache Software Foundation
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/testdata/grepoutput8
Copyright: 10:X ten
  14-Before 333Â
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/CMakeLists.txt
Copyright: Add PCRE_SUPPORT_LIBREADLINE, PCRE_SUPPORT_LIBZ, and
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/testdata/grepinput8
Copyright: Before 333Â
  X ten
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/AUTHORS
Copyright: 1997-2018, University of Cambridge
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/pcre_stringpiece_unittest.cc
Copyright: 2003, and onwards Google Inc.
License: UNKNOWN
 FIXME

Files: ./native/iis/pcre/LICENCE
Copyright: ed and is in the public domain.
License: public-domain
 FIXME

