Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./NEWS
 ./README.md
 ./TODO
 ./data/application.css
 ./data/icons/hicolor/scalable/apps/org.gnome.Documents.svg
 ./data/icons/hicolor/symbolic/apps/org.gnome.Documents-symbolic.svg
 ./data/icons/meson.build
 ./data/media/dnd-counter.svg
 ./data/meson.build
 ./data/org.gnome.Documents.data.gresource.xml
 ./data/org.gnome.Documents.desktop.in
 ./data/org.gnome.Documents.search-provider.ini
 ./data/org.gnome.documents.gschema.xml
 ./data/ui/documents-app-menu.ui
 ./data/ui/help-overlay.ui
 ./data/ui/organize-collection-dialog.ui
 ./data/ui/preview-context-menu.ui
 ./data/ui/preview-menu.ui
 ./data/ui/selection-menu.ui
 ./data/ui/selection-toolbar.ui
 ./data/ui/view-menu.ui
 ./debian/compat
 ./debian/control
 ./debian/control.in
 ./debian/docs
 ./debian/gbp.conf
 ./debian/patches/90_drop-onlyshowin.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/source/lintian-overrides
 ./debian/watch
 ./flatpak/org.gnome.Documents.json
 ./getting-started/C/bookmarking.svg
 ./getting-started/C/editing.svg
 ./getting-started/C/goa.svg
 ./getting-started/C/landing.svg
 ./getting-started/C/learn-more.svg
 ./getting-started/C/scrubbing.svg
 ./getting-started/meson.build
 ./gnome-documents.doap
 ./help/C/collection-send.page.stub
 ./help/C/collections.page
 ./help/C/favorite.page.stub
 ./help/C/filter.page
 ./help/C/formats.page
 ./help/C/index.page
 ./help/C/info.page
 ./help/C/location.page.stub
 ./help/C/print.page
 ./help/C/prob-no-show.page
 ./help/C/prob-previews.page
 ./help/C/search.page
 ./help/C/sort.page.stub
 ./help/C/view-add.page
 ./help/C/view.page
 ./help/C/viewgrid.page
 ./help/LINGUAS
 ./help/as/as.po
 ./help/fi/fi.po
 ./help/gu/gu.po
 ./help/kn/kn.po
 ./help/meson.build
 ./help/ru/ru.po
 ./man/gnome-documents.xml
 ./man/meson.build
 ./meson.build
 ./meson_options.txt
 ./meson_post_install.py
 ./po/LINGUAS
 ./po/POTFILES.in
 ./po/POTFILES.skip
 ./po/be.po
 ./po/meson.build
 ./po/nl.po
 ./src/meson.build
 ./src/org.gnome.Documents.in
 ./src/org.gnome.Documents.service.in
 ./src/org.gnome.Documents.src.gresource.xml
 ./src/preview.js
 ./subprojects/libgd/Makefile.am
 ./subprojects/libgd/README
 ./subprojects/libgd/libgd.m4
 ./subprojects/libgd/libgd/meson.build
 ./subprojects/libgd/meson.build
 ./subprojects/libgd/meson_options.txt
 ./subprojects/libgd/meson_readme.md
 ./subprojects/libgd/test-tagged-entry-2.c
 ./subprojects/libgd/test-tagged-entry.c
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./po/af.po
 ./po/ar.po
 ./po/as.po
 ./po/bn_IN.po
 ./po/ca.po
 ./po/ca@valencia.po
 ./po/cs.po
 ./po/el.po
 ./po/es.po
 ./po/et.po
 ./po/eu.po
 ./po/fa.po
 ./po/fi.po
 ./po/fr.po
 ./po/fur.po
 ./po/gu.po
 ./po/hi.po
 ./po/hr.po
 ./po/id.po
 ./po/kk.po
 ./po/kn.po
 ./po/ko.po
 ./po/lt.po
 ./po/mk.po
 ./po/ml.po
 ./po/mr.po
 ./po/or.po
 ./po/pa.po
 ./po/pt_BR.po
 ./po/ro.po
 ./po/ru.po
 ./po/sl.po
 ./po/sr.po
 ./po/sr@latin.po
 ./po/tr.po
 ./po/vi.po
 ./po/xh.po
 ./po/zh_HK.po
 ./po/zh_TW.po
Copyright: 2011, gnome-documents's COPYRIGHT HOLDER
  2012, gnome-documents's COPYRIGHT HOLDER
  2014, gnome-documents's COPYRIGHT HOLDER
  2015, gnome-documents's COPYRIGHT HOLDER
  2017, gnome-documents's COPYRIGHT HOLDER
  2019, gnome-documents's COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./src/application.js
 ./src/changeMonitor.js
 ./src/documents.js
 ./src/edit.js
 ./src/embed.js
 ./src/errorBox.js
 ./src/evinceview.js
 ./src/lib/gd-bookmark.c
 ./src/lib/gd-bookmark.h
 ./src/lib/gd-display-preview.c
 ./src/lib/gd-display-preview.h
 ./src/lib/gd-nav-bar.c
 ./src/lib/gd-nav-bar.h
 ./src/lib/gd-places-links.c
 ./src/lib/gd-places-links.h
 ./src/lib/gd-utils.c
 ./src/lib/gd-utils.h
 ./src/main.js
 ./src/mainWindow.js
 ./src/manager.js
 ./src/miners.js
 ./src/notifications.js
 ./src/overview.js
 ./src/password.js
 ./src/places.js
 ./src/presentation.js
 ./src/query.js
 ./src/search.js
 ./src/searchbar.js
 ./src/shellSearchProvider.js
 ./src/testentry.js
 ./src/trackerController.js
 ./src/trackerUtils.js
 ./src/utils.js
 ./src/windowMode.js
 ./subprojects/libgd/libgd/gd-icon-utils.c
 ./subprojects/libgd/libgd/gd-icon-utils.h
Copyright: 2004, Red Hat, Inc.
  2011, 2013, 2015, Red Hat, Inc.
  2011, 2013-2014, Red Hat, Inc.
  2011, 2015, Red Hat, Inc.
  2011, Red Hat, Inc.
  2011-2012, 2014-2015, Red Hat, Inc.
  2011-2012, 2015-2016, Red Hat, Inc.
  2011-2012, Red Hat, Inc.
  2011-2014, Red Hat, Inc.
  2012, Red Hat, Inc.
  2012-2013, Red Hat, Inc.
  2013, Red Hat, Inc.
  2013-2014, Red Hat, Inc.
  2013-2015, Red Hat, Inc.
  2014, Red Hat, Inc.
  2015, Red Hat, Inc.
License: GPL-2+
 FIXME

Files: ./src/lib/gd-pdf-loader.c
 ./src/lib/gd-pdf-loader.h
 ./subprojects/libgd/libgd/gd-main-box-child.c
 ./subprojects/libgd/libgd/gd-main-box-child.h
 ./subprojects/libgd/libgd/gd-main-box-generic.c
 ./subprojects/libgd/libgd/gd-main-box-generic.h
 ./subprojects/libgd/libgd/gd-main-box-item.c
 ./subprojects/libgd/libgd/gd-main-box-item.h
 ./subprojects/libgd/libgd/gd-main-box.c
 ./subprojects/libgd/libgd/gd-main-box.h
 ./subprojects/libgd/libgd/gd-main-icon-box-child.c
 ./subprojects/libgd/libgd/gd-main-icon-box-child.h
 ./subprojects/libgd/libgd/gd-main-icon-box-icon.c
 ./subprojects/libgd/libgd/gd-main-icon-box-icon.h
 ./subprojects/libgd/libgd/gd-main-icon-box.c
 ./subprojects/libgd/libgd/gd-main-icon-box.h
 ./subprojects/libgd/libgd/gd-main-icon-view.c
 ./subprojects/libgd/libgd/gd-main-icon-view.h
 ./subprojects/libgd/libgd/gd-main-list-view.c
 ./subprojects/libgd/libgd/gd-main-list-view.h
 ./subprojects/libgd/libgd/gd-main-view-generic.c
 ./subprojects/libgd/libgd/gd-main-view-generic.h
 ./subprojects/libgd/libgd/gd-main-view.c
 ./subprojects/libgd/libgd/gd-main-view.h
 ./subprojects/libgd/libgd/gd-margin-container.c
 ./subprojects/libgd/libgd/gd-margin-container.h
 ./subprojects/libgd/libgd/gd-styled-text-renderer.c
 ./subprojects/libgd/libgd/gd-styled-text-renderer.h
 ./subprojects/libgd/libgd/gd-toggle-pixbuf-renderer.c
 ./subprojects/libgd/libgd/gd-toggle-pixbuf-renderer.h
 ./subprojects/libgd/libgd/gd-two-lines-renderer.c
 ./subprojects/libgd/libgd/gd-two-lines-renderer.h
 ./subprojects/libgd/libgd/gd-types-catalog.c
 ./subprojects/libgd/libgd/gd-types-catalog.h
 ./subprojects/libgd/libgd/gd.h
Copyright: 2011, Red Hat, Inc.
  2011-2012, Red Hat, Inc.
  2011-2013, 2015, Red Hat, Inc.
  2012, Red Hat, Inc.
  2016, Red Hat, Inc.
  2016-2017, Red Hat, Inc.
  2017, Red Hat, Inc.
License: LGPL-2+
 FIXME

Files: ./po/bg.po
 ./po/eo.po
 ./po/ga.po
 ./po/hu.po
 ./po/sk.po
 ./po/sv.po
 ./po/th.po
 ./po/zh_CN.po
Copyright: 2011, Free Software Foundation, Inc.
  2011-2012, 2016-2017, Free Software Foundation, Inc.
  2011-2013, Free Software Foundation, Inc.
  2011-2016, Free Software Foundation, Inc.
  2011-2017, 2019, Free Software Foundation, Inc.
  2011-2019, Free Software Foundation, Inc.
  2013-2015, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./po/is.po
 ./po/ln.po
 ./po/lv.po
 ./po/nb.po
 ./po/ne.po
 ./po/tg.po
 ./po/ug.po
 ./po/uk.po
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./help/hi/hi.po
 ./help/ja/ja.po
 ./help/mr/mr.po
 ./help/sr/sr.po
 ./help/sr@latin/sr@latin.po
 ./help/vi/vi.po
 ./help/zh_CN/zh_CN.po
Copyright: 2011, gnome-user-docs's COPYRIGHT HOLDER
  2011-2014, gnome-user-docs's COPYRIGHT HOLDER
  2012, gnome-user-docs's COPYRIGHT HOLDER
  2013, gnome-user-docs's COPYRIGHT HOLDER
  2014, gnome-user-docs's COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./src/lib/gd-bookmarks.c
 ./src/lib/gd-bookmarks.h
 ./src/lib/gd-places-bookmarks.c
 ./src/lib/gd-places-bookmarks.h
Copyright: 2010, Carlos Garcia Campos <carlosgc@gnome.org>
  2013, Red Hat, Inc.
License: GPL-2+
 FIXME

Files: ./help/de/de.po
 ./help/es/es.po
 ./help/pt_BR/pt_BR.po
Copyright: 2011, gnome-user-docs's COPYRIGHT HOLDER
  2016, gnome-user-docs's COPYRIGHT HOLDER
License: CC-BY-SA-3.0
 FIXME

Files: ./help/C/legal.xml
 ./help/ca/ca.po
Copyright: NONE
License: CC-BY-SA-3.0
 FIXME

Files: ./src/mainToolbar.js
 ./src/selections.js
Copyright: 2011, 2013, 2015, Red Hat, Inc.
  2011, Red Hat, Inc.
  2015, Alessandro Bono
License: GPL-2+
 FIXME

Files: ./src/lib/gd-metadata.c
 ./src/lib/gd-metadata.h
Copyright: 2009, Carlos Garcia Campos <carlosgc@gnome.org>
License: GPL-2+
 FIXME

Files: ./src/lib/gd-places-page.c
 ./src/lib/gd-places-page.h
Copyright: 2005, Marco Pesenti Gritti
  2013, Red Hat, Inc.
License: GPL-2+
 FIXME

Files: ./subprojects/libgd/libgd/gd-notification.c
 ./subprojects/libgd/libgd/gd-notification.h
Copyright: 2012, Red Hat, Inc.
  Erick PÃ©rez Castellanos 2011 <erick.red@gmail.com>
License: LGPL-2+
 FIXME

Files: ./subprojects/libgd/libgd/gd-tagged-entry.c
 ./subprojects/libgd/libgd/gd-tagged-entry.h
Copyright: 2011, Red Hat, Inc.
  2013, Ignacio Casal Quinteiro
License: LGPL-2+
 FIXME

Files: ./help/sl/sl.po
 ./po/he.po
Copyright: 2011, THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./help/cs/cs.po
Copyright: 2012, gnome-user-docs's COPYRIGHT HOLDER
License: CC-BY
 FIXME

Files: ./help/el/el.po
Copyright: 2007
License: CC-BY-SA-3.0
 FIXME

Files: ./help/fr/fr.po
Copyright: 2011, -12 gnome-user-docs's COPYRIGHT HOLDER
  rÃ©mie SERAFFIN <jeremieseraffin@gmail.com>, 2012.
  rÃ´me Sirgue <jsirgue@free.fr>, 2014
License: CC-BY-SA-3.0
 FIXME

Files: ./help/lv/lv.po
Copyright: 2011-2012, DÄgs Ädams GrÄ«nbergs, MÄtÄ«ss JÄnis ÄboltiÅÅ¡, Viesturs RuÅ¾Äns, RÅ«dolfs Mazurs, PÄteris KriÅ¡jÄnis
License: CC-BY-SA-3.0
 FIXME

Files: ./help/hu/hu.po
Copyright: 2011-2016, Free Software Foundation, Inc.
License: CC-BY-SA-3.0
 FIXME

Files: ./help/nl/nl.po
Copyright: 2011, Rosetta Contributors and Canonical Ltd 2011
License: CC-BY-SA-3.0
 FIXME

Files: ./help/it/it.po
Copyright: 2011, gnome-user-docs's COPYRIGHT HOLDER
  2012, Rosetta Contributors and Canonical Ltd 2012
License: CC-BY-SA-3.0
 FIXME

Files: ./help/gl/gl.po
Copyright: geuz <frandieguez@gnome.org>, 2012.
License: CC-BY-SA-3.0
 FIXME

Files: ./help/id/id.po
Copyright: 2011, gnome-document's COPYRIGHT HOLDER
License: CC-BY-SA-3.0
 FIXME

Files: ./help/ko/ko.po
Copyright: 2014, gnome-documents's COPYRIGHT HOLDER
License: CC-BY-SA-3.0
 FIXME

Files: ./help/sv/sv.po
Copyright: 2012-2015, the author of gnome-documents (msgids)
License: CC-BY-SA-3.0
 FIXME

Files: ./help/pl/pl.po
Copyright: 2017, the gnome-documents authors.
License: CC-BY-SA-3.0
 FIXME

Files: ./src/sharing.js
Copyright: (c) 2012 Meg Ford
  header.
License: GPL-2+
 FIXME

Files: ./src/fullscreenAction.js
Copyright: 2016, Endless Mobile, Inc.
License: GPL-2+
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: GPL-2+
 FIXME

Files: ./src/properties.js
Copyright: 2012, Meg Ford
  2012-2013, Red Hat, Inc.
License: GPL-2+
 FIXME

Files: ./src/lokview.js
Copyright: 2015, Pranav Kant
License: GPL-2+
 FIXME

Files: ./data/screenshots/gnome-documents-3.png
Copyright:  OxÜãðÁ÷ÏÂ«^ûzöå¯àÅ÷yV¬X
  ±!ç¬mÆsYÀÌ
  Ñ¸
  1±
  l«¬ÏÌRÄ,ÒAÿ8¼
  Æc1Æ³±¨ð×î,<ÖES
   õø"òº¸ÿd½^ï+?>ìl¬¼~hh¨4$Y°´,8¨-c1Æc
  µMí³l,@o7=â©­ºS½í:¾çÂvèÕvÊ"²bòªçØvf(×9+Ä¿1Îá
   tÀ½"Ð,½<¶ÇÈiÕN±¯,ü^×$öG=+ô«.Mê°/M§Ð±gÆ
  
  äñ3s¬ÍEkÄ
  -0HZ§ÓIB³Ùì
  5
  CºHEgæé§Æ!ûº^Dë«Å~£mè8T7éµZâ=êÇèæwÍ÷çO6¶«µí4å<æíG¢cp
  G¡Õjù>c1Æc1SÏX5Úu¿úN8ñUøæ·¿»-Ã½÷®Ä]Ëáßþ^òWâÚë¯·^¬±u:síµxù«OÂÒÛoG»ÝÆÍ·Üo¹ívK.ÅË^õóËkÐtJS ßvÞö®wãö;îÀn»îÚms4þQÃ-1æD³ÙD¥RAµZ½¿2©ü£ÎËcÌ±ê¾Ûï¸ïùÀ±zõjzÈ!xÑ
  GàO?»ò*Ôëuì´ãX¾bJÇút:ìù
  K®Ã'?õ¸òª£Z­bhh¨Ûvü7õÌsÎÅ×¿ù-¼æ'â%/<NÿüçâÎ;ïÂùçC9Ë/Ç1Ç=[n¹%Î<õ8é'w×¹lÎoó  ó,À	'¾
  Lk©Ø¥ çã=õz½Ûµ&ûR½Y«ÕJÛ§&ãU|EÑ5dèÚ èÑvQÏ2è¨£ææº
  MU§Æ­9fÕ±.
  P|ÌßÐÐõ(`9Pu
  T*¼_c1Æcy 1ußtÛæÊÀ àþµ¶ØÒc1Æc1SÏtÖ}Óul£^pç[c1ÆcZ¦³îÎcÛq
  TP¯×»c"]»NÛF c1ÆcÌäS­VÑiwz¹ª¥Ên¦·TV«U4Í>O4èý*Üc?<Ï©¦ÐC«Õê»zÙy<óöëïÔ¾.TÃÈhªy
  TzNv:¾cívJJ%=OZ­*
  TJ¯>aDxVÀOõa
  T°ývÛm2ïþXøÀß÷|ø<Ü¶ôvò
  Tºµl1Àc1Æc&êê¿²Âu*þ(p½È·ïÓtä8ÖëUóéù¢(º°l×ºyÿ1*]µj]-Ô
  U©Ët®9®Ï
  WOx&ÆÕÚ¡Eÿ2¬/MfÑàùÐ´J¤æNh[Ñz£§
  aÑiÑ@½üõz½/#Ä1ÇhÈÐ?ZbÇhÒñsETý1@sr1Æc1f²Ð ª_¨mT£©Ç;+¸§b<¦lÇkUéñ²øÀú$K":ÕÀt:fcz/uõÎk£Z2:ÅéÀuîØ¶FDãL¥ËÎ3±¯V-¸P¶íC½1¿#³D´h`SVd!sYÛá ^Oc¾|YE­QÙ
  fãË
  jµÚÍx}|É´
  k§5	¢í§1]Àc1Æ3Ð³Ó	uaòªËrùc{Ôjªâµt*z}Ø¦Ç_©ñþ-
  tÌÇ<óxì¡Á
  vãØ9¦V«
  w?n¿À.
  ytë:ÂÄ|ø2:Ì<åÔdÔ§ÔqÇ6m'¦·m¯E
  ¨Ë*Û¡@ÃJhYáhyÊ^µiñ>­e-,Û;ÄÂº÷eöÂ3<
  s¶³Þýv¼÷ìbÑ_  æî¸#N{Ç°å¬YÖ×¤F °H­=¤
  S±®ýÇ5ÊÖènìK×"Ñz£¢/Ý¬A
  _7¿áõh6èt:¨V«øÌ%Ä¿s
  ¢¦PkQ3i
  ?-ÕóõÎÑÌ9«iÕê¤ÖþÙGV Ví§FT]«cýgQë¼_£ÓUê§nÈuÓ1ÇosTàÆø¸ûÀX)bÈ*«ëäzì¡á,¯C
  2åÔAé@ïöCW­V«ÏY­tµAKªzÜíýÛ)N4
  ¥Zëµa1Æc1ÊG¯yxUM¨ÏóOÇæÈÈHOz3#·i<ÈÂâ5?æ³Z­Ö­ú_©TºíiÛÔh:?5LPÜGï¿I·
  §àìÓOÃ^{î
  ¨¹â¸øÉ¢}J###]-wàw-Påêkt|f$P½ÉO]jªCG,Éæ9º,A'¨
  ©äªÛTãhzîWÖ§MEi ª³b$ù =À1è¯]@Ñ®òËÖ-FS°
  ¬z~I®ÎáL'GCøªUÔgõ	x­K¸Ëç«×G#ÈD(¢gZCÐÙ¡Z
  ®äÔ.Õj5MéV±Ëöµ¬è]É<§Ú×dyúÑÙË{øÓ²<
  ±"¶Ï{8_²
  µXPS¬ÙnÔLêüóÊRà£iÜz¶rpà8bD:¿k±A®µoz°_
  Á¢ilG¯ºL¯"XõfæÁg;jäÔ°Á1«qmf£tMøÉud<çI
  Ðq¢QGèýæ$ÕûÎ>ôèB©5GÇ
  Ð´V«Õ~_<}¹Z­V:q}±hi÷lÞl÷tñ
  Ö^UÇ×ëùD«xèbhq>µdIE>ï×ð
  ßt±>^ì;Sí£%Êjë±ß¬f@YÃx(²Ì>È£ËªýÇÊû¹ *£%)~²>A6}àÑZÂÏ/V[ÐS2Ë¹/
  â1Í_&~ãx¢öR»:WiXÐmÝ©ÕPAçx-Ú¡Õj¥
  çÍf³ÛW&n0îH¹`´tÄtv¬!ûB$âÄF¢â^'l_
  ëÿÑ}öÞ
  îNR=744Ô#Jy­
  ð5Ý«kÇ1r1ê!FDpLÙnã¡»
  ó{#þÎ¨uÛ¨] t³"yq QÔ½!îZ
License: UNKNOWN
 FIXME

Files: ./data/screenshots/gnome-documents-2.png
Copyright:  7n$7ü¼ì	éÍ#Ë¯>ÏwÌ40lw¸Ú7Æõ¾è¾ßOÿ# àÈ#þ#yíQ:Óþ0Óþð!Õ}}Ûwþúó1nìXÌ7ÅbÇ|øC8`ÿý ÿß=÷à7Þ¹óæáò«®Æ+ñÆÝvÃ;ßñöAm(ÙjÊøë§qÎ_ÁNûrÈá¼+¯Á¬¹ó0uóÍ¬!7 üâÿ+¾r
  3ð±S?-§NÅòåË §þ)<?w.æ/Xúnxx%jÒ) 1 <úÇðÙ/eËa×]^
  Eÿö¯ÿvôÌ3æ 2}úôöÜsÏ=FzpÆc1ÆcZýË£+v{ÃnïÎìcÛ{=ö°ø7Æc1Æc^¼öµ¯wç/ï<?7{öìS§NÝx¤d1Æc1ÆááÙç­æV­ZU/JNü7Æc1Æc^¡,Y²ùb±hño1Æc1Æôg}f¤ÐoÆ"°z c1Æc1ÆôuMGEäGzÆc1Æc~Y'êõ:f??³fÏÂÒeK×Ú&l0¯ÚúUØzË­ÏÛ>a1Æc1ÃÅhÖ}£ylë*¹J¥[5gõ"ï¸ýÓ5f­
  S  ³çÎÃ¹WÏúQL0aHúÉLè6ì¿?Æ/^r)~y×¯ðü9ãô^ÿ!¡4´ÕÆc-04­ÿ¿â×-bxæ³Ï îþÍ½¸û7÷¶45çù¹ØbóÍðÉOßMû}Ë¹U«VáÀ·í¿ÿýdÜtË÷ñÑÿ:cÇÁ;ÿåøøI'¢í»ÝÛ1ÐñlûWwz£óeÅsÓñÑ¨°ÑF V,_
  TBOO*ÕËår³¨p½^G¡P@¥RéW!Dc1í
  T°ÿ[ÿ/Î<ãtK¥~ 1ÆÑÄ&
  TÊår9c1Æc1fÝ
  h4Ï_v%fÏ/}õpá§?1$ý
  Ñhà} ?ùÙÏ0ã§qæéD¹TJÎ-k|Û®®.|ëëÿ+¿r
License: UNKNOWN
 FIXME

Files: ./data/screenshots/gnome-documents-1.png
Copyright: CßÀ£©c^å=Ãs*1g¿ßJôÚF
  8òmµ®y/P<=È
  ak@Q¥çUøTy(QhôUQSõçzgõå~ç>fAâ|¹-|*­eþ7ÿk}ÍÿæùßüÏårzó¿ùßüoþgÿÊÿç//õ>z@´<`ÚY¨
  m¯HUÐz£Î`,U
  u{æô3qàöµÕi«à Ã0
  uÕ»ÕjÅÕ«WcïÒÞÊr·%à Ã0
  §ùßü¯}õ¥ùßüÿëãÿÄÁÇ5ÞÇyF¸"Ì(ïÕàÔP¤Ç	¬L ´N§Sbþ,/×
  ¯êÔÉmâþÆó<æ'P:Æÿ¢(
  ±PVT
  ¸d
  ºÿÍÿz}V}Ìÿæ-Ãüoþ7ÿÿÍÿæ¤7ÿ7ÿ
  ¼2
  ¾66S§O¨U m6Îªt¯ÒS&wÆAS02§þGÝ!?MõáÔéSÑjµÆ4­Þ2m0vþGþý~?ãÞÃ÷®Ý±ÆàÔéS¥9äd&ËªÆúF×ÿè>ëÓ'ÓZÑMÑÿãTê1Îö¢OzçP>Ñ4¼@(3ë(nòòÀ
  Ä5¢×ëÅh8hEa8*ÔÖ^TEðU
  ßàÔPö ,Ù[ÈåqægxN2
  øø_ùÊWzòoaaa7 ^ô¢í}ÿ£ïÿÖ±cÇN:tèöyWÈ0
License: UNKNOWN
 FIXME

Files: ./help/C/media/icon_gnome-documents.png
Copyright: *B¾±ñ÷¨o¨w|ÛT
  2l÷)
  P¥À»ï¾úÎwï_X¼©µ¥ÕÎ>í7ÖpS
  dúµÉSSçd¯Í[«K;'RqVWW­®bYÂáZjkëçà(148Äää)[ðûýÃ!n¾ù¦² N>ý
  <õÔã­_*´@Vûs©ïvyø_ý'o¾õ;~ûúoøäðÎÍE×]twöà÷×HÆçK;4¬f~q·ÛÃºu®Þþ^©hG|rÇÍ_	Ë²ZÒ*»H±ÁÂâ<Ó§§°,
  èÒ2ù|¾l=U%.?Âù.MwºHùnðs 0s¹7
  «x;:º¼Ó§¦Ý¸ooÜ¸ñÁ^x!}­Ï ¦XeÒñYXO	<Ë²'âÌáøø1>=|Ó4R
  ¯ruWgùËu¨ú?õÇR¢EÅýÛTñ£µ£¹ÔH)mEAS5±ºCwiR×u@P!RJ!Äyà´¬7±ô_>ÿüóÆ!pE¤ªýdÃMª«`rr]×Bá»îUëëë¯ré|çóyÍÌ=ñD¼gnþÂ}/^üÉSóä³î?ò/ûöí³¾2;wîÔL+ó@¤®^äLiIrfYÍfHgRRÚÇ$Å}²¸
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: -- Jeremy Bicha <jbicha@debian.org> Sat, 02 Mar 2019 20:00:28 -0500
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2011, Cosimo Cecchi <cosimoc@gnome.org>
  2012, Julita Inca
License: UNKNOWN
 FIXME

Files: ./po/de.po
Copyright: 2011, Cosimo Cecchi <cosimoc@gnome.org>.
License: UNKNOWN
 FIXME

Files: ./po/it.po
Copyright: 2011, gnome-documents's COPYRIGHT HOLDER
  2012-2019, Free Software Foundation
License: UNKNOWN
 FIXME

Files: ./po/da.po
Copyright: 2011-2016, Free Software Foundation and translators
License: UNKNOWN
 FIXME

Files: ./po/gl.po
Copyright: 2012, Leandro Regueiro
  guez <frandieguez@gnome.org>, 2011, 2012.
License: UNKNOWN
 FIXME

Files: ./subprojects/libgd/libgd.doap
Copyright: Lureau</foaf:name>
License: UNKNOWN
 FIXME

Files: ./data/org.gnome.Documents.appdata.xml.in
Copyright: 2013, Red Hat, Inc. -->
License: UNKNOWN
 FIXME

Files: ./po/bs.po
Copyright: 2013, Rosetta Contributors and Canonical Ltd 2013
License: UNKNOWN
 FIXME

Files: ./po/te.po
Copyright: 2011, e-telugu Localization Team
  2012, Swecha Telugu Localisation Team <localization@swecha.net>
License: UNKNOWN
 FIXME

Files: ./po/oc.po
Copyright: 2012, gnome-documents's COPYRIGHT HOLDER
  dric Valmary (Tot en Ãc) <cvalmary@yahoo.fr>, 2015.
  dric Valmary (Tot en Ã²c) <cvalmary@yahoo.fr>, 2015.
  dric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2016.
License: UNKNOWN
 FIXME

Files: ./help/te/te.po
Copyright: 2011, e-telugu localization Team.
License: UNKNOWN
 FIXME

Files: ./po/pt.po
Copyright: 2011-2014, gnome-documents
License: UNKNOWN
 FIXME

Files: ./po/en_GB.po
Copyright: 2011, gnome-documents'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/ta.po
Copyright: 2011, gnome-documents's COPYRIGHT HOLDER
  à¯ à®à¯à®®à®¾à®°à¯ à®à¯à®®à¯à®³à®¿à®©à¯) <naveenmtp@gmail.com>, 2011.
License: UNKNOWN
 FIXME

Files: ./po/pl.po
Copyright: 2011-2019, the gnome-documents authors.
License: UNKNOWN
 FIXME

Files: ./po/ja.po
Copyright: 2011, the gnome-documents's COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./help/pa/pa.po
Copyright: à¨¤ à¨¸à¨¿à©°à¨ à¨à¨²à¨®à¨µà¨¾à¨²à¨¾n"
License: UNKNOWN
 FIXME

Files: ./help/C/media/go-down.png
Copyright: ª
License: UNKNOWN
 FIXME

Files: ./data/media/thumbnail-frame.png
Copyright: ÙïIú¼CêH©¹+
License: UNKNOWN
 FIXME

Files: ./help/ta/ta.po
Copyright: à¯ à®¤à¯à®à¯à®ªà¯à®ªà¯ à®à®°à¯à®µà®¾à®à¯à®à¯à®¤à®²à¯"
License: UNKNOWN
 FIXME

