Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Build.PL
 ./Changes
 ./MANIFEST
 ./META.json
 ./META.yml
 ./Makefile.PL
 ./debian/README.source
 ./debian/compat
 ./debian/control
 ./debian/control.in
 ./debian/gbp.conf
 ./debian/patches/pod-errors.patch
 ./debian/patches/series
 ./debian/source/format
 ./debian/watch
 ./lib/MARC/File/MiJ.pm
 ./lib/MARC/Record/MiJ.pm
 ./t/00-load.t
 ./t/boilerplate.t
 ./t/camel.usmarc
 ./t/test.ndj
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./README
Copyright: 
  2013, Bill Dueber
  This software is free software and may be distributed under the same
License: Artistic or GPL-1+
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: Artistic or GPL-1+ and/or GPL-3+
 FIXME

Files: ./debian/rules
Copyright: 2014, Jonas Smedegaard <dr@jones.dk>
License: GPL-3+
 FIXME

Files: ./t/test.mrc
Copyright: d1898-1967.2 aArts Council of Great Britain.2 aTate Gallery.0 aMiUbUGLhND673.M33 S990 aMiUbAAELcBKShND 673 .M33 S990 aMiUbFINEhND 673 .M33 S990 aMiUbFINEhND 673 .M33 S99 aBKbBook aCEbElectronic Resource aMiU c20040625 c20040625 c20040625 c20040809 aHTbavail_ht aACbavail_circ umdp.39015009416986ric s9665 acollagebcollage cardboard assemblage cut version newspaper bottle photograph mine mounted pasted gouache fragment readymade woodenc11001531nam^a22004331^^450000100100000000500170001000600190002700700150004600800410006101000130010201500140011503500240012903500240015303500230017704000180020005000210021808200110023911000190025024500240026925000310029326000300032430000510035453800300040565000300043565000400046565000260050585200370053197000130056897000280058197100080060997200130061797300170063097300190064797400270066699800090069399501430070299501180084599501340096300000038519880715000000.0m^^^^^^^^d^^^^^^^^cr^bn^---auaua880715r1967^^^^enkaf^^^^^^^^|00000^eng^^ a70371182 aB68-22096 a(RLIN)MIUG0037351-B a(CaOTULAS)159818471 a(OCoLC)ocm00037351 aDLCcDLCdMiU0 aNK928b.L57 1967 a913.362 aLondon Museum.10aMedieval catalogue. a[1st ed., 4th impression]. aLondon,bH.M.S.O.,c1975. a319 p.b99 plates, illus. (some col.).c26 cm. aMode of access: Internet. 0aDecorative artszEngland. 0aDecorative arts, MedievalzEngland. 0aArchaeology, Medieval0 aMiUbBUHRcGRADhNK928 .L85 1975 aBKbBook aCEbElectronic Resource aMiU c20040625 aHTbavail_ht aACbavail_circ umdp.39015010558024ric s9665 aarchaeologybvessel excavation type pottery fragment excavated grave burial archaeological evidence figurine period date fabric sherdc595 ametalworkbgilt engraved diam oval chased ring head bezel handle hoop circular mount shaped ornament piercedc731 atime periodsbcentury centuries cen tury quarter beginning dating llth turies import ury l7th centur progenitor craftmanshipc77101318nam^a22004331^^450000100100000000500170001000600190002700700150004600800410006101000130010201500140011502000210012903500240015003500240017403500230019804000180022105000200023908200130025910000270027224500330029926000360033230000490036849000360041753800300045365000300048365000200051385200350053385200350056897000130060397000280061697100080064497200130065297200130066597300170067897300190069597400270071499800140074199501290075500000039619880715000000.0m^^^^^^^^d^^^^^^^^cr^bn^---auaua880715s1968^^^^enka^^^^^^^^^^00000^eng^^ a74366090 aB68-24743 a0289277884c25/- a(RLIN)MIUG0040363-B a(CaOTULAS)159818483 a(OCoLC)ocm00040363 aDLCcDLCdMiU0 aN6493 1920b.H5 a709.04/21 aHillier, Bevis,d1940-10aArt Deco of the 20s and 30s. a[London]bStudio Vista,c[1968] a168 p. (chiefly illus. (some col.)).c19 cm.0 aStudio Vista/Dutton pictureback aMode of access: Internet. 0aArt, Moderny20th century 0aDecorative arts0 aMiUbAAELcBKShN 6494 .A7 H650 aMiUbAAELcBKShN 6494 .A7 H65 aBKbBook aCEbElectronic Resource aMiU c20040625 c20040625 aHTbavail_ht aACbavail_circ umdp.39015033751887ric cGACs9125 aerasbtime sixties thirties twenties fifties decade seventies forties eighties own world generation late period examplec28701921nam^a22004931^^450000100100000000500170001000600190002700700150004600800410006101000130010203500240011503500240013903500230016304000180018605000200020408200150022410000350023924500900027425000130036426000310037730000550040850000450046350400640050853800300057265000240060265000190062665000210064585200360066685200390070297000130074197000280075497100080078297200130079097200130080397300170081697300190083397400370085297400370088999800090092699501160093599501060105199501410115799501290129800000049919880715000000.0m^^^^^^^^d^^^^^^^^cr^bn^---auaua880715s1969^^^^nyuabe^^^b^^^^00110^eng^^ a69015754 a(RLIN)MIUG0051006-B a(CaOTULAS)159818600 a(OCoLC)ocm00051006 aDLCcDLCdMiU0 aNA275b.S3 1969 a726.1/2/081 aScully, Vincent Joseph,d1920-14aThe earth, the temple, and the gods;bGreek sacred architecturec[by] Vincent Scully. aRev. ed. aNew York,bPraegerc[1969] axxxii, 271, [192] p.billus., maps, plans.c26 cm. a"Illustrations": p. [1]-[192] (3d group) aBibliographical references included in "Notes" (p. 227-251) aMode of access: Internet. 0aArchitecture, Greek 0aTemples, Greek 0aMythology, Greek0 aMiUbAAELcBKShNA275 .S44 19690 aMiUbHATCHcGRADhNA 275 .S44 1969 aBKbBook aCEbElectronic Resource aMiU c20040625 c20040625 aHTbavail_ht aACbavail_circ umdp.39015004888684ricd20090702 umdp.39015006345766ricd20091105 s9665 adeitiesbgod ancient goddess sacred cult ritual power sun legend priest myth king animal sacrifice worshipc417 anaturebmountain river valley mile rock road lake stream bank near slope plain peak beyond cliffc618 aancient architecturebtemple wall tomb pyramid court palace chamber colonnade capital sanctuary built plan cella stone architecturec683 aspacebeffect space colorplate heavy weight contrast strong powerful power detail force entire structural solid massivec77601692nam^a22004931^^450000100100000000500170001000600190002700700150004600800410006101000130010202000220011503500240013703500240016103500230018504000180020804100110022605000150023710000270025224000240027924502130030326000390051630000590055550000300061450400270064453800300067165000110070170000180071285200360073085200410076685200430080785200420085097000130089297000280090597100080093397200130094197200130095497200130096797200130098097300170099397300190101097400270102999800090105699501330106500000071920010330000000.0m^^^^^^^^d^^^^^^^^cr^bn^---auaua880715s1969^^^^nyuac^^^^^^^^|00100^eng^^ a78093950 a0876631065c10.00 a(RLIN)MIUG0063058-B a(CaOTULAS)159818857 a(OCoLC)ocm00063058 aDLCcDLCdMiU1 aengita0 aNX210b.D61 aDorfles, Gillo,d1910-03aIl Kitsch.lEnglish00aKitsch; the world of bad taste.cWith special contributions by John McHale [and others] and essays by Clement Greenberg and Hermann Broch. [Editorial assistance on the English-language ed. by Vivienne Menkes. a[New York]bUniverse Booksc[1969] a313 p.billus. (part col.), ports. (part col.)c25 cm. aTranslation of Il Kitsch. aIncludes bibliography. aMode of access: Internet. 0aKitsch1 aMcHale, John.0 aMiUbFINEhNX 600 .K5 D693 19690 aMiUbAAELcBKShNX 600 .K5 D693 19690 aMiUbHATCHcGRADhNX 600 .K5 D693 19690 aMiUbFINEhNX 600 .K5 D693 1969xGift aBKbBook aCEbElectronic Resource aMiU c20040625 c20040625 c20040625 c20040625 aHTbavail_ht aACbavail_circ umdp.39015012241918ric s9665 amythologybmyth reality own language anti sense condition situation history context past attempt kitsch truth contradictionc200
License: UNKNOWN
 FIXME

Files: ./debian/copyright_hints
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: put license application notes in a X-Note field
License: UNKNOWN
 FIXME

