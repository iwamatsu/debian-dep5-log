Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./ChangeLog
 ./HACKING
 ./Makefile.in
 ./README
 ./TODO
 ./config.h.in
 ./configure.ac
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/extra/isnsadm.conf.5
 ./debian/extra/isnsd.conf.5
 ./debian/extra/isnsd.service
 ./debian/extra/isnsdd.conf.5
 ./debian/extra/isnsdd.service
 ./debian/gbp.conf
 ./debian/libisns-dev.install
 ./debian/libisns-dev.links
 ./debian/libisns-nocrypto0-udeb.install
 ./debian/libisns0.install
 ./debian/libisns0.symbols
 ./debian/open-isns-discoveryd.config
 ./debian/open-isns-discoveryd.docs
 ./debian/open-isns-discoveryd.install
 ./debian/open-isns-discoveryd.isnsdd.init
 ./debian/open-isns-discoveryd.postinst
 ./debian/open-isns-discoveryd.postrm
 ./debian/open-isns-discoveryd.prerm
 ./debian/open-isns-discoveryd.templates
 ./debian/open-isns-server.docs
 ./debian/open-isns-server.install
 ./debian/open-isns-server.isnsd.init
 ./debian/open-isns-server.lintian-overrides
 ./debian/open-isns-server.postinst
 ./debian/open-isns-server.postrm
 ./debian/open-isns-server.templates
 ./debian/open-isns-utils.docs
 ./debian/open-isns-utils.install
 ./debian/po/POTFILES.in
 ./debian/po/pt.po
 ./debian/rules
 ./debian/source/format
 ./debian/source/lintian-overrides
 ./debian/tests/auth
 ./debian/tests/control
 ./debian/tests/discoveryd
 ./debian/tests/functions.inc
 ./debian/tests/server
 ./debian/watch
 ./doc/isns_config.5
 ./doc/isnsadm.8
 ./doc/isnsd.8
 ./doc/isnsdd.8
 ./etc/isnsadm.conf
 ./etc/isnsd.conf
 ./etc/isnsdd.conf
 ./isnsd.service
 ./isnsd.socket
 ./mdebug.c
 ./tests/Makefile
 ./tests/client.conf
 ./tests/data/test01/01-enroll
 ./tests/data/test01/02-registration
 ./tests/data/test01/03-query
 ./tests/data/test01/03-registration
 ./tests/data/test01/99-unregistration
 ./tests/data/test02/01-enroll
 ./tests/data/test02/02-enroll
 ./tests/data/test02/03-registration
 ./tests/data/test02/04-query
 ./tests/data/test02/05-query
 ./tests/data/test02/06-dd-registration
 ./tests/data/test02/07-query
 ./tests/data/test02/08-query
 ./tests/data/test02/09-query
 ./tests/data/test02/10-dd-registration
 ./tests/data/test02/11-query
 ./tests/data/test02/12-dd-deregistration
 ./tests/data/test02/13-dd-deregistration
 ./tests/data/test02/14-dd-registration
 ./tests/data/test02/15-dd-deregistration
 ./tests/data/test03/01-enroll
 ./tests/data/test03/02-registration
 ./tests/data/test03/03-unregistration
 ./tests/data/test03/04-unregistration
 ./tests/data/test03/99-unregistration
 ./tests/data/test04/01-enroll
 ./tests/data/test04/02-registration
 ./tests/data/test04/03-restart
 ./tests/data/test04/04-query
 ./tests/data/test05/01-enroll
 ./tests/data/test05/02-registration
 ./tests/data/test05/03-expired
 ./tests/data/test06/01-enroll
 ./tests/data/test06/02-registration
 ./tests/data/test06/03-registration
 ./tests/data/test06/04-registration
 ./tests/data/test06/05-dd-registration
 ./tests/data/test06/06-registration
 ./tests/data/test06/07-dd-registration
 ./tests/data/test06/08-registration
 ./tests/data/test06/09-registration
 ./tests/data/test06/10-unregistration
 ./tests/data/test06/11-registration
 ./tests/data/test07/01-enroll
 ./tests/data/test07/02-registration
 ./tests/data/test07/03-expired
 ./tests/data/test07/04-registration
 ./tests/data/test07/05-expired
 ./tests/data/test08/01-pauw1
 ./tests/data/test09/01-pauw2
 ./tests/data/test10/01-pauw3
 ./tests/data/test10/02-expired
 ./tests/data/test10/03-pauw3
 ./tests/data/test10/04-expired
 ./tests/data/test11/01-pauw4
 ./tests/harness.pl
 ./tests/pauw1.c
 ./tests/pauw2.c
 ./tests/pauw3.c
 ./tests/pauw4.c
 ./tests/server.conf
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./attrs.c
 ./authblock.c
 ./bitvector.c
 ./buffer.c
 ./callback.c
 ./client.c
 ./config.c
 ./db-file.c
 ./db-policy.c
 ./db.c
 ./db.h
 ./dd.c
 ./deregister.c
 ./domain.c
 ./entity.c
 ./error.c
 ./esi.c
 ./export.c
 ./getnext.c
 ./include/libisns/attrs.h
 ./include/libisns/buffer.h
 ./include/libisns/isns-proto.h
 ./include/libisns/isns.h
 ./include/libisns/message.h
 ./include/libisns/paths.h.in
 ./include/libisns/source.h
 ./include/libisns/types.h
 ./include/libisns/util.h
 ./internal.h
 ./isnsadm.c
 ./isnsd.c
 ./isnsdd.c
 ./isnssetup
 ./local.c
 ./message.c
 ./objects.c
 ./objects.h
 ./parser.c
 ./pidfile.c
 ./pki.c
 ./policy.c
 ./portal-group.c
 ./query.c
 ./register.c
 ./relation.c
 ./scn.c
 ./scope.c
 ./security.c
 ./security.h
 ./server.c
 ./simple.c
 ./slp.c
 ./socket.c
 ./socket.h
 ./storage-node.c
 ./sysdep-unix.c
 ./tags.c
 ./tests/genkey
 ./tests/test01.pl
 ./tests/test02.pl
 ./tests/test03.pl
 ./tests/test04.pl
 ./tests/test05.pl
 ./tests/test06.pl
 ./tests/test07.pl
 ./tests/test08.pl
 ./tests/test09.pl
 ./tests/test10.pl
 ./tests/test11.pl
 ./timer.c
 ./util.c
 ./vendor.c
 ./vendor.h
Copyright: 2003-2006, Olaf Kirch <olaf.kirch@oracle.com>
  2003-2007, Olaf Kirch <olaf.kirch@oracle.com>
  2006-2007, Olaf Kirch <olaf.kirch@oracle.com>
  2007, Olaf Kirch <olaf.kirch@oracle.com>
License: UNKNOWN
 FIXME

Files: ./compat/my_getopt.c
 ./compat/my_getopt.h
Copyright: 1997, 2000-2002, Benjamin Sittler
License: Expat
 FIXME

Files: ./aclocal/config.guess
 ./aclocal/config.sub
Copyright: 1992-2005, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./debian/po/nl.po
 ./debian/po/templates.pot
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./libisns.vers
Copyright: NONE
License: LGPL-2.1
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2003-2010, Olaf Kirch <olaf.kirch@oracle.com>
License: LGPL-2.1+
 FIXME

Files: ./aclocal/install-sh
Copyright: 1991, the Massachusetts Institute of Technology
License: NTP
 FIXME

Files: ./etc/openisns.init
Copyright: 2007, Albert Pauw
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./debian/po/fr.po
Copyright: 2016, French l10n team <debian-l10n-french@lists.debian.org>
License: UNKNOWN
 FIXME

Files: ./logging.c
Copyright: 2004-2007, Olaf Kirch <okir@suse.de>
License: UNKNOWN
 FIXME

Files: ./debian/po/de.po
Copyright: 2003-2010, Olaf Kirch <olaf.kirch@oracle.com>,
  of this file Chris Leick <c.leick@vollbio.de> 2016.
License: UNKNOWN
 FIXME

Files: ./debian/po/pt_BR.po
Copyright: 2016, THE open-isns'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

