Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.hgignore
 ./.hgtags
 ./Makefile
 ./README.txt
 ./VERSION
 ./compile/Make_bc5.mak
 ./compile/Make_cyg.mak
 ./compile/Make_gcc.mak
 ./compile/Make_mvc.mak
 ./compile/Make_osx.mak
 ./compile/Make_sun.mak
 ./compile/clean.mak
 ./compile/clean_dos.mak
 ./compile/clean_unix.mak
 ./compile/config.mk.in
 ./compile/config_default.mk
 ./compile/dll_migemo.dep
 ./compile/dll_migemo.mak
 ./compile/dos.mak
 ./compile/migemo.dep
 ./compile/migemo.mak
 ./compile/old_mvc.mak
 ./compile/unix.mak
 ./compile/vs2003/CMigemo.sln
 ./compile/vs2003/CMigemo/CMigemo.vcproj
 ./compile/vs2003/CMigemo/clean.bat
 ./compile/vs2003/MigemoDLL/MigemoDLL.vcproj
 ./compile/vs2003/MigemoDLL/clean.bat
 ./compile/vs2003/clean.bat
 ./compile/vs6/clean.bat
 ./compile/vs6/dbg_migemo.dsp
 ./compile/vs6/dll_migemo.dsp
 ./compile/vs6/migemo.dsp
 ./compile/vs6/migemo.dsw
 ./compile/vs6/prof_migemo.dsp
 ./config.mk
 ./configure
 ./debian/cmigemo-common.README.Debian
 ./debian/cmigemo-common.docs
 ./debian/cmigemo-common.install
 ./debian/cmigemo-common.lintian-overrides
 ./debian/cmigemo-common.manpages
 ./debian/cmigemo-common.postinst
 ./debian/cmigemo-common.prerm
 ./debian/cmigemo.1
 ./debian/cmigemo.README.Debian
 ./debian/cmigemo.install
 ./debian/cmigemo.lintian-overrides
 ./debian/cmigemo.manpages
 ./debian/cmigemo.rd
 ./debian/compat
 ./debian/control
 ./debian/libmigemo-dbg.lintian-overrides
 ./debian/libmigemo-dev.install
 ./debian/libmigemo-dev.lintian-overrides
 ./debian/libmigemo1.install
 ./debian/libmigemo1.lintian-overrides
 ./debian/libmigemo1.symbols
 ./debian/patches/0001-Merge-changes-on-dev-1.3-to-trunk.patch
 ./debian/patches/0006-remove-debug-message.patch
 ./debian/patches/0007-VisualC-9.0-VisualStudio-2009.patch
 ./debian/patches/0009-update-tags.patch
 ./debian/patches/0010-add-ignores.patch
 ./debian/patches/0011-add-more-ignores.patch
 ./debian/patches/0012-include-limits.h-for-INT_MAX-const-value.patch
 ./debian/patches/0013-generate-UTF-8-dict-and-dat-files-for-all-platforms.patch
 ./debian/patches/0014-support-MinGW-with-lot-of-warnings.patch
 ./debian/patches/0015-fix-typo-in-region-label-thanks-gageas-https-twitter.patch
 ./debian/patches/0016-add-destructor-thanks-gageas-https-twitter.com-gagea.patch
 ./debian/patches/0017-change-calling-convenstions-thanks-gageas-https-twit.patch
 ./debian/patches/0018-add-gageas-to-contributor.patch
 ./debian/patches/0019-Handle-cmigemo-path.patch
 ./debian/patches/0020-Improve-searching-migemo-dict-path.patch
 ./debian/patches/0021-Improve-searching-migemo-dict-path.patch
 ./debian/patches/0022-Update-README-for-vim-plugin-use.patch
 ./debian/patches/0023-Fix-use-cpo-save.patch
 ./debian/patches/0024-Fix-Emacs-space-or-newline-regexp.patch
 ./debian/patches/0025-Update-README.txt.patch
 ./debian/patches/0026-Fix-clean.-Clean-utf-8-dict-file.patch
 ./debian/patches/0027-Made-cmigemo-binary-to-work-other-than-compile-direc.patch
 ./debian/patches/0028-Revert-Fix-Emacs-space-or-newline-regexp.patch
 ./debian/patches/0029-add-zya-to-roma2hira-table.patch
 ./debian/patches/9001-Fit-Makefile-to-Debian-packageing.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/source/lintian-overrides
 ./debian/update-cmigemo-dict
 ./debian/update-cmigemo-dict.1
 ./debian/update-cmigemo-dict.rd
 ./debian/vim-migemo.dirs
 ./debian/vim-migemo.install
 ./debian/vim-migemo.lintian-overrides
 ./debian/vim-migemo.yaml
 ./debian/watch
 ./dict/Makefile
 ./dict/dict.mak
 ./dict/han2zen.dat
 ./dict/zen2han.dat
 ./doc/doxygen.conf
 ./mkpkg
 ./src/charset.c
 ./src/charset.h
 ./src/dbg.h
 ./src/depend.mak
 ./src/filename.c
 ./src/filename.h
 ./src/main.c
 ./src/migemo.c
 ./src/migemo.def
 ./src/migemo.h
 ./src/mnode.c
 ./src/mnode.h
 ./src/resource.h
 ./src/romaji.c
 ./src/romaji.h
 ./src/rxgen.c
 ./src/rxgen.h
 ./src/testdir/Makefile
 ./src/testdir/profile_speed.c
 ./src/testdir/romaji_main.c
 ./src/wordbuf.h
 ./src/wordlist.h
 ./tools/Migemo.cs
 ./tools/Test.cs
 ./tools/clsMigemo.cls
 ./tools/migemo.vim
 ./tools/optimize-dict.pl
 ./tools/skk2migemo.pl
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./xpcom/Makefile
 ./xpcom/Test_nsIMigemo.js
 ./xpcom/depend.mak
 ./xpcom/nsIMigemo.idl
 ./xpcom/nsIMigemoModule.cpp
 ./xpcom/nsMigemo.cpp
 ./xpcom/nsMigemo.h
Copyright: 2005, MURAOKA Taro(KoRoN)/KaoriYa
License: UNKNOWN
 FIXME

Files: ./debian/patches/0002-Add-MIT-License-file.patch
 ./doc/LICENSE_MIT.txt
Copyright: 2003-2007, MURAOKA Taro (KoRoN)
License: Expat
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  2003-2007, MURAOKA Taro (KoRoN)
  2003-2007, MURAOKA Taro (KoRoN) <koron@tka.att.ne.jp>
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Fix insecure URL
License: UNKNOWN
 FIXME

Files: ./src/migemo.rc
Copyright: MURAOKA Taro (KoRoN)/KaoriYa0"
License: UNKNOWN
 FIXME

Files: ./xpcom/README.txt
Copyright: 2005, MURAOKA Taro(KoRoN)/KaoriYa
  çTest_nsIMigemo.htmlðJ­ÆA{^ðµ½ÛÉ¿éB
License: UNKNOWN
 FIXME

Files: ./xpcom/Test_nsIMigemo.html
Copyright: 2005, MURAOKA Taro(KoRoN)/KaoriYa -->
License: UNKNOWN
 FIXME

Files: ./dict/roma2hira.dat
Copyright: ki	«
License: UNKNOWN
 FIXME

Files: ./debian/patches/0008-VC9-VisualStudio2008-64bit.patch
Copyright: Á½âèÌC³
License: UNKNOWN
 FIXME

Files: ./debian/patches/0005-Fix-position-of-new-item.patch
Copyright: Á½âèÌC³
  ®»Ê@ðÇÁ
License: UNKNOWN
 FIXME

Files: ./src/wordlist.c
Copyright: ÉÄÀB
License: UNKNOWN
 FIXME

Files: ./src/wordbuf.c
Copyright: ÍÄo¤Å»f·éB
License: UNKNOWN
 FIXME

Files: ./doc/README_j.txt
Copyright: çRo[g·é±Æ
  ê½½­ÌtgEFA©çÌpªeÕÉÈé±ÆAyÑ(½Ô
  ªÅÜÁ½­Ì[©ç«ðì¬·é±ÆàÅ«Ü
License: UNKNOWN
 FIXME

Files: ./doc/LICENSE_j.txt
Copyright: çí
License: UNKNOWN
 FIXME

Files: ./doc/vimigemo.txt
Copyright: ê½W
License: UNKNOWN
 FIXME

Files: ./dict/hira2kata.dat
Copyright: J
License: UNKNOWN
 FIXME

Files: ./doc/TODO_j.txt
Copyright: ì
License: UNKNOWN
 FIXME

Files: ./debian/patches/0004-Fix-add-vowel-characters-after-completed-xtu.patch
Copyright: ®»Ê@ðÇÁ
License: UNKNOWN
 FIXME

Files: ./compile/Make_mingw.mak
Copyright: ã¤ãã©ãªæ§ç¯æ³ãå¤æ´ãã
License: UNKNOWN
 FIXME

