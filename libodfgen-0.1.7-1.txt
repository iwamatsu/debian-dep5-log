Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./Makefile.am
 ./NEWS
 ./README
 ./autogen.sh
 ./build/Makefile.am
 ./build/win32/Makefile.am
 ./build/win32/compile-resource
 ./build/win32/lt-compile-resource
 ./config.h.in
 ./configure.ac
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/libodfgen-0.1-1.install
 ./debian/libodfgen-dev.dirs
 ./debian/libodfgen-dev.install
 ./debian/libodfgen-tools.install
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./docs/Makefile.am
 ./docs/doxygen/Makefile.am
 ./docs/doxygen/doxygen.cfg
 ./docs/doxygen/footer.html.in
 ./docs/doxygen/header.html.in
 ./inc/Makefile.am
 ./inc/libodfgen/Makefile.am
 ./libodfgen.pc.in
 ./m4/._libtool.m4
 ./m4/._ltoptions.m4
 ./m4/._ltsugar.m4
 ./m4/._lt~obsolete.m4
 ./m4/ax_gcc_func_attribute.m4
 ./src/Makefile.am
 ./test/Makefile.am
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./inc/libodfgen/OdsGenerator.hxx
 ./inc/libodfgen/OdtGenerator.hxx
 ./src/FillManager.cxx
 ./src/FillManager.hxx
 ./src/GraphicStyle.cxx
 ./src/GraphicStyle.hxx
 ./src/NumberingStyle.cxx
 ./src/NumberingStyle.hxx
 ./src/OdcGenerator.cxx
 ./src/OdfGenerator.cxx
 ./src/OdfGenerator.hxx
 ./src/OdsGenerator.cxx
 ./src/OdtGenerator.cxx
 ./src/SectionStyle.cxx
 ./src/SheetStyle.cxx
 ./src/SheetStyle.hxx
 ./src/TableStyle.cxx
 ./src/TableStyle.hxx
 ./src/TextRunStyle.cxx
 ./src/TextRunStyle.hxx
 ./test/StringDocumentHandler.cxx
 ./test/StringDocumentHandler.hxx
Copyright: 2002-2003, William Lachance (wrlach@gmail.com)
  2002-2004, William Lachance (wrlach@gmail.com)
  2004, Fridrich Strba (fridrich.strba@bluewin.ch)
  2004-2006, Fridrich Strba (fridrich.strba@bluewin.ch)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./src/GraphicFunctions.cxx
 ./src/GraphicFunctions.hxx
 ./src/OdgGenerator.cxx
 ./src/OdpGenerator.cxx
 ./test/testChart1.cxx
 ./test/testGraphic1.cxx
 ./test/testLayer1.cxx
 ./test/testLink1.cxx
 ./test/testList1.cxx
 ./test/testMasterPage1.cxx
 ./test/testPageSpan1.cxx
 ./test/testParagraph1.cxx
 ./test/testSpan1.cxx
 ./test/testTable1.cxx
 ./test/testTextbox1.cxx
Copyright: 2006, Ariya Hidayat (ariya@kde.org)
  2006, Fridrich Strba (fridrich.strba@bluewin.ch)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./src/DocumentElement.cxx
 ./src/DocumentElement.hxx
 ./src/FilterInternal.hxx
 ./src/FontStyle.cxx
 ./src/FontStyle.hxx
 ./src/ListStyle.cxx
 ./src/ListStyle.hxx
 ./src/PageSpan.cxx
 ./src/PageSpan.hxx
 ./src/SectionStyle.hxx
 ./src/Style.hxx
Copyright: 2002-2003, William Lachance (wrlach@gmail.com)
  2002-2004, William Lachance (wrlach@gmail.com)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./COPYING.LGPL
 ./Makefile.in
 ./build/Makefile.in
 ./build/win32/Makefile.in
 ./docs/Makefile.in
 ./docs/doxygen/Makefile.in
 ./inc/Makefile.in
 ./inc/libodfgen/Makefile.in
 ./m4/ltversion.m4
 ./src/Makefile.in
 ./test/Makefile.in
Copyright: 1991, 1999, Free Software Foundation, Inc.
  1994-2017, Free Software Foundation, Inc.
  2004, 2011-2015, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./compile
 ./depcomp
 ./ltmain.sh
 ./missing
Copyright: 1996-2014, Free Software Foundation, Inc.
  1996-2015, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./inc/libodfgen/OdgGenerator.hxx
 ./inc/libodfgen/OdpGenerator.hxx
 ./src/OdcGenerator.hxx
Copyright: 2006, Ariya Hidayat (ariya@kde.org)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./m4/ltoptions.m4
 ./m4/ltsugar.m4
 ./m4/lt~obsolete.m4
Copyright: 2004-2005, 2007, 2009, 2011-2015, Free Software
  2004-2005, 2007-2008, 2011-2015, Free Software
  2004-2005, 2007-2009, 2011-2015, Free Software
License: UNKNOWN
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2014, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./inc/libodfgen/libodfgen-api.hxx
 ./inc/libodfgen/libodfgen.hxx
Copyright: 2011, Eilidh McAdam <tibbylickle@gmail.com>
  2013, Fridrich Strba <fridrich.strba@bluewin.ch>
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./src/InternalHandler.cxx
 ./src/InternalHandler.hxx
Copyright: 2007, Fridrich Strba (fridrich.strba@bluewin.ch)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./m4/ax_cxx_compile_stdcxx.m4
Copyright: 2008, Benjamin Kosnik <bkoz@redhat.com>
  2012, Zack Weinberg <zackw@panix.com>
  2013, Roy Stogner <roystgnr@ices.utexas.edu>
  2014-2015, Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
  2015, Moritz Klammler <moritz@klammler.eu>
  2015, Paul Norman <penorman@mac.com>
  2016, Krzesimir Nowak <qdlacz@gmail.com>
License: FSFAP
 FIXME

Files: ./m4/ax_cxx_compile_stdcxx_11.m4
Copyright: 2008, Benjamin Kosnik <bkoz@redhat.com>
  2012, Zack Weinberg <zackw@panix.com>
  2013, Roy Stogner <roystgnr@ices.utexas.edu>
  2014-2015, Google Inc.; contributed by Alexey Sokolov <sokolov@google.com>
  2015, Moritz Klammler <moritz@klammler.eu>
  2015, Paul Norman <penorman@mac.com>
License: FSFAP
 FIXME

Files: ./aclocal.m4
Copyright: 1996-2017, Free Software Foundation, Inc.
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: GPL
 FIXME

Files: ./src/FilterInternal.cxx
Copyright: NONE
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./inc/libodfgen/OdfDocumentHandler.hxx
Copyright: 2004, William Lachance (wlach@interlog.com)
License: LGPL-2.1+ and/or MPL-2.0
 FIXME

Files: ./COPYING.MPL
Copyright: NONE
License: MPL-2.0
 FIXME

Files: ./debian/copyright
Copyright: 2002-2004, William Lachance (wrlach@gmail.com)
  Copyright (c) 2004-2007 Fridrich Strba (fridrich.strba@bluewin.ch)
License: MPL-2.0
 FIXME

Files: ./debian/changelog
Copyright: (add full MPL 2.0 text) (closes: #750803)
License: UNKNOWN
 FIXME

Files: ./src/libodfgen.rc.in
Copyright: 2002-2013, William Lachance, Marc Maurer, Fridrich Strba, other contributers"
License: UNKNOWN
 FIXME

