Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./CMakeLists.txt
 ./Makefile.am
 ./Makefile.cvs
 ./common/crc32.c
 ./configure.in
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/libmpcdec-dev.install
 ./debian/libmpcdec6.install
 ./debian/musepack-tools.install
 ./debian/patches/01_am-maintainer-mode.patch
 ./debian/patches/03_mpcchap.patch
 ./debian/patches/04_link-order.patch
 ./debian/patches/05_visibility.patch
 ./debian/patches/add_subdir-objects.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./docs/Doxyfile
 ./docs/custom.css
 ./docs/mainpage.txt
 ./include/Makefile.am
 ./libmpc.kdevelop
 ./libmpcdec/AUTHORS
 ./libmpcdec/CMakeLists.txt
 ./libmpcdec/ChangeLog
 ./libmpcdec/Makefile.am
 ./libmpcdec/README
 ./libmpcenc/CMakeLists.txt
 ./libmpcenc/Makefile.am
 ./libmpcpsy/CMakeLists.txt
 ./libmpcpsy/Makefile.am
 ./libwavformat/CMakeLists.txt
 ./libwavformat/Makefile.am
 ./libwavformat/input.c
 ./libwavformat/libwaveformat.h
 ./libwavformat/output.c
 ./mpc2sv8/CMakeLists.txt
 ./mpc2sv8/Makefile.am
 ./mpcchap/CMakeLists.txt
 ./mpcchap/Makefile.am
 ./mpcchap/dictionary.c
 ./mpcchap/dictionary.h
 ./mpcchap/iniparser.c
 ./mpcchap/iniparser.h
 ./mpccut/CMakeLists.txt
 ./mpccut/Makefile.am
 ./mpcdec/CMakeLists.txt
 ./mpcdec/Makefile.am
 ./mpcenc/CMakeLists.txt
 ./mpcenc/Makefile.am
 ./mpcgain/CMakeLists.txt
 ./mpcgain/Makefile.am
 ./wavcmp/CMakeLists.txt
 ./wavcmp/Makefile.am
 ./win32/attgetopt.c
 ./win32/basename.c
 ./win32/getopt.h
 ./win32/libcommon.vcproj
 ./win32/libcommon_2005.vcproj
 ./win32/libgen.h
 ./win32/libmpcdec.vcproj
 ./win32/libmpcdec_2005.vcproj
 ./win32/libmpcenc.vcproj
 ./win32/libmpcpsy.vcproj
 ./win32/libwavformat.vcproj
 ./win32/mpc2sv8.vcproj
 ./win32/mpccut.vcproj
 ./win32/mpcdec.vcproj
 ./win32/mpcenc.vcproj
 ./win32/mpcgain.vcproj
 ./win32/musepack.sln
 ./win32/musepack_2005.sln
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./include/mpc/mpc_types.h
 ./include/mpc/mpcdec.h
 ./include/mpc/reader.h
 ./include/mpc/streaminfo.h
 ./libmpcdec/COPYING
 ./libmpcdec/decoder.h
 ./libmpcdec/huffman.c
 ./libmpcdec/huffman.h
 ./libmpcdec/internal.h
 ./libmpcdec/mpc_bits_reader.c
 ./libmpcdec/mpc_bits_reader.h
 ./libmpcdec/mpc_decoder.c
 ./libmpcdec/mpc_demux.c
 ./libmpcdec/mpc_reader.c
 ./libmpcdec/mpcdec_math.h
 ./libmpcdec/requant.c
 ./libmpcdec/requant.h
 ./libmpcdec/streaminfo.c
 ./libmpcdec/synth_filter.c
 ./mpc2sv8/mpc2sv8.c
 ./mpccut/mpccut.c
 ./mpcdec/mpcdec.c
 ./mpcgain/mpcgain.c
Copyright: 2005, The Musepack Development Team
  2005-2009, The Musepack Development Team
  2006-2009, The Musepack Development Team
  2007-2009, The Musepack Development Team
License: BSD-3-clause
 FIXME

Files: ./libmpcenc/analy_filter.c
 ./libmpcenc/bitstream.c
 ./libmpcenc/encode_sv7.c
 ./libmpcenc/huffsv7.c
 ./libmpcenc/quant.c
 ./libmpcpsy/ans.c
 ./libmpcpsy/cvd.c
 ./libmpcpsy/fft4g.c
 ./libmpcpsy/fft_routines.c
 ./libmpcpsy/profile.c
 ./libmpcpsy/psy.c
 ./libmpcpsy/psy_tab.c
 ./mpcenc/config.h
 ./mpcenc/mpcenc.c
 ./mpcenc/mpcenc.h
 ./mpcenc/predict.h
 ./mpcenc/wave_in.c
Copyright: 1999-2004, Buschmann/Klemm/Piecha/Wolf
  2005-2009, The Musepack Development Team
License: LGPL-2.1+
 FIXME

Files: ./include/mpc/datatypes.h
 ./include/mpc/mpcmath.h
 ./libmpcenc/libmpcenc.h
 ./libmpcpsy/libmpcpsy.h
Copyright: NONE
License: LGPL-2.1+
 FIXME

Files: ./common/fastmath.c
 ./include/mpc/minimax.h
 ./mpcenc/winmsg.c
Copyright: 1999-2004, Buschmann/Klemm/Piecha/Wolf
License: LGPL-2.1+
 FIXME

Files: ./win32/dirent.c
 ./win32/dirent.h
Copyright: 2002-2006, Matthew Wilson and Synesis Software
License: BSD-3-clause
 FIXME

Files: ./mpcchap/mpcchap.c
Copyright: 2008-2009, The Musepack Development Team
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: 1999-2004, Buschmann/Klemm/Piecha/Wolf
  For mpcenc,
License: LGPL-2.1+
 FIXME

Files: ./wavcmp/wavcmp.c
Copyright: 2007
  See COPYING file that comes with this distribution
License: UNKNOWN
 FIXME

Files: ./mpcenc/pipeopen.c
Copyright: Frank Klemm 2001,02.
License: UNKNOWN
 FIXME

Files: ./mpcenc/keyboard.c
Copyright: Frank Klemm 2002.
License: UNKNOWN
 FIXME

Files: ./common/tags.c
Copyright: Frank Klemm 2002. Janne Hyvï¿½inen 2002.
License: UNKNOWN
 FIXME

Files: ./mpcenc/stderr.c
Copyright: Frank Klemm, Janne Hyvï¿½inen 2002.
License: UNKNOWN
 FIXME

Files: ./common/huffman-bcl.c
Copyright: 2003-2006, Marcus Geelnard
License: Zlib
 FIXME

