Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.testr.conf
 ./HACKING
 ./INSTALL
 ./Makefile
 ./NEWS
 ./README
 ./TODO
 ./bzr-receive-pack
 ./bzr-upload-pack
 ./debian/bzr-builddeb.conf
 ./debian/compat
 ./debian/control
 ./debian/manpages
 ./debian/patches/fix_test_pristine_tar.diff
 ./debian/patches/series
 ./debian/pycompat
 ./debian/rules
 ./debian/source/format
 ./debian/tests/control
 ./debian/tests/git-remote
 ./debian/tests/testsuite
 ./debian/upstream/metadata
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./info.py
 ./notes/git-serve.txt
 ./notes/mapping.txt
 ./notes/roundtripping.txt
 ./setup.py
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./cache.py
 ./commit.py
 ./config.py
 ./directory.py
 ./fetch.py
 ./help.py
 ./pristine_tar.py
 ./push.py
 ./refs.py
 ./remote.py
 ./revspec.py
 ./roundtrip.py
 ./tests/test_cache.py
 ./tests/test_fetch.py
 ./tests/test_mapping.py
 ./tests/test_object_store.py
 ./tests/test_pristine_tar.py
 ./tests/test_push.py
 ./tests/test_revspec.py
 ./tests/test_roundtrip.py
 ./tests/test_server.py
 ./tests/test_transportgit.py
 ./tests/test_unpeel_map.py
 ./transportgit.py
 ./tree.py
 ./unpeel_map.py
 ./workingtree.py
Copyright: 2007, Jelmer Vernooij <jelmer@samba.org>
  2007-2012, Jelmer Vernooij <jelmer@samba.org>
  2008-2010, Jelmer Vernooij <jelmer@samba.org>
  2008-2011, Jelmer Vernooij <jelmer@samba.org>
  2009, Jelmer Vernooij <jelmer@samba.org>
  2009-2010, Jelmer Vernooij <jelmer@samba.org>
  2010, Jelmer Vernooij <jelmer@samba.org>
  2010-2012, Jelmer Vernooij <jelmer@samba.org>
  2011, Jelmer Vernooij <jelmer@samba.org>
  2012, Jelmer Vernooij <jelmer@samba.org>
License: GPL-2+
 FIXME

Files: ./__init__.py
 ./commands.py
 ./errors.py
 ./filegraph.py
 ./tests/__init__.py
 ./tests/test_branch.py
 ./tests/test_builder.py
 ./tests/test_dir.py
 ./tests/test_remote.py
 ./tests/test_repository.py
Copyright: 2006-2007, Canonical Ltd
  2006-2009, Canonical Ltd
  2007, Canonical Ltd
  2010, Canonical Ltd
  2011, Canonical Ltd
License: GPL-2+
 FIXME

Files: ./branch.py
 ./object_store.py
 ./repository.py
Copyright: 2007, 2012, Canonical Ltd
  2007, Canonical Ltd
  2008-2009, Jelmer Vernooij <jelmer@samba.org>
  2009-2012, Jelmer Vernooij <jelmer@samba.org>
  2012, Canonical Ltd
License: GPL-2+
 FIXME

Files: ./git-remote-bzr
 ./git_remote_helper.py
 ./tests/test_git_remote_helper.py
Copyright: 2011, Jelmer Vernooij <jelmer@apache.org>
License: GPL-2+
 FIXME

Files: ./git-remote-bzr.1
Copyright: NONE
License: GPL-2
 FIXME

Files: ./debian/update-deps.py
Copyright: 2010, Jelmer Vernooij <jelmer@debian.org>
License: GPL-2
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-2+
 FIXME

Files: ./dir.py
Copyright: 2007, Canonical Ltd
  2010, Jelmer Vernooij
License: GPL-2+
 FIXME

Files: ./server.py
Copyright: 2008, John Carr
  2008-2011, Canonical Ltd
  2008-2012, Jelmer Vernooij
License: GPL-2+
 FIXME

Files: ./mapping.py
Copyright: 2007, Canonical Ltd
  2008, John Carr
  2008-2010, Jelmer Vernooij <jelmer@samba.org>
License: GPL-2+
 FIXME

Files: ./tests/test_workingtree.py
Copyright: 2010, Jelmer Vernooij
  2011, Canonical Ltd.
License: GPL-2+
 FIXME

Files: ./tests/test_blackbox.py
Copyright: 2007, David Allouche <ddaa@ddaa.net>
License: GPL-2+
 FIXME

Files: ./tests/test_refs.py
Copyright: 2010, Jelmer Vernooij
License: GPL-2+
 FIXME

Files: ./send.py
Copyright: 2009, Jelmer Vernooij <jelmer@samba.org>
  2009, Lukas Lalinsky <lalinsky@gmail.com>
License: GPL-2+
 FIXME

Files: ./hg.py
Copyright: 2009, Jelmer Vernooij <jelmer@samba.org>
  2009, Scott Chacon <schacon@gmail.com>
License: GPL-2+
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./po/bzr-git.pot
Copyright: YEAR Canonical Ltd <canonical-bazaar@lists.canonical.com>
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: file specification URI.
License: UNKNOWN
 FIXME

