Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.csscomb.json
 ./.gitattributes
 ./.hound.yml
 ./.scss-lint.yml
 ./CHANGELOG.md
 ./README.md
 ./bower.json
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libjs-spectre-docs.doc-base
 ./debian/libjs-spectre-docs.docs
 ./debian/libjs-spectre.docs
 ./debian/libjs-spectre.install
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./dist/spectre-exp.css
 ./dist/spectre-exp.min.css
 ./dist/spectre-icons.css
 ./dist/spectre-icons.min.css
 ./dist/spectre.css
 ./dist/spectre.min.css
 ./docs/components.html
 ./docs/dist/docs.css
 ./docs/dist/docs.min.css
 ./docs/dist/spectre-exp.css
 ./docs/dist/spectre-exp.min.css
 ./docs/dist/spectre-icons.css
 ./docs/dist/spectre-icons.min.css
 ./docs/dist/spectre-rtl.css
 ./docs/dist/spectre-rtl.min.css
 ./docs/dist/spectre.css
 ./docs/dist/spectre.min.css
 ./docs/elements.html
 ./docs/examples/starter.html
 ./docs/experimentals.html
 ./docs/getting-started.html
 ./docs/gettings.html
 ./docs/img/spectre-logo.svg
 ./docs/index.html
 ./docs/layout.html
 ./docs/rtl.html
 ./docs/src/_docs-layout.pug
 ./docs/src/_footer.pug
 ./docs/src/_layout.pug
 ./docs/src/_mixins.pug
 ./docs/src/components.pug
 ./docs/src/contents/_ad-c.pug
 ./docs/src/contents/_ad-g.pug
 ./docs/src/contents/accordions.pug
 ./docs/src/contents/autocomplete.pug
 ./docs/src/contents/avatars.pug
 ./docs/src/contents/badges.pug
 ./docs/src/contents/bars.pug
 ./docs/src/contents/breadcrumbs.pug
 ./docs/src/contents/browers.pug
 ./docs/src/contents/buttons.pug
 ./docs/src/contents/calendars.pug
 ./docs/src/contents/cards.pug
 ./docs/src/contents/carousels.pug
 ./docs/src/contents/chips.pug
 ./docs/src/contents/code.pug
 ./docs/src/contents/colors.pug
 ./docs/src/contents/comparison.pug
 ./docs/src/contents/cursors.pug
 ./docs/src/contents/custom.pug
 ./docs/src/contents/display.pug
 ./docs/src/contents/divider.pug
 ./docs/src/contents/empty.pug
 ./docs/src/contents/experimentals.pug
 ./docs/src/contents/filters.pug
 ./docs/src/contents/forms.pug
 ./docs/src/contents/grid.pug
 ./docs/src/contents/icons.pug
 ./docs/src/contents/installation.pug
 ./docs/src/contents/introduction.pug
 ./docs/src/contents/labels.pug
 ./docs/src/contents/loading.pug
 ./docs/src/contents/media.pug
 ./docs/src/contents/menu.pug
 ./docs/src/contents/meters.pug
 ./docs/src/contents/modals.pug
 ./docs/src/contents/nav.pug
 ./docs/src/contents/navbar.pug
 ./docs/src/contents/off-canvas.pug
 ./docs/src/contents/pagination.pug
 ./docs/src/contents/panels.pug
 ./docs/src/contents/parallax.pug
 ./docs/src/contents/popovers.pug
 ./docs/src/contents/position.pug
 ./docs/src/contents/progress.pug
 ./docs/src/contents/responsive.pug
 ./docs/src/contents/shapes.pug
 ./docs/src/contents/sliders.pug
 ./docs/src/contents/steps.pug
 ./docs/src/contents/tables.pug
 ./docs/src/contents/tabs.pug
 ./docs/src/contents/text.pug
 ./docs/src/contents/tiles.pug
 ./docs/src/contents/timelines.pug
 ./docs/src/contents/toasts.pug
 ./docs/src/contents/tooltips.pug
 ./docs/src/contents/typography.pug
 ./docs/src/contents/whatsnew.pug
 ./docs/src/elements.pug
 ./docs/src/experimentals.pug
 ./docs/src/getting-started.pug
 ./docs/src/index.pug
 ./docs/src/layout.pug
 ./docs/src/scss/docs.scss
 ./docs/src/scss/spectre-rtl.scss
 ./docs/src/utilities.pug
 ./docs/utilities.html
 ./gulpfile.js
 ./package.json
 ./src/_accordions.scss
 ./src/_animations.scss
 ./src/_asian.scss
 ./src/_autocomplete.scss
 ./src/_avatars.scss
 ./src/_badges.scss
 ./src/_bars.scss
 ./src/_base.scss
 ./src/_breadcrumbs.scss
 ./src/_buttons.scss
 ./src/_calendars.scss
 ./src/_cards.scss
 ./src/_carousels.scss
 ./src/_chips.scss
 ./src/_codes.scss
 ./src/_comparison-sliders.scss
 ./src/_dropdowns.scss
 ./src/_empty.scss
 ./src/_filters.scss
 ./src/_forms.scss
 ./src/_icons.scss
 ./src/_labels.scss
 ./src/_layout.scss
 ./src/_media.scss
 ./src/_menus.scss
 ./src/_meters.scss
 ./src/_mixins.scss
 ./src/_modals.scss
 ./src/_navbar.scss
 ./src/_navs.scss
 ./src/_normalize.scss
 ./src/_off-canvas.scss
 ./src/_pagination.scss
 ./src/_panels.scss
 ./src/_parallax.scss
 ./src/_popovers.scss
 ./src/_progress.scss
 ./src/_sliders.scss
 ./src/_steps.scss
 ./src/_tables.scss
 ./src/_tabs.scss
 ./src/_tiles.scss
 ./src/_timelines.scss
 ./src/_toasts.scss
 ./src/_tooltips.scss
 ./src/_typography.scss
 ./src/_utilities.scss
 ./src/_variables.scss
 ./src/icons/_icons-action.scss
 ./src/icons/_icons-core.scss
 ./src/icons/_icons-navigation.scss
 ./src/icons/_icons-object.scss
 ./src/mixins/_avatar.scss
 ./src/mixins/_button.scss
 ./src/mixins/_clearfix.scss
 ./src/mixins/_color.scss
 ./src/mixins/_label.scss
 ./src/mixins/_position.scss
 ./src/mixins/_shadow.scss
 ./src/mixins/_text.scss
 ./src/mixins/_toast.scss
 ./src/mixins/_transition.scss
 ./src/spectre-exp.scss
 ./src/spectre-icons.scss
 ./src/spectre.scss
 ./src/utilities/_colors.scss
 ./src/utilities/_cursors.scss
 ./src/utilities/_display.scss
 ./src/utilities/_divider.scss
 ./src/utilities/_loading.scss
 ./src/utilities/_position.scss
 ./src/utilities/_shapes.scss
 ./src/utilities/_text.scss
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./LICENSE
Copyright: 2016-2018, Yan Zhu
License: Expat
 FIXME

Files: ./docs/img/avatar-4.png
Copyright:  ç?bH¡ÀKèÎB|vª87
  8vRrÓ¡Í?5IÐF¡NàêRo
  z|7°Ù¨n5·-7¤©éÀzêhõ¯UÊ±fñ~5þÓ¯ú¿ûVúûhûÚ;<ïq¤MÀqlÎm'ù%¿}x#ÌOXy³9Ùî¨
  æøÔgß­~]´Þ¸«W®d?%%¥¶
  äÇõkDä@Å¹
  -÷ÒVgÁMõº4ápdí#8âuEideAàÊº~`´ë@
  C¶l8 ,©hÂ ÑíN*m¸¼2|Ñ|L
  D.sã â$àÐ»wÎ_3)æyN0µÌùû£.NP7b#¶CÁ
  I¼¨&£ÒÑjÒ8v~ÕIÈøOF§ã!Þù|9Ï¦YRdööå®Z¹R_[[_G·c»ñqÏ¸yþÇ~ô5 (_¶
  WÁ´´e
  [§Af­ºwé6bÈ §6¯[tòðÆ_o:	áû¢ÝCÐ¿	ÿøÐv<ÌÿN$ê 05á	¤ÖXäf»}À¥üþöiR%Óêª©ñ$ZµæI møØPokÛ]ÐSØÔ	mSSS
  h°òa>"XÊ'98bó.¥>oÂ&E½ýá!n?pâzõêÕíÛk×¬©Y²$V´Y°Ã¶wè³ÝûÁÁ¸)ßÎUár[ïóðlDølô«4
  kkËIW°¶Î+WóþãØR©¬ÆiÑ.ò
  lÝ¢(7doýT£¢ød´QITÌ#¿zlÞððÀ I&]¸À	h'ñ2Z|©&0eÛÞ©KÒx`à»	p
  uË
  ß_¼d[b¸
  ¶æÉt³/JÕÔÔTé£¾ýLô
  ¥¹EfàøâÑd ²px+a±v³þê}ÙJ$yþv
  ¢8²dÔ[ëöíÛÐö9}«2¥W]ûYØ­ùEÎ#Ìæúé!àdÍ[>ªR¾W/YIUßwQNÄÁ{«.ÑÏáËßðÍâd7b·²óàhÕÆ#íD
  ¹¥Yi
  õ×Ãj·)ÿ¯ó§Oª³5&:zÃºu¤Çq#yi®Ããx}ôÈá0ð,9s4_Úx¡Ûhióç6ç¯D^¡
  Ò3¶öºMà>y"4À1"Û5{wï&¢pÆ::mûô¤¬Pä}¨$ro÷0_¦­1ÿDì§£¤$"	§§§¤á¾><Û5ÊµP4
  l Þ
  ¯ô­øÕýöÆÉ$©5ãÉs¯Þ½ÌÝ$ ö	ê³Ýûâ#w
  ¸É²Éà.z!¡CVb¶PØâ-yï2Îë¨=>.ÓÑÒ*	D*ß7ßºØI
  ¹³'ï* ?Ü'O0Vg
  ÁK·ÉíÄi¥Pþ8ÖVp?ÿô$
  Íaáß¥mµ
  ×'>;$¥¡c@ÀÚî~öµe®+76ÂÇ¡0<õp^Åè¦+Ô£
  Û'XÒ|B[
  Ûû=Iìnì}ßµ¶Ää¿^:ºCR_ÊÀ_®p¨UJ"	;:1rÁ¬<»oejZ0²}ÉH $¨å¼Ã9'¸dÏÙÒ*Ð÷fr²DØ	«ôÉ4­6àZ-û¸*HÝù­gÈ¤
  ëj}iÀÁë{62Hj5'ôDJ
License: UNKNOWN
 FIXME

Files: ./docs/img/avatar-2.png
Copyright: Wn~Ãì_Gá	¿ÁÄØ¸lL(ÜÃSgÎE:"l/Ð d"-îäi$^î³geK}æÜy°@Ø<Í£#¨¨E%'4 áP©¶a³p'à(eì!K¥u|ÖñAnì,8]B+qÑÒÆõ5 Sò2¦È?++,d5?¾{÷Þ­ÛwîÜ½ûðá£ÒÓCÞã'OîÞËasvÇ·@^*ÙúrÕ (~f´GüÆZÝßaqUµ(úAB3zw§­XuîÞáQ
  %`Ä9U êriÍ6ñ[­&ZÂôÜ·ÓæézGgéìÑpS·ïÀ:?l5<ïN¦Ú3Þ{ÑæîßóWº~¤ó6ø'änáÓ;ìÙx³^~ÒùËN_jýÏÓ®¨÷§¬p§v£Ã£ f­kÚ·´ê7=ÀU'ôíÛzâQ#~K¶
  O«pÔêL,«J01I©íæ±È+hìÍ[Ø¬Òùpìî+1K-ûäé(¬(1±!fà±cÇÔhËÉ©7¨©íV³É,d¼:OWwòÝ1Z«dNåÚG
  ,þ|8ª0÷ä¤ÓfyÃð'±ìæÊÛÿ&8$ã²^/n£EjÉ¦M¢`ÇK¿PTr.ÎÀ"rAÀoì|i¶ö¯ßûâqwmxpÈà®ÁÝ5@î,lÜgÆßaAÂ²
  ¸¤?hKI]$')dÐÇÕ+^é[t2RÜÍ
  2#mÑFÀ
  þ±!·ÏÖ±GÜ:½uñFW,GoBÚÍ
  6fúË±ÏÆ´[:N¨m´µf:À:`rr½qQ¬¯áqÂW¤4ï9¼v6ÈsÙM"Çë,Þl­ék[ßõe§;¸P³jUÌáÎqÅM
  psJ¬ÅÖ»ô«W¯]³Vo¾ý~¾¡"55mÁù½*=ÍJû.?
  p¸¤S{è@yµIøDV½<§ÎÇÝyðÜù
  ¸¥4Z4cýxÙ¹¹LLM
  $
  $Ê½êüºãûßzÌÉÈÝòþàÇû=º¾ª-ÿ¼_ jpÁ¯ö§WÕ86Rsp×¡¯v²|Åó
  -ÿösÊò |'9k'9Öéº=»èÛ6
  B)rNW¹×,*2©´55]±ð¼o¾ürµÜ÷6;Hçï .bVgößÖ¿FHovm!·
  JÈx]»vmV^LºëÛ¹|¼¼ÄÔçH³nÉPTxÂJ·¡ImZYÔb­!¯iö{'Ðp¤øJý3Ùvh8ÔújGÁÜÊ¢ÙÏwÃ¬Ü²ìº]cëxã{óÒ¿ûMÐ&¤gCMÚr¦LÒ¦~fpÀÌÙ¿ü4R1Å
  O®0p1doE·*G¶¸ìõ¯!ÚA%
  S§BÑÔhÃÒ¸qÐ¦TÓÂ]Çi9l<Ãþjd¶­Q8ñ
  T
  TìmÙ²ï¿þ´
  Y7gSëëëYÝ)IbÂÎ*ôX ïãÍmÛtµÀiJàóúäµQÉ`¬gùÔtLQù^l´V
  Y±°rma!¡tg­^½æ×_µÚÕ«Wßÿ}
  a½±ºöD¡½èpï¾©I
  e¹»ó¶ö}åäãÔ£p·Oæk2SµýÔ¶s¿<óK	deò½&|ûpÅ·¿¬ÉpÅg-W«y1ùïâys_ì[>×/±Nl ­_}*¤-e§&mÑ&ÓNZmýÚ»gÏþý{÷ìÙ·wï
  hÝå-aÝRbÉ3Ô
  l¡$T4'ÕOmüþwkùáôü¿g}öë%ïûdMAõ¾B?ðÌA>Ü
  vzÃt«K
  ¹ù5åHi=Å÷FiçíâÍ<m)©ôhR67gç
  åIÊ,Ïø·²·¦ÿÿ ¶%D§¾$µ6ðùÄBojØã¤
  ÇÅpCr=ØS¾ÝybSØÔ§:'méÓ[¶¿Ë¯©ýntïPë5J,aÑàûÞOAñ©ÇJËØ­¨"b­?óiPCú~p@«ô¬6F&Â3û¥Ôªÿû÷_ìVe mrÆí>ó·CT6øãIb4!Ô¸4ÕJUA>6X_¤-ÁBÚ`<Ö(/»vî¼ÛhbÛðhC7Ã}Ý{?8°
  +xsøXDÔAÞpêÌÙoyÍ÷
  udÉ¿+Ú´¿T~»¹ûw÷¯V=à¤Ü êË/Þ?
  £
  ¦fÚ?Wý~áÜ©Ïn¥åÞH-jaf=LNØ1¬6A|¹`%wLÌ¨Wà#
  §-âO²mjNºù¡æ¯÷Þ{XßàBÜÀ¡XIâÇz lÁ¤ýåý%¯MÛÞý³sIÅ{Gh2sû§K?
  §È6Ð¡
  ©¥ÐÏ¸Îîí4ªëÌîÂ2Spbo×]	¬Ñ#ïÉÃR¡Ö$ü#Ü?Â3í=º#^ù'¡LÕC±Ò®pñ~ÊhÉ3RÜ´Q´*³ Ó¨¶®3º]'qEcAXJ å1§sÖíéE¿hõ%Ü ¹Úh5s=k%ojÀ}*Ãw3õX«r
  ¯Ìëí¨«à¤¾Ñ®g¬¯	ÒËcL5¤Mxmâ
  »{LóÆ}Ï{.¼sîð¨RîS0àÖ«ù¶'Àª¦º°È@Ü'WVjØm*U©S¥ÒØðïZao!þÒå«È¹Ý3m/-&Å³Ú»r
  ½ÖcN½
  Ä.¡D&SÔÖ­[ÕtîÜyÝ¢M`:((V²Ûñ)RÔ+»Òã
  Êy¾|É¹3Pg^îî!ÙÈ²8¸ój¤¼!==?IJJf)<$4ÀÏO>ð)Èóprs~Ô)(È¯Ã¦
  Ø§úrâiëÐ®Ý©S§®ß×Æ
  ÙhR)4U¬ nÁ
  ÚÑ%ýk,>Ë:V^Ñ©ê¯]«Nù9Ä
  ÝqH`Sgf½`j¯ë£Gfvö
  à'z ¦5<ùbCÚyúéëÕþjÞ´)eCü46lcÌi.æø0éUÃJÑ0÷Ú÷OÏCÅÓF
  ìïho_äWÓ
  îÑ&
  ñI±5öàüRî+"#ºúÌ0
  ò
  ô­¡«!å
  úDïÃß}nÙ
  ýäðïåå1ÇÝllÎ§Æ^/Í7ÊØ¼Ø-õÙÍ´1}ºµ¨U©F­-ê­Ù9Ýû¹
License: UNKNOWN
 FIXME

Files: ./docs/img/osx-el-capitan.jpg
Copyright: OØI[ÂÝØV¶}Ë§ÚbÜÂÞî¡¦~oúÝý þvrÑeº²ýíe«éÅYCç¼¢êÔ¬×©¡x{ÏkÌ!Ðí·¡eºÙÔ3Æ³Zzï°ÀkQ,à~1¥Î-fðYèûöGe·
  ']@q:ú 0cÌé&ª×áó´A~åÄØÍõc¹6]Y¯­X´¶ui®Ë)zrz'úåþZÈBV8jç°_7ô/ÿ 5"6ß
  
  w<è"¡ÒÙË¥¨ûü
  2	©%²&
  3¼<È6»ÌFÆ­®
  7=æåq^Ú6p`_õìA±q¦°Ïè)¦<ñÏ;zÅ£<Ió:fÂùãh>jÎN×y0ì7öo«-
  8d1"lZ7u>ðTÌ1+2½w6ÚJ>AaH(0«´)@í_Æü%°Å]ªç$qNÈ`Z>T
  EØÎ~0«ìJÜ
  Ví
  d«)1è±éX_&Ðeãâ.º]¤dcè©ùÁ³`¤¯%55!(ÑÂÍÑk ,î;À6Hmä
  gª¸üíHteâPc5ÜCYç!ã[(¯å,XWR4]Z_aÒ§ØIÉ
  p9åÔUnºë¾íW]*¢üó÷ç_QË
  p]Õ6æ¡¤/»N®§ôfÆÃö¨ò²ó´l#1ÄúÓçE¡­«KbÁ´¯|×ý5nÄGÁ
  {ë{UPã¸GM[ÍåBC3ôcÒük7y©9®½:õ¨âêo[WâÌíw/s5aO¸îMû%øåË·ÒOÉMÎÍKI"÷çïÝ'×æm Å¨»GòcÑÕXØ8·éq_wÎ*·ÎÞc"öqni³íÐS
  r.
  ¬iÑõÔ¤d×
  É¯¿.¤I
  9cZÄV2Æö5BÞ®¹
  ç;TWÔ
  ¸d 'bÆZq^fÿ B«GÔk]Ô¤{@j¼
  ¤}©¿
  ¨Á,| ë{XÙ×kéú¿ùèö'Í
  ª?CýãóüÇ.G_¤â
  ­î1ÄLyî²¨©Îmê¢ªÛrYÄ¶ÝÖÿ YÅH'òËÌúÓ«HË9Î­ó¤/J5MHIFî>öç¶
  ¶É6vf0¥OXÙy
  ¿Õe¨do4©%­Ëú§²ÁuÛ¹L.Ý¬4~¶¨ÐR¬Ô·,ý%EÖ45Kÿ I>ä¾_äXØÛñ1­ÜÌÊ¸[óÏ(<ÒôcÔÌíç¯
  À!Î=ìÆãLY1 É£röd²IÎÜåR­)È Ì×.À
  ÅÂ;°õÿ ¦ûÏ¬l5·JÛ.ø¼?$ì7+bÑWºË9¨ZDÏ­°j"YIîlf¡f2P½Û>©Ú÷íé*Û+Ý¢4ÿ Ïu6|Ý­ÆÀBÀ­YË
  Êw¡}ÅêEZ:H@~U(·èêúÃs_$EiiB·¾Ïé¥ÃÑá<Ô;	¡¥ó¿ºtWbl#pÕFj ûAomuÖP)eVY!´Æ6¸Ìö£Cçy«b94òëë5K¬ÚS3|£]ÇN2Idètc²kÃ/N«
  Ò®ädKò¼¬Ôz9£FMÑYÒóõvÿ >våã®xtñRBòóQüq¡°èì©#ÊuèÌé.9°4=¦(E¨û·{±H+Íws}¯}£rEGýr¿¶L
  ÙìSg]`°ry_½ÅT¹¯#ÍkT§ZËÇ-çnú#»Ûký=ÊÏÁÊ:Èe¶Yñ´~ëjèÔ®lCùì9G«ï²FvÕYgM^Åiú×£tÛ¿
  ò
  ó¹y/óG"ºóÕ^[pü¥àÝw^Y§F÷ßt¼6:ôXozä¤Q¡èf!,,Æ«û/ÖÚø¼lLåSúíùA
  ùÜ
  ûæÁ
  ý:6¸,çmbb«F#PZ%»¸A®ã_õ)
License: UNKNOWN
 FIXME

Files: ./docs/img/osx-yosemite.jpg
Copyright: Å£áìÕå«6EõØP5[àÖæÛ¦Ñ7fÉ-6[É¦ÛDZ¡æZ7Cg¤|g6_OÒÃ¨©
  
  ÅXóYÉd8@?ûÓÊá»æZ"Õ²6ConÙ4TSÇ[*ãfE op_*Ø¼Õb
  
  -¢Ôu­ÙH:/¤¿ZPØßw
  ´¯gÆXOÿ æF2¹ç'?eEVèRui «Êà²|­:nã(%öx×ß$'Ë0ÿ ¿ÙóhaËkJY¥ëSdæd¥k
  -½È9Æ¶Q$7C$P3	ËõÇªÅârMÉØ	ÅÂ×íçÞÌE°_udÁæg¤ê~þ·_D´i¸jãøFVï«ùÝÄlÅwØó75n	Þ47:S¦ä
  @x2ü4éz
  GkÏ^»çR¦ÈñT
  GÅ!®pçoe&mÕ?n®ùI$±¶3õõE<ÏE_É'äÛì£8ö­$¦ùÎØ]ÆÎä¦$ÑN:5£fi8ÑI6ÕEÃ­×Äj
  JùØ×êõÁòyÓß´ÕßÆ
  K Ë³CÎdÐÐÝ
  Lû¹euø[#ófZ DÅG7zVº¾ù÷
  O2PyÒ]Ì#ëM2;
  XÝÖ!½v01&hT¼g/îc½JpêÆ7ÃÊt¤HRzª,±Ý_ÇDVÊJÜåIWwÜðM.¨»*ê¯´?Iìí¢xSoy<e-5öþK¸# $´Õ=¢$T*cÝR39,s-PÝMëæøBmp|hÂ¶¿;
  Z¶ÖØ¸À½¼ R°:ã$R©
  [ä¡0{ã+4uã5|H×ÏxúG»?93ùúë$0$s_MæóyÛ$³¶Dï&2s®HäD}'&3Y!'!S9â(ÎÖr3^pSÎºúëåÖQ=¶Û¦Ö/lpuâ£ýimÞÝ§#Ghv©ðA&Ôx¯kÖNÂß 0w6PÝ³ñQxÙ¹³f[lÞ<×UQÑ?¬<-õ&F1
  b>²&))æÓ
  nWùXî2¹3ºêúV9Ç5kM§J
  o)4µh?×ò^Î¨³lÙªM"¢
  qZÝÔ%Ó¶0©ÕÝ­AIZÁ5ç³a³HÓ½rúSS*ïÔá¯rl¬@C»NTsÇüpuBäifÚ(Ù¼½±ô
  rÎLmT5rNXû·7lÜõ1!"!0Ã8Rµ{cçW'GÉUT	ËhÅcÅ÷RMzì(H2DÚlØbL÷Å×ÙL
  wÛõ=àbNÄya,d{xÆÔõ BÕa»(É*Gö~c"c¦#;ûjs¼æç=óßé¼Âçé:ÍäNwÍæòrc:æ³Y¬ÎÙÛé¯¬æó¶LäÎLæó]þÁÁÏ|Á
  
  Õ$õGÊ{Úç*m§ó}1SfMvU£é)ã "~ÑÍñ+² ÉºZ7CL$«LöÑñ¡k¢õS	áôN=®ê·jwÙ_¬XODñ¦¾ÎùGÞÛÚ)é¢ß;ï¶º4Ñ]Ö~íWÈ
  »ãnJF4e9w
  (Í­q=w'T~Xöù:öy³D#Ø}H&GØ7P±GÛÓø×
  ºqmûËÒ0®øÚ¹3ªær¹ÿ »åÄÏÂK5U² ?sl
  GÌ_
  <S>µúVíØã²ëºöOGñµ3ærn·0ýe.j4Võ¯¹!ô'^»byÖì²ÝÓøövj{Ì4¬m¿7Ûµh¡K0%rq
  ®ûÕ)4j¾ËìÌ åá^OûòEÚ>g2E&
  ·3´Þ
  ·Õ0XÉY.h»f(W¦ì^hmdÍkÏÌ.*=éT]CÇ}r³«¢.¾åzëh"íÖÏ!
  Å¾Ù1ÿ §®øcÄä2»¥&AÉ÷þ®¿¾EWÇ
  ÇFC;Vê(©zB6
  ÇòªbeÖ¥¦9Ls,éi]FNÏÑãi>Øo3{PFÅb§FxòÅJLp²Û«ÖÐ]1±zÍËW/vto}
  È/ÇíÝ¿åx
  ËÐYFÎ8·êL/1Ç|Ç/?+)TGË×mãÇ6BK*&êé²¸ÃÔ.Ák,Ê-La¤r;¸q´r>J*1
  Õa)yÃÑËï´y
  Õ×Qå1?*±1Õ¡
  Ö,° }¼Ü8ÝJ_#ºËñ,jN§>óùF²'¸äb$Ðb§E·Txª¬´PYY¤k}ÏÔ6ãÓH¬º­1Ô|b5ÕàõàÜÄÏâw¶$PÈaø%À^ÑÌÉB
  ×)¡¬|ÚïçG1GvÎÛB²,y¢ï¶Ñ4¶Iv­QA³t}TÙ6(Öc©·ï¦¡t~íÙÙÍÛ"4fÝ$£äOeUö¾ÎS×M·úÞ´´ºylÕ`ÒçR: ÎÄK§NZBG´|O!¤RMÑhÕº(6lÝ$ÒCk¦Û
  Øm)¿/U¹eL
  èw¤ñZ°¼ZÎ
  êséò'çòó
  î°ëañRºØ¶$¬±çÝY9øÊ*[l60
License: UNKNOWN
 FIXME

Files: ./docs/img/macos-sierra-2.jpg
Copyright: Z¦_´Ak÷ùÙs¨E¼sô§*ôVóJÂ»L|WªÓèÓ$®1ÕÚSírökÊ.¬½'C­jÂÍr>s¥ãô|lÑ`äv}Kr©sÏi¿îp_wz
  Ø¬¢.ví`(X¦Ñ¯	åÄVhÑ
  &ÿ èä²Î¨ÌàsC[
  1
  9;ÓÓÓ¦©»Åfr"ÐüP¶ýrvål:fWê1&-× ªÖF«ßµ
  @èB~@Í)°bá!KsÁO>*HIÙ¢,òò%¡GY¹Ë$vÈ:e·éñ×Û¾¸)
  L2¥,|4Kkò=¡ßº·S®(§V÷«yÏd.½[®
  OYp¥²eúm¦O¨²?{;`>Ãa~#Gw¹ñ©»&#´"Q¹mt´.s¡íêÇ.qÃY1±NBÿ ?ÖÔ{EgÏ`ïQ¡²3ãðð¯tBçºÉú%-4;Hy.¶»*4s{c4K°]&â¸v(Ðß&«Uà+")mpTùÑî>ºÓù Ñwé¾%ÿ §o¥¶æÎíüÙ0Y§Ë%¡Tä5ÏaI@¸ð|ÛFúo½Çè]÷èþ¥Öñ=eªëÒèþv49Ñ?¦Xh,ÓbÅÔ)¸,O0ì±´~b4^ºnÝõ{¬Ê6
  YÌè
  hó×~÷ïêXBQâ}ñhJ=ó×²ÂíÍ®¸IÃËãGøe¥Ääí¡ÂL±¯8Ýy¤´6ÏÍüÄdm|f«0M9Û+2Çq$ï
  s4¥?=Òí¶ª|¾Ð ®·×ej³]+6è*_FËÒufg¾ñ×AsTüï?¶å×£Ëë>r¸Òf¸
  tmõyÏt¾	Îzþ{¤g°[µÝ
  {¥BOWu·ÂµÖùZrÍovËoÄzkéZhå·ò6¹¶Éfwz«Ù¦jí>X«ü~½V§nµúF¯cÏ"ÓêàÈî©­»ÌÁ¢ZíZ´CT¥
  -~
  ½"C«Ö¡#!ââb@B'ï?SßñÇóFú[8ën©m°ÃmóP°¶(!0${
  ½þ6m)0c^ãr³¬ãÊ9Ø«^í;I«9õ#¼³Të`ä§0:¦HÑt½rø÷WI*Ó
  ¥á=GböÃúúñZü°ãÇ;ÇéGu~[~³à_ódìnhè
  µ	KW³+ªY£¯õh»}KU¹`ºzFÃµDdÒuÂÝ-K·©ýTÅÞèL®*·y¥»·3ë,NsIåì
  µ.¬ëS^:Ç Íf/Ô:¦òÖQùËÓµoy!î³üÌÛ]5ÎPâî0ýMé]
  Ä²ÊùV 
  ÉY
  ËÎè¡!uªÖ)õèÿ ´:óÎ½i?%×ãIuøøàáój
  Ñ±ÊÇâ8ËUºÖÆµ{¥$uÌ¸CãÜP-}ïù÷Ó×»½x,Ñ:ÌòU¹ñVèÌ_ÈÜ+s~E`Ë	òPò-Iø÷coã|Ë¿,¿úKóoì¿æè®q_Nã® ÇLn¿=,ó³gUé?Oêê¬µJÀi
  ×ùJª[äv[Åÿ ùíÕBÞ3oOòïÕ=7ô?qRÛô>ºÀûcÝºs²¼3:sö×ó;£ÿ 
  Ú¾g#Ë5Y»Ò4é/ÎàtuRù(¤Ú3LrZý ó
  â_÷å-~{ë¾¸úüiãaã£ÇiCD
  å;½è;0ÜC{¶m}ÌîÞ÷Çûojç.à
  ëË®K
  ðÁ²ÃnrùUò;¦¡ÉüøîpqÎãÏZ+ÖÛÎãWçKüUtÓ¦8Tì¥Öïè?äèäYø×¥ªz'èµ7Û¥»_e¿Kÿ 0yÿ OØîÛ¾G?_ÇóØ®æ°Çø¤É;F«ÑÈÞÈ.+HùØÕ±-ækTÎ[Øq¸
  ô)9uô2Þ=Ù6Ëp¼Ù´ö½%ÍÙß5-Ù`1?½Ò×ç·Mþ³Ë|§Þ|ÐVþ[á+<~Ó»#AýýBätù&ÌÚ§ÓO
  úÛb²="Ø/	fQðXùM³
  ÿ ÏÏíUËþeóoôi¶{(i¦(NtüÃÄt¬ãZÝ¹Ë1
License: UNKNOWN
 FIXME

Files: ./docs/img/favicons/favicon.png
Copyright: mhDk@åLÛ²åÂ#näù°ËE_­g½
  5ÒvÇíé:BkhíUýë{5<Õ±Ú¸<«U¶¼ûgz±ÊöÊ¢åûØcZ)§í=zé¤M2ÅõIÕ|òYË ÐZ9ór5 ¾cÞGCK*AíYï7IÀ5ÓÁ.6g>»AµÐ>âÐd~(Ú·+.[£"Kò
  üü¢3Àúõí¾-Ö®Ýl6n@M
  A-jú1úüBZÐ¶Ê¹Z·´;Bÿ8r
  G
  'ïÉô»@¹ªGÑ¿ä|{f[Z÷Á»Í{ï
  ¬bû?7Ä%C>ÿl].ach¡,"4ª[:µ>¹{Ê qFÀ~#
  ÁUQÓ~`¼Ò½ébþô«t|ÉÀ¼sÎZi@ébh<ã8fÜñ&&]åµ·ÜÜa~üq£æÏ.õ²ô×­+|èîLï
  ä®>ç)ÇÝ²ºîÕå{«iÉç6/úXÜ>%îeÕ°ìö½mPx`æ
License: UNKNOWN
 FIXME

Files: ./docs/img/avatar-1.png
Copyright: 6oÎj(ËIºuS?u|TEKcb)Þðj§,¶¾.p~*R´S÷.@KC0ý¹dh0ÐÕãæº{Ì^´»bÇ¾$y¢}Þª{±zXAAn«Q@ø>PN³¨ßÚÀ
  
  uöª$ä¢
  Ø+¬¬á´C>5
  ¹âtd$ËÂæ­H[Ñüºe-~pù¦
  LÌKwÏ½Õ-ïN_MæôÂ7~eÚT©$Q=l´1
  %à g'­!d¤äBÖ¼_èÞ6
  *¿,KH*úa¿fÂTK3ÊùYZ)Xùÿ1ò·&'Ïî=v
  7,_Ê°°¤ÛÝÖ¬ZEj:y!dsòìÙ$;G%°¡äÙ?xHé#9Ç©Ri²@d
  l%ËaC/g¨ÇÿäáöCÙ
  ÚçzLXùÛeÌ>åÑëdÿ|C"ÒhÜ*#~¼uÈçÄÆU¦MÜßÃ½]×F_7´³
  
  £¾TAåpóPSÖ,AÉÕk=
  ¨ÏVs>3¢>KþSÉài¶"
  ±K·ÎG¾`Þ¼M6øûù
  ºYã§
  ºÞZ_*§$º
  ÁEJ¼T+Â+]¼¡*çª}HZÌ'¢ìÀ!>íAÈ®b½|y÷¦ÎNPpå6°YÓ§V¨ÁÞ¾yÓ©CÏ22bò¤¨«W5Òr,¬KÆO­<²bÈ(ÃÄidõ:r8¤¤©³²oßÞ¸sg»àöð}þRXÛÀÕõhÓ*#ð8/6ºY W¡ZÛjRZüÏ0&¨»|T¿%sá
  Ät{b¹ìùVçpDG/
  ÄZâI¨HN)Æ
  ìÉ[7¡õ 3r#[$óG
  ð7Ç6HSUmÁR'Í
  ô[·d99eee¤ºVüþÝ»;·o?ïõý÷ãÆi×¦½ýÏã/*pï§ãc2÷!ÊÃ ÷gÅãÒ§÷+ëåÙo>ÕAU¤²JöP(¹Dï«t jà¦íß3±_oe
  ý¾
License: UNKNOWN
 FIXME

Files: ./docs/img/osx-el-capitan-2.jpg
Copyright: R
  áy9mÔ¥ngáÌ£Î{Þk9ÃÑÛKm¥¦ý6¬/Ì£
  w5ÝwXN=-JV¯ «5`-æ~VË×}¥På-L{êu/R¦×­ÌÆ¡ýg
  89r£
  Bã3éW;Z°)UfÙçÄ6ïf;ÄMÛ£_¥~×IV$Ê¡YÓbUøyA{7WÉÐ9V¦Ám2ÂØJR¶¡Ý]©·VÙ-WÊ%~q[ÊÂ28¼#FèC®A¬}y°¶8òôþ²T¼XfMxÿÄ            	ÿÚ    ýTB&:EEXØá
  HC~[)B1äåC^Lßû&ù²l¥kóHm=¹ì{8òó¸öÄ¬'Í%ÌÉË
  MN&ðR²Ô8cjcÙê=700ÙÌDIÉºEëÎ¸ÚH4Ô
  NG})J×²ÒåBS^<¹AÂWèA èNe@Tí¥YÄèZ³åyÄ<ðÔÊ<y¯#mµ5c²c
  mCzLy#ç
  pµåq²öx³n;M½¤.7:Ït
  rYP¹
  v:
  ÑËí»6Ã¶¯Ø¾få{åTn¤>zÿ Ò¿<­Ûª·*å­:Ðÿ ,Ò
  ò°¶2ëSxJÓ¦Ã>Kt?,IAf.T&e"4FÜõ¦2 
  ¦E+Ë(¸·õu­~pß:8«AlÅõ&Qè>«çîs§ÕëSëduòS|ØÂ´­7¨äË~ÊJ5jýR9=»Äë©¶ã};Îü<2áÌ±,µO¡orÔ«@U7î±7¥oçöÌ+_¬ëQÑÜßQ
  »Ï*÷­êÔz6Á«¶ª7Bé
  ¼/a>o8TU'	q©pq#!%á°îPZ!*ð¢.#²NÙq¨ìEæoK8
  ÇÙòä0Þw
  ÎZNRÒpÞgÉÎV]ÛÉJäGÎTã-©´£ØNÙSm?b[®<ë[{Ï-KsÉF[Z£!9öâ[[)òpö´¹gc
  ìÝÌ>÷Ë
  ôÛç°3µwí²4
License: UNKNOWN
 FIXME

Files: ./docs/img/osx-yosemite-2.jpg
Copyright: Bâbò×4SFB	çïeq¿G#ÆMqR¯¯ý'C*%«#Uä×nzibÑS6xû£Í/+×LµXX»uÒJµv7[mÒ(-gÚÐPüåÅÇL<Þ ´*¥¹oþæCÍ8pÃ~sÔòs´6±ÉL¨4BùÏ0{Ì¾4UØþU_Z­µKe±ÙØm
  X¬äÔ ñ^Yl­*´êXls1`òÏc_»Ñ4±ÊYó@Ä;ý5}mºBúòÂ°`ðUBú¹HQÂÓ<
  4qQê¹/ôíZîL³zü#õ~bÓ	Bß-ËWk}Í¢F¢|mü`û¶~rí(¿Ý±ÝßsãÔ
  OaLE]s¯õó«ÍUç­;O÷Ó÷LÝ.¬¦Ù¸vÖ8ëðlð¶nÎP|ëØã«V1¬h¤äzàÙCºÜúî¥r/'¸d
  Q-tÉ
  U¡½ÁW8Ø§üàâTpÛ mÂÊü]ýµOn´ãÁåüúdÅhK(pþ¨|ÅXúÊ4÷çØ4»mgå´°ßö¬éræ·ßìJ¬¿Wréóÿ ÈxÑÈó 	¤
  WÑ](ðºÞp(¼ÅS²zIb¶z`Á¼Ü
  s`Ís¯'qcþ÷ùÀ`­ç4 t3KXYý²Tgub&_R	7§çï9÷¿¿þLÞÒß
  {r³h?8²ß=gu¯ûIêØòîjøm,=W×¯]$L[þ=åãÚ»ÕãHÕH÷¸
  vª9dèÚÓ¤c	dÐîÕÚú*Úûî>±ü}üË§´Ôtù4=ªÜÚ£VvèVý±´¿ªÛ¾sÎ+rTµWÙ«
  ©rêÀ5­çNùn
  ·fUý1Ðô:êB=.·5rgDæZnÊ>aúz¢îb¤¿[
  ¾Äî,ô¼¿º×nó6
  Åëuý£-XýõÛª2÷÷´T°ÇóåP
  ÓICòñ]»géØ|ºæ¢ft§ÇñF|Ê¦®é°åºícUõw<M°[ª5{}½-gúE9wÄ½õ)á(¦÷®_89êTÙ¹½élxaÀn<²6½üZµO
  ÕóõæxË4¥%¸~ú;±ò
  ÖÂoôSZÝkF
  Ýµÿ þz_
  çû	
  ð;ôñ$`¯ïµ4iq|;^ºw^4«%+ øEÏÜýòL4$¹wÎ6¨÷¾]À)¨a¸é{ ù ÅÀ8È±¢bývmx¼	Z-¶ðÉû:³JêádÅ6È©
  ðM
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./docs/img/avatar-3.png
Copyright: X YRÄh~Á]ì- ÅÍo¬xïWåìô29Ãgï0ýK±Ë?¶Ó)Za£ LËÞZ
  Y
  xîRzoJöü2Ù}uÒ}yÊ}©øì¾*Ùó
  5«[©çRÊ¼wA¹HÞùgV¨í#£W¨à,BÒÀÝÈDë¦WGÃONzé¶Ã¦_OEýýUie.ÌIààÀ5¤ÛbgH,~y@Z÷ÍÖï[ç7¸×ê¤ý]Þ8ô	P÷Äoû¡úÿòò 
  S2-ûf[}jÞÖÍ,x
  dÏ¯X½¹Ïeÿz7Pð}Ó	mzX}nÅÂÓVor~´mBÃåE|ty¯|¿©
  ¥/)ø
  ®
  â¹/ÕqFÆ¹-üe£ü
  ã(@æFÊ ªj#+iÄò<²"U­Þ
  çbøs:"É{îB±
  órn£¿z¼:Oõ¿
License: UNKNOWN
 FIXME

Files: ./docs/img/macos-sierra.jpg
Copyright: ,fFNwô¹Ò¬V:	¼»]3­ç@­6¸r[RºÂÏÉ=Ó!¯
License: UNKNOWN
 FIXME

Files: ./docs/img/favicons/favicon.ico
Copyright: ªÿøßßÿóËÌÿÝijÿÙTVÿÙUWÿÙUWÿÙUWÿØTVÿÐQS¸HIËOQHÖTVíÙUWÿÙTVÿéÿúèéÿæÿÛ`bÿÙVXÿÙUWÿÙSUÿÙSUÿÙUWÿÙUWÿÖTVíËOQHÐQS¦ØUWÿÙUWÿÙWYÿí¯°ÿí°±ÿÙWYÿÙTVÿÙUWÿÙSUÿäÿçÿÚY[ÿÙUWÿØUWÿÐQS¦ÓSUâÙUWÿÙUWÿÙUWÿÛ^ÿÚZÿÙSUÿÙSUÿÚZÿãÿøáâÿï¹¹ÿÙWYÿÙUWÿÙUWÿÓSUâÔSUúÙUWÿÙUWÿÙUWÿÙTVÿÙUWÿßoqÿéÿóÊËÿúççÿòÆÇÿÞlnÿÙTVÿÙUWÿÙUWÿÔSUúÔSUúÙUWÿÙUWÿÙUWÿÚXZÿèÿøááÿøÞÞÿï¹ºÿåÿÛ^`ÿÙSUÿÙUWÿÙUWÿÙUWÿÔSUúÓSUâÙUWÿÙUWÿÙSUÿãÿûììÿí¯°ÿÝgiÿÙUWÿØRTÿÚY[ÿÛ^ÿÙUWÿÙUWÿÙUWÿÓSUâÐQS¦ØUWÿÙUWÿÙSUÿâ~ÿì¬­ÿÜbdÿÙSUÿÙTVÿÙTVÿë¨©ÿîµ¶ÿÚY[ÿÙUWÿØUWÿÐQS¦ËOQHÖTVíÙUWÿÙUWÿÙUWÿÙTVÿÙUWÿÙVXÿÛ]_ÿãÿùääÿë§¨ÿÙTVÿÙUWÿÖTVíËOQH¸HIÐQSØTVÿÙUWÿÙUWÿÙUWÿÙTVÿÜdfÿòÆÆÿøááÿï¶·ÿÜbdÿÙTVÿØTVÿÐQS¸HI    ÅMOÑRT°ØTVÿÙUWÿÙUWÿÙTVÿÛ_aÿåÿáxzÿÚY[ÿÙTVÿØUWÿÑRT°ÅMO            ÆMOÐQSÖTVîØUWÿÙUWÿÙUWÿÙSUÿÙSUÿØTVÿÖTVîÐQSÆMO                    ½JLËOQHÐQS§ÓSUãÔSUúÔSUúÓSUãÐQS§ËOQH½JL            ø  à  À                                  À  à  ø  
  ÿØSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÚYZÿÝegÿÝgiÿÜacÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSÝÁLM)ÆMOjÔSUúÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿåÿòÅÅÿñÂÃÿâ|~ÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØRTÿâ|~ÿøÞÞÿùääÿî±²ÿÙVXÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÔSUúÆMOjÉOQª×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿÙWYÿÙWYÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÚZÿï·¸ÿýõõÿý÷öÿë¥¦ÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÉOQªÌPRÖØUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿØRTÿÚXYÿàtvÿï¹ºÿüððÿýôôÿùääÿßrtÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿÌPRÖÎQRïÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿØSUÿÚYZÿßnpÿçÿñÀÁÿúççÿýôôÿýôôÿúëëÿçÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÎQRïÏQSúÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÜacÿæÿñÃÄÿùååÿüóóÿýõõÿýõõÿüóóÿõÔÕÿäÿÙVXÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSúÏQSúÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿäÿõÒÒÿüòòÿýõõÿýöõÿýôôÿúééÿóËËÿçÿÜceÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSúÎQRïÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿèÿûííÿýõôÿýõôÿúééÿòÆÇÿèÿàtvÿÚ[]ÿÙSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÎQRïÌPRÖØUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿ-ÿúëëÿüóóÿüððÿð»»ÿàwyÿÚY[ÿØRUÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿÌPRÖÉOQª×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿïº»ÿýõõÿüôôÿí¯¯ÿÚY[ÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙWYÿÙWYÿÙVXÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÉOQªÆMOjÔSUúÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÛ^ÿôÎÎÿüòòÿùääÿßoqÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿßqsÿð¾¿ÿòÄÅÿèÿÙVXÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÔSUúÆMOjÁKM)ÏQSÝÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÚXZÿàvxÿâÿáyzÿÚY[ÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØRTÿæÿýööÿþùøÿí®¯ÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSÝÁLM)°DDÉOPÖTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿØSUÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÛ^`ÿóÈÉÿýôôÿüððÿäÿØSUÿÙUWÿÙUWÿÙUWÿÙUWÿÖTVÿÉOP°DE    ÃLN7ÏQSèÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿÚY[ÿßpqÿï¹ºÿüòòÿýõõÿòÆÇÿÛ]_ÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSèÃLN7        °EFÈNPÕSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿÛ`bÿî²²ÿóËÌÿùååÿýõõÿýööÿõÕÖÿßqrÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÕSUÿÈNP°EF            ÁKMËPQ¿×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿÝhjÿùææÿþúùÿýööÿúêêÿï¹ºÿÞlnÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿËOQ¿ÁKM                    ÃLN5ÍPR×ØTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿÝfhÿñÁÂÿð»¼ÿê
  ÿÙSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿËOQæÂKN5ÃLNkÏQSûÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÙSUÿÙSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØRTÿäÿüññÿüòòÿüññÿýõõÿåÿØSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSûÃLNkÄMO¡ÓSTÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÙTVÿâ|}ÿ÷ÝÝÿýõõÿüññÿýôôÿøàáÿÝikÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÓSTÿÄMO¡ÆMOÅÕTUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿØRUÿÙVXÿÞmnÿë§¨ÿúèèÿýööÿüññÿüòòÿýööÿê¡¢ÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÕTUÿÆMOÅÈNPâ×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿØRUÿÙVXÿÝfhÿäÿï¸¹ÿùääÿý÷÷ÿýôóÿüññÿüòòÿýøøÿñÁÂÿÛ`aÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÈNPâÉNQð×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÙUWÿÝikÿæÿð¼¼ÿøààÿüóôÿý÷÷ÿüôóÿüññÿüññÿýõõÿýõõÿð¼¼ÿÜdfÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÉNQðÉOQúØUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿÚXZÿá{}ÿî´µÿøââÿýõõÿý÷÷ÿýôôÿüññÿüññÿüññÿýõõÿý÷÷ÿ÷ÝÞÿèÿÚ^ÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿÉOQúÉOQúØUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿßrtÿð»¼ÿûíîÿýø÷ÿýôôÿüññÿüññÿüòòÿýôôÿý÷÷ÿüòòÿöØØÿê¢£ÿÝikÿÙSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿÉOQúÉNPñ×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙVXÿæÿùãäÿýøøÿüóóÿüññÿüññÿýõõÿý÷÷ÿüññÿöÚÚÿî³³ÿäÿÜbcÿÙSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÉNPñÈNPã×TVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿçÿûððÿýõõÿüññÿüññÿýööÿüóóÿö××ÿì©ªÿâ~ÿÜacÿÙUWÿØSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿ×TVÿÈNPâÆMOÅÕTUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿá{|ÿúêêÿýôôÿüññÿüóóÿýõöÿôÎÏÿåÿÜacÿÙSUÿØSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÕTUÿÆMOÅÄMO¡ÓSTÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÚXZÿñÁÂÿý÷÷ÿüññÿüòòÿüòóÿìª«ÿÛ`aÿØRTÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÓSTÿÄMO¡ÃLNkÏQSûÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSVÿÞmoÿùççÿüóóÿüññÿý÷÷ÿï¸¹ÿÚY[ÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿÙSUÿÙSUÿÙSUÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÏQSûÃLNkÂKN5ËOQæØUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿâÿüôôÿýööÿýööÿüððÿáy{ÿØSUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÚXZÿçÿí¯°ÿí­®ÿí®¯ÿãÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿØUWÿËOQæÂKN5ÁIMÆMO¹ÕTUÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙSUÿâÿôÎÏÿõÑÑÿõÒÒÿñÀÁÿÜbdÿÙTVÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙUWÿÙTVÿÛ^`ÿöÚÚÿþúúÿý÷÷ÿþûûÿê
License: UNKNOWN
 FIXME

Files: ./docs/img/avatar-5.png
Copyright: ó68^ÃÃã¥ÌÛ ß3Ì±
License: UNKNOWN
 FIXME

