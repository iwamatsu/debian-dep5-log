Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./CHANGELOG
 ./CMakeLists.txt
 ./README
 ./TODO
 ./debian/README.source
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/libtuxcap-dev.dirs
 ./debian/libtuxcap-dev.examples
 ./debian/libtuxcap-dev.install
 ./debian/libtuxcap4.0.dirs
 ./debian/libtuxcap4.0.install
 ./debian/patches/cmake_imagemagick.patch
 ./debian/patches/gcc6.patch
 ./debian/patches/includes.patch
 ./debian/patches/kfreebsd.patch
 ./debian/patches/series
 ./debian/rules
 ./tuxcap-build/fonts/Andy28Bold.txt
 ./tuxcap-build/fonts/ArmorPiercing22.txt
 ./tuxcap-build/fonts/ContinuumBold12.txt
 ./tuxcap-build/fonts/Kiloton9.txt
 ./tuxcap-build/fonts/supernova20.txt
 ./tuxcap-build/images/atomicexplosion.jpg
 ./tuxcap-build/images/atomicexplosion_.jpg
 ./tuxcap-build/images/beam_opaque.jpg
 ./tuxcap-build/images/bomb_radial_death.jpg
 ./tuxcap-build/images/bomb_radial_death_.jpg
 ./tuxcap-build/images/particle1.psi
 ./tuxcap-build/images/particle10.psi
 ./tuxcap-build/images/particle3.psi
 ./tuxcap-build/images/particle4.psi
 ./tuxcap-build/images/particle6.psi
 ./tuxcap-build/images/particle7.psi
 ./tuxcap-build/images/unicron_baby_.GIF
 ./tuxcap-build/images/unicron_beam_down.jpg
 ./tuxcap-build/images/unicron_horiz.jpg
 ./tuxcap-build/images/unicron_vert.jpg
 ./tuxcap-build/music/m.Mid
 ./tuxcap-build/properties/demo.xml
 ./tuxcap-build/properties/resources.xml
 ./tuxcap-build/pythondemo1/game.py
 ./tuxcap-build/pythondemo2/game.py
 ./tuxcap-build/pythondemo_template/game.py
 ./tuxcap/CMakeLists.txt
 ./tuxcap/CMakeMacros/IJMacros.txt
 ./tuxcap/CMakeModules/FindAudiereLib.cmake
 ./tuxcap/CMakeModules/FindChipmunkLib.cmake
 ./tuxcap/CMakeModules/FindImageMagickLib.cmake
 ./tuxcap/demo1/Board.cpp
 ./tuxcap/demo1/Board.h
 ./tuxcap/demo1/CMakeLists.txt
 ./tuxcap/demo1/GameApp.cpp
 ./tuxcap/demo1/GameApp.h
 ./tuxcap/demo1/main.cpp
 ./tuxcap/demo2/Board.cpp
 ./tuxcap/demo2/Board.h
 ./tuxcap/demo2/CMakeLists.txt
 ./tuxcap/demo2/GameApp.cpp
 ./tuxcap/demo2/GameApp.h
 ./tuxcap/demo2/main.cpp
 ./tuxcap/demo3/Board.cpp
 ./tuxcap/demo3/Board.h
 ./tuxcap/demo3/CMakeLists.txt
 ./tuxcap/demo3/GameApp.cpp
 ./tuxcap/demo3/GameApp.h
 ./tuxcap/demo3/main.cpp
 ./tuxcap/demo4/Board.cpp
 ./tuxcap/demo4/Board.h
 ./tuxcap/demo4/CMakeLists.txt
 ./tuxcap/demo4/GameApp.cpp
 ./tuxcap/demo4/GameApp.h
 ./tuxcap/demo4/Res.cpp
 ./tuxcap/demo4/Res.h
 ./tuxcap/demo4/TitleScreen.cpp
 ./tuxcap/demo4/TitleScreen.h
 ./tuxcap/demo4/main.cpp
 ./tuxcap/demo5/Board.cpp
 ./tuxcap/demo5/Board.h
 ./tuxcap/demo5/CMakeLists.txt
 ./tuxcap/demo5/DemoDialog.cpp
 ./tuxcap/demo5/DemoDialog.h
 ./tuxcap/demo5/GameApp.cpp
 ./tuxcap/demo5/GameApp.h
 ./tuxcap/demo5/Res.cpp
 ./tuxcap/demo5/Res.h
 ./tuxcap/demo5/TitleScreen.cpp
 ./tuxcap/demo5/TitleScreen.h
 ./tuxcap/demo5/main.cpp
 ./tuxcap/hgeparticle/hgeRandom.cpp
 ./tuxcap/hgeparticle/hgeRandom.h
 ./tuxcap/hungarr/Board.cpp
 ./tuxcap/hungarr/Board.h
 ./tuxcap/hungarr/CMakeLists.txt
 ./tuxcap/hungarr/GameApp.cpp
 ./tuxcap/hungarr/GameApp.h
 ./tuxcap/hungarr/GameOverEffect.cpp
 ./tuxcap/hungarr/GameOverEffect.h
 ./tuxcap/hungarr/LevelupEffect.cpp
 ./tuxcap/hungarr/LevelupEffect.h
 ./tuxcap/hungarr/OptionsDialog.cpp
 ./tuxcap/hungarr/OptionsDialog.h
 ./tuxcap/hungarr/Res.cpp
 ./tuxcap/hungarr/Res.h
 ./tuxcap/hungarr/TitleScreen.cpp
 ./tuxcap/hungarr/TitleScreen.h
 ./tuxcap/hungarr/main.cpp
 ./tuxcap/include/AudiereLoader.h
 ./tuxcap/include/AudiereMusicInterface.h
 ./tuxcap/include/AudiereSoundInstance.h
 ./tuxcap/include/AudiereSoundManager.h
 ./tuxcap/include/BltRotatedHelper.inc
 ./tuxcap/include/Buffer.h
 ./tuxcap/include/ButtonListener.h
 ./tuxcap/include/ButtonWidget.h
 ./tuxcap/include/Checkbox.h
 ./tuxcap/include/CheckboxListener.h
 ./tuxcap/include/Color.h
 ./tuxcap/include/Common.h
 ./tuxcap/include/CursorWidget.h
 ./tuxcap/include/D3DInterface.h
 ./tuxcap/include/DDI_Additive.inc
 ./tuxcap/include/DDI_AlphaBlt.inc
 ./tuxcap/include/DDI_BltRotated.inc
 ./tuxcap/include/DDI_BltRotated_Additive.inc
 ./tuxcap/include/DDI_FastBlt_NoAlpha.inc
 ./tuxcap/include/DDI_FastStretch.inc
 ./tuxcap/include/DDI_FastStretch_Additive.inc
 ./tuxcap/include/DDI_NormalBlt_Volatile.inc
 ./tuxcap/include/DDImage.h
 ./tuxcap/include/DDInterface.h
 ./tuxcap/include/DescParser.h
 ./tuxcap/include/Dialog.h
 ./tuxcap/include/DialogButton.h
 ./tuxcap/include/DialogListener.h
 ./tuxcap/include/EditListener.h
 ./tuxcap/include/EditWidget.h
 ./tuxcap/include/Flags.h
 ./tuxcap/include/Font.h
 ./tuxcap/include/GENERIC_DrawLineAA.inc
 ./tuxcap/include/Graphics.h
 ./tuxcap/include/HyperlinkWidget.h
 ./tuxcap/include/Image.h
 ./tuxcap/include/ImageFont.h
 ./tuxcap/include/ImageLib.h
 ./tuxcap/include/Insets.h
 ./tuxcap/include/KeyCodes.h
 ./tuxcap/include/ListListener.h
 ./tuxcap/include/ListWidget.h
 ./tuxcap/include/MI_AdditiveBlt.inc
 ./tuxcap/include/MI_BltRotated.inc
 ./tuxcap/include/MI_BltRotated_Additive.inc
 ./tuxcap/include/MI_GetNativeAlphaData.inc
 ./tuxcap/include/MI_GetRLAlphaData.inc
 ./tuxcap/include/MI_NormalBlt.inc
 ./tuxcap/include/MI_SlowStretchBlt.inc
 ./tuxcap/include/MTRand.h
 ./tuxcap/include/MemoryImage.h
 ./tuxcap/include/MusicInterface.h
 ./tuxcap/include/NativeDisplay.h
 ./tuxcap/include/NaturalCubicSpline.h
 ./tuxcap/include/Point.h
 ./tuxcap/include/PropertiesParser.h
 ./tuxcap/include/Quantize.h
 ./tuxcap/include/Ratio.h
 ./tuxcap/include/Rect.h
 ./tuxcap/include/ResourceManager.h
 ./tuxcap/include/SDLMain.h
 ./tuxcap/include/SWTri.h
 ./tuxcap/include/ScrollListener.h
 ./tuxcap/include/ScrollbarWidget.h
 ./tuxcap/include/ScrollbuttonWidget.h
 ./tuxcap/include/SexyAppBase.h
 ./tuxcap/include/SexyMatrix.h
 ./tuxcap/include/SexyVector.h
 ./tuxcap/include/SharedImage.h
 ./tuxcap/include/Slider.h
 ./tuxcap/include/SliderListener.h
 ./tuxcap/include/SoundInstance.h
 ./tuxcap/include/SoundManager.h
 ./tuxcap/include/TextWidget.h
 ./tuxcap/include/TriVertex.h
 ./tuxcap/include/Widget.h
 ./tuxcap/include/WidgetContainer.h
 ./tuxcap/include/WidgetManager.h
 ./tuxcap/include/XMLParser.h
 ./tuxcap/include/XMLWriter.h
 ./tuxcap/lib/AudiereLoader.cpp
 ./tuxcap/lib/AudiereMusicInterface.cpp
 ./tuxcap/lib/AudiereSoundInstance.cpp
 ./tuxcap/lib/AudiereSoundManager.cpp
 ./tuxcap/lib/Buffer.cpp
 ./tuxcap/lib/ButtonWidget.cpp
 ./tuxcap/lib/CMakeLists.txt
 ./tuxcap/lib/Checkbox.cpp
 ./tuxcap/lib/Color.cpp
 ./tuxcap/lib/Common.cpp
 ./tuxcap/lib/CursorWidget.cpp
 ./tuxcap/lib/D3DInterface.cpp
 ./tuxcap/lib/DDImage.cpp
 ./tuxcap/lib/DDInterface.cpp
 ./tuxcap/lib/DescParser.cpp
 ./tuxcap/lib/Dialog.cpp
 ./tuxcap/lib/DialogButton.cpp
 ./tuxcap/lib/EditWidget.cpp
 ./tuxcap/lib/Flags.cpp
 ./tuxcap/lib/Font.cpp
 ./tuxcap/lib/Graphics.cpp
 ./tuxcap/lib/HyperlinkWidget.cpp
 ./tuxcap/lib/Image.cpp
 ./tuxcap/lib/ImageFont.cpp
 ./tuxcap/lib/ImageLib.cpp
 ./tuxcap/lib/Insets.cpp
 ./tuxcap/lib/KeyCodes.cpp
 ./tuxcap/lib/ListWidget.cpp
 ./tuxcap/lib/MemoryImage.cpp
 ./tuxcap/lib/MusicInterface.cpp
 ./tuxcap/lib/NativeDisplay.cpp
 ./tuxcap/lib/NaturalCubicSpline.cpp
 ./tuxcap/lib/PropertiesParser.cpp
 ./tuxcap/lib/Quantize.cpp
 ./tuxcap/lib/Ratio.cpp
 ./tuxcap/lib/ResourceManager.cpp
 ./tuxcap/lib/SDLMain.m
 ./tuxcap/lib/SWTri.cpp
 ./tuxcap/lib/SWTri_DrawTriangle.cpp
 ./tuxcap/lib/SWTri_DrawTriangleInc1.cpp
 ./tuxcap/lib/SWTri_DrawTriangleInc2.cpp
 ./tuxcap/lib/SWTri_GetTexel.cpp
 ./tuxcap/lib/SWTri_Loop.cpp
 ./tuxcap/lib/SWTri_Pixel555.cpp
 ./tuxcap/lib/SWTri_Pixel565.cpp
 ./tuxcap/lib/SWTri_Pixel888.cpp
 ./tuxcap/lib/SWTri_Pixel8888.cpp
 ./tuxcap/lib/SWTri_TexelARGB.cpp
 ./tuxcap/lib/ScrollbarWidget.cpp
 ./tuxcap/lib/ScrollbuttonWidget.cpp
 ./tuxcap/lib/SexyAppBase.cpp
 ./tuxcap/lib/SexyMatrix.cpp
 ./tuxcap/lib/SharedImage.cpp
 ./tuxcap/lib/Slider.cpp
 ./tuxcap/lib/TextWidget.cpp
 ./tuxcap/lib/Widget.cpp
 ./tuxcap/lib/WidgetContainer.cpp
 ./tuxcap/lib/WidgetManager.cpp
 ./tuxcap/lib/XMLParser.cpp
 ./tuxcap/lib/XMLWriter.cpp
 ./tuxcap/particledemo/Board.cpp
 ./tuxcap/particledemo/Board.h
 ./tuxcap/particledemo/CMakeLists.txt
 ./tuxcap/particledemo/GameApp.cpp
 ./tuxcap/particledemo/GameApp.h
 ./tuxcap/particledemo/main.cpp
 ./tuxcap/physicsdemo/Board.cpp
 ./tuxcap/physicsdemo/Board.h
 ./tuxcap/physicsdemo/CMakeLists.txt
 ./tuxcap/physicsdemo/GameApp.cpp
 ./tuxcap/physicsdemo/GameApp.h
 ./tuxcap/physicsdemo/main.cpp
 ./tuxcap/physicsdemo2/Board.cpp
 ./tuxcap/physicsdemo2/Board.h
 ./tuxcap/physicsdemo2/CMakeLists.txt
 ./tuxcap/physicsdemo2/GameApp.cpp
 ./tuxcap/physicsdemo2/GameApp.h
 ./tuxcap/physicsdemo2/main.cpp
 ./tuxcap/physicsdemo3/Board.cpp
 ./tuxcap/physicsdemo3/Board.h
 ./tuxcap/physicsdemo3/CMakeLists.txt
 ./tuxcap/physicsdemo3/GameApp.cpp
 ./tuxcap/physicsdemo3/GameApp.h
 ./tuxcap/physicsdemo3/main.cpp
 ./tuxcap/physicsdemo4/Board.cpp
 ./tuxcap/physicsdemo4/Board.h
 ./tuxcap/physicsdemo4/CMakeLists.txt
 ./tuxcap/physicsdemo4/GameApp.cpp
 ./tuxcap/physicsdemo4/GameApp.h
 ./tuxcap/physicsdemo4/main.cpp
 ./tuxcap/physicsdemo5/Board.cpp
 ./tuxcap/physicsdemo5/Board.h
 ./tuxcap/physicsdemo5/CMakeLists.txt
 ./tuxcap/physicsdemo5/GameApp.cpp
 ./tuxcap/physicsdemo5/GameApp.h
 ./tuxcap/physicsdemo5/main.cpp
 ./tuxcap/physicsdemo6/Board.cpp
 ./tuxcap/physicsdemo6/Board.h
 ./tuxcap/physicsdemo6/CMakeLists.txt
 ./tuxcap/physicsdemo6/GameApp.cpp
 ./tuxcap/physicsdemo6/GameApp.h
 ./tuxcap/physicsdemo6/main.cpp
 ./tuxcap/physicsdemo7/Board.cpp
 ./tuxcap/physicsdemo7/Board.h
 ./tuxcap/physicsdemo7/CMakeLists.txt
 ./tuxcap/physicsdemo7/GameApp.cpp
 ./tuxcap/physicsdemo7/GameApp.h
 ./tuxcap/physicsdemo7/main.cpp
 ./tuxcap/pycap/PycapApp.cpp
 ./tuxcap/pycap/PycapApp.h
 ./tuxcap/pycap/PycapBoard.cpp
 ./tuxcap/pycap/PycapBoard.h
 ./tuxcap/pycap/PycapResources.cpp
 ./tuxcap/pycap/PycapResources.h
 ./tuxcap/pycap/main.cpp
 ./tuxcap/pythondemo1/CMakeLists.txt
 ./tuxcap/pythondemo1/main.cpp
 ./tuxcap/pythondemo2/CMakeLists.txt
 ./tuxcap/pythondemo2/main.cpp
 ./tuxcap/pythondemo_template/CMakeLists.txt
 ./tuxcap/pythondemo_template/main.cpp
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./tuxcap/chipmunk/LICENSE.txt
 ./tuxcap/chipmunk/chipmunk.c
 ./tuxcap/chipmunk/chipmunk.h
 ./tuxcap/chipmunk/cpArbiter.c
 ./tuxcap/chipmunk/cpArbiter.h
 ./tuxcap/chipmunk/cpArray.c
 ./tuxcap/chipmunk/cpArray.h
 ./tuxcap/chipmunk/cpBB.c
 ./tuxcap/chipmunk/cpBB.h
 ./tuxcap/chipmunk/cpBody.c
 ./tuxcap/chipmunk/cpBody.h
 ./tuxcap/chipmunk/cpCollision.c
 ./tuxcap/chipmunk/cpCollision.h
 ./tuxcap/chipmunk/cpHashSet.c
 ./tuxcap/chipmunk/cpHashSet.h
 ./tuxcap/chipmunk/cpJoint.c
 ./tuxcap/chipmunk/cpJoint.h
 ./tuxcap/chipmunk/cpPolyShape.c
 ./tuxcap/chipmunk/cpPolyShape.h
 ./tuxcap/chipmunk/cpShape.c
 ./tuxcap/chipmunk/cpShape.h
 ./tuxcap/chipmunk/cpSpace.c
 ./tuxcap/chipmunk/cpSpace.h
 ./tuxcap/chipmunk/cpSpaceHash.c
 ./tuxcap/chipmunk/cpSpaceHash.h
 ./tuxcap/chipmunk/cpVect.c
 ./tuxcap/chipmunk/cpVect.h
 ./tuxcap/chipmunk/prime.h
Copyright: 2007, Scott Lembcke
License: Expat
 FIXME

Files: ./tuxcap/include/ParticlePhysicsSystem.h
 ./tuxcap/include/Physics.h
 ./tuxcap/include/PhysicsListener.h
 ./tuxcap/include/SDLMixerMusicInterface.h
 ./tuxcap/include/SDLMixerSoundInstance.h
 ./tuxcap/include/SDLMixerSoundManager.h
 ./tuxcap/lib/Physics.cpp
 ./tuxcap/lib/SDLMixerMusicInterface.cpp
 ./tuxcap/lib/SDLMixerSoundInstance.cpp
 ./tuxcap/lib/SDLMixerSoundManager.cpp
Copyright: 2007, W.P. van Paassen
  2007-2008, W.P. van Paassen
  2008, W.P. van Paassen
License: Expat
 FIXME

Files: ./tuxcap/hgeparticle/hgecolor.h
 ./tuxcap/hgeparticle/hgeparticle.cpp
 ./tuxcap/hgeparticle/hgeparticle.h
 ./tuxcap/hgeparticle/hgepmanager.cpp
 ./tuxcap/hgeparticle/hgerect.cpp
 ./tuxcap/hgeparticle/hgerect.h
 ./tuxcap/hgeparticle/hgevector.cpp
 ./tuxcap/hgeparticle/hgevector.h
 ./tuxcap/lib/ParticlePhysicsSystem.cpp
Copyright: 2003-2004, Relish Games
License: UNKNOWN
 FIXME

Files: ./debian/demos.patch
 ./debian/patches/abs_dirs.patch
 ./debian/patches/cmakelists.patch
 ./debian/patches/particles_res.patch
Copyright: 2008-2009, Miriam Ruiz <little_miry@yahoo.es>
  2009, Miriam Ruiz <little_miry@yahoo.es>
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/hero.JPG
 ./tuxcap-build/images/heroDamage.jpg
Copyright: 9èLú3áY]F{áF{è49NÜpmÞ'ÈÓJ#5´M'~´p=Â:ÖÝûÓú±»ÆêèÌ®§*ÊpAò4A°ñyÚéÄÏKrESYfòR¤ýÎt
  âÚ0ÍGW#òbHûÚGgvfv9f'$'AÿÙ
License: UNKNOWN
 FIXME

Files: ./PopCap Framework License.txt
 ./tuxcap/pycap/PopCap Framework License.txt
Copyright: 2005, PopCap Games.
License: UNKNOWN
 FIXME

Files: ./tuxcap/lib/MTRand.cpp
Copyright: 1997-2002, Makoto Matsumoto and Takuji Nishimura,
License: BSD-3-clause
 FIXME

Files: ./tuxcap-build/images/sky_.jpg
Copyright:  @`vt
   v-R^Iä¶*öÀk§®«FL·ÌÊå¿Ï@NcS&Øau2d±×²ÒÑ1æÀÓ,y×³#»]Ë%ûjh;FEuìuuì
  T¥
  ª5ä·Ù§O¾Øo$/$o"ðIïÓïÓÛ2k~Ý}0)°ìIÛe£}À
  
  %ÔÚöNîWYs>Òg§Tú¶«,ÿ $ë"~Ðuô Úßc¶½_@Ðèî¡[B:¿ O³=['YÞQ^Se`ù(
  4:hÆ³!
  7>(©{<o^B³µÙ°=¥h/"<oÅZìÀøË^õrN:]z8¾©m~dAñÕè¯[ÿ  cÌP³¯%8Ñ¶åËú2WGÐJ8áo¸.÷?¹o;öÏRö
  H~Wè: Jê2G$2@rC.Ç$2] F
  Veà
  Y®ÀªaÙ4ÆØ
  f
  m
  m#±5Ð<ÚÉ­$d¬º%Y*¼è
  ukü§T¿ä	Mu4aÌç}L*ºÆöÛøìeÄ¯?ôcåûaSöÅÄué[)9öaR½±××{ìÉ[d0óPuìc=±}:ey>Àñ
  w`G_GnW¡*Àííî`
  òx~ºÒÙè`mkiéãÉL¾;úf,y:"Ñ{tR~
  @
  çÄú®àoç¤cùs/`jmÔ>éù~ÀòýG8½9ìf¼2e~êýH^DÏ2óW²o¹})êÞ$ûö
  l U¢u[Ùl
  ®éò#E-æ} ì	ÝvE¹'}ÉkÀTû²³#`wEØ+¨Óòè	¤2ChîPÐvÐt æ^Ì½
  ¾9uäU_@ó÷<õÄ¤µ
  Ðy@TH*FH /¡¤d¥ô:R$u EOÐ~¨îTg/Ð&û´?Äü#¾6
  ÛÉÓlÐ±ë«*KòÉ
  ë£3ñ¶¶ºRê>~YÓ ñ¿Ìÿ Ùæät©®gþÍYx®É½°©û`ÙÚmö9Åz &saä
  îQZ]8ôåH^m
  îÐÙóÝoOF;Û}^À93%ý½Y
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/loader_bar.jpg
Copyright:  ® ² · ¼ Á Æ Ë Ð Õ Û à å ë ð ö û
  ±¹ÁÉÑÙáéòú
  1998, Hewlett-Packard Company  desc       sRGB IEC61966-2.1           sRGB IEC61966-2.1                                                  XYZ       óQ    ÌXYZ                 XYZ       o¢  8õ  XYZ       b  ·
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/layer0.jpg
Copyright: b®0¦
  JÃ¹
  Ã5GqR¢ÁsMeqOYyÁ¬År:TÈÌÝéXw4J#÷¥.Â«Dì85v)u¤4M>´In½iÊùèi%cC)Éæ¢û*±©ç4ÔksLDðÙ
  Í(çµ0`ÕJpýh°îjAÔÓ|tÅgocÉ4ªÙ<Ò°½çÒOZ¬TÁ±Ò¹eA=*Eg)}EN&ã¥!ÜyÅGåbMkÊ½é à£°¥Î:ÕW»L=iØW4REº·J¦ìjxÔ·¨rO,52EUà:A?wH
  ¬F¤ÐÀW1ªñG´§¡UäÍ^UI<Pp§¨¨¹§.E$ÛJÀôíô úLâ£/IL¤I1U¥4"Z×X#ÆªÔLzÓÛä
  ¯p4ÿ (]SS¢Â)êÅi
  
   4äpi¾YÎr*MÛ»T¹@
   =iÜ#·êaÒ¥òÛS8íÅ+À¨%T=ÅI$ES¥$.§hW!¸EJ@=*éFê
  ©'Í<MïJÃ¹x1
  0*8Z§cR2ÊªÆ9­)
  ~9x«1KïY¨Õ:1©h¤ÍT¤
  LÔ{
  -¶OJÐíQ9¥p*GnDµ+µ@íLEY
  4f©É*Ó$¤éPH¤½¶ñS¸
  6ÒL
  C/¥E	sV#Ù1RRño^*«ÂÀõ­QÒ=á¹+ÆB#5^zVÀQ¾aI²2})ÜV*,GµH°zTÅ1÷[4|ÔqSÇ
  D´¬A5æÓÕÍ+ËDæTqÆ¬m8¤*ÄÎ R¨^ jDà)Ê("Sª0qN4=["¹¨@©,#·ò*SDpjXÍ»r
  OËYÓM$·`µqs»88©HMK ÇZ¦ì3Ö
  Q=hÌõcx"ª¢â¥U5,dÀÚå<Ñ lCå­5£¥AµaaÝÐQp±PÀ©!µ.zV·*:U(ÎîE)!¶vº;NEäd
  SùTõJ.hµb4¥Hýªu¥°f4§ÉíJàSÆ)r*Y#"£
  UE50jP1H²°+*»`J²Tñ¤¼±ÒB*·PªÂ½K(ÐR1NI%©JV*å¥lTèõE[5b&¤µcRY1P!§JÆsQm
  VN+=ÔÈÔ¬;ÉÖ jqS*³®*ìÔf0i¦ªI«Q%9c«Æ(¸	Õ¸ÄZaG5,e8¥,*°¿4qÒ5Q¸]ù«MÈ¨ÑsLaWNsTIVVÅVgæ;UVnj,¡v¦³ÔNùª¤æº­ ©¹¥"i}(Ó©TÐ1¥x¨U£PÈ(QÍFMJëP8Å1ÍÅ7<ÓsÍ=ij&`Õ8Á­GRÆL¢¤À"G
  VPG&(sÐûÕä"©+­J&ªÄ(ã½95&ÍJ³Zå
  c û6M)¶ UâÈ¢ã)<UD{UòÒ
  cEøÑAVcÚ«BëÔÕ´EC,qHèi¾Z'N´æ+Ud®(
  c`½EFAÜ ¸ íR	°ª
  cqÈ¨7SZb£®)å£zTR
  eÈÍF")ÑMH²°*E sSL$Õ.wv©29®¶ügc§¹ó;{ÓOüXÍ¸uC@xäû±µsü?/pþ¬ÖÆ@Ú*¤Ê#dV]«
  l5D '5­ÌR{Öz¢ôg&ñ²EYA]4Ö°JÙUÅ@öa>èªæR¹^E@QAé[2Â äU£ÇJi$ vªÎÇµ_ w¨gTÇHJ$#­2fÛUj«°òTfaU@z
  qoZDPv¨Ý1Vfåg
  qïYËq]*lUËj'¸Üj7rG
  s7&£Ó'CdjEQ7,Pg&%UÜzV
   ã»vj/1`t¤2äQ"õÅ
  æ³eµ1ç¢1hE5"[V£éWb qCb(ÅhjÊÚ{VQJ ¦ã±-=©ËnAéZTmEÂÄQÇµ&ÑLf¨ËÒù"ã­VaR³u¨Ó¡³OíM*B(
  ©Å©dVK8Ðç­XP À¤-@j¬JéñY0*ÏÐDO´óQMp¤u¨®{â±îådÎ
  B*l3Z*Ê¾k!&«	?½KC¹}ÆzUYö¥[?Ê»sÁÉ-ò:VS$ Ó¸Îy­zQäJèþÄµ=tÝÝ¨æ
  ºqV,åV´R0Â¥²)DØ4÷bâ®PuÐàéJåw,w*(²OzÔÑ¨¨ÒÔ©éUqG=jO³ç¥LW`äTF/À
  £5±
  ¥ú9ì`*;u«
  ©ª~jT@[ßC%E»"Ñ`,#ÔêÕI*ÌmL
  ª²³´µ¶·¸¹ºÂÃÄÅÆÇÈÉÊÒÓÔÕÖ×ØÙÚáâãäåæçèéêñòóôõö÷øùúÿÄ        
  ª²³´µ¶·¸¹ºÂÃÄÅÆÇÈÉÊÒÓÔÕÖ×ØÙÚâãäåæçèéêòóôõö÷øùúÿÚ 
  «¼R¬Dx§9¥+Â) çûR´*¨4¥R/JBi Ò Ræ`)Å47PZF(v(Ò8¨¬íÏZ
  ®$ã¡!&©ØæoznEGIT+âp£Æcª"ÀÒhiB]Ô+jlHâ¬Å¿Ò¤«#ÔÆPÆ[½N­ÅHÆ}?^Ób½N­Ï5:WIl¹ç-z±øÒä¦dp¦yr%UèVóg ði
  °îi	­8Üc½f
  ¶feSÇhÀÖ4½)¹&©b'µ1i»M_KsÜR|R¸¬g4¥x«m¦ùtî6SQWYBéNàQUd<ÕéS§"sTV5Ôìµ
  ¸3húÕ)c­É`ÍTj¤ÉhÆd Ò­ÑoíUrlP(iÊµ¢-³ÚkÔv(
  ½ÀSÖ¨IrO ÕgrÝê£%Ø'H³ûÖfy©©ØW5a¸æ¯Å98¬H$ ÕøçUjZfºMÍ2KzÊ÷
  ÂÓ~ÔL¡4«jßíN	EÄUò±MÛW|ºcÅè(¸Ëô¥òMnÛÛ+ðEZþÇg+>bùNaÒ­Fö­Ïìjh´}ì
  Éb±Z$õ©²TnÛjsLEznê¦ÒiôX.]5Mfæ¬Å(
  É-RB¹$³f«4µÉ´å%aª¾e9^Ë!ÉíK¸Ô"AK¼PdzR>µ"DjÄp(¸6íS$mWb´lt©~ÎGlT¶dZbe©#LÒ¸ÈØdTMZh]x"¡ÌJàgóJ1ÜTï/jZbÔ
  Ì;i Å0éK´÷§N_z@G¶/µKòÑ@*ÜJ@ïUbÎ= ¹p3íV
  ÑÃVL{«F!µ2hh.>CUdz|²
  ÓïG01íàºZ	m^©þÊàb¤HBN*[Q{qÛô¦ª<kHZàåÓ æ«#sS¤ûG&­yQT3B
  ÕÖ²Ø§	­
  Ü
  âBjejÔpJàTÆqQÎkI£ T~X¢ã±MPÓ>*×)
  êÇµ,@Ì¤VÕ¬ÌÑñXðsÖ¶ì
  þËHûÀãªK$Ã¨ãÚÊ*ÂÆÃÓ§¤YC2·¤lê5(^Â®´S7Þm ÖBLÑ'¥O¸5{ì©¢´£
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/planets_small.jpg
Copyright: ì1d
  7däXËÐä±ÐÝ]ªiÙ§Fp
  ±v©ùPWWÇ_ÛyM-Í5ñ¶'­[
  Cn½Îß­ë¤ÞiE²mµÜtë±¾.qù¿¹{Ïö÷kÖ¿ê®Nå}>:¼qOUi
  M}bKÎE6Jó=Èòò<s¤ñÏ,ÒÍF¢Ìiê©'¥5âÅU¶çnÏÉæÌ¡¸¬G¢Ð
  Oó×Oå^!3½;Iù17òî¯ñ×"¹eÖH¬rI>æF¸6¹&ÈjcuÑÂi_ã¡à5É¦ZØXIt.îm"+q*¶ôÒ¼ÿ (zWÂµ§M Èö÷_
  Rhª£êf>@
  X®-%éý>¦k½ÔøÖ¨½J©r{þ[H÷+ü®JÆÉàµ
  q
  @¯ü]q·ZÔÖ³~qþÈà$ò|bk~@¸Åêdp¨~!MëöyÒ|F<<¯¢"ÿ Ìü.ÇA¾KÆI(QI§©[Ã]ø7»'ü×JÕ5ç"
  Þ}FMbA/Mv±bRI%o¥O@5
  ¦£%¬½NIãÅZ(ªîq8·¸RÇ¶Þà*È®².ãð%)®*Î®Qg
  §Ô]¤b²²ãç2 VVdÅQ
  ©ñ©'Pz´¥òpõ±ñ¯äÉ±=«Ìu¢ 6SwmÈí z¾jEuN×x²»(õ/ì§[Z#Ô£]¾ÿ ¨Û^Gí^gÛuÅ5/#ÇÁ?Ü¤ìñ¤Ç¡n
  ª&Ï= ÃÄ{M5ÖòWl¶#2PÇrÓôÐyÌ4Ø»³
  ¯¾«þ(O'U[·)¶ÈEld
  ¯Æ½t/æöP+þà
  ¾	WÜÇBZèkóÐz2F:Æf«2J*Ï@ð¼*ÉÝ
  ×¡×:ÚõRµûq%KDÁæ?Äãÿ ½;tÿ ­ÿ %)êÛZý5ñ§»qõIw÷'Â$ÃgÀKÄ³®Éíå©G"»J·CàA®©Ú²f&]<ÊÚÞhñ¶ÙËp¦9'ÈÒlo©AÞ¼>4×Yç³:ËÜÕZ×S
  ÙïZo¿ßu¬i?´¹l¿ãöæí´ýõ¥Õ~ôG¯~Ýkáâ^Çd3GÉZ[$¶ÕMÄ­·y; §Ï];}Ü]z6«Ïúðô»¢Ö··ËT£_¸Í8Sbå"äÒXZ|Áó3bYqþ6*ôûíÝÒÎaÇô
  Ü_¡ü³{]§iÛeÖòör,tKj­èl{sîYÀ©¶¸·v®PI?5$¨dëÅ¸6éjÚ©=ÐIÉ}ÈÀÞÜÉ0Dn(iH 
  ã´
  êÝJ~ô×?oÕëânpîBÆÒhñ7ßË¤±ÙÜÐ
  ú/ü
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/unicron_beam_up.jpg
Copyright: d[Ý½gãÑaL{´IXLlhyJ¦7äßg²Þf_ê¬×¿JñV
  d[Ý½gãÑaL{´IXLlhyJ¦:	5çí7¥fÙú«uïÒ¼WöL±¤T2-îÞ³ñè°¦=Ú$¬&64<¥SòoÏ3ÙoJÍ³/õVkß¥x«@:ÄRØ¹½xÎÅÇ»DÆÆªczMùæ{-éY¶eþªÀ{ô¯`¨Xâ*A÷oYøôXSíVR©ù7çì·¥fÙú«5ïÒ¼U
  d[Ý½gãÑaL{´IXLlhyJ¦:	5çí7¥fÙú«uïÒ¼WöL±¤T6.o^3±E
  d[Ý½gãÑaL{´IXLlhyJ¦:	7çYí7¥f¹wú«uïÒ¼U!b?H©lÞ¼gb
  d[Ý½gãÑaL{´IXLlxyJ¦:	7çYí·¥fÙú«uïÔ¼Ub?H©lÞ¼gb
  lÞ¼gb
  x«0:Ä~RØ¹½xÎÅÇ»DÆÆªczMùæ{-éY¶eþªÀ{ô¯`¨Xâ*A7¯Ø¢Â÷h°ØÐòLoÉ¯8Ïi½+6Ì¿ÕX¯~â¿´
  x«@BÄ~RÈ·»zÏÇ¢Â÷h°ØÐòLoÉ¯:Ïm½+5Ë¿ÕX¯~¥â¬
  x«@BÄ~RÈ·»zÏÇ¢Â÷h°ØÐòLoÉ¿<Ïe½+6Ì¿ÕX¯~â¬ ëüEH#"Þíë?
  x¯íb?H©d[Ý½gãÑaL{´IXLlhyJ¦7ä×g¶Þåßê¬	×¿RñV
  *t
  é7çYí7¥fÙú«uïÒ¼U
  é7çYí·¥fÙú«uïÒ¼WöP±¤T6.n^3±E
  ù5çí·¥f¹wú«uïÔ¼U
  ù7çYí·¥fÙú«uïÒ¼WöP±¤T2-îÞ³ñè°¦=Ú$¬&64<¥SòoÎ³ÚoJÍ²ïõVkß¥x«@:ÄRØ¹¹yÏÇ¢Â÷h°ØðòLoI¿:Ïm½+6Ì¿ÕX¯~â¿´
  ù7çì·¥fÙú«5ïÒ¼U
  üÇp{Ù¥«çô)0üðXW´ÿ ª(Ø^	GøÕ?öcvHsã}q¯õþ«ÞþiQ¶bþ}¿è
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/hungarr_logo.jpg
Copyright: UNµYæº¹>¯ÿ $þÿ  r
  O¨¡´Î'Ý×eO<?¬úi«¿Ù89ì:Ûzl¿á3çÝM[aõQ pIDQQj­mT§CÌ{D[-ÔÝ[%JÀ>ØãwáN|îtûúÞ~Løñ#º¶ò|Á¼Á©â+M:Í=Ú­Ö6¼RT¡27ÊqT¨çâÐø~O°wÀ
  à
  æAó©s0·^ÈJRe«âÝ;¿oìawûNTôRÔ0pHÚ'ÚáB¢*¥{*¥|un¼RÖVÀT¥XÝ'ä4èLô(Ófs7Ìì©8
  õ°¡¢"U*¡5±^©ý.Õ']E:¾êüÝÖQÉÂ>ðÆ¸Üàc|-ÔÊC1£bUVÂZ"ÕÕ{¢$äT{eîlÈJ.>Q©Ú½ðÉÅÖkyt9oCt"°&nºb¢"=ÕWB¯
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/unicron_baby.bmp
Copyright: 6¯0'z
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/backgroundbig_.jpg
Copyright: (¿¾NA«ÚÑ
  Z-nÑ«{mþuù,ëßÑ>
  îVrhºíÕÔ}òoÕ«N
  ¿Ü
  ½£y	cG»èXÂ-Â9ÂÉ¹éJÔ§
  FyûÅnÔ´êr°­VJyqpã@r
  o]hv®§Bu)FU<6þ¾`xbï©5j5%E©Ð{ÆI¦*ýA¬TM;ê±_éx=Ô
  Úüþ#^Þ_cjÔ£Mµéÿ C_îxõw·W3¹»¯V½i¼Ê¥I9Iý[(
  %EûµÆ¯9IÉ¾é07«rµÕ]êa7Ë{VZí%)ª=_Ìe}^£x·îæórõ®.¼§%)mäEõõIpÒÛÍÆÏLÕ®æ£JßØÝ:oÃ=[Q]ÅYRYÝy}osUöÓ«³ô2º>£©êÕ*p­UË¶Í¯¤<!Ñ­*ßT©rùÃ{3ªèú^¦P+ZT[b8Gé~Õ*(Ö¼J_ùÞd×Ðß4í*ÖÊ	B
  (EO¹EKl]7ÅfIÖO¶yy~@{%W¢ø©ÈûàÚÄ©æ=Å{ùT*ºrÄÒ7:7Nõçê»#Q'/Ù´¹^
  (ÜZÏ¶<É'
  4Jñ©m©Ü4¹æåöeN¬ðóªºf¿ÁÕ´Yµ÷Å8É{N-ÅþMRµ*¤ãR÷@{¥zFêU£Gâv¦þEº~e=c¡m/³ROÏo}åþêëÍ½6§7~Ö£ö:Îãm½(Ú×EÌRJ/ëÀÉëUª¤íi½PXÆ_«4êÞueÅõKM:Ö½ËÇt"½w{ë¡¼Oé­nÜU¥Ì¶J_6Nawo;xº?
  4ù`QÒ´ç%*}±õfvRQEDXK  ¶½¾µ³¤êÜV
  5üðy©z·QÔëÔN¬£JRríOfMëß)[GMÓ3FÒÑIï-¹f
  5ô2P£)¾
  6ñW?sR¸­W/.EZ¯>`lWåwÿ [ûêú½Ä¿âHÃÊ£õd¨óÕ.ÿ xÈ½J´©J~¦=Õt)¾I2 ",È¸&Aùè<Hdy
  BÖâ­ÍvÜa$½JVåÂHÔu-Bmå9TK×/ïuµÕZ3£g9R0ðqýR5o®%:Órmç#Ô:õ9O5àÚÎÉí=B½ýG
  CdÊÔáó/ÉÅ5íàø©n÷oÈÀÝø±kKeRSiyèIjQ{)¬]'»_sÌ÷1Ý¤Õµ­ÐÃj+kuéI;©G»lClêÊÚ¶m½{ªkíÊO©lÅ)GYãÝy«Î£Æmå¶ù(Wë­z{BæPÛ
  CÊS{ÛRñX¿¬êÝÎYo1OZÜxa)OàTÂÊß?üXÐm­çZòiÂRÍ7cèx¾]O|Q­$°×>¦>®³u(´ªK8Æ[òÜ6þ/ôãT¬kVg²U!Øú³1¥_-BnæSRsI¬3ÁúV¿}mUN5÷ÎÕºÅk.¥8Öu,áÆ_4qæ½õí¼V2éÅ`ç=âNë4#MßR£ZQÂS7.*u¥º¸	UJPÜ
  E§²{2¯B2íjiú9#çEºêÆÊZvÔ
  F©Û·-î7·ò8Û	u©ÎO
  F2rI,{æÚ©qA9I¼$'¦<èÚô¨S»º§óË
  FÖ¿ÞÈíZ÷Rx|åÌ*o»ïlçÝOÕV¥8.÷WÍÂRÿ ôÖµ+©Õr©QÅoò­ö5Û¡
  J¥Ì©¦»£ÕO#m     2K/	Ô)Ê¤ÚRËlã^%øJÞ¥JõVh¾ñ{¯©iôjiÖuwkRi`êmf­ÝÅIÎ¤^^àe:»­®î¥7²Ýù=ÌuÍfî«;[¼ÝI8§
  JÝ)w$éÿ Ü¼}ah»c9Uk
  JêkÕ9e?±¹ÐªvwÓ]Ñõ[{«
  Nù`ý
  N§|áñ&ÞòåXi±XîÂ^
  OO²¸«¾2ÓXü?³§so8fW¤wlóÖ½Gà_N)cp2ú¿QÙÝT¡e8'ÆeÁ¸»øâ
  RSyä÷ØØL&D&@¹d ïàG!$r&ÀC$r 6ý" 
  W«I|:RîÊ[ÞÑ%Ri(Éac«t®KK²XU%ÈzP:q§b°    NQäðË8'}PêÊv´g²ÊK'RëÍiYXT£J¢äÙ¬äòßº¾*U¯^£[xßªncMÍ¶ûäÿ uUÔyÎæG¨5us&Ù1 l !l²#x"Ø1 	±²
  ZÐrR[ß9Ë9gJß¥Û,<'ë±½ØßwF/»ÿ P6«xÊ8ÿ â'%èÙy9É¯ÛÞ¯9ù±Ô`ì
  c7ÜCòU
  hÔ,úÂçû·T£
  iVò(%·ÕuK4£%Ú6×õ~ªP¶ºb·G0êëN«Õ¡SõíÛXÃ{i}=ã©XÉÓÊ¦ñCQÕ´ögôÍZ§ATÄêÖ¸©RMüÏÕú;Î
  ibZñËYN­féË	­IK÷³iºµÓî¡ÝmyB_üÉ»6òéÁÊ+ü»ì4¥<9K>LØ´½Æ}0iÿ Þ7VRÅZu7ÝÝcmoñj4×õ£ÙiöÐáGý
  jÏeQïîi
  k
  kµ"rK>¦ªõ
  lRéí7ãÔÛþeðû§k]Õ+Z
  s/BÞ§LÆ0ò=A{Ò´ÚÖ-yáyæ«ÒN
  s'´-gw^ÍþJ-
  suZ4`æâñÃ@¡¤SÎKÝ{ÞXùâß¶æoÔTj]¿&ÒxîÎMÏJÖ¨Gù¨
  v¬ù»ûyÎ~¯¨IQûV}Íåû­ÔaLÎ§
  wZ|&J¾"ßÔ¸§J
  xsU´ª¿Üó¹¯õµìÛÿ çK¡J½^!7ö/iiw2ÿ 
  {,¶*Pâ
  Ig'
  Mø22¸§ÊuPâ2Ö£ÓwíKþn
  Âöouöe¥:êÌ5Ûýe''ýXIc(Éf-5ìË;Í_J³¬¨Ýêvvõe²
  £i4£>Êj¯mîlÚç4hG¶
  ¤i©¹ü©®
  ¨Êêo¶=±þ¬Ç7î6EÆF``E7èÄ¥%æ& 7RÕó#" IÎ_æe7)>[d@ÉùdÉ",&2,'ÀÄÀÄÀäCb2lÛÈLß¹ ² 0C 0C;û ÀC <Hä m   !dgad  "lY lL  Ù{ØxÆsxÉ	<²âÊ£
  ¨Ñ¡
  ¨á(HôÒhN­4ÞrGXÓ­akmqò@   §%»5f
  ©S¯uBÂUMµé¤	°;Oh
  ¬õ­ÍV×Åk}÷9½m{»ù´$òç²k]Euq&YcêkWWÕ¥¿sõRÊãQÝ<U5¿ 'qV¬m;³ÈT»rá'^Mª)g'*o
  ­&|`   ` )Î¯oµ«y8ú /uíüèþÈglëÝZqÛ¿$t«ÝÜìÚ}ù!þÑ(ÔP©}
  ³{ªÝÓVûÉ%ÉøÒ¶Ùøú½×
  ³òÀ×Eù2t5È¿æþ§6Ó¥4Ä¯Jó´²g¬»&Ò
  µö/iô}h¦åü÷Gi½mÕ8l°òjÚÎ»NÝIS
  ·ËHª¹¦ÿ å¢Uß
  ¿.gDÐº;ôÊb¢øFý+TåÕø+Q¤¡ÂËGÐ¨QIÉgêl¶´éRI,4ÜÖÉ2¬]LeðMWQc©km#ófRÆqRë:W¥£Û¬kvõùi:Íý)ê_ºN}î¡ñs·ËÛ^t¤ë½B*+XS§mæ[ãÐÃxiâMj}gZÛ¨õú[Êq§Nsx
  ¿Ü¹2:uÌ±¸øÉKtîiá|ËòVÅ6¿v@¬#QKÉ§     ä¢²À`cn5Z4¥Û×mb³9%¶y.&PióÆjÆ9ßú
  ÁqCD¯ÿ vQÓôº_ÉI. #JÁ      !(¶EÒo¨Cô°¹¶)[SÆ!ýË_MLïm>Þg%Îx3
  Ã5ªcúßWÐoèGhÏ9Q^íú=üa¥°:­ÿ ´©Óq·¥OÊR|åïWòå]ùá-ZeË[÷eÇ+êc®ì«C-÷cþ
  ÃûÊj©m_3Ï
  Å92ít%Ìj¸ô;?Lÿ ³òhêv²onÝÓþ¨ß(hZ}j&ñä@Þ¥?Áe¨x{¬J¥BÖ_/òkõU]>Þ2oüKþTË
  ÅaB+ì/Òú>¼n
  Å¥»XVÖÔ­!Ü÷kÔµÔ5zPNQú·Võµ
  Åº³XÍÕI9¶Ûä»£|ã¥]×r}Õv÷hÒµ
  Æ1tÆ6`zúUZTþ8û¥ÔVùÄb³îÏ+Zõþ
  Éï"êt_òËöï(h§
  ËeÅ§.L%n$a¼Ì?f¤Òú¥]2æQwôipÖKZúä"°¤Í+G/i¦þcK»×¥·W]¸rXÎë`7ºÚVw1×ZÃÜÑëëÕ¢²ÛÝdÅÜu+M÷6¶ÈÍÖ¥»Ã1·:wóõ4ªK÷MëñXyk­xßkR¿w,×V±)µGr­-BskæàegÙ,e'¹BT(Ë"ö%i7W÷5ø2­#R<îÀÓú¦(^QÏÅtc¼±QÕ
  Îqpñþe¹¢ê½^RKÝnÀçÒ"g5=êÕË¾¶å¤aªÓpM`
  ÏÉºtl:~Åj×ÑsxKcÔÙ«KÔ.ºv}m­gõZ²ÅãojØ÷]Íù¥CÉvöw}OÔzgNÙÆ£ÕxQ]¯l7»hú¤Ø[ézU¦gË{J0¡J>RKðºm6³×°À   
  Ð­QvOåÝÿ SÈ´ïÝ*ÜãSzè¤«owJ_K
  ÑM?¨´û¸Ã÷*UL k·Ò¶
  ÓôÝo¹Ô.®ï«'rÒM½Ûmûµõ_=QÔSëµKSm¾Å6àÔ§9I·)6ß¨7îDÛ"ß¸6E	±|)4òyi}*-wn²Yk;¸i:õµ'%$Ôp°üÍÏJêJUã
  ÔÆÑý¿SDÔn¾%G&ßÝGªT¼½û|
  Ö1ñ?©
  ÙRµ·ÎPDñJÔîÕ¶kss,þîÎØ¯»:V
  ßºi7¯¦ºçH±ë5ÍJ~hÐR³ë ôtµ+H,Ê¢E5¬X6ÿ ÇÆÍäóßSxý
  è3yt£ø"´ÕF<µ¨èúõÒs§mW8äÃôPÝAT)E5è{h¶I(ªQImÀÞfð¾qôÆ¯Â½]ÁJtç²ö+áF¡®êRÏìW£Ù·¼à§SC²îäËÃ;Øc48
  èß+Rx[Íõ¯ÃK7ýRÊúæO¦ð»V
  ìä×JÎ£EMÅ,ñbé¾§^ÚuëÆRRiyE?_rê¦Vâêµ*kºRQÛ|¼e>«¬uÄ5ª©¨ZK½4øÁì³xÑ´:[¥©Ká¨×¯å±Ñ@   ÂmêZßIÓ*ME%·»Kñk©iézuLÕK+
  ñ.;ªKÉË|aá×NÁ%|cØ!Ûè?=ß¡µÑ¯¾SþÞg¬!Ðx
  ñ©6õ:G]Ô¾¥ñ0ûù`lÀ  rKÍ	Î+ù $P¼¥R­'rI~$|~@j×VZ¢ô;Ó~E´¬e,|kY¬ÿ ¥ñên`ñæôj«ý×+;¯/RÚ¿LÑÊ¤Ù[ÝEO)oì[Õt·ÌyØqwÒ´°ñE?±ÔzJ³ÿ ÃÇÑ|¿Ôë¥¾Wõ1·R£ºãP8~µÒTc	5kÖËýNoÔ]'FrV9?cÔÔíjg¹EçmÑ¾Óté¦åJÏú@ò&¯Ò°e¿Ëôó5-SDøRiSkÏì{Téý"¼Zø·ÿ I¥k]
  óKø¦bï¯ªWï+È¶¹d ÁìF@ `) "0b> "6 dXØD[ú|`!66E, LÄ`&Ää@&!È É2 þ ÀBcd øn
  õ,VÑß%Å>
  ö,9Î_ó0&  ;ñ/
  öüÎKmÖt¦ññWä³ên¦£[G­ÖH8ã@Òºîÿ 
  ûbó÷=áÇSBÆpxßÒ>¯Ü÷^½:0î­k=gc§7ß%èËMFò¥ÝÆqÌ~³«,ç)ÊNâM%°¶¡âå¥¶~´jcß^ÔümS¥(CNq~±©ÿ ±Í.í"¿tÛÔÃÝÂ^Àl}Iâ
  ÿ ¤ô
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/particle_lightning.jpg
Copyright: î?øª¹æwR÷Þ÷bìÕþÆ7
  %60¼Ð-Ùï7Ù·»®ÈôÕ³TïÝÞôõ¢êÒ9ád%©´¥N²f[¤KÓí
  *ÚeÇÕ¤+à0ªVùPÂ
  9oæ
  FðÍ4Vì2LàØÇRMùLtåßK^ì@*å	èéU(V
  JZÎTq4ÑÀÃ$ Ð,¯vs¶Ùõf¸Z
  SóÅ@anÒç8Ô^ßdÕ$c!$âÓ¸úæÿ mõÝnaËèyÌÕáN_×KñmýÚmõLW²zkù[¦o÷¯ôÙ~»¨Éÿ g
  d< ÇnÑFÐªÎ¹µj¹7%æX}°BVÜÃ)$ÅpüðÃÓõi¡LòÝ[iÕWTÊ´Ú ÙãµDÅWÈ*ÊPäJ%O¬xÓ;ºÅXÿ îy.&2=ÛßMÔq$7J
  j	 % I4Å1ZÆdÐ¶;åíóÜgoº2Á®·AÞìMÌ´L,¤(^aa$}.%
   )MV7}aÇV´ððÆè
  p£hr2Rr2¬CQhr7VóßDcPOÒ£ºcHöÑG-T§î¨´6Ú¥ÊéB)õs3å¥x×x
  ¯5ù{kË»7Ac(wi¿nêAP+ôRaÕv¡fNç÷3EnMò
  ±×!JÎ@eiCîPåãMäCñk¬~.êêË by[wm²lpi{
  ÃBôfò­<é_Ã kçãóÀ0!
  ÉÊW;ÏÂ®?¿·J¦kÝ4x×Îµûkþx¹$½÷+ÿ ÔyÙæå§rùb·|U÷R©êdõÃÕóy¿5xøæÄÅ)§DQ:¾r:>w:£''6zÔRxøàvÚ{º!-Ý?srê' ÉÔs2åòËoÇ_m*©µKð}¦_CÖtù~¾W3&^>4áá÷b·|u©TõHó9æææf9óVµ¯×,Wbõ<ÑÒçæùrëåáÇÓõ!êUwQ_.nzxW
  ÏÏÍÌ­x×7^ÚSÛÑ%r_Ô#ÓùýE~>zÓË''m§ºBW»þèå§Ö}OÃ'WÎËJSÂÄñ~U3^éµRY
  Ö:;o«¿èk$M;§§OS-vTx6
  ò>1ó<¥Ë¼«Îð,¼øm/{
  ôÑeÜOku%|ðZÐ¾¦r9Fà)>
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/explosion_.jpg
Copyright: 9)üR¯t|½)<°óé}à,'§úff#R¹*ÇIScðõ|2àÍÒë¸ Ð2¿MRÒ
  RÛ¥£N§ÁökOÏ,­¡ÊWþ(
  XÈNy?5E-ú+
  ûÈ
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/unicron_beam_right.jpg
Copyright: 90Õ¹¾ÂèAâoÂE
  90Õ¹¾Òúðx§ðaq©90Õ¹¾ÂèAâoÂE
  90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾ÂèAâoÂE
  90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾ÂèAâoÂE
  90Õ¹¾Òúðx§ðaqÓ©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾Òúðx§ðaq©90Õ¹¾ÂèAâoÂE
  O%å?]
  ¥â®~Ø°·¾ôÅ³xï
  ¥â²~Ø°·¾ôÅÍc7TÒñY?lX[ßNzb¿³xï
  ¥â²~Ø°·¾ôÅÍã¼7TÒñY?lX[ßNzbÃf±Þºix«¶,-ï«=1a³xï
  ¥â²~Ø°·¾ôÅÍã¼7TÒñY?lX[ßNzbÃf±Þºix«¶,-ï«=1blÖ;ÁuM/söÅ
  ¥â²~Ø°·¾ôÅÍã¼7TÒñY?lX[ßNzbÃfñÞªix¬¶,-ï§=1a³xï
  ¥â²~Ø±-ï§=1blÖ;ÁuM/söÅ
  ¥ä¬¶,-ï§=1a³Xç
  ¥å«¶,-ï§=1a³xï
  ¥å¬¶,-ï§=1_ÀÙ¼wî^JÉûbÂÞú³Ó&Íã7tÒòÖOÛ÷Ó¯àlÞ9ÃwM/%dý±ao}9éþ
  ¥å¬¶,-ï§=1a³xïÝ4¼öÅ
  ¾Êúðx§ðaq©90Õ¹¾Òúðx§ðaq©91ung²¾¼)æü$XdêNLM[í/¯y¿	:
  Êó¾´LT¹q!ùüQI+öõ¯ð¦dµKTR¨|FS§§V'£5jo²¾¼tÏ5?àcÓw:Væû+ëÁâoÂE
  ý<Å¨y-¿EíüÅ¦?0brøCèOÖHYrÑ
License: UNKNOWN
 FIXME

Files: ./debian/patches/no_fsck_32bit_colors.patch
Copyright: 2009, Evgeni Golov <sargentd@die-welt.net>
License: UNKNOWN
 FIXME

Files: ./COPYRIGHT
Copyright: 2003-2006, Relish Games
  2007, Jarrad Woods
  2007, Scott Lembcke
  2007-2008, W.P. van Paassen
  PopCap Games Framework - Copyright (c) 2005-2008 PopCap Games.
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: 2003-2006, Relish Games
  2005, PopCap Games.
  2005-2008, PopCap Games.
  2007, Jarrad Woods
  2007, Scott Lembcke
  2007-2008, W.P. van Paassen
  PopCap Games Framework:
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/hero_.JPG
Copyright: V K<Ç`
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/music/music.mo3
Copyright: a	ÌnÈª©«ó,¬	b­(®r¯(°¢±²³³Eoû9´¯
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/unicron_beam_left.jpg
Copyright: é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãê¾¯þçMÿ ?©é4·}_ýÎþRÿ ÒkÊÒKÕý_ùýGþTíãÿÙÿí	`Photoshop 3.0 8BIM%                     8BIMí      H     H    8BIM&               ?  8BIM
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/bigcanvas_.jpg
Copyright: úr´qöª#Ï}JË§´1
  ôöØJ0,:AÜO'4ï§É3"ÇFÏ
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/smallShot_.JPG
Copyright: ª²³´µ¶·¸¹ºÂÃÄÅÆÇÈÉÊÒÓÔÕÖ×ØÙÚáâãäåæçèéêñòóôõö÷øùúÿÄ        
  ª²³´µ¶·¸¹ºÂÃÄÅÆÇÈÉÊÒÓÔÕÖ×ØÙÚâãäåæçèéêòóôõö÷øùúÿÚ 
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/heroDamage_.jpg
Copyright: µÔµ>ú|
  Ï±qÅ5¾Z%
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/dragonfly_.jpg
Copyright: ¶º¹*­ÄãÎXÈé¼òiz½EÏK]f¸Ã[ ¡¦2Là«w	`pö$`õ¯õUÕ<>ô­'öàuÃ
  Þíjf~ÐZi)¡TÛ<C3¹<}É|uu¿ôíyý¿½:jéÅ·RÈß%ãd#?,:Yõ1må_oÕ
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/unicron_baby.JPG
Copyright: ½bv=
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/images/spark.jpg
Copyright: ã×¡ä«~ä@Å¢éC¤ýx¯©j½[82À´C»ÙÖ%¯¯}««8&4íNS^Ö¦½$þ50,Ki(Ñ{ñ"
License: UNKNOWN
 FIXME

Files: ./tuxcap-build/sounds/metal_loop.wav
Copyright: ÿ¥ÿ8üViû=¿ýø
  ÿªûÊÿ.ý¶ ý[ÿÌúæÿ0þN Ïÿ; éTÒ Ã W ýjþlûTþû£ý±üþIü®ý>þÐýì©þÚþéX ² 
License: UNKNOWN
 FIXME

Files: ./tuxcap/hgeparticle/LICENSE.TXT
Copyright: 2003-2006, Relish Games
License: Zlib
 FIXME

