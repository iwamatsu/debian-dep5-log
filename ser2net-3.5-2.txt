Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./INSTALL
 ./Makefile.am
 ./NEWS
 ./README
 ./configure.ac
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/control.in
 ./debian/default
 ./debian/dirs
 ./debian/docs
 ./debian/examples
 ./debian/init.d
 ./debian/install
 ./debian/postinst
 ./debian/postrm
 ./debian/prerm
 ./debian/rules
 ./debian/ser2net.conf
 ./debian/source/format
 ./debian/watch
 ./m4/ax_config_feature.m4
 ./ser2net.8
 ./ser2net.conf
 ./ser2net.init
 ./ser2net.spec
 ./ser2net.spec.in
 ./telnet.c
 ./telnet.h
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./buffer.c
 ./buffer.h
 ./controller.c
 ./controller.h
 ./dataxfer.c
 ./dataxfer.h
 ./devcfg.c
 ./devio.h
 ./locking.h
 ./readconfig.c
 ./readconfig.h
 ./ser2net.c
 ./ser2net.h
 ./sol.c
 ./utils.c
 ./utils.h
Copyright: 2001, Corey Minyard <minyard@acm.org>
License: GPL-2+
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./aclocal.m4
 ./m4/ltsugar.m4
 ./m4/ltversion.m4
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2014, Free Software Foundation, Inc.
  1996-2014, Free Software Foundation, Inc.
  2004, 2011-2015, Free Software Foundation, Inc.
  2004-2005, 2007-2008, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./compile
 ./depcomp
 ./ltmain.sh
 ./missing
Copyright: 1996-2014, Free Software Foundation, Inc.
  1996-2015, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./heap.h
 ./selector.c
 ./selector.h
Copyright: 2002, MontaVista Software Inc.
  2002-2003, MontaVista Software Inc.
License: BSD~unspecified and/or LGPL-2+
 FIXME

Files: ./led.c
 ./led.h
 ./led_sysfs.h
Copyright: 2001-2016, Corey Minyard <minyard@acm.org>
  2016, Michael Heimpold <mhei@heimpold.de>
License: GPL-2+
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2015, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./m4/ltoptions.m4
 ./m4/lt~obsolete.m4
Copyright: 2004-2005, 2007, 2009, 2011-2015, Free Software
  2004-2005, 2007-2009, 2011-2015, Free Software
License: UNKNOWN
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./m4/ax_have_epoll.m4
Copyright: 2008, Peter Simons <simons@cryp.to>
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: GPL
 FIXME

Files: ./debian/copyright
Copyright: 2001, Marc Haber, and also
  GNU General Public License, Version 2
License: GPL-2
 FIXME

Files: ./led_sysfs.c
Copyright: 2001-2016, Corey Minyard <minyard@acm.org>
  2015, I2SE GmbH <info@i2se.com>
  2016, Michael Heimpold <mhei@heimpold.de>
License: GPL-2+
 FIXME

