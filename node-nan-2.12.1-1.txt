Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./examples/async_pi_estimate/addon.cc
 ./examples/async_pi_estimate/addon.js
 ./examples/async_pi_estimate/async.cc
 ./examples/async_pi_estimate/async.h
 ./examples/async_pi_estimate/pi_est.cc
 ./examples/async_pi_estimate/pi_est.h
 ./examples/async_pi_estimate/sync.cc
 ./examples/async_pi_estimate/sync.h
 ./nan_callbacks.h
 ./nan_callbacks_12_inl.h
 ./nan_callbacks_pre_12_inl.h
 ./nan_converters.h
 ./nan_converters_43_inl.h
 ./nan_converters_pre_43_inl.h
 ./nan_define_own_property_helper.h
 ./nan_implementation_12_inl.h
 ./nan_implementation_pre_12_inl.h
 ./nan_json.h
 ./nan_maybe_43_inl.h
 ./nan_maybe_pre_43_inl.h
 ./nan_new.h
 ./nan_object_wrap.h
 ./nan_persistent_12_inl.h
 ./nan_persistent_pre_12_inl.h
 ./nan_private.h
 ./nan_typedarray_contents.h
 ./nan_weak.h
 ./test/cpp/accessors.cpp
 ./test/cpp/accessors2.cpp
 ./test/cpp/asyncprogressqueueworker.cpp
 ./test/cpp/asyncprogressqueueworkerstream.cpp
 ./test/cpp/asyncprogressworker.cpp
 ./test/cpp/asyncprogressworkersignal.cpp
 ./test/cpp/asyncprogressworkerstream.cpp
 ./test/cpp/asyncresource.cpp
 ./test/cpp/asyncworker.cpp
 ./test/cpp/asyncworkererror.cpp
 ./test/cpp/buffer.cpp
 ./test/cpp/bufferworkerpersistent.cpp
 ./test/cpp/callbackcontext.cpp
 ./test/cpp/converters.cpp
 ./test/cpp/error.cpp
 ./test/cpp/gc.cpp
 ./test/cpp/indexedinterceptors.cpp
 ./test/cpp/isolatedata.cpp
 ./test/cpp/json-parse.cpp
 ./test/cpp/json-stringify.cpp
 ./test/cpp/makecallback.cpp
 ./test/cpp/morenews.cpp
 ./test/cpp/multifile1.cpp
 ./test/cpp/multifile2.cpp
 ./test/cpp/multifile2.h
 ./test/cpp/namedinterceptors.cpp
 ./test/cpp/nancallback.cpp
 ./test/cpp/nannew.cpp
 ./test/cpp/news.cpp
 ./test/cpp/objectwraphandle.cpp
 ./test/cpp/persistent.cpp
 ./test/cpp/private.cpp
 ./test/cpp/returnemptystring.cpp
 ./test/cpp/returnnull.cpp
 ./test/cpp/returnundefined.cpp
 ./test/cpp/returnvalue.cpp
 ./test/cpp/setcallhandler.cpp
 ./test/cpp/settemplate.cpp
 ./test/cpp/sleep.h
 ./test/cpp/strings.cpp
 ./test/cpp/symbols.cpp
 ./test/cpp/threadlocal.cpp
 ./test/cpp/trycatch.cpp
 ./test/cpp/typedarrays.cpp
 ./test/cpp/weak.cpp
 ./test/cpp/weak2.cpp
 ./test/cpp/wrappedobjectfactory.cpp
 ./test/js/accessors-test.js
 ./test/js/accessors2-test.js
 ./test/js/asyncprogressqueueworker-test.js
 ./test/js/asyncprogressqueueworkerstream-test.js
 ./test/js/asyncprogressworker-test.js
 ./test/js/asyncprogressworkersignal-test.js
 ./test/js/asyncprogressworkerstream-test.js
 ./test/js/asyncresource-test.js
 ./test/js/asyncworker-test.js
 ./test/js/asyncworkererror-test.js
 ./test/js/buffer-test.js
 ./test/js/bufferworkerpersistent-test.js
 ./test/js/callbackcontext-test.js
 ./test/js/converters-test.js
 ./test/js/error-test.js
 ./test/js/gc-test.js
 ./test/js/indexedinterceptors-test.js
 ./test/js/isolatedata-test.js
 ./test/js/json-parse-test.js
 ./test/js/json-stringify-test.js
 ./test/js/makecallback-test.js
 ./test/js/morenews-test.js
 ./test/js/multifile-test.js
 ./test/js/namedinterceptors-test.js
 ./test/js/nancallback-test.js
 ./test/js/nannew-test.js
 ./test/js/news-test.js
 ./test/js/objectwraphandle-test.js
 ./test/js/persistent-test.js
 ./test/js/private-test.js
 ./test/js/returnemptystring-test.js
 ./test/js/returnnull-test.js
 ./test/js/returnundefined-test.js
 ./test/js/returnvalue-test.js
 ./test/js/setcallhandler-test.js
 ./test/js/settemplate-test.js
 ./test/js/strings-test.js
 ./test/js/symbols-test.js
 ./test/js/threadlocal-test.js
 ./test/js/trycatch-test.js
 ./test/js/typedarrays-test.js
 ./test/js/weak-test.js
 ./test/js/weak2-test.js
 ./test/js/wrappedobjectfactory-test.js
 ./tools/1to2.js
Copyright: 2018, NAN contributors
License: UNKNOWN
 FIXME

Files: ./.dntrc
 ./.npmignore
 ./.travis.yml
 ./CHANGELOG.md
 ./Makefile
 ./README.md
 ./appveyor.yml
 ./debian/README.Debian
 ./debian/compat
 ./debian/docs
 ./debian/examples
 ./debian/install
 ./debian/links
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/tests/control
 ./debian/tests/require
 ./debian/tests/runtestsuite
 ./debian/watch
 ./doc/.build.sh
 ./doc/asyncworker.md
 ./doc/buffers.md
 ./doc/callback.md
 ./doc/converters.md
 ./doc/errors.md
 ./doc/json.md
 ./doc/maybe_types.md
 ./doc/methods.md
 ./doc/new.md
 ./doc/node_misc.md
 ./doc/object_wrappers.md
 ./doc/persistent.md
 ./doc/scopes.md
 ./doc/script.md
 ./doc/string_bytes.md
 ./doc/v8_internals.md
 ./doc/v8_misc.md
 ./examples/async_pi_estimate/README.md
 ./examples/async_pi_estimate/binding.gyp
 ./examples/async_pi_estimate/package.json
 ./include_dirs.js
 ./package.json
 ./test/.jshintrc
 ./test/binding.gyp
 ./test/tap-as-worker.js
 ./tools/README.md
 ./tools/package.json
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/control
 ./debian/patches/fix_returnvalue_test.patch
Copyright: rÃ©my Lal <kapouer@melix.org>
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: BSD-3-clause
 FIXME

Files: ./cpplint.py
Copyright: 2009, Google Inc.
License: BSD-3-clause
 FIXME

Files: ./nan_string_bytes.h
Copyright: Joyent, Inc. and other Node contributors.
License: Expat
 FIXME

Files: ./LICENSE.md
Copyright: 2018, NAN contributors
License: Expat
 FIXME

Files: ./nan.h
Copyright: 2018, NAN contributors:
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: rÃ©my Lal <kapouer@melix.org> Wed, 02 Jan 2019 15:59:33 +0100
License: UNKNOWN
 FIXME

