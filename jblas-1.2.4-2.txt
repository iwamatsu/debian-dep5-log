Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./Makefile
 ./config/config.rb
 ./config/config_cc.rb
 ./config/config_fortran.rb
 ./config/config_java.rb
 ./config/config_lapack_sources.rb
 ./config/config_libs.rb
 ./config/config_make.rb
 ./config/config_os_arch.rb
 ./config/config_tools.rb
 ./config/configure.rb
 ./config/lib_helpers.rb
 ./config/opts.rb
 ./config/path.rb
 ./config/string_ext.rb
 ./config/windows.rb
 ./scripts/add-license.rb
 ./scripts/c-file.c
 ./scripts/c-header.h
 ./scripts/class_to_float.rb
 ./scripts/fortran/java.rb
 ./scripts/fortran/parser.rb
 ./scripts/fortran/types.rb
 ./scripts/fortranwrapper.rb
 ./scripts/java-class.java
 ./scripts/java-impl.c
 ./scripts/render-textile.rb
 ./scripts/rjpp.rb
 ./scripts/static_class_to_float.rb
 ./scripts/template_context.rb
 ./src/main/c/NativeBlas.c
 ./src/main/java/org/jblas/ComplexDouble.java
 ./src/main/java/org/jblas/ComplexDoubleMatrix.java
 ./src/main/java/org/jblas/ComplexFloat.java
 ./src/main/java/org/jblas/ComplexFloatMatrix.java
 ./src/main/java/org/jblas/DoubleFunction.java
 ./src/main/java/org/jblas/FloatFunction.java
 ./src/main/java/org/jblas/Geometry.java
 ./src/main/java/org/jblas/JavaBlas.java
 ./src/main/java/org/jblas/MatrixFunctions.java
 ./src/main/java/org/jblas/NativeBlas.java
 ./src/main/java/org/jblas/SimpleBlas.java
 ./src/main/java/org/jblas/Solve.java
 ./src/main/java/org/jblas/benchmark/Benchmark.java
 ./src/main/java/org/jblas/benchmark/BenchmarkResult.java
 ./src/main/java/org/jblas/benchmark/JavaDoubleMultiplicationBenchmark.java
 ./src/main/java/org/jblas/benchmark/JavaFloatMultiplicationBenchmark.java
 ./src/main/java/org/jblas/benchmark/Main.java
 ./src/main/java/org/jblas/benchmark/NativeDoubleMultiplicationBenchmark.java
 ./src/main/java/org/jblas/benchmark/NativeFloatMultiplicationBenchmark.java
 ./src/main/java/org/jblas/benchmark/Timer.java
 ./src/main/java/org/jblas/benchmark/package-info.java
 ./src/main/java/org/jblas/exceptions/LapackArgumentException.java
 ./src/main/java/org/jblas/exceptions/LapackConvergenceException.java
 ./src/main/java/org/jblas/exceptions/LapackException.java
 ./src/main/java/org/jblas/exceptions/LapackSingularityException.java
 ./src/main/java/org/jblas/exceptions/NoEigenResultException.java
 ./src/main/java/org/jblas/exceptions/SizeException.java
 ./src/main/java/org/jblas/exceptions/package-info.java
 ./src/main/java/org/jblas/package-info.java
 ./src/main/java/org/jblas/ranges/AllRange.java
 ./src/main/java/org/jblas/ranges/IndicesRange.java
 ./src/main/java/org/jblas/ranges/IntervalRange.java
 ./src/main/java/org/jblas/ranges/PointRange.java
 ./src/main/java/org/jblas/ranges/Range.java
 ./src/main/java/org/jblas/ranges/RangeUtils.java
 ./src/main/java/org/jblas/ranges/package-info.java
 ./src/main/java/org/jblas/util/ArchFlavor.java
 ./src/main/java/org/jblas/util/Functions.java
 ./src/main/java/org/jblas/util/LibraryLoader.java
 ./src/main/java/org/jblas/util/Logger.java
 ./src/main/java/org/jblas/util/Permutations.java
 ./src/main/java/org/jblas/util/SanityChecks.java
 ./src/main/java/org/jblas/util/package-info.java
 ./src/test/java/org/jblas/AbstractTestJblas.java
 ./src/test/java/org/jblas/ComplexDoubleMatrixTest.java
 ./src/test/java/org/jblas/JblasAssert.java
 ./src/test/java/org/jblas/SimpleBlasTest.java
 ./src/test/java/org/jblas/TestBlasDouble.java
 ./src/test/java/org/jblas/TestBlasDoubleComplex.java
 ./src/test/java/org/jblas/TestBlasFloat.java
 ./src/test/java/org/jblas/TestComplexFloat.java
 ./src/test/java/org/jblas/TestDoubleMatrix.java
 ./src/test/java/org/jblas/TestEigen.java
 ./src/test/java/org/jblas/TestFloatMatrix.java
 ./src/test/java/org/jblas/TestGeometry.java
 ./src/test/java/org/jblas/TestSolve.java
Copyright: 2009, Mikio L. Braun
  2009-2011, Mikio L. Braun
  2009-2013, Mikio L. Braun
  2012, Mikio L. Braun
License: BSD-3-clause
 FIXME

Files: ./.travis.yml
 ./AUTHORS
 ./BUGS
 ./BUILDING_ATLAS
 ./INSTALL
 ./README.md
 ./RELEASE_NOTES
 ./ROADMAP
 ./config/PrintProperty.java
 ./config/arch_flavor.c
 ./configure
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/jblas-doc.doc-base
 ./debian/jblas-doc.docs
 ./debian/jblas.docs
 ./debian/patches/debug-symbols.patch
 ./debian/patches/java10.patch
 ./debian/patches/java9.patch
 ./debian/patches/javadoc.patch
 ./debian/patches/series
 ./debian/patches/version-property.patch
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./docs/jblas-logo.svg
 ./docs/render.rb
 ./docs/tutorial.textile
 ./examples/complex.rb
 ./examples/complex_svd.rb
 ./examples/jmatrix.rb
 ./examples/test_abitofml.rb
 ./examples/test_matrix.rb
 ./examples/test_time_eigen.rb
 ./examples/tictoc.rb
 ./fortranwrapper.dump
 ./pom.xml
 ./scripts/test_fortranwrapper
 ./src/main/c/jblas_arch_flavor.c
 ./src/main/c/org_jblas_NativeBlas.h
 ./src/main/c/org_jblas_util_ArchFlavor.h
 ./src/main/java/org/jblas/ConvertsToDoubleMatrix.java
 ./src/main/java/org/jblas/ConvertsToFloatMatrix.java
 ./src/main/java/org/jblas/Decompose.java
 ./src/main/java/org/jblas/Info.java
 ./src/main/java/org/jblas/NativeBlasLibraryLoader.java
 ./src/main/java/org/jblas/Singular.java
 ./src/main/java/org/jblas/exceptions/LapackPositivityException.java
 ./src/main/java/org/jblas/exceptions/UnsupportedArchitectureException.java
 ./src/main/java/org/jblas/util/Random.java
 ./src/test/java/org/jblas/TestDecompose.java
 ./src/test/java/org/jblas/TestSingular.java
 ./src/test/java/org/jblas/ranges/RangeTest.java
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/main/java/org/jblas/DoubleMatrix.java
 ./src/main/java/org/jblas/FloatMatrix.java
Copyright: 2008, Johannes Schaback
  2009, Jan Saputra MÃ¼ller
  2009, Mikio L. Braun
License: BSD-3-clause
 FIXME

Files: ./COPYING
 ./debian/copyright
Copyright: 2009, Mikio L. Braun and contributors
License: BSD-3-clause
 FIXME

Files: ./src/main/java/org/jblas/Eigen.java
Copyright: 2009, -13, Mikio L. Braun
License: BSD-3-clause
 FIXME

Files: ./build.xml
Copyright: 2008, Johannes Schaback
  2009, Mikio L. Braun
License: BSD-3-clause
 FIXME

Files: ./src/main/java/org/jblas/Trigonometry.java
Copyright: 2009, Mikio L. Braun
License: BSD-3-clause and/or LGPL-2.1+
 FIXME

Files: ./docs/jblas-logo.png
Copyright: êBÂâ`ùuD
  UäBJ«ð;ÂõðÙ}+°xL){»ß÷C`2nf`B^ØÜ,Ã|I¶`¼Zø]eüÏx!Nnq
  fÀ>dLe¥XìÇüPÊ^IÿuªéµÁD.ñ ã¸£	JÙôÓI)[+eß{òAO`rÂy`8î¦ß§Ó×´ïcA.mø¥ì×¨p¬÷Òò·â-Ôßcâú`®Rv¤ýg$h(¥èi<HÜ ÿçg¥ì½£?>ù7¶üÈ#Ï{Àçe:ÆÞ÷L)Þû+oëÒY)û]O/æ4÷ú¦À¥mY-}nñì¶>úÇ?ÅtrkÃ]ÀÞjÆNHw$G
  n§3GÜG®èíÈ´¤'yâ×LÝÑÜI1ñ<§BÍVÏïBd£Icª=Îh¢,!Ä"`7
  V)µ7A²«¤¢;ÊÆÔÖz)Î)ÄØßÂÂ¼ýªc%fv/·¹»ìÌýýºì;òªÖWÇÀd¥ì÷k¼Ò
  ²÷ã¬*0ÑÃ%G2`Õ?Ô´ÆÏ »¥´¾ÒÊÒúÖqA
  À Ì
  Èl,wjxëÂ
License: UNKNOWN
 FIXME

Files: ./docs/jblas-logo-64.png
Copyright: <ÎÞ
  V¯æÝâmuqÌ^ö@NÛRÈL9´sÖi_ïÂ`ÇâÏáÜx5®Ä;ëéÌ	¦~kððÐ¡ñõÀÎ%kýC'ÞÐÑvç×(í;Äeå
  hÌ&**VèBmPât2¶Ö¾HÛP1¢?ÔV:¶Óg_j)VNÔ2MH¬bE22°Hd É>÷ôçîîóì[v³Ïn`ºgæÎs÷ÞûûÝ{¾¿ïy¹çeAdAþ?Kèèë¬%ÎÆãÄöÁÁoÏÛý;æ[áîÞÕ
  Ü/½ÇQ.&X«wh4
  Äpïàà)¯¬À¸·a69Æòrù¹ÍjQ|ã4wâÁgÝ3­_ÇgÓêJÞßd2s	ÞÖ¤to¥±åâÃøétÂøámsÇ±:ÌÀbb°fcLßÁ3ÇðS±
  ¶[+Ý£l8q[Òñ-e¼UoJùóâLgªeÆ2~.ó2¤§È-çìzDÇê+p..©¨2zpPxkT÷:ºsºÀ¸¯©3Kýomêýjuilôõ¶bõh]oO/aGÛl¿¹ùÄ]îl?¦³ÒýcØ}é¨Ì&Íòé6|º
  »ÕüôZáOÑ«æÀsºPº|*%,Ý7ù~ÂbLiÿd?Ia»E¾[|M§½Â_5)pïkJ½Â
  Æ
License: UNKNOWN
 FIXME

Files: ./docs/jblas-thumb.png
Copyright: <ÖeØy~8|6{þ`è×ÝNO½Ø[§³iþ$¤õÊ©jyê53_lJÛ+¿ú
  W`OöN9uuâçü=¶
  jÕyÁMM½·äæT9GRÂóN6¤ª^ÌÁ,ÔpT°^r`ÙÈóuZ×³é;m£]òáoàÍ&Ø%i
License: UNKNOWN
 FIXME

Files: ./docs/jblas-logo-square.png
Copyright: Sg¹SUE0§'Us>* íB%6Ã¥*¢~ü"ÚöüfÀÇFé×rqêéUÑ¨Ä?
  Ý8#CESÚìü_t0¢ Ïy¥äÞMx?sÄrA³Í¾Íøe$¤Tãuî­À8,sýÜfk
  Æ9.1Æ
License: UNKNOWN
 FIXME

