Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./CHANGES
 ./Contributors
 ./README
 ./WISHES
 ./addons/nextispell/Makefile
 ./addons/nextispell/README
 ./addons/nextispell/configure
 ./addons/nextispell/configure.h.template
 ./addons/nextispell/configureTeX
 ./addons/nextispell/nextispell.m
 ./addons/nextispell/services.template
 ./addons/xspell.shar
 ./debian/NEWS
 ./debian/README.source
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/ispell.lintian-overrides
 ./debian/local/Makefile.languages.inc
 ./debian/local/defmt-c.1
 ./debian/packages.d/dictionaries.in
 ./debian/packages.d/gen_debhelper_files.pl
 ./debian/packages.d/ienglish-common.in
 ./debian/packages.d/ispell.in
 ./debian/patches/0001-Configuration.patch
 ./debian/patches/0003-Fix-FTBFS-on-Hurd.patch
 ./debian/patches/0006-Fix-sq-and-unsq.patch
 ./debian/patches/0007-Use-termios.patch
 ./debian/patches/0008-Tex-backslash.patch
 ./debian/patches/0009-Fix-FTBFS-on-glibc.patch
 ./debian/patches/0010-Debian-control-file.patch
 ./debian/patches/0011-Missing-prototypes.patch
 ./debian/patches/0013-Fix-man-pages.patch
 ./debian/patches/0015-CFLAGS-from-environment.patch
 ./debian/patches/0018-Dont-strip-binaries.patch
 ./debian/patches/0020-Mark-Rcs_Id-as-unused.patch
 ./debian/patches/0021-Fix-gcc-warnings.patch
 ./debian/patches/0022-Ispell-man-page.patch
 ./debian/patches/0023-Exclusive-options.patch
 ./debian/patches/0024-Check-tempdir-creation.patch
 ./debian/patches/0026-POSIX-sort.patch
 ./debian/patches/0027-Include-Debian-Makefile.languages.inc.patch
 ./debian/patches/0028-Fix-hardening-warnings.patch
 ./debian/patches/0029-Generate-hex-in-fix8bit.patch
 ./debian/patches/0030-Display-whole-multibyte-character.patch
 ./debian/patches/0031-Initialize-table.patch
 ./debian/patches/0032-Check-munchlist-errors.patch
 ./debian/patches/0033-Fix-NULL-pointer-dereference.patch
 ./debian/patches/0034-Fix-munchlist-failure.patch
 ./debian/patches/0035-Force-text-grep-in-munchlist.patch
 ./debian/patches/0036-Reproducible-hashes.patch
 ./debian/patches/0037-CC-from-environment.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/source/options
 ./debian/watch
 ./deformatters/README
 ./exp_table.c
 ./exp_table.h
 ./fields.3
 ./fields.c
 ./fields.h
 ./iwhich
 ./languages/Where
 ./languages/english/altamer.0
 ./languages/english/altamer.1
 ./languages/english/altamer.2
 ./languages/english/american.0
 ./languages/english/american.1
 ./languages/english/american.2
 ./languages/english/british.0
 ./languages/english/british.1
 ./languages/english/british.2
 ./languages/english/english.0
 ./languages/english/english.1
 ./languages/english/english.2
 ./languages/english/english.3
 ./pc/README
 ./pc/cfglang.sed
 ./pc/cfgmain.sed
 ./pc/configdj.bat
 ./pc/local.djgpp
 ./pc/local.emx
 ./pc/make-dj.bat
 ./pc/makeemx.bat
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./Magiclines
 ./Makefile
 ./Makekit
 ./Makepatch
 ./buildhash.c
 ./config.X
 ./deformatters/Makefile
 ./deformatters/defmt-c.c
 ./deformatters/defmt-sh.c
 ./dump.c
 ./findaffix.X
 ./good.c
 ./hash.c
 ./icombine.c
 ./ijoin.c
 ./ispell.1X
 ./ispell.5X
 ./ispell.h
 ./languages/Makefile
 ./languages/altamer/Makefile
 ./languages/american/Makefile
 ./languages/british/Makefile
 ./languages/dansk/Makefile
 ./languages/deutsch/Makefile
 ./languages/english/Makefile
 ./languages/english/english.aff
 ./languages/english/msgs.h
 ./languages/espanol/Makefile
 ./languages/fix8bit.c
 ./languages/francais/Makefile
 ./languages/nederlands/Makefile
 ./languages/norsk/Makefile
 ./languages/portugues/Makefile
 ./languages/svenska/Makefile
 ./local.h.bsd
 ./local.h.cygwin
 ./local.h.generic
 ./local.h.linux
 ./local.h.macos
 ./local.h.solaris
 ./parse.y
 ./proto.h
 ./sq.1
 ./sq.c
 ./subset.X
 ./unsq.c
 ./zapdups.X
Copyright: 1992-1993, 1999, 2001, 2005, Geoff Kuenning, Claremont, CA
  1992-1993, 1999, 2001, Geoff Kuenning, Claremont, CA
  1992-1993, 1999-2001, 2005, Geoff Kuenning, Claremont, CA
  1992-1993, 2001, 2005, Geoff Kuenning, Claremont, CA
  1993, 1999, 2001, 2005, Geoff Kuenning, Claremont, CA
  1993, 1999, 2001, Geoff Kuenning, Claremont, CA
  1993, 1999, 2001-2002, Geoff Kuenning, Claremont, CA
  1993, 2001, Geoff Kuenning, Claremont, CA
  1994, 2001, 2015, Geoff Kuenning, Claremont, CA
  1995, 2001, Geoff Kuenning, Claremont, CA
  2001, Geoff Kuenning, Claremont, CA
  2005, Geoff Kuenning, Claremont, CA
License: BSD-3-clause
 FIXME

Files: ./lookup.c
 ./makedent.c
 ./makedict.sh
 ./munchlist.X
 ./term.c
 ./tgood.c
 ./tree.c
 ./tryaffix.X
 ./xgets.c
Copyright: 1987-1989, 1992-1993, 1999, 2001, 2005, Geoff Kuenning,
  1987-1989, 1992-1993, 1999, 2001, Geoff Kuenning,
  1988-1989, 1992-1993, 1999, 2001, 2005, Geoff Kuenning,
License: BSD-3-clause
 FIXME

Files: ./correct.c
 ./defmt.c
 ./ispell.c
Copyright: 1983, Pace Willisson
  1992-1993, 1999, 2001, 2005, Geoff Kuenning, Claremont, CA
  1992-1993, 1999, 2001, Geoff Kuenning, Claremont, CA
License: BSD-3-clause
 FIXME

Files: ./debian/copyright
Copyright: (C) 1983 Pace Willisson
  -format/1.0/
  1987-1988, 1990-1995, 1999, Geoff Kuenning, Claremont, CA.
  2001-2002, 2005, 2015, Geoff Kuenning, Claremont, CA.
License: BSD-3-clause
 FIXME

Files: ./pc/djterm.c
Copyright: 1996, Geoff Kuenning, Granada Hills, CA
License: BSD-3-clause
 FIXME

Files: ./version.h
Copyright: 1983, Pace Willisson",
  1987-1988, 1990-1995, 1999
License: UNKNOWN
 FIXME

Files: ./debian/addons/strix.py
Copyright: 2000, Drew Parsons <dfparsons@ucdavis.edu>
License: UNKNOWN
 FIXME

