Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.gitreview
 ./.zuul.yaml
 ./CONTRIBUTING.rst
 ./MANIFEST.in
 ./README.rst
 ./debian/compat
 ./debian/control
 ./debian/docs
 ./debian/patches/install-missing-files.patch
 ./debian/patches/series
 ./debian/python3-mistral-dashboard.install
 ./debian/rules
 ./debian/source/format
 ./debian/source/options
 ./debian/watch
 ./doc/source/contributing.rst
 ./doc/source/index.rst
 ./doc/source/readme.rst
 ./mistraldashboard/action_executions/templates/action_executions/_update.html
 ./mistraldashboard/action_executions/templates/action_executions/detail.html
 ./mistraldashboard/action_executions/templates/action_executions/filtered.html
 ./mistraldashboard/action_executions/templates/action_executions/index.html
 ./mistraldashboard/action_executions/templates/action_executions/update.html
 ./mistraldashboard/actions/templates/actions/_create.html
 ./mistraldashboard/actions/templates/actions/_run.html
 ./mistraldashboard/actions/templates/actions/_update.html
 ./mistraldashboard/actions/templates/actions/create.html
 ./mistraldashboard/actions/templates/actions/detail.html
 ./mistraldashboard/actions/templates/actions/index.html
 ./mistraldashboard/actions/templates/actions/run.html
 ./mistraldashboard/actions/templates/actions/update.html
 ./mistraldashboard/cron_triggers/templates/cron_triggers/_create.html
 ./mistraldashboard/cron_triggers/templates/cron_triggers/create.html
 ./mistraldashboard/cron_triggers/templates/cron_triggers/detail.html
 ./mistraldashboard/cron_triggers/templates/cron_triggers/index.html
 ./mistraldashboard/default/templates/default/_booleanfield.html
 ./mistraldashboard/default/templates/default/_code.html
 ./mistraldashboard/default/templates/default/_humantime.html
 ./mistraldashboard/default/templates/default/_label.html
 ./mistraldashboard/default/templates/default/_preprint.html
 ./mistraldashboard/default/templates/default/_prettyprint.html
 ./mistraldashboard/default/templates/default/base.html
 ./mistraldashboard/default/templates/default/table.html
 ./mistraldashboard/executions/templates/executions/_update_description.html
 ./mistraldashboard/executions/templates/executions/detail.html
 ./mistraldashboard/executions/templates/executions/index.html
 ./mistraldashboard/executions/templates/executions/index_filtered_task.html
 ./mistraldashboard/executions/templates/executions/update_description.html
 ./mistraldashboard/static/mistraldashboard/css/style.css
 ./mistraldashboard/tasks/templates/tasks/detail.html
 ./mistraldashboard/tasks/templates/tasks/filtered.html
 ./mistraldashboard/tasks/templates/tasks/index.html
 ./mistraldashboard/workbooks/templates/workbooks/_create.html
 ./mistraldashboard/workbooks/templates/workbooks/_select_definition.html
 ./mistraldashboard/workbooks/templates/workbooks/_update.html
 ./mistraldashboard/workbooks/templates/workbooks/create.html
 ./mistraldashboard/workbooks/templates/workbooks/detail.html
 ./mistraldashboard/workbooks/templates/workbooks/index.html
 ./mistraldashboard/workbooks/templates/workbooks/select_definition.html
 ./mistraldashboard/workbooks/templates/workbooks/update.html
 ./mistraldashboard/workflows/templates/workflows/_create.html
 ./mistraldashboard/workflows/templates/workflows/_execute.html
 ./mistraldashboard/workflows/templates/workflows/_select_definition.html
 ./mistraldashboard/workflows/templates/workflows/_update.html
 ./mistraldashboard/workflows/templates/workflows/create.html
 ./mistraldashboard/workflows/templates/workflows/detail.html
 ./mistraldashboard/workflows/templates/workflows/execute.html
 ./mistraldashboard/workflows/templates/workflows/index.html
 ./mistraldashboard/workflows/templates/workflows/select_definition.html
 ./mistraldashboard/workflows/templates/workflows/update.html
 ./releasenotes/source/index.rst
 ./releasenotes/source/liberty.rst
 ./releasenotes/source/mitaka.rst
 ./releasenotes/source/newton.rst
 ./releasenotes/source/ocata.rst
 ./releasenotes/source/pike.rst
 ./releasenotes/source/queens.rst
 ./releasenotes/source/unreleased.rst
 ./requirements.txt
 ./run_tests.sh
 ./test-requirements.txt
 ./tox.ini
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./mistraldashboard/api.py
 ./mistraldashboard/dashboard.py
 ./mistraldashboard/default/panel.py
 ./mistraldashboard/default/utils.py
 ./mistraldashboard/executions/panel.py
 ./mistraldashboard/executions/tables.py
 ./mistraldashboard/executions/urls.py
 ./mistraldashboard/executions/views.py
 ./mistraldashboard/tasks/panel.py
 ./mistraldashboard/tasks/tables.py
 ./mistraldashboard/tasks/urls.py
 ./mistraldashboard/tasks/views.py
 ./mistraldashboard/workbooks/panel.py
 ./mistraldashboard/workbooks/tables.py
 ./mistraldashboard/workbooks/urls.py
 ./mistraldashboard/workbooks/views.py
 ./mistraldashboard/workflows/forms.py
 ./mistraldashboard/workflows/panel.py
 ./mistraldashboard/workflows/tables.py
 ./mistraldashboard/workflows/urls.py
 ./mistraldashboard/workflows/views.py
Copyright: 2014, - StackStorm, Inc.
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/action_executions/tests.py
 ./mistraldashboard/actions/forms.py
 ./mistraldashboard/actions/panel.py
 ./mistraldashboard/actions/tables.py
 ./mistraldashboard/actions/tests.py
 ./mistraldashboard/actions/urls.py
 ./mistraldashboard/actions/views.py
 ./mistraldashboard/cron_triggers/tests.py
 ./mistraldashboard/exceptions.py
 ./mistraldashboard/executions/tests.py
 ./mistraldashboard/handle_errors.py
 ./mistraldashboard/tasks/tests.py
 ./mistraldashboard/test/helpers.py
 ./mistraldashboard/test/test_data/mistral_data.py
 ./mistraldashboard/test/test_data/utils.py
 ./mistraldashboard/workbooks/forms.py
 ./mistraldashboard/workbooks/tests.py
 ./mistraldashboard/workflows/tests.py
Copyright: 2015, Huawei Technologies Co., Ltd.
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/action_executions/forms.py
 ./mistraldashboard/action_executions/panel.py
 ./mistraldashboard/action_executions/tables.py
 ./mistraldashboard/action_executions/urls.py
 ./mistraldashboard/action_executions/views.py
 ./mistraldashboard/cron_triggers/forms.py
 ./mistraldashboard/cron_triggers/panel.py
 ./mistraldashboard/cron_triggers/tables.py
 ./mistraldashboard/cron_triggers/urls.py
 ./mistraldashboard/cron_triggers/views.py
Copyright: 2016, - Nokia.
License: Apache-2.0
 FIXME

Files: ./LICENSE
 ./doc/source/conf.py
 ./manage.py
 ./mistraldashboard/executions/forms.py
 ./mistraldashboard/forms.py
 ./mistraldashboard/test/urls.py
 ./releasenotes/source/conf.py
 ./setup.cfg
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./debian/copyright
Copyright: (c) 2013, Hewlett-Packard Development Company, L.P.
  (c) 2014-2016, Thomas Goirand <zigo@debian.org>
  -format/1.0/
  2010-2016, OpenStack Foundation
  2014, StackStorm, Inc.
  2014-2016, Mirantis, Inc.
  2015, ASD Technologies Co.
  2015, Alcatel-Lucent
  2015, Huawei Technologies Co., Ltd.
  2016, NEC Corporation
  2016, Nokia
  2019, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/default/smart_cell.py
Copyright: 2015, - Alcatel-Lucent
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/test/tests/error_handle.py
Copyright: 2015, ASD Technologies Co.
License: Apache-2.0
 FIXME

Files: ./setup.py
Copyright: 2013, Hewlett-Packard Development Company, L.P.
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/test/settings.py
Copyright: 2015, Huawei Technologies Co., Ltd.
  2016, NEC Corporation.
License: Apache-2.0
 FIXME

Files: ./mistraldashboard/enabled/_50_mistral.py
Copyright: 2012, OpenStack Foundation.
License: Apache-2.0
 FIXME

Files: ./debian/changelog
Copyright: Update copyright
License: UNKNOWN
 FIXME

