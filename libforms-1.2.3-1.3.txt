Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./ChangeLog
 ./INSTALL
 ./INSTALL.git
 ./README.rpmbuild
 ./autogen.sh
 ./config/Makefile.am
 ./config/common.am
 ./config/cygwin.m4
 ./config/xformsinclude.m4
 ./configure.ac
 ./debian/changelog
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/libforms-bin.install
 ./debian/libforms-bin.manpages
 ./debian/libforms-dev.info
 ./debian/libforms-dev.install
 ./debian/libforms-dev.links
 ./debian/libforms-dev.manpages
 ./debian/libforms-doc.doc-base
 ./debian/libforms-doc.docs
 ./debian/libforms-doc.install
 ./debian/libforms2.install
 ./debian/libformsgl-dev.install
 ./debian/libformsgl2.install
 ./debian/libformsgl2.postinst
 ./debian/libformsgl2.postrm
 ./debian/postinst
 ./debian/postrm
 ./debian/rules
 ./debian/source/format
 ./demos/Makefile.am
 ./demos/Readme
 ./demos/bm1.xbm
 ./demos/bm2.xbm
 ./demos/crab.xpm
 ./demos/crab45.xpm
 ./demos/crab_tran.xpm
 ./demos/demo.menu
 ./demos/fd/Makefile.am
 ./demos/fd/buttons_gui.fd
 ./demos/fd/butttypes_gui.fd
 ./demos/fd/dim_porsche.xpm
 ./demos/fd/fbtest_gui.fd
 ./demos/fd/folder_gui.fd
 ./demos/fd/formbrowser_gui.fd
 ./demos/fd/ibrowser_gui.fd
 ./demos/fd/inputall_gui.fd
 ./demos/fd/is_gui.fd
 ./demos/fd/pmbrowse_gui.fd
 ./demos/fd/porsche.xpm
 ./demos/fd/scrollbar_gui.fd
 ./demos/fd/twheel_gui.fd
 ./demos/newmail.xbm
 ./demos/nomail.xbm
 ./demos/picture.xbm
 ./demos/porsche.xpm
 ./demos/srs.xbm
 ./demos/test.xpm
 ./demos/xconq.xpm
 ./demos/xterm.xpm
 ./doc/Makefile.am
 ./doc/part0_preface.texi
 ./doc/part1.texi
 ./doc/part1_defining_forms.texi
 ./doc/part1_doing_interaction.texi
 ./doc/part1_free_objects.texi
 ./doc/part1_getting_started.texi
 ./doc/part1_goodies.texi
 ./doc/part1_introduction.texi
 ./doc/part2.texi
 ./doc/part2_command_line.texi
 ./doc/part2_creating_forms.texi
 ./doc/part2_generating_hardcopies.texi
 ./doc/part2_getting_started.texi
 ./doc/part2_introduction.texi
 ./doc/part2_language_filters.texi
 ./doc/part2_saving_loading.texi
 ./doc/part3.texi
 ./doc/part3_buttons.texi
 ./doc/part3_choice_objects.texi
 ./doc/part3_container_objects.texi
 ./doc/part3_deprecated_objects.texi
 ./doc/part3_input_objects.texi
 ./doc/part3_introduction.texi
 ./doc/part3_other_objects.texi
 ./doc/part3_popups.texi
 ./doc/part3_static_objects.texi
 ./doc/part3_valuator_objects.texi
 ./doc/part4.texi
 ./doc/part4_an_example.texi
 ./doc/part4_drawing_objects.texi
 ./doc/part4_events.texi
 ./doc/part4_fl_object.texi
 ./doc/part4_global_structure.texi
 ./doc/part4_introduction.texi
 ./doc/part4_new_buttons.texi
 ./doc/part4_preemptive_handler.texi
 ./doc/part5.texi
 ./doc/part5_dirty_tricks.texi
 ./doc/part5_overview.texi
 ./doc/part5_resources.texi
 ./doc/part5_trouble_shooting.texi
 ./doc/part5_useful_functions.texi
 ./doc/part6.texi
 ./doc/part6_images.texi
 ./doc/xforms.css
 ./doc/xforms.info
 ./doc/xforms.info-1
 ./doc/xforms.info-2
 ./doc/xforms.info-3
 ./doc/xforms.texi
 ./doc/xforms_images/FL_BLACK.png
 ./doc/xforms_images/FL_BLUE.png
 ./doc/xforms_images/FL_BOTTOM_BCOL.png
 ./doc/xforms_images/FL_CHARTEUSE.png
 ./doc/xforms_images/FL_COL1.png
 ./doc/xforms_images/FL_DARKCYAN.png
 ./doc/xforms_images/FL_DARKER_COL1.png
 ./doc/xforms_images/FL_DARKGOLD.png
 ./doc/xforms_images/FL_DARKORANGE.png
 ./doc/xforms_images/FL_DARKPINK.png
 ./doc/xforms_images/FL_DARKVIOLET.png
 ./doc/xforms_images/FL_DODGERBLUE.png
 ./doc/xforms_images/FL_GREEN.png
 ./doc/xforms_images/FL_INACTIVE.png
 ./doc/xforms_images/FL_INDIANARED.png
 ./doc/xforms_images/FL_LEFT_BCOL.png
 ./doc/xforms_images/FL_LIGHTER_COL1.png
 ./doc/xforms_images/FL_MCOL.png
 ./doc/xforms_images/FL_ORCHID.png
 ./doc/xforms_images/FL_PALEGREEN.png
 ./doc/xforms_images/FL_RED.png
 ./doc/xforms_images/FL_SLATEBLUE.png
 ./doc/xforms_images/FL_SPRINGGREEN.png
 ./doc/xforms_images/FL_TOP_BCOL.png
 ./doc/xforms_images/FL_WHEAT.png
 ./doc/xforms_images/FL_WHITE.png
 ./doc/xforms_images/Makefile.am
 ./doc/xforms_images/pushme.png
 ./fd2ps/Makefile.am
 ./fd2ps/Readme
 ./fd2ps/test/Makefile.am
 ./fd2ps/test/all.fd
 ./fd2ps/test/allcmm.fd
 ./fd2ps/test/allpoint.fd
 ./fd2ps/test/bm1.xbm
 ./fd2ps/test/buttons.fd
 ./fd2ps/test/crab.xpm
 ./fd2ps/test/dial.fd
 ./fd2ps/test/fd_logo.xpm
 ./fd2ps/test/folder_gui.fd
 ./fd2ps/test/junk.fd
 ./fd2ps/test/picture.xbm
 ./fd2ps/test/sym.fd
 ./fd2ps/test/xconq.xpm
 ./fdesign/Makefile.am
 ./fdesign/Readme
 ./fdesign/fd/Makefile.am
 ./fdesign/fd/Readme
 ./fdesign/fd/pallette.c
 ./fdesign/fd/pallette.fd
 ./fdesign/fd/pallette.h
 ./fdesign/fd/ui_attrib.c
 ./fdesign/fd/ui_attrib.fd
 ./fdesign/fd/ui_attrib.h
 ./fdesign/fd/ui_theforms.c
 ./fdesign/fd/ui_theforms.fd
 ./fdesign/fd/ui_theforms.h
 ./fdesign/spec/Makefile.am
 ./fdesign/spec/Readme
 ./fdesign/spec/browser_spec.c
 ./fdesign/spec/browser_spec.fd
 ./fdesign/spec/browser_spec.h
 ./fdesign/spec/button_spec.c
 ./fdesign/spec/button_spec.fd
 ./fdesign/spec/button_spec.h
 ./fdesign/spec/choice_spec.c
 ./fdesign/spec/choice_spec.fd
 ./fdesign/spec/choice_spec.h
 ./fdesign/spec/counter_spec.c
 ./fdesign/spec/counter_spec.fd
 ./fdesign/spec/counter_spec.h
 ./fdesign/spec/dial_spec.c
 ./fdesign/spec/dial_spec.fd
 ./fdesign/spec/dial_spec.h
 ./fdesign/spec/freeobj_spec.c
 ./fdesign/spec/freeobj_spec.fd
 ./fdesign/spec/freeobj_spec.h
 ./fdesign/spec/menu_spec.c
 ./fdesign/spec/menu_spec.fd
 ./fdesign/spec/menu_spec.h
 ./fdesign/spec/pixmap_spec.c
 ./fdesign/spec/pixmap_spec.fd
 ./fdesign/spec/pixmap_spec.h
 ./fdesign/spec/positioner_spec.c
 ./fdesign/spec/positioner_spec.fd
 ./fdesign/spec/positioner_spec.h
 ./fdesign/spec/scrollbar_spec.c
 ./fdesign/spec/scrollbar_spec.fd
 ./fdesign/spec/scrollbar_spec.h
 ./fdesign/spec/slider_spec.c
 ./fdesign/spec/slider_spec.fd
 ./fdesign/spec/slider_spec.h
 ./fdesign/spec/spinner_spec.c
 ./fdesign/spec/spinner_spec.fd
 ./fdesign/spec/spinner_spec.h
 ./fdesign/spec/twheel_spec.c
 ./fdesign/spec/twheel_spec.fd
 ./fdesign/spec/twheel_spec.h
 ./fdesign/spec/xyplot_spec.c
 ./fdesign/spec/xyplot_spec.fd
 ./fdesign/spec/xyplot_spec.h
 ./fdesign/xpm/Makefile.am
 ./fdesign/xpm/allbutton.fd
 ./fdesign/xpm/bm1.xbm
 ./fdesign/xpm/bottom.xbm
 ./fdesign/xpm/box.xpm
 ./fdesign/xpm/br.xpm
 ./fdesign/xpm/broken.xbm
 ./fdesign/xpm/broken.xpm
 ./fdesign/xpm/butt.xpm
 ./fdesign/xpm/chart.xpm
 ./fdesign/xpm/check.xpm
 ./fdesign/xpm/choice.fd
 ./fdesign/xpm/choice.xpm
 ./fdesign/xpm/clock.xpm
 ./fdesign/xpm/cnt.xpm
 ./fdesign/xpm/crab.xpm
 ./fdesign/xpm/crab_focus.xpm
 ./fdesign/xpm/dial.xpm
 ./fdesign/xpm/fd_logo.xpm
 ./fdesign/xpm/frame.xpm
 ./fdesign/xpm/glcan.xpm
 ./fdesign/xpm/hcenter.xbm
 ./fdesign/xpm/left.xbm
 ./fdesign/xpm/lightb.xpm
 ./fdesign/xpm/menu.xpm
 ./fdesign/xpm/misc.fd
 ./fdesign/xpm/nomail.xbm
 ./fdesign/xpm/picture.xbm
 ./fdesign/xpm/picture1.xbm
 ./fdesign/xpm/porsche.xpm
 ./fdesign/xpm/pos.xpm
 ./fdesign/xpm/r3dbut.xpm
 ./fdesign/xpm/right.xbm
 ./fdesign/xpm/roundb.xpm
 ./fdesign/xpm/scb.xpm
 ./fdesign/xpm/sld.xpm
 ./fdesign/xpm/tab.xpm
 ./fdesign/xpm/text.xpm
 ./fdesign/xpm/timer.xpm
 ./fdesign/xpm/top.xbm
 ./fdesign/xpm/twheel.xpm
 ./fdesign/xpm/val.fd
 ./fdesign/xpm/vals.xpm
 ./fdesign/xpm/vcenter.xbm
 ./fdesign/xpm/xconq.xpm
 ./fdesign/xpm/xconq1.xpm
 ./fdesign/xpm/xyplot.xpm
 ./gl/Makefile.am
 ./gl/glcanvas.h
 ./image/Makefile.am
 ./lib/Makefile.am
 ./lib/bitmaps/Makefile.am
 ./lib/bitmaps/plaid.xpm
 ./lib/bitmaps/q.xbm
 ./lib/bitmaps/warn.xbm
 ./lib/fd/Makefile.am
 ./lib/fd/cmdbr.fd
 ./lib/include/Makefile.am
 ./lib/private/Makefile.am
 ./libforms.spec
 ./libforms.spec.in
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./demos/arrowbutton.c
 ./demos/borderwidth.c
 ./demos/boxtype.c
 ./demos/browserall.c
 ./demos/browserop.c
 ./demos/buttonall.c
 ./demos/butttypes.c
 ./demos/canvas.c
 ./demos/chartall.c
 ./demos/chartstrip.c
 ./demos/choice.c
 ./demos/colbrowser.c
 ./demos/colsel.c
 ./demos/colsel1.c
 ./demos/counter.c
 ./demos/crossbut.c
 ./demos/crossbut.h
 ./demos/cursor.c
 ./demos/demo.c
 ./demos/demo05.c
 ./demos/demo06.c
 ./demos/demo27.c
 ./demos/demo33.c
 ./demos/demotest.c
 ./demos/demotest2.c
 ./demos/demotest3.c
 ./demos/dirlist.c
 ./demos/fbrowse.c
 ./demos/fbrowse1.c
 ./demos/fdial.c
 ./demos/flclock.c
 ./demos/folder.c
 ./demos/fonts.c
 ./demos/formbrowser.c
 ./demos/free1.c
 ./demos/freedraw.c
 ./demos/freedraw_leak.c
 ./demos/gl.c
 ./demos/glwin.c
 ./demos/goodies.c
 ./demos/grav.c
 ./demos/group.c
 ./demos/ibrowser.c
 ./demos/iconify.c
 ./demos/iconvert.c
 ./demos/inputall.c
 ./demos/invslider.c
 ./demos/itest.c
 ./demos/lalign.c
 ./demos/ldial.c
 ./demos/ll.c
 ./demos/longlabel.c
 ./demos/menu.c
 ./demos/minput.c
 ./demos/minput2.c
 ./demos/multilabel.c
 ./demos/ndial.c
 ./demos/new_popup.c
 ./demos/newbutton.c
 ./demos/nmenu.c
 ./demos/objinactive.c
 ./demos/objpos.c
 ./demos/objreturn.c
 ./demos/pmbrowse.c
 ./demos/popup.c
 ./demos/positioner.c
 ./demos/positionerXOR.c
 ./demos/preemptive.c
 ./demos/pup.c
 ./demos/pushbutton.c
 ./demos/pushme.c
 ./demos/rescale.c
 ./demos/scrollbar.c
 ./demos/secretinput.c
 ./demos/select.c
 ./demos/sld_alt.c
 ./demos/sld_radio.c
 ./demos/sldinactive.c
 ./demos/sldsize.c
 ./demos/sliderall.c
 ./demos/strange_button.c
 ./demos/strsize.c
 ./demos/symbols.c
 ./demos/thumbwheel.c
 ./demos/timeoutprec.c
 ./demos/timer.c
 ./demos/timerprec.c
 ./demos/touchbutton.c
 ./demos/xyplotactive.c
 ./demos/xyplotactivelog.c
 ./demos/xyplotall.c
 ./demos/xyplotover.c
 ./demos/yesno.c
 ./demos/yesno_cb.c
 ./fd2ps/fd2ps.h
 ./fd2ps/global.h
 ./fd2ps/sys.c
 ./fdesign/fd_attribs.c
 ./fdesign/fd_file_fun.c
 ./fdesign/fd_help.h
 ./fdesign/fd_iconinfo.c
 ./fdesign/fd_iconinfo.h
 ./fdesign/fd_main.h
 ./fdesign/fd_spec.h
 ./fdesign/fd_util.c
 ./fdesign/sp_browser.h
 ./fdesign/sp_button.h
 ./fdesign/sp_choice.h
 ./fdesign/sp_counter.h
 ./fdesign/sp_dial.h
 ./fdesign/sp_freeobj.h
 ./fdesign/sp_menu.h
 ./fdesign/sp_pixmap.h
 ./fdesign/sp_positioner.h
 ./fdesign/sp_scrollbar.h
 ./fdesign/sp_slider.h
 ./fdesign/sp_spinner.c
 ./fdesign/sp_spinner.h
 ./fdesign/sp_twheel.h
 ./fdesign/sp_util.c
 ./fdesign/sp_xyplot.h
 ./image/flimage.h
 ./lib/config.h.in
 ./lib/dirent_vms.h
 ./lib/display.c
 ./lib/fd/cmdbr.c
 ./lib/fd/cmdbr.h
 ./lib/handling.c
 ./lib/include/Basic.h
 ./lib/include/XBasic.h
 ./lib/include/bitmap.h
 ./lib/include/box.h
 ./lib/include/browser.h
 ./lib/include/button.h
 ./lib/include/canvas.h
 ./lib/include/chart.h
 ./lib/include/choice.h
 ./lib/include/clipbd.h
 ./lib/include/clock.h
 ./lib/include/counter.h
 ./lib/include/cursor.h
 ./lib/include/dial.h
 ./lib/include/filesys.h
 ./lib/include/flps.h
 ./lib/include/formbrowser.h
 ./lib/include/frame.h
 ./lib/include/free.h
 ./lib/include/goodies.h
 ./lib/include/input.h
 ./lib/include/menu.h
 ./lib/include/nmenu.h
 ./lib/include/popup.h
 ./lib/include/positioner.h
 ./lib/include/scrollbar.h
 ./lib/include/select.h
 ./lib/include/slider.h
 ./lib/include/spinner.h
 ./lib/include/tabfolder.h
 ./lib/include/text.h
 ./lib/include/thumbwheel.h
 ./lib/include/timer.h
 ./lib/include/xpopup.h
 ./lib/include/xyplot.h
 ./lib/include/zzz.h
 ./lib/nmenu.c
 ./lib/popup.c
 ./lib/private/flsnprintf.h
 ./lib/private/flvasprintf.h
 ./lib/private/pflps.h
 ./lib/private/pformbrowser.h
 ./lib/private/pinput.h
 ./lib/private/pmenu.h
 ./lib/private/pnmenu.h
 ./lib/private/pscrollbar.h
 ./lib/private/pselect.h
 ./lib/private/pspinner.h
 ./lib/private/ptbox.h
 ./lib/read4lsb.c
 ./lib/select.c
 ./lib/spinner.c
 ./lib/tbox.c
 ./lib/ulib.h
 ./lib/vms_readdir.c
 ./lib/vn_pair.c
Copyright: NONE
License: LGPL-2.1+
 FIXME

Files: ./fd2ps/align.c
 ./fdesign/fd_control.c
 ./fdesign/fd_fake.c
 ./fdesign/fd_file.c
 ./fdesign/fd_forms.c
 ./fdesign/fd_groups.c
 ./fdesign/fd_help.c
 ./fdesign/fd_initforms.c
 ./fdesign/fd_main.c
 ./fdesign/fd_names.c
 ./fdesign/fd_objects.c
 ./fdesign/fd_pallette.c
 ./fdesign/fd_printC.c
 ./fdesign/fd_rubber.c
 ./fdesign/fd_select.c
 ./fdesign/fd_spec.c
 ./fdesign/fd_super.c
 ./fdesign/sp_browser.c
 ./fdesign/sp_button.c
 ./fdesign/sp_choice.c
 ./fdesign/sp_counter.c
 ./fdesign/sp_dial.c
 ./fdesign/sp_freeobj.c
 ./fdesign/sp_menu.c
 ./fdesign/sp_pixmap.c
 ./fdesign/sp_positioner.c
 ./fdesign/sp_scrollbar.c
 ./fdesign/sp_slider.c
 ./fdesign/sp_twheel.c
 ./fdesign/sp_xyplot.c
 ./gl/glcanvas.c
 ./image/image_combine.c
 ./lib/align.c
 ./lib/bitmap.c
 ./lib/box.c
 ./lib/browser.c
 ./lib/button.c
 ./lib/chart.c
 ./lib/choice.c
 ./lib/clock.c
 ./lib/colsel.c
 ./lib/combo.c
 ./lib/counter.c
 ./lib/dial.c
 ./lib/dmalloc.h
 ./lib/events.c
 ./lib/extern.h
 ./lib/flcolor.c
 ./lib/fldraw.c
 ./lib/flinternal.h
 ./lib/flvisual.c
 ./lib/fonts.c
 ./lib/forms.c
 ./lib/frame.c
 ./lib/free.c
 ./lib/fselect.c
 ./lib/global.c
 ./lib/goodie_alert.c
 ./lib/goodie_choice.c
 ./lib/goodie_input.c
 ./lib/goodie_msg.c
 ./lib/goodie_sinput.c
 ./lib/goodie_yesno.c
 ./lib/goodies.c
 ./lib/input.c
 ./lib/lightbut.c
 ./lib/local.h
 ./lib/menu.c
 ./lib/objects.c
 ./lib/positioner.c
 ./lib/private/pbrowser.h
 ./lib/private/pcanvas.h
 ./lib/private/pchoice.h
 ./lib/private/pcounter.h
 ./lib/private/pdial.h
 ./lib/private/ppositioner.h
 ./lib/private/pslider.h
 ./lib/private/ptwheel.h
 ./lib/private/pvaluator.h
 ./lib/private/pxyplot.h
 ./lib/roundbut.c
 ./lib/scrollbut.c
 ./lib/sldraw.c
 ./lib/slider.c
 ./lib/symbols.c
 ./lib/sysdep.c
 ./lib/text.c
 ./lib/timeout.c
 ./lib/timer.c
 ./lib/util.c
 ./lib/xdraw.c
 ./lib/xsupport.c
 ./lib/xtext.c
Copyright: 1995-1997, T.C. Zhao and Mark Overmars
  1996-1998, T.C. Zhao and Mark Overmars
  1996-2000, T.C. Zhao and Mark Overmars
  1996-2002, T.C. Zhao and Mark Overmars
  T.C. Zhao and Mark Overmars
License: LGPL-2.1+
 FIXME

Files: ./fd2ps/fd2ps.c
 ./fd2ps/flsupport.c
 ./fd2ps/image2ps.c
 ./fd2ps/load.c
 ./fd2ps/papers.c
 ./fd2ps/pscol.c
 ./fd2ps/psdraw.c
 ./fd2ps/psobj.c
 ./fd2ps/pstext.c
 ./fd2ps/readxpm.c
 ./fd2ps/version.c
 ./fd2ps/xbmtops.c
 ./fd2ps/xpmtops.c
 ./image/flimage_int.h
 ./image/image.c
 ./image/image_bmp.c
 ./image/image_convolve.c
 ./image/image_crop.c
 ./image/image_disp.c
 ./image/image_fits.c
 ./image/image_gzip.c
 ./image/image_io_filter.c
 ./image/image_jpeg.c
 ./image/image_marker.c
 ./image/image_replace.c
 ./image/image_scale.c
 ./image/image_sgi.c
 ./image/image_text.c
 ./image/image_warp.c
 ./image/matrix.c
 ./image/postscript.c
 ./image/ps_core.c
 ./image/ps_draw.c
 ./image/ps_text.c
 ./image/rgb_db.c
 ./lib/appwin.c
 ./lib/asyn_io.c
 ./lib/canvas.c
 ./lib/checkbut.c
 ./lib/child.c
 ./lib/clipboard.c
 ./lib/cmd_br.c
 ./lib/cursor.c
 ./lib/flresource.c
 ./lib/interpol.c
 ./lib/keyboard.c
 ./lib/labelbut.c
 ./lib/lframe.c
 ./lib/listdir.c
 ./lib/oneliner.c
 ./lib/pixmap.c
 ./lib/round3d.c
 ./lib/scrollbar.c
 ./lib/signal.c
 ./lib/strdup.c
 ./lib/thumbwheel.c
 ./lib/tooltip.c
 ./lib/valuator.c
 ./lib/version.c
 ./lib/win.c
 ./lib/xpopup.c
 ./lib/xyplot.c
Copyright: 1993, 1998-2002, T.C. Zhao
  1996-2001, T.C. Zhao
  1996-2002, T.C. Zhao
  1997-2000, T.C. Zhao
  1997-2002, T.C. Zhao
  1998-2002, T.C. Zhao
License: LGPL-2.1+
 FIXME

Files: ./Makefile.in
 ./config/Makefile.in
 ./demos/Makefile.in
 ./demos/fd/Makefile.in
 ./doc/Makefile.in
 ./doc/xforms_images/Makefile.in
 ./fd2ps/Makefile.in
 ./fd2ps/test/Makefile.in
 ./fdesign/Makefile.in
 ./fdesign/fd/Makefile.in
 ./fdesign/spec/Makefile.in
 ./fdesign/xpm/Makefile.in
 ./gl/Makefile.in
 ./image/Makefile.in
 ./lib/Makefile.in
 ./lib/bitmaps/Makefile.in
 ./lib/fd/Makefile.in
 ./lib/include/Makefile.in
 ./lib/private/Makefile.in
Copyright: 1994-2011, Free Software
License: UNKNOWN
 FIXME

Files: ./image/image_genesis.c
 ./image/image_png.c
 ./image/image_pnm.c
 ./image/image_postscript.c
 ./image/image_proc.c
 ./image/image_rotate.c
 ./image/image_tiff.c
 ./image/image_type.c
 ./image/image_xbm.c
 ./image/image_xpm.c
 ./image/image_xwd.c
 ./lib/tabfolder.c
Copyright: 1993, 1998-2002, By T.C. Zhao
  1997-2002, By T.C. Zhao
  1998-2002, By T.C. Zhao
  2000-2002, By T.C. Zhao
License: LGPL-2.1+
 FIXME

Files: ./lib/errmsg.c
 ./lib/read2lsbf.c
 ./lib/read2msbf.c
 ./lib/read4msb.c
 ./lib/readint.c
 ./lib/space.c
Copyright: (c) 1993,1994 by T.C. Zhao
License: LGPL-2.1+
 FIXME

Files: ./config/config.guess
 ./config/config.sub
 ./config/depcomp
 ./config/ltmain.sh
 ./config/missing
Copyright: 1992-2012, Free Software Foundation, Inc.
  1996-1997, 1999-2000, 2002-2006, 2008-2012, Free Software Foundation, Inc.
  1996-2001, 2003-2011, Free Software Foundation, Inc.
  1999-2000, 2003-2007, 2009-2011, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./demos/iconvert.1
 ./fd2ps/fd2ps.man
 ./fdesign/fdesign.man
 ./lib/xforms.man
Copyright: el (co
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/FL_CYAN.png
 ./doc/xforms_images/FL_MAGENTA.png
 ./doc/xforms_images/FL_YELLOW.png
Copyright: t}}³?1`,ÀX± cÆ
License: UNKNOWN
 FIXME

Files: ./acinclude.m4
 ./config/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: GPL-2+
 FIXME

Files: ./Copyright
 ./debian/copyright
Copyright: 1996-2000, T.C. Zhao and Mark Overmars
  1999-2002, T.C. Zhao and Steve Lamont
License: UNKNOWN
 FIXME

Files: ./lib/flsnprintf.c
Copyright: 1999, Mark Martinec.
License: Artistic
 FIXME

Files: ./config/install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2010, Free Software
License: FSFUL
 FIXME

Files: ./config/texinfo.tex
Copyright: 1985-1986, 1988, 1990-2011, Free Software Foundation, Inc.
License: GPL-3+
 FIXME

Files: ./README
Copyright: NONE
License: LGPL
 FIXME

Files: ./lib/include/AAA.h.in
Copyright: (c) 1996-2002 by T.C. Zhao and Mark Overmars,
  1999-2002, T.C. Zhao
License: LGPL-2.1+
 FIXME

Files: ./lib/formbrowser.c
Copyright: 1997, By T.C. Zhao and Mark Overmars
  1998, By Steve Lamont of the National Center for
  1999-2002, T.C. Zhao and Steve Lamont
License: LGPL-2.1+
 FIXME

Files: ./image/image_gif.c
Copyright: 1993, 1998-2002, By T.C. Zhao
  property of
License: LGPL-2.1+
 FIXME

Files: ./image/image_jquant.c
Copyright: 1991-1996, Thomas G. Lane.
License: LGPL-2.1+
 FIXME

Files: ./doc/xforms_images/boxtypes.png
Copyright:   `*Ý(YÄLEm`)Èf  O;Ü1[H§ °¬êBäÐÝÓét:  LËÜd`ur¸ A×ëõz½jtÝ¹Éý¯ê±Õp¹.ý
  ôj]É¦ªuªG	¶×V%_9t*RÃ<þº±£®üê§æÐÕ?C¹´YÉk¼]³ú¦zF[
  í×¨k±½PX(¹®ý;t
  KÝøtþ«jÁ´ÌDfãB@	A@îuE?fÕÑïù|>OíP¾ùkú©~Bº¡ósç¿:Õ/óÜE3òmKîVÊq5jædOS1ÒßÛ­?Co¸×~åR5VÕÂ¾GLEJAFt_94È0{H¡~:hs½ÜºÁD»åçÙ¿ãÖ¹ÿïí®ùü¡Ì[j_¨U÷ûý~¿/mÝì¯¾¼ö£BÕÂÞÇå,¨HÊ
  Í
  P|ÜUZcµ"þ4%;)æª*5GÉÓÅNÒ`þÞaÛ´a±ýó÷áòZÿ
  QCýÐ         "    $D     H
  S¾wéS¹
  Z¤NÓ7Þ¿Ì¿>ê-lÛ<}<_È½nå·-LL¥n-íÇÓÿàuuÛ¿Äøu
  lã)Rbå×«ØbÚßf9ÓªÝîþ×Û/bÚ[·Füí(ùããããã£y
  n·Ûív³ ¶¤êËY     ÐO
  1ët¥ÜÎ(4?(÷L®õêÚ*ÇµÇíÓôåÆÜ
  ¡jås<Ç£vzÜn·Ûí¦ XêBd r
  ®Æ½ÔiÚXZ#!rö  Æ6.[¯=¤0 b¸/E¡áòçççççgÌ'
  ´%«yKüqÞÿx5ë_Ê´ýh]ÕoÜþ2ºÛª#ÚuÍ>VßîÆí¯{­»ïTÚæ§"oJè'*ó/wu)£¥µc©u3«Åñ9ÕrçYçuÕºò«k9kX~Ì²¶*-ó¥ó¯×Ô¼jtõmkhtÇ¶	
  ´°TÿvÜ5çÈ°ÿ5ùÎýUQHg&2QBßû3ôÊòÿ®þuZCïmÿä¤ÝJñ?Eê;À©Z/fK»GEûYßjÂT=7¦õ¿f¥í¯1Ïvï%òó=ÕÊ4®o¦÷ÐiÍ?òZ-S{oª´0íHoªþß§ªÆsû«¢íWª¢LËLdHeÜþôå¦Üí4%ìÞj©MßRßÂ²}¶ýø´53¥2´ÞUfKª~P¥Fb)KIYçjYò¨U¥
  Ë¸-ùéÍû+Çú#¥)§W*L¾K[¨~°
  ÒÇãñóÊÛív»Ý¶±-kÜ:    `(3gÑ    ÌÏLä
  Öm©ôü.    ¨È     ÌÄÜ¤    ¶ÄLd2   ÀÚÌÄ    ¶ÄLd     È     	    "    $D     H
  ÚjÜ²R¶44zÜ
  Ý<Wû¹Fü{¾f]§¸)Ûâd¾~ú%ôÓ|ý1¥Æ.ÕªÖï§òWó³¯¯¯¯¯¯ö÷ûý~¿çXäét:NÍó-%d·Ûív;;(ß²×&V-põv§"@	B³8Ïçó¹ü5oþ­ÓÚj
  æ
  öQ=ûw©£=f [Ýû¤ï¯ò÷Ñk3û»Î¾¦ÒÖiÙDû-¾ÜÓË¡æ°Uê[ùíoYFwíåÆ?Î¶	¦[Æ´O¿ô¥ôÿÙÎwi|Gû´æ¿ ùiÛg{µn-GTüÏËÿqJOmWÓË÷Ën-J¿ÌQÌq»ê§æÔSsBºòÚ×mªãV}[K=1ºzÅ?^>W@N!DÞi¿=§(¤¬sè½1ßÖ[nï'§mç©.?ÿ7±s®ÂµSëJ®u%Wi×°ü+¦´JÛÏYâ®yjì´_§-ÕkÆõ£»jNÒndWÚþrÉê[	ÕcÚ54ºçh#§ûk¿ßï÷û¯¯¯¯¯¯ö÷ûý~¿çXäét:NÍó-%äp8îãó¯Iü7Zã.ÓáSJÆÐoPS>Õö¦,wþN9¢òµê¸õªÒ÷ÂÐgçÑTÅîãçóù|>Ï¿>Ó¨u5Ôº|5dÚ*²uýûzÜq>îª³Ò3~ërB2mßOù«1´Êý¦ê×Fw5Ë¥ÅÇkCõÍè.¾=ÇíY£»emi´6O¦Ê(ªÂÉF1wtê]¹×9å¸KµFúÒ[;þRZx©£¢#vNKÅÇôjÝ²­cSµpîöÜmTÚrFtý³kÞû¦]î´_È¥<kt·¥SkK÷ÙÒÆrý³K¬¾Ý­
  üÐòwKî¥hm E%¿Î¨@¡¡3ö&Û8NÚ±p§´~ZO_Vµ¶­]aº3ÇLd
  üúõë×¯_ö,P²Çãñx<´C¼êBäÆöþHëUZdðýýýýý}¹.½¦îÃáp¿ßï÷û·"TùQ¡NÛ«	»Ýn·Û}|||||Ø¿@3ßóù|>µFJCd ú5Cÿõ4ÀVN§Óéôõõõõõµ®5oDÂb
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/labelframe.png
Copyright: z¶L ÏêëëëëëKÐ Éñx<ê.Ü 
  Ô{[c
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/fontsize.png
Copyright: ýÖ÷EO« ¿8c86×|v­D`[!ÊþmÁPu¯óêïä5*ý_q>^*
  ZµR±¶*Ó¶µcwM8å^GÌ¡:ãåoÏdw¢»FoÞµÙIþ¹¹3î»*ví<£
  [ÊÖ®y´¼¶ê	(ë£ëº®ë¦i¦içyga×Ïf îá^ù×¨,déúÔÌÐÌÿÂ9;ú<! ¡	B B &M  4A@hÐ! ¡	B B &M  4A@hÐ! ¡	B B &M  4A@hÐ! ¡	B B &í³þ&ö}ß÷}ÎÆqÇqmï¿­sß¯iç{µ}îG­ì§ßµ_õôºí}ÙÛÂ¾Zªz)×­®Ô@ÓkPKö­
  dÊ{-Lù+lz%×Ú³ÖÚô¾ý¾ý÷ïªÂ¶þËÐÛñ°7<ò£.?¼ÛuF}ò#_¼A)Fy
  o¢¹ëm}z
  §Û­Êé!{ß<UÕAøRÏ`´@×bïý·ÇB(ý5×Ö½5ÌékõrÀ¥ÑÝÖÐëÖvòkjØÖ)pAXØû°ûúôy;¿*	¤;,#-ý:SYïC|zÐF¨a+u êÑÀ5Âí¡íÞÉôkZË ({[MÕðØö[éWÀ]Ü,S¬mg
  ßLYhFþ³÷Þtc9´N¾bÂõ´Äê¯
  ßG×u]×MÓ4MÓ<Ïó<+
  í×_Â{MÎCW©e¨N=r#À0ük?¥¾z­ì²Ûí9½òÒ¤³è¤¶Ç$D µ<Ä±ÞÒ«ú
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/counters.png
Copyright: p,SªÃ{Þ!ÿäPòäy:æ¯Òð.?¶??un(Íç§ç©ºðoù¢ØZäáóJëÇüUÞb(¯[á-¶ÕW¿Jí!J×*ðCZj&¶KmÓÛív»Ý¦þï÷}z<cïãcL¡
  g
  »îam¦ÍY]Ê`÷õ*©´¤^ëâ
  Äö:÷ÿ¡ÛÌcüðÖfÌð`ÂUìz½^¯W
  ÛôvWózÔ¼
  õ¼T¥´ÂÔ0µN¦¾vjoY£aáÍèñÃ÷ëö)Qôj8S>.ù+ùÔó²m4¯~Æ×vúªyïbÌÇ¥ú
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/fdesign.png
Copyright: 
  ñ4èß°ÓR@eeì¨6qs:ÐmnOáv¤8 b:ªíÅÃàõ:
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/crossbutton.png
Copyright: Ñeßçê«ÅÛÛÛ[6ã³n?|$ªBÎNÿþï¦¡Ý¾kºkí¹u-IJ{^[¶ãsÚxSGÔèËºö
  >½Cô
  )ûéýíûßumû®6Bu«#ÍBô·yûüüüüü¼ÿ¦$eCÓ<ü²Yi¾Þ~×µíÓ9eÃñ,·è»gPGó©£ô¥UG÷7£=ÏçóùÛ·oß¾}kËår¹ÜgùSöjÛ¸R³ù¿)¥xmûá¾~ýúõë×ÇìWuX¯×ëõz÷üRGSÕÑðñ«:º
  ·áÔ'ÕÑ}¶6½Ú¸Ïðô	æ©ÞPõü"ÏAb*Î@ !
  ®ÄTø+Åêø¢ò<#Ê:"rÏÇËÅét:No @²?ÿüûïþù-Ë²,Ë¬ H÷×_üñûï_êÔQZeYV
  öÓÛ[¿¾ýíQz¿øTGÄÑ§×UäCö
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/dials.png
Copyright: bùÝ?àÖÐ*¾ûb6ÌvT|÷Çláó+½ûb6¼ï¿~aÜ9à+¢kaõÇl.Tûd6LßùÞþ
  1	 
  WÞó¿w_-õM2ØW~·Ë®ZL·5R­é5	(Ä³gºmÿwÓY/1¨ÅÄ¦Óu¦Ûæbå=ÿªj19sa@ ÛÏtÛ
  hµÒ4G«J0 k¬«¹³Ùl6]|3ÒgßêN''/¿|ïã;µíTw89¹w/ÿ«O®»#ý©o°ÆÖgÐ
  k$é¬%v£ø2f~SÎnÛ!ß¾·2ÞúË¾2h6¼-õñó+»ÛR{>¾&ÁÝ©té¬.³áv/©ç7ì¦7	V|<ÖJðîaVõ
  ñÝ!lÀzHá«¿;<Ne0·üJ/é4úH1È}Mþ°6g¬
  °üJo¾L
  ¹Ð»ªWú»)Ûåz­ìÂ>ò[ãJ·e»Ý|)Á  Çe  P7oÞ¼y³ú»
  ¿12¨
  ¿efÐ¨¿l_ÙMúÛÕññññññ£VÚ$¸ÉÍÔRX¦Âãqß_vÍo¾ÙÍìm/¶WÜ$¸©þnsfÃÃ¨mõï6g6<úÑVÙ5¿Ujew(õã­þvµX,Eý%øB«¿«Âý±í.T
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/genattr.png
Copyright: :x).¢X·zòÿz¼º337Ä[-ÖæµZ¯96o,9S×W".µc´Þ¡lÚÃ+%gâÄ¬ ¯ëpÙU
  *Þq^×AH K~ÀTYòâ ïlç
  B:1oIÞeÞy²®m+Þ­Öµ1Í>
  b,2Ã¶S½w¤¹§Ö;¨¢¼±âÁ,¯ë¬N~®îFJéýÚw}U«¥Ðóx}¥­wX-c¬âÀ~;¤lUäyG~ÒnyÓª(KZ¯Ö·xéxÓ¡ÍÇª+6cb¦TL7Í¼c?h¼§UQ¯Íò.yï°NÞ÷ðÎ!¼8ïy){þ¢v·ÛFÌE½C:mñÆIì&¼cÅtî÷´@ë-Ékã!=ð¢õ÷¸;^mÁ»d^ù®ïS½fûéÞR;ùnßÛ(¯y[h'_ðV
  Þ*o©¼¿_Mð:Ô_nÍRFÃèÏ¸ºW¯þ'ð¼à/xÁ[·Õ
  ÎÙ-agxOÓ
  ÛlÁK:V'­BÖ¯9æVEÞ©ÛlÈ+¨ßëÊß7ÞWG"ÒÊw^~¡²¼z&lâ6[ðöA£[ó=ãÖÚé<tc¼òmü6òêùOË§×ç½¼o³eëZ´
  Ý=/ûIC³)øWúKNÌ&xebL¼÷Ë»ió®à/xÁ
  ÞÚ	¨Gá
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/folders.png
Copyright: 1Ù  ÀÄY  g ÌÜY~à¯¿þúë·ß >q®×ëõz}{{{{{S @úÄY>ív»Ýn§ }Õ	gQEQ(
  FÚìòþî×®Òo/ëëâçi
  mà¥çÀÜY îVGúj¿×b±X,Ír»÷uú-¢g¸y±LP;%& ð?Ô#¬`®>õ¢jÍ
  q bùùçþùgåP'ÎòÝ´oUíKd«WZ "zæ^SýEÚSÁø©þ/¼Àô%»y¥U¨Ý×¹ Daîl85HUZ ¢0w¶IÍÛWÕÇh«WZ ÒgîlSà8["<#çqÙf¨ b	gÏçóù|þ×¿þüóÏ?[Â¿ÿ}¹.¹
  zNÆñx<ÊÿUå¿Ýn·Û­~¤Õf:µé¨O_kr2ó*ã®|zÕáÛãsÇ<((ÿtÊ_?Rj3µÚTþùôµf½;yU-}ºò%N  <ÎÉÌÆ<]q)  ³  &Î 8
  jlÖáÌö*n!7tä%3ª¼h1¥Ì[ð¨ßÝQ£ ó×yè6
  Óª»ü¸Ü^Îj?ÏþÛ½Fê¯O5²ÜÛ>xÿpåÜW]ëÅÃµ¨fKpDT]vÐ}iÍÎÙ~È¸÷ýÑ5~Vª¯jÿý³Ú×ÕH÷ÒhgÚÿ½eÞe8¦Lûªkmf¸²rD­Ôdîw6bí9kv9wzíý
  ñÔ3TjÓQµÉjüìÓ§GÉ.ïgKxÜpã£ÿ¿ä¶Àjtö6þi0w^ì?Qãö¥]]Þ3Î<ï5wxáìkÖÙ®FiLÏ8OÔ#5îl IHg|tè-É%`½6J
  µö[å¬öóì¿µôoÿØ´gJãÖwJó*õ.Ë¿·»à=³µõwvÙéM6§E9¢6}¾.Ë¥úÇét:N}-z±X,!Ì-õCÃñx<ÊÿUå¿Ýn·Û­~¤Õf:µé¨O_Óûr¨ßúòÃÎöuE6Ã¶rÖþ`8áã¬¯d¥­F 9s)  ³  æÎ @Föûý~¿WS2RÕtt]Ô#jSùóZëõz½^+qÌçóù|~>ÏçóÐë<Îj:¯m:Ê]W=N£Õf
  ½_w©åè­âÞ½ëÞBºÀ½ÇÏ{ðÌº¦Ý÷ùF{þ°ý»Û1º`£³Ï4¦ú§ºüÝlÜöCa};»¼¿ßgÝË¼û¦¾ê¢}9í°qÊ0ÍúêÒª_[z}µ~ûu­býz¾-u_Âø½>n-ß»Íýîã½Çs½/OÆÙ[¥óÓRûÜûþñ·püÒ»7Ú¶/g=½µä!J2Íúzlnèzn]Ï÷ë|zñûõ|9<ÓJÕr
  Ç~×>úºw]Ïl^·ß»wCom¬cZúµ<¾9íÞÇ-_ÒÜ¬Ãáp8Úé­÷tiÍÏ>³Ì~"·¶­½dR>¬Ü»_}ÕÅpu:Õú1ûõ´[
  år¹.ë¯ÃáðÌ§ÿ{ïòi¨¾¯ÇãñýÆïMÛív»Ýö»äÅb±X,¦QJâlvªÐÙ
  åîõòØ[×ýzÚ­âù½{¦-=VíKxl]úþ½õØåXúÌw·z¡òùr¹.ê§Óét:õµèÅb±X,X2é>Ò¶ßï÷û}õ÷jµZ­VjS}¡U
  îK<U¡vQ3ÄcgyÐc7äzæ6^ @wõiÓ~ä¸É  ÁÔGd»Ï£*qø
  öÏ*y NO×ëõz½®^ÉçqµGä¾¯¡/Ëår¹¤¿)ô}¿8A:ÄY:©ÜÝ§(
  üóékuÕÍ¹V«ÕjµRòc§Ì¼ï,  
  þþâ   .q ÀÄY  g L  0q ÀÄY  g L  °/c®l6Íf3
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/symbols.png
Copyright: 5w®±}sõñ-$- ¯qÊþl¬±¥nYsèº]TµFlèÛÙç«6ÆcçÿYPö¯Þ©eéD%®qï¾ô£¡±ìÅ¸fÑ[P?åË4cÕèÄX>gFwsqÂhGi¹¿ýÞ©¨¡1 k]a0{Æ0wÇ¸7Û÷iôBüVpÓP»î90.ÿhøï±µÞS~
  AÆG;1
  hßòV&<u
  5¸½Æ¬p[#`¼üzcËV0ë­7ãM0÷ôö<FÆ<ýñ(g>7cÅ{Æ2nyOFg'£{}¥ÛÏÖÈÀø®¾ÊÓ°¿|Ñ cBÆÊ`Æ1þ;d¶&¡FoGãìýÝÀÀc
  q;&xÓt~ô«$ª¤NÉÆ¥ëµ¸Ì9òQ+Æ²öøè[.¹'½RÒ9zb´5º$"²qýT©ýëßm¢Ä
  ´ë~ôùï_Ë#³3sÆe®
  µçX¿ãÑò½½<gãkÆ:1Kù½Îé)]½UKóÇT9TÚQÆZ$ÞåõûKöÐ±VÇßÉGzN#=Þe
  ÂØøÃ3Sy÷
  Ì*}
  Ì­ÆÆÌT|ãÏÏTÔê¼ÑLóÆÅøóÆ­²¶7orï3ëÏÃ¸VÎ6wñXSñyç¯©°±É
  æã¯^ÛÛ~kÝ-Qy}(-dß{;êÝÌ8Ìj?åþÓ¥Z}óêµýáWi×½'Æ
  æþÖHò
  ðgÕõÆ=q¾FÏ¤aÆ{>ýaE÷ÑxÔ³x?o­0^ãîCñ¼&ñ
  ñþ7>>í|¶<ãósW7åb,kÇ¾Öª-ËF^|,1ùdy*ÆG×h»
  ñþW/©ÿTóTÖ%ý>ÚñyiKD#âKôêÅeJIÃï©ç9¹¬zcãåõã£D
  ôkômÆù××¼Õ!s3e¼eçi»¶^!qÌÖç6îAó4äM=ë*;1Ö7Y¬wQáÏTXQã,
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/thumbwheels.png
Copyright: 9 Â6`  À À[ÐµÇãñx|«÷Îì !Àµu:N§c ¤àÚò<ÏóÜ H!À5EQ
  ²´ÊË^`ý¶ÃÕùþDÊßgø2óùé»#Õ=WÊWÝÆÄÚð'Ë²,ËÞßßßßß×ëõz½þÍ¿ØU²ååþþþþþ~·Ûív»[ºOOOOOOååçççççg;À¹L&ÉdR^¶ÄéhF8!·¡îveoC:NâtÜA
  Óét:ò¨æå¡UËã»¥d¦îmÞÝÝÝÝÝ5»ýÑh4æóù|>?ïã^ý»ìv»Ýn×öªßfús#ýVýyÞ?2Íf³Yùñòú·úÒVk;^LÔ]d @É6`  À À[ÐIf8óÐ ')÷4 êà$müÙC,/) .ÃNX @  @  
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/slidertypes.png
Copyright: B*®¡1tKM#`ñ÷<Î4½@{¿nøët:N§ÿ  ¿ÿ©ªªª*Ý  À|®×ëõzý'fYeYÛ¶mÛê   ¦UeYÏÿGw   I   
  ó·(¢(Þ_o¦içïÖë;·íÖw[ýÌeYå>öÅÁx~îu¸ ÊqTUUUÕ:Ûv½^¯×«°ukþiÂOdX ÊneYeÙÚþ.µ§¿À:Ú¦ðåfE   D  %  J   
License: UNKNOWN
 FIXME

Files: ./aclocal.m4
Copyright: 1996-2011, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./COPYING.LIB
Copyright: 1991, 1999, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/borderwidth.png
Copyright: H
License: UNKNOWN
 FIXME

Files: ./Makefile.am
Copyright: INSTALL INSTALL.git README
License: UNKNOWN
 FIXME

Files: ./fd2ps/test/parameter.xpm
Copyright: International Business Machines Corp. 1995
License: UNKNOWN
 FIXME

Files: ./doc/xforms.info-4
Copyright: 1991-1996, Thomas G. Lane and the IJG.
  1998, Steve Lamont and the National Center for
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/scrollbars.png
Copyright: aÀTÚ4¶ë
  aÀTÚ3fízf&
  ªªª*9`{)f;ñú¯O{ºäßÃÜï½Gäùî#µo,o¿ßï÷{9Ì§,Ë²,å°LÎÇãñx|~~~~~ÊÄöÌPY"¦zBÝ}Ú²ÌÌÛë;¦Ö2ï¡¤½^ßgã8fXò½ÀÚ¹phu]×umi-¹-mü3f·®2Ó{rùLÜå5tæÈjï `YÖú&æv(9Õ'V-é½1³
  ²O(f)iÀÚÙw@l³Q4`-ì£  fÙd4 6öE °Ù,4 û X#ÅlvJ07û X;ÅlQÏJ4Ê²,ËR¼ÖÝ{(c °vY0Ýv>Ïç³drV×u]×ý¿On¾¾¾¾¾¾¯1 HbX@ö ª¬Ù/&tdüv»Ýn·m¤l9kg mÙ³æÇãñx<>?????-3Ó2Rxï CeWÌÖu¯; Ok|ïÛ_À?D   b  b  Çå¯Øáp8Ëår¹eóöw¾þ>KØgùû/U÷5Ó&°äº¿7:1¬×{ËÜ«~ï]0ÕzÅóî  Ò
  ¶ç¿ÿþûï¿ÿî~ÿ×¯_¿~ýóëº®ëÚÃåb Ál6ÍfóbQEQ)bØZ5~{¾^¯×ëÕx"   ËÃáp¹.ËøW6¯igèëé³C×ú½ßßÿ¿víÞ[æþãûÞö0ÕzÅ¹  ¼G1#öAs÷°øõO_¿ýisÌOÇ¬Ýølçöº¾µöò÷ße¶ ñÊH0ï®1¿yùåcCýÝPÛÃ|k¡ ñPÌXTÿÙæ5ý/H
  ûÓµKû0zÌ8úoûÎÐK
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/buttontypes.png
Copyright: bÌVÎ¹´p=qÿÕW'©iÓ£
  pÎ
  i2bz{9ä.ÃÛÛÛÛÛ[»,Ëåry<Çc¾znþÂ¥Ýõ;SÚs«]©=ÌèVúéi¦i2·­ªªªªº6X¹å;$âL§:MÃóCóp8Üõ*þgFs«]©fe6¦©Ì­÷ûPOµæøÁ!kÂ|JÚÆ93$;ÀÂKædè*<HRÚÖ#ÌL×s ©>Å¾þF¾|øSúEdRIÉ·æ)ÕFí{ÚÆÔ|$³jHÄÉ§t]>üòyYò-q#Ó÷äõiOµ¾Ä÷Ñû­ÚÕÏõb±X,ætn9g§ìÌ9¶êåË7o~ÿ{É¯^}ýõÏ?çûæïÒf3æ.èS7¨WÈQÍß3ê=ØÍÍWW77Ç»w?ü@uòÍ"Ì÷ÃÂe¢ Rî~ù,ÂL2TxiàñææÅ/ß¼ùá| x3 ò|ÈÑ°Mïíq@Ò,ÃU Pª
  Xzì¥ÅöÏ¨ECä0cèlYÌp"âí÷ûý~oý&ÆÌ-Ms<XÇ×¨
  Ïçóù¼.Ë¥y<lÚõÜ3ãÁX¦irð©ªªªªæÉt`ÃápÈ±þÃcT0%L  ¨G Ô+t6cªùT9æÌ  JShùTïß¿ßo
  ©6ÇáYòoó^ 6ÅMsØ£Ìz(ÿERá @bD0<îÍ P8«af ²X­V«ÕrÀ03 Méb
  Ù¶ç·þõlÃtì©O3Ç(gGZ×÷c(f}?æ½ÃOý@Éa"í1Øúßòª«?ýéïýGÆÈu´2cÌ ²OÝ-§>¤Ò5Òb"Z4®	6÷¿]×pwhìGieÆoîû¯áåéÄï·$%e/3VZcw ¹ôé~Bf¦~ëÌhbøÎ¥å1ùþò-ê>Ôx"­Ï½©EZÌèvVÆã#Z¦<:7®>§GûBi¤bGÚét:ND¿[ssaíff1}²§î´çñ/N±±Pèªß]ó¡ÑÄÃÌ~LûØLwøÑícùâÊ](Gè´M~6ã×_ÿü³U!Lof*r` @9fwÎ,ìRD½yóø×§OÿúîÿD `ÖaötÇ(ÙËP] 
  ï1¦WÚ»´aô¶1®Î@Â¯
  ö1æO~úô·¿=
  ùákúÇ4 =³
  ú
License: UNKNOWN
 FIXME

Files: ./doc/xforms.init
Copyright: etc).
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/FL_TOMATO.png
Copyright: w®ªªJdÒîîîõÊ4ÆqìûÛ¾kÛdy{Æ
  ÃÕ   %tEXtcreate-date 2009-05-07T00:36:49+02:00À
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/yesno.png
Copyright: w·Õ~Ï9:ïtý3pìhzöö»
  ^Ò^m¾KÐ¶¼h×÷ã½`[øÖ^=zå6®ia8CEó,Aó {Àaîf«¹ÇõoÉõ`5ûN,API/¹µjîà<®UË ¾|AnÛR1C¶f×~£ë²
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/slider.png
Copyright: yjìÐujÊNÙ	)®ãºw2Ø
  ÇçÝÿì<»,°FÀðéôÎÀ##`5ÌêpÄùøûí9£R#W@a±dÞïv<gùQõØ²ó§ÊMtÃÖLAÃ,ÇÃGÏÖÚÖËNý0Qc£Û9SÓËL¯y~§Gºs¦Êzak
  Ýá¶{7w¸Îáäð=ë¼An^²ÖØt,~Þ+
  ÷ì Q0
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/input.png
Copyright: $àS÷9Æ
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/fd_align.png
Copyright: öjêª#
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/fselect.png
Copyright: §´Fùð
  ½´@¹Dþ1Ñ%(¥½-Aù(ÆêÍÙñå¢XÝ¯õô±z³ºY¯«
  äêÊ}¦»ÕcNèõ)/É)HRÓ¥KÓÛ²eÊÎj6PÆâ2§p§$(ãS4KoK7¥[:âºüç(]%J­òùyO¦äBYÞ~`K«±~NæqKR¸û7ê¥Ý ÈäTñBJß©kPþJPÞ§üç_P
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/gravity.png
Copyright: ¬I`Ù¿dUÙ·e)ë{­{bYßÃm`h#@Û×UÀEï3¸ïïµïí 4Ä¾¬é¿9¤¬ý	AÚÌ  @)TÍ! Ø	P¡¾@)4  @0     F  #    Á¸ °7ãñx<W½
  ¬ÀK_ÿüüüüüü}î	ÏÃÃÃÃÃCÕ«háp8Ëßïáéééééi<?????'É|>ÏçÛvÒ7´Ïêd?ïïy~¯¯¯¯¯¯U¯¨Â`0
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/FL_DARKTOMATO.png
Copyright: µÞgö¤uE×}³gü± cÆ
License: UNKNOWN
 FIXME

Files: ./doc/xforms_images/FL_RIGHT_BCOL.png
Copyright: »»ûíIßUUUu¼=ãO
License: UNKNOWN
 FIXME

