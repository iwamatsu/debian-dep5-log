Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./CREDITS
 ./ChangeLog
 ./Makefile.am
 ./NEWS
 ./TODO.ntfsprogs
 ./autogen.sh
 ./autom4te.cache/requests
 ./config.h.in
 ./debian/TODO
 ./debian/changelog
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/libntfs-3g883.install
 ./debian/local/25-ntfs-3g-policy.fdi
 ./debian/local/changelog
 ./debian/local/ntfs-3g.hook
 ./debian/local/ntfs-3g.local-bottom
 ./debian/local/ntfs-3g.local-premount
 ./debian/ntfs-3g-dev.install
 ./debian/ntfs-3g-dev.links
 ./debian/ntfs-3g-udeb.install
 ./debian/ntfs-3g-udeb.links
 ./debian/ntfs-3g-udeb.lintian-overrides
 ./debian/ntfs-3g.README.Debian
 ./debian/ntfs-3g.install
 ./debian/ntfs-3g.links
 ./debian/ntfs-3g.postinst
 ./debian/ntfs-3g.postrm
 ./debian/ntfs-3g.triggers
 ./debian/patches/0001-link-with-gpg-error.patch
 ./debian/patches/0002-Fixed-reporting-an-error-when-failed-to-build-the-mo.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./fedora/ntfs-3g.spec
 ./include/Makefile.am
 ./include/fuse-lite/Makefile.am
 ./include/ntfs-3g/Makefile.am
 ./include/ntfs-3g/realpath.h
 ./libfuse-lite/Makefile.am
 ./libntfs-3g/Makefile.am
 ./libntfs-3g/libntfs-3g.pc.in
 ./libntfs-3g/libntfs-3g.script.so.in
 ./libntfs-3g/realpath.c
 ./ntfsprogs/Makefile.am
 ./ntfsprogs/attrdef.c
 ./ntfsprogs/attrdef.h
 ./ntfsprogs/boot.h
 ./ntfsprogs/sd.c
 ./ntfsprogs/sd.h
 ./src/Makefile.am
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./include/ntfs-3g/acls.h
 ./include/ntfs-3g/cache.h
 ./include/ntfs-3g/ea.h
 ./include/ntfs-3g/ioctl.h
 ./include/ntfs-3g/misc.h
 ./include/ntfs-3g/object_id.h
 ./include/ntfs-3g/param.h
 ./include/ntfs-3g/plugin.h
 ./include/ntfs-3g/reparse.h
 ./include/ntfs-3g/xattrs.h
 ./libntfs-3g/acls.c
 ./libntfs-3g/cache.c
 ./libntfs-3g/ea.c
 ./libntfs-3g/misc.c
 ./libntfs-3g/object_id.c
 ./libntfs-3g/reparse.c
 ./libntfs-3g/xattrs.c
 ./ntfsprogs/ntfsfallocate.c
 ./ntfsprogs/ntfsrecover.c
 ./ntfsprogs/playlog.c
Copyright: 2007-2008, Jean-Pierre Andre
  2007-2017, Jean-Pierre Andre
  2008, Jean-Pierre Andre
  2008-2010, Jean-Pierre Andre
  2008-2016, Jean-Pierre Andre
  2009-2010, Jean-Pierre Andre
  2009-2017, Jean-Pierre Andre
  2010, Jean-Pierre Andre
  2010-2014, Jean-Pierre Andre
  2012-2017, Jean-Pierre Andre
  2013-2014, Jean-Pierre Andre
  2014, Jean-Pierre Andre
  2014-2017, Jean-Pierre Andre
  2015, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./include/fuse-lite/fuse.h
 ./include/fuse-lite/fuse_common.h
 ./include/fuse-lite/fuse_lowlevel.h
 ./include/fuse-lite/fuse_lowlevel_compat.h
 ./include/fuse-lite/fuse_opt.h
 ./libfuse-lite/fuse.c
 ./libfuse-lite/fuse_i.h
 ./libfuse-lite/fuse_kern_chan.c
 ./libfuse-lite/fuse_loop.c
 ./libfuse-lite/fuse_lowlevel.c
 ./libfuse-lite/fuse_misc.h
 ./libfuse-lite/fuse_opt.c
 ./libfuse-lite/fuse_session.c
 ./libfuse-lite/fuse_signals.c
 ./libfuse-lite/helper.c
 ./libfuse-lite/mount.c
 ./libfuse-lite/mount_util.c
 ./libfuse-lite/mount_util.h
Copyright: 2001-2007, Miklos Szeredi <miklos@szeredi.hu>
License: LGPL-2
 FIXME

Files: ./COPYING
 ./COPYING.LIB
 ./Makefile.in
 ./aclocal.m4
 ./include/Makefile.in
 ./include/fuse-lite/Makefile.in
 ./include/ntfs-3g/Makefile.in
 ./libfuse-lite/Makefile.in
 ./libntfs-3g/Makefile.in
 ./m4/ltsugar.m4
 ./m4/ltversion.m4
 ./m4/lt~obsolete.m4
 ./ntfsprogs/Makefile.in
 ./src/Makefile.in
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1991, Free Software Foundation, Inc.
  1994-2013, Free Software Foundation, Inc.
  1996-2013, Free Software Foundation, Inc.
  2004, Free Software Foundation, Inc.
  2004-2005, 2007, 2009, Free Software Foundation, Inc.
  2004-2005, 2007-2008, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./include/ntfs-3g/compress.h
 ./include/ntfs-3g/debug.h
 ./include/ntfs-3g/device_io.h
 ./include/ntfs-3g/endians.h
 ./include/ntfs-3g/mst.h
 ./include/ntfs-3g/support.h
 ./include/ntfs-3g/types.h
 ./include/ntfs-3g/unistr.h
 ./libntfs-3g/unix_io.c
 ./ntfsprogs/ntfsdump_logfile.c
 ./ntfsprogs/ntfsmftalloc.c
 ./ntfsprogs/ntfstruncate.c
Copyright: 2000-2002, Anton Altaparmakov
  2000-2004, Anton Altaparmakov
  2000-2005, Anton Altaparmakov
  2000-2006, Anton Altaparmakov
  2002-2004, Anton Altaparmakov
  2002-2005, Anton Altaparmakov
  2004, Anton Altaparmakov
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/volume.h
 ./libntfs-3g/dir.c
 ./libntfs-3g/index.c
 ./libntfs-3g/inode.c
 ./libntfs-3g/mft.c
 ./libntfs-3g/runlist.c
 ./ntfsprogs/ntfsresize.c
Copyright: 2000-2004, Anton Altaparmakov
  2002-2003, Richard Russon
  2002-2005, Anton Altaparmakov
  2002-2005, Richard Russon
  2002-2006, Szabolcs Szakacsits
  2002-2008, Szabolcs Szakacsits
  2004, Yura Pakhuchiy
  2004-2005, Anton Altaparmakov
  2004-2005, Richard Russon
  2004-2007, Yura Pakhuchiy
  2004-2008, Szabolcs Szakacsits
  2005, Yura Pakhuchiy
  2005-2006, Yura Pakhuchiy
  2005-2007, Yura Pakhuchiy
  2005-2008, Szabolcs Szakacsits
  2005-2009, Szabolcs Szakacsits
  2007, Jean-Pierre Andre
  2007, Yura Pakhuchiy
  2007-2010, Jean-Pierre Andre
  2008-2014, Jean-Pierre Andre
  2009-2010, Jean-Pierre Andre
  2010, Jean-Pierre Andre
  2011-2018, Jean-Pierre Andre
  2014-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsdecrypt.8.in
 ./ntfsprogs/ntfsfallocate.8.in
 ./ntfsprogs/ntfsrecover.8.in
 ./ntfsprogs/ntfssecaudit.c
 ./ntfsprogs/ntfstruncate.8.in
 ./ntfsprogs/ntfsusermap.c
 ./ntfsprogs/ntfswipe.8.in
Copyright: 2007-2016, Jean-Pierre Andre
  2014, Jean-Pierre Andre
  2015, Jean-Pierre Andre
License: UNKNOWN
 FIXME

Files: ./include/ntfs-3g/bitmap.h
 ./include/ntfs-3g/runlist.h
 ./libntfs-3g/compat.c
 ./ntfsprogs/ntfscat.h
 ./ntfsprogs/ntfsmove.c
 ./ntfsprogs/utils.h
Copyright: 2000-2004, Anton Altaparmakov
  2002, Anton Altaparmakov
  2002, Richard Russon
  2002-2004, Anton Altaparmakov
  2002-2005, Richard Russon
  2003, Anton Altaparmakov
  2003, Richard Russon
  2003-2005, Anton Altaparmakov
  2004, Anton Altaparmakov
  2004-2005, Richard Russon
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/layout.h
 ./libntfs-3g/attrlist.c
 ./libntfs-3g/bootsect.c
 ./libntfs-3g/logfile.c
 ./ntfsprogs/ntfscmp.c
Copyright: 2000-2005, Anton Altaparmakov
  2000-2006, Anton Altaparmakov
  2002-2005, Anton Altaparmakov
  2003-2008, Szabolcs Szakacsits
  2004-2005, Anton Altaparmakov
  2004-2005, Yura Pakhuchiy
  2005, Anton Altaparmakov
  2005, Yura Pakhuchiy
  2005-2006, Szabolcs Szakacsits
  2005-2009, Szabolcs Szakacsits
  2006, Szabolcs Szakacsits
  2007, Yura Pakhuchiy
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/attrib.h
 ./libntfs-3g/lcnalloc.c
 ./libntfs-3g/security.c
 ./ntfsprogs/ntfsfix.c
Copyright: 2000-2004, Anton Altaparmakov
  2000-2006, Anton Altaparmakov
  2002-2004, Anton Altaparmakov
  2002-2006, Szabolcs Szakacsits
  2004, Anton Altaparmakov
  2004, Yura Pakhuchiy
  2004-2005, Yura Pakhuchiy
  2004-2008, Szabolcs Szakacsits
  2005-2006, Szabolcs Szakacsits
  2006, Yura Pakhuchiy
  2006-2007, Szabolcs Szakacsits
  2007, Yura Pakhuchiy
  2007-2015, Jean-Pierre Andre
  2008-2009, Jean-Pierre Andre
  2010, Jean-Pierre Andre
  2011-2015, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/compat.h
 ./include/ntfs-3g/mft.h
 ./libntfs-3g/bitmap.c
 ./ntfsprogs/ntfscluster.c
Copyright: 2000-2002, Anton Altaparmakov
  2002, Richard Russon
  2002-2003, Richard Russon
  2002-2004, Anton Altaparmakov
  2002-2006, Anton Altaparmakov
  2004-2005, Richard Russon
  2004-2008, Szabolcs Szakacsits
  2005, Anton Altaparmakov
  2005-2006, Szabolcs Szakacsits
  2006-2008, Szabolcs Szakacsits
  2008-2009, Szabolcs Szakacsits
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/dir.h
 ./include/ntfs-3g/index.h
 ./include/ntfs-3g/inode.h
 ./ntfsprogs/ntfscat.c
Copyright: 2001-2004, Anton Altaparmakov
  2002, Anton Altaparmakov
  2003-2005, Anton Altaparmakov
  2003-2005, Richard Russon
  2003-2005, Szabolcs Szakacsits
  2004, Anton Altaparmakov
  2004-2005, Richard Russon
  2004-2007, Yura Pakhuchiy
  2005, Yura Pakhuchiy
  2005-2006, Yura Pakhuchiy
  2005-2008, Szabolcs Szakacsits
  2006-2008, Szabolcs Szakacsits
  2007, Yura Pakhuchiy
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/attrlist.h
 ./include/ntfs-3g/collate.h
 ./include/ntfs-3g/lcnalloc.h
 ./libntfs-3g/collate.c
Copyright: 2002, Anton Altaparmakov
  2004, Anton Altaparmakov
  2004, Yura Pakhuchiy
  2005, Yura Pakhuchiy
License: GPL-2+
 FIXME

Files: ./compile
 ./depcomp
 ./ltmain.sh
 ./missing
Copyright: 1996-2001, 2003-2011, Free Software Foundation, Inc.
  1996-2013, Free Software Foundation, Inc.
  1999-2013, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./ntfsprogs/cluster.h
 ./ntfsprogs/ntfscluster.h
 ./ntfsprogs/ntfsmove.h
 ./ntfsprogs/ntfswipe.h
Copyright: 2002, Richard Russon
  2002-2003, Richard Russon
  2003, Richard Russon
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/bootsect.h
 ./libntfs-3g/debug.c
 ./libntfs-3g/mst.c
Copyright: 2000-2002, Anton Altaparmakov
  2000-2004, Anton Altaparmakov
  2002-2004, Anton Altaparmakov
  2004-2006, Szabolcs Szakacsits
  2006, Szabolcs Szakacsits
  2006-2009, Szabolcs Szakacsits
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/logfile.h
 ./ntfsprogs/ntfsrecover.h
Copyright: 2000-2005, Anton Altaparmakov
  2014-2016, Jean-Pierre Andre
  2016, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./src/ntfs-3g_common.c
 ./src/ntfs-3g_common.h
Copyright: 2010, Erik Larsson
  2010-2011, Jean-Pierre Andre
  2010-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./src/lowntfs-3g.c
 ./src/ntfs-3g.c
Copyright: 2005, Yuval Fledel
  2005-2007, Yura Pakhuchiy
  2006-2009, Szabolcs Szakacsits
  2007-2017, Jean-Pierre Andre
  2009, Erik Larsson
License: GPL-2+
 FIXME

Files: ./config.guess
 ./config.sub
Copyright: 1992-2013, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./ntfsprogs/ntfsinfo.8.in
 ./ntfsprogs/ntfsls.8.in
Copyright: 2002-2004, Anton Altaparmakov.
  2003, Anton Altaparmakov.
  2005, Richard Russon.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfssecaudit.8.in
 ./ntfsprogs/ntfsusermap.8.in
Copyright: 2007-2016, Jean-Pierre AndrÃ©.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfscluster.8.in
 ./ntfsprogs/ntfsundelete.8.in
Copyright: 2002-2005, Richard Russon.
  2003-2005, Richard Russon.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfscat.8.in
 ./ntfsprogs/ntfscp.8.in
Copyright: 2003-2005, Richard Russon.
  2004-2007, Yura Pakhuchiy.
  2005, Richard Russon.
  2007, Yura Pakhuchiy.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfscmp.8.in
 ./src/ntfs-3g.probe.8.in
Copyright: 2005-2006, Szabolcs Szakacsits.
  2008, Szabolcs Szakacsits.
License: UNKNOWN
 FIXME

Files: ./include/fuse-lite/fuse_kernel.h
Copyright: 2001-2007, Miklos Szeredi <miklos@szeredi.hu>
License: BSD-2-clause and/or GPL
 FIXME

Files: ./install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013, Free Software Foundation,
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./libfuse-lite/fusermount.c
Copyright: 2001-2007, Miklos Szeredi <miklos@szeredi.hu>
License: GPL
 FIXME

Files: ./configure.ac
Copyright: 2000-2013, Anton Altaparmakov
  2003, Jan Kratochvil
  2005-2009, Szabolcs Szakacsits
  2007-2008, Alon Bar-Lev
License: GPL-2+
 FIXME

Files: ./libntfs-3g/unistr.c
Copyright: 2000-2004, Anton Altaparmakov
  2002-2009, Szabolcs Szakacsits
  2008, Bernhard Kaindl
  2008-2015, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsls.c
Copyright: 2003, Lode Leroy
  2003, Richard Russon
  2003-2005, Anton Altaparmakov
  2004, Carmelo Kintana
  2004, Giang Nguyen
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsinfo.c
Copyright: 2002-2004, Matthew J. Fanto
  2002-2005, Richard Russon
  2002-2006, Anton Altaparmakov
  2003-2006, Szabolcs Szakacsits
  2004-2005, Yuval Fledel
  2004-2007, Yura Pakhuchiy
  2005, Cristian Klein
  2011-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/compress.c
Copyright: 2004-2005, Anton Altaparmakov
  2004-2006, Szabolcs Szakacsits
  2005, Yura Pakhuchiy
  2009-2014, Jean-Pierre Andre
  2014, Eric Biggers
License: GPL-2+
 FIXME

Files: ./libntfs-3g/attrib.c
Copyright: 2000-2010, Anton Altaparmakov
  2002-2005, Richard Russon
  2002-2008, Szabolcs Szakacsits
  2004-2007, Yura Pakhuchiy
  2007-2015, Jean-Pierre Andre
  2010, Erik Larsson
License: GPL-2+
 FIXME

Files: ./ntfsprogs/mkntfs.c
Copyright: 2000-2011, Anton Altaparmakov
  2001-2005, Richard Russon
  2002-2006, Szabolcs Szakacsits
  2005, Erik Sornes
  2007, Yura Pakhuchiy
  2010-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfscp.c
Copyright: 2004-2007, Yura Pakhuchiy
  2005, Anton Altaparmakov
  2006, Hil Liao
  2014, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsundelete.c
Copyright: 2002-2005, Richard Russon
  2004-2005, Holger Ohmacht
  2005, Anton Altaparmakov
  2007, Yura Pakhuchiy
  2013-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/utils.c
Copyright: 2002-2005, Richard Russon
  2003, Lode Leroy
  2003-2006, Anton Altaparmakov
  2005-2007, Yura Pakhuchiy
  2014, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/win32_io.c
Copyright: 2003-2004, Lode Leroy
  2003-2006, Anton Altaparmakov
  2004-2005, Yuval Fledel
  2012-2014, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfslabel.c
Copyright: 2002, Matthew J. Fanto
  2002-2003, Richard Russon
  2002-2005, Anton Altaparmakov
  2012-2014, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsclone.c
Copyright: 2003-2006, Szabolcs Szakacsits
  2004, Per Olofsson
  2004-2006, Anton Altaparmakov
  2010-2018, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/volume.c
Copyright: 2000-2006, Anton Altaparmakov
  2002-2009, Szabolcs Szakacsits
  2004-2005, Richard Russon
  2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/security.h
Copyright: 2004, Anton Altaparmakov
  2005-2006, Szabolcs Szakacsits
  2007-2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/device.c
Copyright: 2004-2006, Szabolcs Szakacsits
  2004-2013, Anton Altaparmakov
  2008-2013, Tuxera Inc.
  2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/ntfstime.h
Copyright: 2005, Anton Altaparmakov
  2005, Yura Pakhuchiy
  2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsdecrypt.c
Copyright: 2005, Yuval Fledel
  2005-2007, Anton Altaparmakov
  2007, Yura Pakhuchiy
  2014-2015, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfswipe.c
Copyright: 2002-2005, Richard Russon
  2004, Yura Pakhuchiy
  2005, Anton Altaparmakov
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/device.h
Copyright: 2000-2013, Anton Altaparmakov
  2008-2013, Tuxera Inc.
License: GPL-2+
 FIXME

Files: ./ntfsprogs/list.h
Copyright: 2000-2002, Anton Altaparmakov and others
License: GPL-2+
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2011, Free Software
License: GPL-2+
 FIXME

Files: ./libntfs-3g/efs.c
Copyright: 2009, Martin Bene
  2009-2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/ioctl.c
Copyright: 2014, Red Hat, Inc.
  2014-2015, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./ntfsprogs/cluster.c
Copyright: 2002-2003, Richard Russon
  2014, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./libntfs-3g/logging.c
Copyright: 2005, Richard Russon
  2005-2008, Szabolcs Szakacsits
  2010, Jean-Pierre Andre
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/efs.h
Copyright: 2009, Martin Bene
License: GPL-2+
 FIXME

Files: ./include/ntfs-3g/logging.h
Copyright: 2005, Richard Russon
  2007-2008, Szabolcs Szakacsits
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsundelete.h
Copyright: 2002, Richard Russon
  2007, Yura Pakhuchiy
License: GPL-2+
 FIXME

Files: ./src/ntfs-3g.probe.c
Copyright: 2007-2009, Szabolcs Szakacsits
License: GPL-2+
 FIXME

Files: ./ntfsprogs/ntfsck.c
Copyright: 2006, Yuval Fledel
License: GPL-2+
 FIXME

Files: ./README
Copyright: NONE
License: GPL-2+ and/or LGPL-2
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-2+ and/or LGPL-2+
 FIXME

Files: ./ntfsprogs/boot.c
Copyright: 1991, Linus Torvalds <torvalds@klaava.helsinki.fi>
  1992-1993, Remy Card <card@masi.ibp.fr>
  1993-1994, David Hudson <dave@humbug.demon.co.uk>
  1998, H. Peter Anvin <hpa@zytor.com>
  1998-2005, Roman Hodek <Roman.Hodek@informatik.uni-erlangen.de>
  2008-2014, Daniel Baumann <mail@daniel-baumann.ch>
  2015, Andreas Bombe <aeb@debian.org>
License: GPL-3+
 FIXME

Files: ./ntfsprogs/ntfslabel.8.in
Copyright: 2002-2004, Anton Altaparmakov.
  2005, Richard Russon.
  2012, Jean-Pierre Andre.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/mkntfs.8.in
Copyright: 2001-2006, Anton Altaparmakov.
  2005, Richard Russon.
  2005-2006, Szabolcs Szakacsits.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfsprogs.8.in
Copyright: 2002-2003, Anton Altaparmakov.
  2002-2005, Richard Russon.
  2005-2006, Szabolcs Szakacsits.
  2005-2007, Yura Pakhuchiy.
License: UNKNOWN
 FIXME

Files: ./m4/ltoptions.m4
Copyright: 2004-2005, 2007-2009, Free Software Foundation,
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfsfix.8.in
Copyright: 2005, Richard Russon.
  2005-2006, Szabolcs Szakacsits.
  2011-2013, Jean-Pierre Andre
License: UNKNOWN
 FIXME

Files: ./src/ntfs-3g.8.in
Copyright: 2005, Richard Russon.
  2005-2006, Yura Pakhuchiy.
  2006-2009, Szabolcs Szakacsits.
  2009-2014, Jean-Pierre Andre
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfsclone.8.in
Copyright: 2003-2005, Richard Russon.
  2003-2006, Szabolcs Szakacsits.
  2004, Per Olofsson.
  2010-2013, Jean-Pierre Andre.
License: UNKNOWN
 FIXME

Files: ./ntfsprogs/ntfsresize.8.in
Copyright: 2002-2006, Szabolcs Szakacsits.
  2005, Richard Russon.
  2011-2013, Jean-Pierre Andre.
License: UNKNOWN
 FIXME

