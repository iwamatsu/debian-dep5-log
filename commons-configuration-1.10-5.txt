Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./LICENSE.txt
 ./conf/CommonsConfiguration.xsd
 ./conf/HEADER.txt
 ./conf/checkstyle-suppressions.xml
 ./conf/checkstyle.xml
 ./conf/findbugs-exclude-filter.xml
 ./pom.xml
 ./src/changes/changes.xml
 ./src/main/assembly/bin.xml
 ./src/main/assembly/src.xml
 ./src/main/java/org/apache/commons/configuration/AbstractConfiguration.java
 ./src/main/java/org/apache/commons/configuration/AbstractFileConfiguration.java
 ./src/main/java/org/apache/commons/configuration/AbstractHierarchicalFileConfiguration.java
 ./src/main/java/org/apache/commons/configuration/BaseConfiguration.java
 ./src/main/java/org/apache/commons/configuration/BaseConfigurationXMLReader.java
 ./src/main/java/org/apache/commons/configuration/CombinedConfiguration.java
 ./src/main/java/org/apache/commons/configuration/CompositeConfiguration.java
 ./src/main/java/org/apache/commons/configuration/Configuration.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationBuilder.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationComparator.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationConverter.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationException.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationFactory.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationKey.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationMap.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationRuntimeException.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationUtils.java
 ./src/main/java/org/apache/commons/configuration/ConfigurationXMLReader.java
 ./src/main/java/org/apache/commons/configuration/ConversionException.java
 ./src/main/java/org/apache/commons/configuration/DataConfiguration.java
 ./src/main/java/org/apache/commons/configuration/DatabaseConfiguration.java
 ./src/main/java/org/apache/commons/configuration/DefaultConfigurationBuilder.java
 ./src/main/java/org/apache/commons/configuration/DefaultFileSystem.java
 ./src/main/java/org/apache/commons/configuration/DynamicCombinedConfiguration.java
 ./src/main/java/org/apache/commons/configuration/EnvironmentConfiguration.java
 ./src/main/java/org/apache/commons/configuration/FileConfiguration.java
 ./src/main/java/org/apache/commons/configuration/FileOptionsProvider.java
 ./src/main/java/org/apache/commons/configuration/FileSystem.java
 ./src/main/java/org/apache/commons/configuration/FileSystemBased.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalConfiguration.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalConfigurationConverter.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalConfigurationXMLReader.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalINIConfiguration.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalReloadableConfiguration.java
 ./src/main/java/org/apache/commons/configuration/HierarchicalXMLConfiguration.java
 ./src/main/java/org/apache/commons/configuration/INIConfiguration.java
 ./src/main/java/org/apache/commons/configuration/JNDIConfiguration.java
 ./src/main/java/org/apache/commons/configuration/Lock.java
 ./src/main/java/org/apache/commons/configuration/MapConfiguration.java
 ./src/main/java/org/apache/commons/configuration/MultiFileHierarchicalConfiguration.java
 ./src/main/java/org/apache/commons/configuration/PatternSubtreeConfigurationWrapper.java
 ./src/main/java/org/apache/commons/configuration/PrefixedKeysIterator.java
 ./src/main/java/org/apache/commons/configuration/PropertiesConfiguration.java
 ./src/main/java/org/apache/commons/configuration/PropertiesConfigurationLayout.java
 ./src/main/java/org/apache/commons/configuration/PropertyConverter.java
 ./src/main/java/org/apache/commons/configuration/StrictConfigurationComparator.java
 ./src/main/java/org/apache/commons/configuration/SubnodeConfiguration.java
 ./src/main/java/org/apache/commons/configuration/SubsetConfiguration.java
 ./src/main/java/org/apache/commons/configuration/SystemConfiguration.java
 ./src/main/java/org/apache/commons/configuration/VFSFileSystem.java
 ./src/main/java/org/apache/commons/configuration/VerifiableOutputStream.java
 ./src/main/java/org/apache/commons/configuration/XMLConfiguration.java
 ./src/main/java/org/apache/commons/configuration/XMLPropertiesConfiguration.java
 ./src/main/java/org/apache/commons/configuration/beanutils/BeanDeclaration.java
 ./src/main/java/org/apache/commons/configuration/beanutils/BeanFactory.java
 ./src/main/java/org/apache/commons/configuration/beanutils/BeanHelper.java
 ./src/main/java/org/apache/commons/configuration/beanutils/ConfigurationDynaBean.java
 ./src/main/java/org/apache/commons/configuration/beanutils/ConfigurationDynaClass.java
 ./src/main/java/org/apache/commons/configuration/beanutils/DefaultBeanFactory.java
 ./src/main/java/org/apache/commons/configuration/beanutils/XMLBeanDeclaration.java
 ./src/main/java/org/apache/commons/configuration/beanutils/package.html
 ./src/main/java/org/apache/commons/configuration/event/ConfigurationErrorEvent.java
 ./src/main/java/org/apache/commons/configuration/event/ConfigurationErrorListener.java
 ./src/main/java/org/apache/commons/configuration/event/ConfigurationEvent.java
 ./src/main/java/org/apache/commons/configuration/event/ConfigurationListener.java
 ./src/main/java/org/apache/commons/configuration/event/EventSource.java
 ./src/main/java/org/apache/commons/configuration/event/package.html
 ./src/main/java/org/apache/commons/configuration/interpol/ConfigurationInterpolator.java
 ./src/main/java/org/apache/commons/configuration/interpol/ConstantLookup.java
 ./src/main/java/org/apache/commons/configuration/interpol/EnvironmentLookup.java
 ./src/main/java/org/apache/commons/configuration/interpol/ExprLookup.java
 ./src/main/java/org/apache/commons/configuration/interpol/package.html
 ./src/main/java/org/apache/commons/configuration/package.html
 ./src/main/java/org/apache/commons/configuration/plist/PropertyListConfiguration.java
 ./src/main/java/org/apache/commons/configuration/plist/XMLPropertyListConfiguration.java
 ./src/main/java/org/apache/commons/configuration/plist/package.html
 ./src/main/java/org/apache/commons/configuration/reloading/FileChangedReloadingStrategy.java
 ./src/main/java/org/apache/commons/configuration/reloading/InvariantReloadingStrategy.java
 ./src/main/java/org/apache/commons/configuration/reloading/ManagedReloadingStrategy.java
 ./src/main/java/org/apache/commons/configuration/reloading/ManagedReloadingStrategyMBean.java
 ./src/main/java/org/apache/commons/configuration/reloading/Reloadable.java
 ./src/main/java/org/apache/commons/configuration/reloading/ReloadingStrategy.java
 ./src/main/java/org/apache/commons/configuration/reloading/VFSFileChangedReloadingStrategy.java
 ./src/main/java/org/apache/commons/configuration/reloading/package.html
 ./src/main/java/org/apache/commons/configuration/resolver/CatalogResolver.java
 ./src/main/java/org/apache/commons/configuration/resolver/DefaultEntityResolver.java
 ./src/main/java/org/apache/commons/configuration/resolver/EntityRegistry.java
 ./src/main/java/org/apache/commons/configuration/resolver/EntityResolverSupport.java
 ./src/main/java/org/apache/commons/configuration/resolver/package.html
 ./src/main/java/org/apache/commons/configuration/tree/ConfigurationNode.java
 ./src/main/java/org/apache/commons/configuration/tree/ConfigurationNodeVisitor.java
 ./src/main/java/org/apache/commons/configuration/tree/ConfigurationNodeVisitorAdapter.java
 ./src/main/java/org/apache/commons/configuration/tree/DefaultConfigurationKey.java
 ./src/main/java/org/apache/commons/configuration/tree/DefaultConfigurationNode.java
 ./src/main/java/org/apache/commons/configuration/tree/DefaultExpressionEngine.java
 ./src/main/java/org/apache/commons/configuration/tree/ExpressionEngine.java
 ./src/main/java/org/apache/commons/configuration/tree/MergeCombiner.java
 ./src/main/java/org/apache/commons/configuration/tree/NodeAddData.java
 ./src/main/java/org/apache/commons/configuration/tree/NodeCombiner.java
 ./src/main/java/org/apache/commons/configuration/tree/OverrideCombiner.java
 ./src/main/java/org/apache/commons/configuration/tree/TreeUtils.java
 ./src/main/java/org/apache/commons/configuration/tree/UnionCombiner.java
 ./src/main/java/org/apache/commons/configuration/tree/ViewNode.java
 ./src/main/java/org/apache/commons/configuration/tree/package.html
 ./src/main/java/org/apache/commons/configuration/tree/xpath/ConfigurationNodeIteratorAttribute.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/ConfigurationNodeIteratorBase.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/ConfigurationNodeIteratorChildren.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/ConfigurationNodePointer.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/ConfigurationNodePointerFactory.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/XPathExpressionEngine.java
 ./src/main/java/org/apache/commons/configuration/tree/xpath/package.html
 ./src/main/java/org/apache/commons/configuration/web/AppletConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/BaseWebConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/ServletConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/ServletContextConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/ServletFilterConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/ServletRequestConfiguration.java
 ./src/main/java/org/apache/commons/configuration/web/package.html
 ./src/main/javacc/PropertyListParser.jj
 ./src/main/resources/PropertyList-1.0.dtd
 ./src/main/resources/digesterRules.xml
 ./src/main/resources/properties.dtd
 ./src/site/site.xml
 ./src/site/xdoc/building.xml
 ./src/site/xdoc/dependencies.xml
 ./src/site/xdoc/download_configuration.xml
 ./src/site/xdoc/index.xml
 ./src/site/xdoc/issue-tracking.xml
 ./src/site/xdoc/javadocarchive.xml
 ./src/site/xdoc/mail-lists.xml
 ./src/site/xdoc/userguide/howto_basicfeatures.xml
 ./src/site/xdoc/userguide/howto_beans.xml
 ./src/site/xdoc/userguide/howto_combinedconfiguration.xml
 ./src/site/xdoc/userguide/howto_compositeconfiguration.xml
 ./src/site/xdoc/userguide/howto_configurationbuilder.xml
 ./src/site/xdoc/userguide/howto_events.xml
 ./src/site/xdoc/userguide/howto_filebased.xml
 ./src/site/xdoc/userguide/howto_filesystems.xml
 ./src/site/xdoc/userguide/howto_multitenant.xml
 ./src/site/xdoc/userguide/howto_properties.xml
 ./src/site/xdoc/userguide/howto_utilities.xml
 ./src/site/xdoc/userguide/howto_xml.xml
 ./src/site/xdoc/userguide/overview.xml
 ./src/site/xdoc/userguide/user_guide.xml
 ./src/test/java/org/apache/commons/configuration/BaseNonStringProperties.java
 ./src/test/java/org/apache/commons/configuration/ConfigurationAssert.java
 ./src/test/java/org/apache/commons/configuration/ConfigurationErrorListenerImpl.java
 ./src/test/java/org/apache/commons/configuration/DatabaseConfigurationTestHelper.java
 ./src/test/java/org/apache/commons/configuration/FileURLStreamHandler.java
 ./src/test/java/org/apache/commons/configuration/InterpolationTestHelper.java
 ./src/test/java/org/apache/commons/configuration/Logging.java
 ./src/test/java/org/apache/commons/configuration/MockInitialContextFactory.java
 ./src/test/java/org/apache/commons/configuration/NonCloneableConfiguration.java
 ./src/test/java/org/apache/commons/configuration/NonStringTestHolder.java
 ./src/test/java/org/apache/commons/configuration/TestAbstractConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestAbstractConfigurationBasicFeatures.java
 ./src/test/java/org/apache/commons/configuration/TestBaseConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestBaseConfigurationXMLReader.java
 ./src/test/java/org/apache/commons/configuration/TestBaseNullConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestCatalogResolver.java
 ./src/test/java/org/apache/commons/configuration/TestCombinedConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestCompositeConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestCompositeConfigurationNonStringProperties.java
 ./src/test/java/org/apache/commons/configuration/TestConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationConverter.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationFactory.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationKey.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationMap.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationSet.java
 ./src/test/java/org/apache/commons/configuration/TestConfigurationUtils.java
 ./src/test/java/org/apache/commons/configuration/TestDataConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestDatabaseConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestDefaultConfigurationBuilder.java
 ./src/test/java/org/apache/commons/configuration/TestDynamicCombinedConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestEnvironmentConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestEqualBehaviour.java
 ./src/test/java/org/apache/commons/configuration/TestEqualsProperty.java
 ./src/test/java/org/apache/commons/configuration/TestFileConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestHierarchicalConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestHierarchicalConfigurationXMLReader.java
 ./src/test/java/org/apache/commons/configuration/TestHierarchicalINIConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestHierarchicalXMLConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestINIConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestJNDIConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestJNDIEnvironmentValues.java
 ./src/test/java/org/apache/commons/configuration/TestMapConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestMapConfigurationRegression.java
 ./src/test/java/org/apache/commons/configuration/TestMultiFileHierarchicalConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestNonStringProperties.java
 ./src/test/java/org/apache/commons/configuration/TestNullCompositeConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestNullJNDIEnvironmentValues.java
 ./src/test/java/org/apache/commons/configuration/TestPatternSubtreeConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestPropertiesConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestPropertiesConfigurationLayout.java
 ./src/test/java/org/apache/commons/configuration/TestPropertiesSequence.java
 ./src/test/java/org/apache/commons/configuration/TestPropertyConverter.java
 ./src/test/java/org/apache/commons/configuration/TestStrictConfigurationComparator.java
 ./src/test/java/org/apache/commons/configuration/TestSubnodeConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestSubsetConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestSystemConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestSystemConfigurationRegression.java
 ./src/test/java/org/apache/commons/configuration/TestThreesomeConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestVFSConfigurationBuilder.java
 ./src/test/java/org/apache/commons/configuration/TestWebdavConfigurationBuilder.java
 ./src/test/java/org/apache/commons/configuration/TestXMLConfiguration.java
 ./src/test/java/org/apache/commons/configuration/TestXMLPropertiesConfiguration.java
 ./src/test/java/org/apache/commons/configuration/beanutils/BeanCreationTestBean.java
 ./src/test/java/org/apache/commons/configuration/beanutils/BeanCreationTestBeanWithListChild.java
 ./src/test/java/org/apache/commons/configuration/beanutils/TestBeanHelper.java
 ./src/test/java/org/apache/commons/configuration/beanutils/TestConfigurationDynaBean.java
 ./src/test/java/org/apache/commons/configuration/beanutils/TestConfigurationDynaBeanXMLConfig.java
 ./src/test/java/org/apache/commons/configuration/beanutils/TestDefaultBeanFactory.java
 ./src/test/java/org/apache/commons/configuration/beanutils/TestXMLBeanDeclaration.java
 ./src/test/java/org/apache/commons/configuration/event/AbstractTestConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/AbstractTestFileConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/ConfigurationListenerTestImpl.java
 ./src/test/java/org/apache/commons/configuration/event/TestDatabaseConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/TestEventSource.java
 ./src/test/java/org/apache/commons/configuration/event/TestHierarchicalConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/TestMapConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/TestPropertiesConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/TestSubsetConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/event/TestXMLConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/interpol/TestConfigurationInterpolator.java
 ./src/test/java/org/apache/commons/configuration/interpol/TestConstantLookup.java
 ./src/test/java/org/apache/commons/configuration/interpol/TestEnvironmentLookup.java
 ./src/test/java/org/apache/commons/configuration/interpol/TestExprLookup.java
 ./src/test/java/org/apache/commons/configuration/plist/AbstractTestPListEvents.java
 ./src/test/java/org/apache/commons/configuration/plist/TestPropertyListConfiguration.java
 ./src/test/java/org/apache/commons/configuration/plist/TestPropertyListConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/plist/TestPropertyListParser.java
 ./src/test/java/org/apache/commons/configuration/plist/TestXMLPropertyListConfiguration.java
 ./src/test/java/org/apache/commons/configuration/plist/TestXMLPropertyListConfigurationEvents.java
 ./src/test/java/org/apache/commons/configuration/reloading/FileAlwaysReloadingStrategy.java
 ./src/test/java/org/apache/commons/configuration/reloading/FileRandomReloadingStrategy.java
 ./src/test/java/org/apache/commons/configuration/reloading/TestFileChangedReloadingStrategy.java
 ./src/test/java/org/apache/commons/configuration/reloading/TestManagedReloadingStrategy.java
 ./src/test/java/org/apache/commons/configuration/reloading/TestVFSFileChangedReloadingStrategy.java
 ./src/test/java/org/apache/commons/configuration/test/HsqlDB.java
 ./src/test/java/org/apache/commons/configuration/tree/AbstractCombinerTest.java
 ./src/test/java/org/apache/commons/configuration/tree/TestDefaultConfigurationKey.java
 ./src/test/java/org/apache/commons/configuration/tree/TestDefaultConfigurationNode.java
 ./src/test/java/org/apache/commons/configuration/tree/TestDefaultExpressionEngine.java
 ./src/test/java/org/apache/commons/configuration/tree/TestMergeCombiner.java
 ./src/test/java/org/apache/commons/configuration/tree/TestNodeAddData.java
 ./src/test/java/org/apache/commons/configuration/tree/TestOverrideCombiner.java
 ./src/test/java/org/apache/commons/configuration/tree/TestUnionCombiner.java
 ./src/test/java/org/apache/commons/configuration/tree/TestViewNode.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/AbstractXPathTest.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestConfigurationIteratorAttributes.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestConfigurationNodeIteratorChildren.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestConfigurationNodePointer.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestConfigurationNodePointerFactory.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestXPathExpressionEngine.java
 ./src/test/java/org/apache/commons/configuration/tree/xpath/TestXPathExpressionEngineInConfig.java
 ./src/test/java/org/apache/commons/configuration/web/TestAppletConfiguration.java
 ./src/test/java/org/apache/commons/configuration/web/TestServletConfiguration.java
 ./src/test/java/org/apache/commons/configuration/web/TestServletContextConfiguration.java
 ./src/test/java/org/apache/commons/configuration/web/TestServletFilterConfiguration.java
 ./src/test/java/org/apache/commons/configuration/web/TestServletRequestConfiguration.java
 ./src/test/resources/01/testMultiConfiguration_1001.xml
 ./src/test/resources/catalog.xml
 ./src/test/resources/catalog2.xml
 ./src/test/resources/config/deep/deepinclude.properties
 ./src/test/resources/config/deep/deeptest.properties
 ./src/test/resources/config/deep/deeptestinvalid.properties
 ./src/test/resources/config/deep/test.properties
 ./src/test/resources/config/deep/testEqualDeep.properties
 ./src/test/resources/config/deep/testFileFromClasspath.xml
 ./src/test/resources/config/test.properties
 ./src/test/resources/configA.xml
 ./src/test/resources/configB.xml
 ./src/test/resources/dataset.xml
 ./src/test/resources/include-interpol.properties
 ./src/test/resources/include.properties
 ./src/test/resources/jndi.properties
 ./src/test/resources/log4j-test.xml
 ./src/test/resources/resolver.dtd
 ./src/test/resources/sample.xml
 ./src/test/resources/sample.xsd
 ./src/test/resources/sample_1001.xml
 ./src/test/resources/test.ini
 ./src/test/resources/test.plist
 ./src/test/resources/test.plist.xml
 ./src/test/resources/test.properties
 ./src/test/resources/test.properties.xml
 ./src/test/resources/test.xml
 ./src/test/resources/test2.plist.xml
 ./src/test/resources/test2.properties
 ./src/test/resources/testClasspath.properties
 ./src/test/resources/testComplexInitialization.xml
 ./src/test/resources/testConfigurationInterpolatorUpdate.xml
 ./src/test/resources/testConfigurationProvider.xml
 ./src/test/resources/testConfigurationXMLDocument.xml
 ./src/test/resources/testDigesterBadXML.xml
 ./src/test/resources/testDigesterConfiguration.xml
 ./src/test/resources/testDigesterConfiguration2.xml
 ./src/test/resources/testDigesterConfiguration3.xml
 ./src/test/resources/testDigesterConfigurationBasePath.xml
 ./src/test/resources/testDigesterConfigurationInclude1.xml
 ./src/test/resources/testDigesterConfigurationInclude2.properties
 ./src/test/resources/testDigesterConfigurationNamespaceAware.xml
 ./src/test/resources/testDigesterConfigurationOverwrite.properties
 ./src/test/resources/testDigesterConfigurationReverseOrder.xml
 ./src/test/resources/testDigesterConfigurationSysProps.xml
 ./src/test/resources/testDigesterConfigurationWithProps.xml
 ./src/test/resources/testDigesterCreateObject.xml
 ./src/test/resources/testDigesterOptionalConfiguration.xml
 ./src/test/resources/testDigesterOptionalConfigurationEx.xml
 ./src/test/resources/testDtd.xml
 ./src/test/resources/testEncoding.xml
 ./src/test/resources/testEqual.properties
 ./src/test/resources/testEqualDigester.xml
 ./src/test/resources/testExpression.xml
 ./src/test/resources/testExtendedClass.xml
 ./src/test/resources/testExtendedXMLConfigurationProvider.xml
 ./src/test/resources/testFactoryPropertiesInclude.xml
 ./src/test/resources/testFileReloadConfigurationBuilder.xml
 ./src/test/resources/testFileReloadConfigurationBuilder2.xml
 ./src/test/resources/testFileSystem.xml
 ./src/test/resources/testGlobalLookup.xml
 ./src/test/resources/testHierarchicalXMLConfiguration.xml
 ./src/test/resources/testHierarchicalXMLConfiguration2.xml
 ./src/test/resources/testInterpolation.properties
 ./src/test/resources/testInterpolation.xml
 ./src/test/resources/testInterpolationBuilder.xml
 ./src/test/resources/testMultiConfiguration.xsd
 ./src/test/resources/testMultiConfiguration_1001.xml
 ./src/test/resources/testMultiConfiguration_1002.xml
 ./src/test/resources/testMultiConfiguration_1003.xml
 ./src/test/resources/testMultiConfiguration_1004.xml
 ./src/test/resources/testMultiConfiguration_2001.xml
 ./src/test/resources/testMultiConfiguration_2002.xml
 ./src/test/resources/testMultiConfiguration_3001.xml
 ./src/test/resources/testMultiConfiguration_3002.xml
 ./src/test/resources/testMultiConfiguration_default.xml
 ./src/test/resources/testMultiDynamic_default.xml
 ./src/test/resources/testMultiDynamic_default2.xml
 ./src/test/resources/testMultiTenentConfigurationBuilder.xml
 ./src/test/resources/testMultiTenentConfigurationBuilder2.xml
 ./src/test/resources/testMultiTenentConfigurationBuilder3.xml
 ./src/test/resources/testMultiTenentConfigurationBuilder4.xml
 ./src/test/resources/testMultiTenentConfigurationBuilder5.xml
 ./src/test/resources/testPatternSubtreeConfig.xml
 ./src/test/resources/testResolver.xml
 ./src/test/resources/testSequence.properties
 ./src/test/resources/testSequenceDigester.xml
 ./src/test/resources/testSystemProperties.xml
 ./src/test/resources/testVFSMultiTenentConfigurationBuilder1.xml
 ./src/test/resources/testVFSMultiTenentConfigurationBuilder2.xml
 ./src/test/resources/testValidateInvalid.xml
 ./src/test/resources/testValidateValid.xml
 ./src/test/resources/testValidation.xml
 ./src/test/resources/testValidation2.xml
 ./src/test/resources/testValidation3.xml
 ./src/test/resources/test_invalid_date.plist.xml
 ./src/test/resources/testcombine1.xml
 ./src/test/resources/testcombine2.xml
 ./src/test/resources/testdb.script
 ./src/test/resources/threesome.properties
Copyright: NONE
License: Apache-2.0
 FIXME

Files: ./RELEASE-NOTES.txt
 ./conf/README
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/libcommons-configuration-java-doc.doc-base
 ./debian/libcommons-configuration-java-doc.install
 ./debian/libcommons-configuration-java.classpath
 ./debian/libcommons-configuration-java.poms
 ./debian/maven.ignoreRules
 ./debian/maven.properties
 ./debian/maven.rules
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./src/site/resources/images/logo.xcf
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: Apache
 FIXME

Files: ./src/site/resources/images/logo.png
Copyright: ×)-¹vÒé®Á`hhh
  ¸@×××wùòåÐNZl
  @VÅÕ«W£FFFvèÐ
  AIHHØ±c°,)¼yúôi°ì
  T*[·nÅ§ªªªyænÝº	
  T}úôá.^¼Å¬-Ë¹å83F2p£¹|ùò1cÆdgg£EcÙúóÿ
  TøkÌÐ4½N8zôhø0kÖ¬N:FäR©T«ÕÿùÿÉM9^|ñÅt¢(Î?50s0Í0
  uvvqá1e>Gh04åc(a
  ©ËN§[½z5êBrrrUUUuuõìÙ³mbPPÐýû÷«ªªjkkÏ;gÃ0Æ<==6###cÞ¼yr¯ªªªªªºvíÊUHHH¨¨¨ÞqÎSqqq
  ¬¬¬ªª²Ýß¶ÍUÂÚþ¹ªªêÞ½{åååeee¥¥¥¡gÏt$¶oß~ïÞ½êêj9W¯^h{öì©¬¬äHéõúaÃQj÷ïß§ÔÌfó¼yóF!÷ê¼¼<8¯Ù³g'''ïÚµ+..Ã²dIiiéîÝ»áßèèè½{÷&%%íÜ¹súôéräû÷ï×ÔÔ
  ¬¬ÌËËÃF=z4,,lË-íÛ·Ì8;;¢xîÜ9DÖjµFA1¯pqq9tèmÃªU«&MÔ¾}{ÆÙl²&ÉÏÏ/55ûûÓO?mÜ¸qÜ¸qhVù¹çûöÛoõz= L0y
  ¾¾óxÐ7ß|377·¼¼Ú dÿú×¿RÌÉ'CO-[xRTTtëÖ­õë×sÈ¥¥¥êt:ÔÀ#GÞ¿zÍ1¶¨¨hóæÍÎäååqßP__?räH]ÐµÒÒÒ²²2ÁPTTtóæÍ ¹~ýúÂÂB}êÿZ­¶¢¢ZXRR¢×ë
  Úª÷£Ð<x?ûøøp¨V«qÀÁ?þñß}÷]vvö?þQ
License: UNKNOWN
 FIXME

Files: ./NOTICE.txt
Copyright: 2001-2013, The Apache Software Foundation
License: UNKNOWN
 FIXME

