Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./script/BuildLangModel.py
 ./script/charsets/codepoints.py
 ./script/charsets/db.py
 ./script/charsets/iso-8859-1.py
 ./script/charsets/iso-8859-11.py
 ./script/charsets/iso-8859-15.py
 ./script/charsets/iso-8859-2.py
 ./script/charsets/iso-8859-3.py
 ./script/charsets/iso-8859-6.py
 ./script/charsets/iso-8859-7.py
 ./script/charsets/iso-8859-9.py
 ./script/charsets/tis-620.py
 ./script/charsets/viscii.py
 ./script/charsets/windows-1250.py
 ./script/charsets/windows-1252.py
 ./script/charsets/windows-1253.py
 ./script/charsets/windows-1256.py
 ./script/charsets/windows-1258.py
 ./script/header-template.cpp
 ./script/langs/ar.py
 ./script/langs/da.py
 ./script/langs/de.py
 ./script/langs/el.py
 ./script/langs/eo.py
 ./script/langs/es.py
 ./script/langs/fr.py
 ./script/langs/hu.py
 ./script/langs/th.py
 ./script/langs/tr.py
 ./script/langs/vi.py
 ./src/Big5Freq.tab
 ./src/CharDistribution.cpp
 ./src/CharDistribution.h
 ./src/EUCKRFreq.tab
 ./src/EUCTWFreq.tab
 ./src/GB2312Freq.tab
 ./src/JISFreq.tab
 ./src/JpCntx.cpp
 ./src/JpCntx.h
 ./src/LangModels/LangArabicModel.cpp
 ./src/LangModels/LangBulgarianModel.cpp
 ./src/LangModels/LangDanishModel.cpp
 ./src/LangModels/LangEsperantoModel.cpp
 ./src/LangModels/LangFrenchModel.cpp
 ./src/LangModels/LangGermanModel.cpp
 ./src/LangModels/LangGreekModel.cpp
 ./src/LangModels/LangHebrewModel.cpp
 ./src/LangModels/LangHungarianModel.cpp
 ./src/LangModels/LangRussianModel.cpp
 ./src/LangModels/LangSpanishModel.cpp
 ./src/LangModels/LangThaiModel.cpp
 ./src/LangModels/LangTurkishModel.cpp
 ./src/LangModels/LangVietnameseModel.cpp
 ./src/nsBig5Prober.cpp
 ./src/nsBig5Prober.h
 ./src/nsCharSetProber.cpp
 ./src/nsCharSetProber.h
 ./src/nsCodingStateMachine.h
 ./src/nsEUCJPProber.cpp
 ./src/nsEUCJPProber.h
 ./src/nsEUCKRProber.cpp
 ./src/nsEUCKRProber.h
 ./src/nsEUCTWProber.cpp
 ./src/nsEUCTWProber.h
 ./src/nsEscCharsetProber.cpp
 ./src/nsEscCharsetProber.h
 ./src/nsEscSM.cpp
 ./src/nsGB2312Prober.cpp
 ./src/nsGB2312Prober.h
 ./src/nsHebrewProber.cpp
 ./src/nsLatin1Prober.cpp
 ./src/nsLatin1Prober.h
 ./src/nsMBCSGroupProber.cpp
 ./src/nsMBCSGroupProber.h
 ./src/nsMBCSSM.cpp
 ./src/nsPkgInt.h
 ./src/nsSBCSGroupProber.cpp
 ./src/nsSBCSGroupProber.h
 ./src/nsSBCharSetProber.cpp
 ./src/nsSBCharSetProber.h
 ./src/nsSJISProber.cpp
 ./src/nsSJISProber.h
 ./src/nsUTF8Prober.cpp
 ./src/nsUTF8Prober.h
 ./src/nsUniversalDetector.cpp
 ./src/nsUniversalDetector.h
 ./src/nscore.h
 ./src/prmem.h
 ./src/tools/uchardet.cpp
 ./src/uchardet.cpp
 ./src/uchardet.h
 ./test/uchardet-tests.c
Copyright: 1998, the Initial Developer.
  2001, the Initial Developer.
  2005, the Initial Developer.
License: GPL-2+ or LGPL-2.1+ and/or MPL-1.1
 FIXME

Files: ./AUTHORS
 ./CMakeLists.txt
 ./COPYING
 ./INSTALL
 ./README.md
 ./build-mac/uchardet.cpp
 ./build-mac/uchardet.xcodeproj/project.pbxproj
 ./build-mac/uchardet.xcodeproj/project.xcworkspace/contents.xcworkspacedata
 ./build-mac/uchardet.xcodeproj/project.xcworkspace/xcshareddata/uchardet.xccheckout
 ./build-mac/uchardet.xcodeproj/xcshareddata/xcschemes/uchardet-ios.xcscheme
 ./build-mac/uchardet.xcodeproj/xcshareddata/xcschemes/uchardet.xcscheme
 ./debian/compat
 ./debian/control
 ./debian/gbp.conf
 ./debian/libuchardet-dev.install
 ./debian/libuchardet0.install
 ./debian/libuchardet0.symbols
 ./debian/rules
 ./debian/source/format
 ./debian/tests/build
 ./debian/tests/cli
 ./debian/tests/control
 ./debian/uchardet.install
 ./debian/watch
 ./doc/CMakeLists.txt
 ./doc/uchardet.1
 ./script/BuildLangModelLogs/LangDanishModel.log
 ./script/BuildLangModelLogs/LangGreekModel.log
 ./script/BuildLangModelLogs/LangTurkishModel.log
 ./script/BuildLangModelLogs/LangVietnameseModel.log
 ./script/README
 ./script/debug.sh
 ./script/release.sh
 ./script/win32.sh
 ./src/CMakeLists.txt
 ./src/symbols.cmake
 ./src/tools/CMakeLists.txt
 ./test/CMakeLists.txt
 ./test/ar/iso-8859-6.txt
 ./test/ar/windows-1256.txt
 ./test/bg/windows-1251.txt
 ./test/da/iso-8859-1.txt
 ./test/da/iso-8859-15.txt
 ./test/da/utf-8.txt
 ./test/da/windows-1252.txt
 ./test/de/iso-8859-1.txt
 ./test/de/windows-1252.txt
 ./test/el/iso-8859-7.txt
 ./test/el/utf-8.txt
 ./test/el/windows-1253.txt
 ./test/en/ascii.txt
 ./test/eo/iso-8859-3.txt
 ./test/es/iso-8859-1.txt
 ./test/es/iso-8859-15.txt
 ./test/es/utf-8.txt
 ./test/es/windows-1252.txt
 ./test/fr/iso-8859-1.txt
 ./test/fr/iso-8859-15.txt
 ./test/fr/utf-16.be
 ./test/fr/utf-32.le
 ./test/fr/windows-1252.txt
 ./test/he/iso-8859-8.txt
 ./test/he/windows-1255.txt
 ./test/hu/iso-8859-2.txt
 ./test/hu/windows-1250.txt
 ./test/ja/euc-jp.txt
 ./test/ja/iso-2022-jp.txt
 ./test/ja/shift_jis.txt
 ./test/ja/utf-16be.txt
 ./test/ja/utf-16le.txt
 ./test/ko/iso-2022-kr.txt
 ./test/ru/ibm855.txt
 ./test/ru/iso-8859-5.txt
 ./test/ru/koi8-r.txt
 ./test/ru/mac-cyrillic.txt
 ./test/ru/windows-1251.txt
 ./test/th/iso-8859-11.txt
 ./test/th/tis-620.txt
 ./test/tr/iso-8859-9.txt
 ./test/vi/viscii.txt
 ./test/vi/windows-1258.txt
 ./test/zh/big5.txt
 ./test/zh/euc-tw.txt
 ./test/zh/gb18030.txt
 ./test/zh/utf-8.txt
 ./uchardet.pc.in
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
  1998, 2001, 2005, Netscape Communications Corporation
  2011, Asias He <asias.hejun@gmail.com>
License: GPL-2+ and/or LGPL-2.1+
 FIXME

Files: ./src/nsHebrewProber.h
Copyright: 2005, the Initial Developer:
License: GPL-2+ or LGPL-2.1+ and/or MPL-1.1
 FIXME

Files: ./test/ko/utf-32.be
Copyright:   ´  ²      ®0  ¼ø      ²ä  ­m  Å´      ÓÉ  ºt       (   B   M   P   ,       B   a   s   i   c       m   u   l   t   i   l   i   n   g   u   a   l       p   l   a   n   e   )  ÅÐ   
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangGermanModel.log
Copyright: (revision 148673951)
  bec (Stadt) (revision 148716893)
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangArabicModel.log
Copyright: (revision 17217037)
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangEsperantoModel.log
Copyright: Glucksmann (revision 5792591)
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangSpanishModel.log
Copyright: SubirÃ
License: UNKNOWN
 FIXME

Files: ./test/th/utf-8.txt
Copyright: Unicode) à¸à¸·à¸­à¸¡à¸²à¸à¸£à¸à¸²à¸à¸­à¸¸à¸à¸ªà¸²à¸«à¸à¸£à¸£à¸¡à¸à¸µà¹à¸à¹à¸§à¸¢à¹à¸«à¹à¸à¸­à¸¡à¸à¸´à¸§à¹à¸à¸­à¸£à¹à¹à¸ªà¸à¸à¸à¸¥à¹à¸¥à¸°à¸à¸±à¸à¸à¸²à¸£à¸à¹à¸­à¸à¸§à¸²à¸¡à¸à¸£à¸£à¸¡à¸à¸²à¸à¸µà¹à¹à¸à¹à¹à¸à¸£à¸°à¸à¸à¸à¸²à¸£à¹à¸à¸µà¸¢à¸à¸à¸­à¸à¸
  à¸²à¸ªà¹à¸§à¸à¹à¸«à¸à¹à¹à¸à¹à¸¥à¸à¹à¸à¹à¸­à¸¢à¹à¸²à¸à¸ªà¸­à¸à¸à¸¥à¹à¸­à¸à¸à¸±à¸ à¸¢à¸¹à¸à¸´à¹à¸à¸à¸à¸£à¸°à¸à¸­à¸à¸à¹à¸§à¸¢à¸£à¸²à¸¢à¸à¸²à¸£à¸­à¸±à¸à¸à¸£à¸°à¸à¸µà¹à¹à¸ªà¸à¸à¸à¸¥à¹à¸à¹à¸¡à¸²à¸à¸à¸§à¹à¸² 100,000 à¸à¸±à¸§ à¸à¸±à¸à¸à¸²à¸à¹à¸­à¸¢à¸­à¸à¸¡à¸²à¸à¸²à¸à¸¡à¸²à¸à¸£à¸à¸²à¸à¸à¸¸à¸à¸­à¸±à¸à¸à¸£à¸°à¸ªà¸²à¸à¸¥ (Universal Character Set: UCS) à¹à¸¥à¸°à¸¡à¸µà¸à¸²à¸£à¸à¸µà¸à¸´à¸¡à¸à¹à¸¥à¸à¹à¸à¸«à¸à¸±à¸à¸ªà¸·à¸­ The Unicode Standard à¹à¸à¹à¸à¹à¸à¸à¸à¸±à¸à¸£à¸«à¸±à¸ªà¹à¸à¸·à¹à¸­à¹à¸à¹à¹à¸à¹à¸à¸£à¸²à¸¢à¸à¸²à¸£à¸­à¹à¸²à¸à¸­à¸´à¸ à¸à¸­à¸à¸à¸²à¸à¸à¸±à¹à¸à¸¢à¸±à¸à¸¡à¸µà¸à¸²à¸£à¸­à¸à¸´à¸à¸²à¸¢à¸§à¸´à¸à¸µà¸à¸²à¸£à¸à¸µà¹à¹à¸à¹à¹à¸à¹à¸²à¸£à¸«à¸±à¸ªà¹à¸¥à¸°à¸à¸²à¸£à¸à¸³à¹à¸ªà¸à¸­à¸¡à¸²à¸à¸£à¸à¸²à¸à¸à¸­à¸à¸à¸²à¸£à¹à¸à¹à¸²à¸£à¸«à¸±à¸ªà¸­à¸±à¸à¸à¸£à¸°à¸­à¸µà¸à¸à¸³à¸à¸§à¸à¸«à¸à¸¶à¹à¸ à¸à¸²à¸£à¹à¸£à¸µà¸¢à¸à¸¥à¸³à¸à¸±à¸à¸­à¸±à¸à¸©à¸£ à¸à¸à¹à¸à¸à¸à¹à¸à¸­à¸à¸à¸²à¸£à¸£à¸§à¸¡à¹à¸¥à¸°à¸à¸²à¸£à¹à¸¢à¸à¸­à¸±à¸à¸à¸£à¸° à¸£à¸§à¸¡à¹à¸à¸à¸¶à¸à¸¥à¸³à¸à¸±à¸à¸à¸²à¸£à¹à¸ªà¸à¸à¸à¸¥à¸à¸­à¸à¸­à¸±à¸à¸à¸£à¸°à¸ªà¸­à¸à¸à¸´à¸¨à¸à¸²à¸ (à¹à¸à¹à¸à¸­à¸±à¸à¸©à¸£à¸­à¸²à¸«à¸£à¸±à¸à¸«à¸£à¸·à¸­à¸­à¸±à¸à¸©à¸£à¸®à¸µà¸à¸£à¸¹à¸à¸µà¹à¹à¸à¸µà¸¢à¸à¸à¸²à¸à¸à¸§à¸²à¹à¸à¸à¹à¸²à¸¢)
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: Use secure copyright format URL.
License: UNKNOWN
 FIXME

Files: ./test/vi/utf-8.txt
Copyright: c hiá»n nay cá»§a tiáº¿ng Viá»t, sá»­
License: UNKNOWN
 FIXME

Files: ./test/fr/utf-8.txt
Copyright: changer de l'information. Il sâagit Ã©galement du codage le plus
  critures et tous les alphabets du monde.
  dans les systÃ¨mes GNU, Linux et compatibles pour gÃ©rer le plus
  e Ã
  par 82,2 % des sites web en dÃ©cembre 20141. De par sa
  pertoire universel de caractÃ¨res codÃ©s Â», initialement
  quentes), mais trÃ¨s largement rÃ©pandue depuis des dÃ©cennies.
  veloppÃ© par lâISO dans la norme internationale ISO/CEI 10646, aujourdâhui
  viation de lâanglais Universal Character Set Transformation Format -
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangFrenchModel.log
Copyright: dia:Accueil_principal (revision 115957655)
License: UNKNOWN
 FIXME

Files: ./uchardet.doap
Copyright: e, reprÃ©sentant un texte, et tente d'en dÃ©terminer le
  est compatible iconv.
  mentation actuelle peut dÃ©tecter
  tait originellement un binding en C de l'implÃ©mentation
  tection de codage, prenant une sÃ©quence
License: UNKNOWN
 FIXME

Files: ./test/tr/iso-8859-3.txt
Copyright: mparatorlu»u co»rafyas¹nda konuºulan Türkçe, dünyada en fazla
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangHungarianModel.log
Copyright: s szabadsÃ¡gharc (revision 16955214)
License: UNKNOWN
 FIXME

Files: ./test/ru/ibm866.txt
Copyright: ª®¤¨à®¢ª¨, ­® ¢á¥
  áª¨¥ á¨¬¢®«ë ¢® ¢â®à®©
License: UNKNOWN
 FIXME

Files: ./test/ko/utf-8.txt
Copyright: °, ê±°ì ì¬ì©ëì§ ìëë¤. UTF-16ê³¼ UTF-8 ì¤ ì´ë ì¸ì½ë©ì´ ë ì
  ë°©ì ì¤ íëë¡, ì¼ í°íì¨ê³¼ ë¡­ íì´í¬ê° ë§ë¤ìë¤. ë³¸ëë FSS-UTF(File System Safe UCS/Unicode Transformation Format)ë¼ë ì´ë¦ì¼ë¡ ì
  ìì ëì
  ì ì
  í
  íê¸° íë¤ê³
  íëì§ë ë¬¸ìì´ìì ì¬ì©ë ì½ë í¬ì¸í¸ì ë°ë¼ ë¬ë¼ì§ë©°, ì¤ì
  íë¤. ìë¥¼ ë¤ì´ì, U+0000ë¶í° U+007F ë²ìì ìë ASCII ë¬¸ìë¤ì UTF-8ìì 1ë°ì´í¸ë§ì¼ë¡ íìëë¤. 4ë°ì´í¸ë¡ ííëë ë¬¸ìë ëª¨ë ê¸°ë³¸ ë¤êµ­ì´ íë©´(BMP) ë°ê¹¥ì ì
License: UNKNOWN
 FIXME

Files: ./test/ko/euc-kr.smi
Copyright: °Å³ª °°Àº ¹ÙÀÌÆ®¿¡ KS X 1001À» ¹è´çÇÑ´Ù. °¢ ±ÛÀÚ´Â Çà°ú ¿­¿¡ 128À» ´õÇÑ ÄÚµå°ªÀ» »ç¿ëÇÏ¿© 2¹ÙÀÌÆ®·Î Ç¥ÇöµÈ´Ù.
  ±Ô°ÝÀÇ ¹®ÀÚ ÁýÇÕ¿¡ Æ÷ÇÔµÇÁö ¾ÊÀº ÇÑ±ÛÀ» Ç¥ÇöÇÏ´Â È®Àå ¹æ¹ýÀÌ ÀÖÁö¸¸, ´ëºÎºÐÀÇ °æ¿ì ÀÌ ¹æ¹ýÀº EUC-KR¿¡¼­ »ç¿ëµÇÁö ¾Ê°í ´ë½Å CP949¿Í °°Àº ´Ù¸¥ ¹æ¹ýÀ» »ç¿ëÇÏ¿© KS X 1001 ¹Ù±ùÀÇ Çö´ë ÇÑ±ÛÀ» Ç¥ÇöÇÑ´Ù.
License: UNKNOWN
 FIXME

Files: ./test/ja/utf-8.txt
Copyright: ¶æã«ããã¦Plan 9ã§ç¨ããã¨ã³ã³ã¼ãã¨ãã¦ãã­ãã»ãã¤ã¯ã«ããè¨­è¨æéã®ãã¨ãã±ã³ã»ãã³ãã½ã³ã«ãã£ã¦èæ¡ããã
  ã®ASCIIæå­ãç¾ããªãããã«å·¥å¤«ããã¦ãããã¨ãããUTF-FSS (File System Safe) ã¨ããããããæ§åç§°ã¯UTF-2ã
License: UNKNOWN
 FIXME

Files: ./test/ko/utf-16.le
Copyright: Æ´²  0®ø¼  ä²m­´Å  ÉÓtº  ( B M P ,   B a s i c   m u l t i l i n g u a l   p l a n e ) ÐÅ
License: UNKNOWN
 FIXME

Files: ./test/he/utf-8.txt
Copyright: ×
  ××ª×ª ××ª×× ××ª×××××ª ×××××¨ ×-ASCII. ×-IETF ××¢×××£ ××××¨××¨ ××ª UTF-8 ×××××× ×× ×¤×¨××××§×× ×××
  × ×ª××××ª ×©× 8â-bit Unicode Transformation Format ×× 8â-bit UCS Transformation Format) ××× ×§×××× ×ª×××× ××××¨× ××©×ª×
  ××××© ×××× ×¢× ××¨××¢× ××ª××, ×ª××× ××ª×. ××§×××× ×-UTF-8 ××¢×
  ××××© ××§×××× ××××
  ×× ×ª×××
License: UNKNOWN
 FIXME

Files: ./test/ar/utf-8.txt
Copyright: Ø§ÙØªÙ ØªØ³ØªØ®Ø¯Ù
  Ø§ÙÙÙØ¯ ÙØ°Ù ÙØ§ ØªØªÙØ§ÙÙ Ù
  Ø§ÙÙØºØ© Ø§ÙØ¹Ø±Ø¨ÙØ© Ø¹Ù
  Ù
  ÙÙØ¯ ØªØ³ØªØ®Ø¯Ù
  ÙØ§ÙÙÙØ±Ø¯ÙØ©. ÙØ°ÙÙ ØªØ­Øª ÙØ¸Ø§Ù
License: UNKNOWN
 FIXME

Files: ./script/BuildLangModelLogs/LangThaiModel.log
Copyright: à¸à¸µ (revision 5606447)
License: UNKNOWN
 FIXME

