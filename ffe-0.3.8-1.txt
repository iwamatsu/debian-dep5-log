Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./AUTHORS
 ./ChangeLog
 ./Makefile.am
 ./NEWS
 ./config.h.in
 ./configure.ac
 ./debian/clean
 ./debian/compat
 ./debian/control
 ./debian/doc-base
 ./debian/docs
 ./debian/info
 ./debian/patches/01-spelling-error-edianness.patch
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./doc/Makefile.am
 ./doc/stamp-vti
 ./doc/texinfo.css
 ./doc/version.texi
 ./src/Makefile.am
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./README
 ./src/anonymize.c
 ./src/endian.c
 ./src/execute.c
 ./src/ffe.c
 ./src/ffe.h
 ./src/level.c
 ./src/parserc.c
 ./src/xmalloc.c
Copyright: 2006, Timo Savinen
  2008, Timo Savinen
  2017, Timo Savinen
License: GPL-2+
 FIXME

Files: ./COPYING
 ./Makefile.in
 ./aclocal.m4
 ./doc/Makefile.in
 ./src/Makefile.in
Copyright: 1989, 1991, Free Software Foundation, Inc.
  1994-2014, Free Software Foundation, Inc.
  1996-2014, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./config/compile
 ./config/depcomp
 ./config/mdate-sh
 ./config/missing
Copyright: 1995-2014, Free Software Foundation, Inc.
  1996-2014, Free Software Foundation, Inc.
  1999-2014, Free Software Foundation, Inc.
License: GPL-2+
 FIXME

Files: ./config/config.guess
 ./config/config.sub
Copyright: 1992-2016, Free Software Foundation, Inc.
License: GPL-3
 FIXME

Files: ./doc/ffe.html
 ./doc/ffe.info
Copyright: 2014, Timo Savinen
License: UNKNOWN
 FIXME

Files: ./config/install-sh
Copyright: 1994, X Consortium
License: Expat
 FIXME

Files: ./INSTALL
Copyright: 1994-1996, 1999-2002, 2004-2013, Free Software Foundation,
License: FSFAP
 FIXME

Files: ./configure
Copyright: 1992-1996, 1998-2012, Free Software Foundation, Inc.
License: FSFUL
 FIXME

Files: ./doc/ffe.1
Copyright: NONE
License: GPL-2+
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: GPL-2+
 FIXME

Files: ./config/texinfo.tex
Copyright: 1985-1986, 1988, 1990-2006, Free
License: GPL-2+
 FIXME

Files: ./debian/changelog
Copyright: - Fix spelling mistake, ffm.texi must be ffe.texi.
License: UNKNOWN
 FIXME

Files: ./doc/ffe.texi
Copyright: @copyright{} 2014 Timo Savinen
License: UNKNOWN
 FIXME

