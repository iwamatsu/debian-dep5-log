Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./MAINTAINERS
 ./Makefile
 ./README.markdown
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/patches/debian_adapt_errormsg_on_failure_to_open_device.patch
 ./debian/patches/debian_has_recent_glibc.patch
 ./debian/patches/dont-build-backfire.patch
 ./debian/patches/fix_comment_about_realtime_group.patch
 ./debian/patches/install_hwlatdetect_into_sbindir.patch
 ./debian/patches/install_manpage_rt-migrate-test.patch
 ./debian/patches/manpage_pip_stress.patch
 ./debian/patches/series
 ./debian/rt-tests.README.Debian
 ./debian/rt-tests.doc
 ./debian/rules
 ./debian/source/format
 ./debian/source/options
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./src/arch/android/Makefile
 ./src/backfire/Makefile
 ./src/backfire/Makefile.module
 ./src/backfire/backfire.4
 ./src/backfire/sendme.8
 ./src/cyclictest/cyclictest.8
 ./src/hackbench/hackbench.8
 ./src/hackbench/hackbench.c
 ./src/hwlatdetect/hwlat.txt
 ./src/hwlatdetect/hwlatdetect.8
 ./src/include/bionic.h
 ./src/include/error.h
 ./src/include/pip_stress.h
 ./src/include/rt-get_cpu.h
 ./src/include/rt-utils.h
 ./src/pi_tests/README
 ./src/pi_tests/pi_stress.8
 ./src/pi_tests/pi_tests.spec
 ./src/pi_tests/pip_stress.8
 ./src/pmqtest/pmqtest.8
 ./src/ptsematest/ptsematest.8
 ./src/rt-migrate-test/rt-migrate-test.8
 ./src/sigwaittest/sigwaittest.8
 ./src/svsematest/svsematest.8
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./src/backfire/backfire.c
 ./src/backfire/sendme.c
 ./src/pmqtest/pmqtest.c
 ./src/ptsematest/ptsematest.c
 ./src/sigwaittest/sigwaittest.c
 ./src/svsematest/svsematest.c
Copyright: 2007, Carsten Emde <C.Emde@osadl.org>
  2009, Carsten Emde <C.Emde@osadl.org>
License: GPL-2+
 FIXME

Files: ./src/pi_tests/classic_pi.c
 ./src/pi_tests/pi_stress.c
 ./src/pi_tests/sigtest.c
Copyright: 2006-2007, Clark Williams <williams@redhat.com>
License: GPL-2+
 FIXME

Files: ./src/include/rt-sched.h
 ./src/lib/rt-sched.c
Copyright: 2014, BMW Car IT GmbH, Daniel Wagner <daniel.wagner@bmw-carit.de
  Dario Faggioli <raistlin@linux.it>, 2009, 2010
License: GPL-2+
 FIXME

Files: ./src/signaltest/signaltest.8
Copyright: NONE
License: FSFAP
 FIXME

Files: ./src/hwlatdetect/hwlatdetect.py
Copyright: 2009, Clark Williams <williams@redhat.com>
  2015, Clark Williams <williams@redhat.com>
License: GPL-2
 FIXME

Files: ./src/cyclictest/cyclictest.c
Copyright: 2005-2007, Thomas Gleixner <tglx@linutronix.de>
  2008-2012, Clark Williams <williams@redhat.com>
  2013, Clark Williams <williams@redhat.com>
  2013, John Kacur <jkacur@redhat.com>
License: GPL-2
 FIXME

Files: ./src/rt-migrate-test/rt-migrate-test.c
Copyright: 2007-2009, Steven Rostedt <srostedt@redhat.com>
License: GPL-2
 FIXME

Files: ./src/signaltest/signaltest.c
Copyright: 2007, Thomas Gleixner <tglx@linutronix.de>
License: GPL-2
 FIXME

Files: ./src/pi_tests/pip_stress.c
Copyright: 2009, John Kacur <jkacur@redhat.com>
License: GPL-2+
 FIXME

Files: ./src/pi_tests/tst-mutexpi10.c
Copyright: 2006, Free Software Foundation, Inc.
License: LGPL-2.1+
 FIXME

Files: ./src/pi_tests/classic_pi.odg
Copyright: TÓg
  
  Sce×Ç9C7É5"CÞM2dÆ¬dÈ!ó&ùqÇãçFó´7CÖæéé
  [Q-Xô»Íê<%¯G`°8ûkOæ¸
  g·üÔ°û×ð9jïf´fâ]&8öh°Ç¡:Àò:ëÀ4ziøog$"Úi
  ¤îU÷Qè×'Ñ!ÒÔ_¸Ë×AÄÈ:
  §Ê
  Üè3O£Ï¸Vúv£î¶ßAñ=·Ý:°ÜµÕ.èÄà>È­uíÁ=BèÓ6
  äîÝkN^0ÍHÏtec?	H¼+?ý®zÊ»ûîÅøx$þ:Âq®úI³ï	³³Y1:WÖ4%(#Ù,FÎf¹?KRWV3Y{&UH²|6Ê²u_ó¡Æ·e?Y(ËÖE¡Æ*/¡Æ¯Y¨.Æz¢ìÌâ5$ñóYåy:Ó´Íf6&HèRÓéT£õýZ/]ÓPh¾CÌi@­ÒpÎëÊS×Ñ¦©A9Ú[Õâ©0¸Ü1ÝH¶iù×Ër°w½,;höWö3¡Üv3î*f ÛF(_u¬¯§}`âãÃCãW4ú,®Û¢Ê§$
  î[¨)e$ÐàlÂ' j3Gê*¡ä
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./src/lib/rt-utils.c
Copyright: 2005-2007, Thomas Gleixner <tglx@linutronix.de>
  2008-2009, Clark Williams <williams@redhat.com>
  2009, Carsten Emde <carsten.emde@osadl.org>
  2010, Clark Williams <williams@redhat.com>
  2015, John Kacur <jkacur@redhat.com>
License: UNKNOWN
 FIXME

Files: ./src/cyclictest/rt_numa.h
Copyright: 2010, Clark Williams <williams@redhat.com>
  2010, John Kacur <jkacur@redhat.com>
License: UNKNOWN
 FIXME

Files: ./src/pi_tests/COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: UNKNOWN
 FIXME

Files: ./src/lib/error.c
Copyright: 2009, John Kacur <jkacur@redhat.com>
License: UNKNOWN
 FIXME

