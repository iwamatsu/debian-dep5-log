Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./.arcconfig
 ./CMakeLists.txt
 ./Mainpage.dox
 ./Messages.sh
 ./README.ARTISTS
 ./README.PACKAGERS
 ./debian/compat
 ./debian/man/kdeopt.part
 ./debian/man/palapeli.man
 ./debian/palapeli-data.install
 ./debian/palapeli.install
 ./debian/palapeli.lintian-overrides
 ./debian/palapeli.manpages
 ./debian/rules
 ./debian/source/format
 ./debian/upstream/metadata
 ./debian/upstream/signing-key.asc
 ./debian/watch
 ./doc/CMakeLists.txt
 ./doc/index.docbook
 ./libpala/CMakeLists.txt
 ./libpala/Mainpage.dox
 ./libpala/Pala/README
 ./libpala/Pala/Slicer
 ./libpala/Pala/SlicerJob
 ./libpala/Pala/SlicerMode
 ./libpala/Pala/SlicerProperty
 ./libpala/Pala/SlicerPropertySet
 ./libpala/libpala-config.cmake
 ./mime/CMakeLists.txt
 ./mime/palapeli-mimetypes.xml
 ./mime/palathumbcreator.desktop
 ./pics/CMakeLists.txt
 ./po/de/docs/palapeli/index.docbook
 ./po/de/palapeli.po
 ./po/eo/palapeli.po
 ./po/es/docs/palapeli/index.docbook
 ./po/et/docs/palapeli/index.docbook
 ./po/fr/docs/palapeli/index.docbook
 ./po/hr/palapeli.po
 ./po/it/docs/palapeli/index.docbook
 ./po/nb/palapeli.po
 ./po/nl/docs/palapeli/index.docbook
 ./po/nn/palapeli.po
 ./po/pt/docs/palapeli/index.docbook
 ./po/pt_BR/docs/palapeli/index.docbook
 ./po/ru/docs/palapeli/index.docbook
 ./po/sr/palapeli.po
 ./po/sv/docs/palapeli/index.docbook
 ./po/uk/docs/palapeli/index.docbook
 ./po/zh_CN/palapeli.po
 ./puzzles/CMakeLists.txt
 ./puzzles/default-collection.conf
 ./slicers/.krazy
 ./slicers/CMakeLists.txt
 ./slicers/goldberg/TODO
 ./slicers/goldberg/grid-cairo.svg
 ./slicers/goldberg/grid-hex.svg
 ./slicers/goldberg/grid-rotrex.svg
 ./src/CMakeLists.txt
 ./src/file-io/visual-documentation.svgz
 ./src/org.kde.palapeli.appdata.xml
 ./src/org.kde.palapeli.desktop
 ./src/palapeli.kcfg
 ./src/palapeliui.rc
 ./src/pics/CMakeLists.txt
 ./src/pics/LICENSE
 ./src/pics/README.artists
 ./src/pics/background.svg
 ./src/settings.kcfgc
 ./src/settings.ui
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./mime/thumbnail-creator.cpp
 ./slicers/slicer-jigsaw.cpp
 ./slicers/slicer-jigsaw.h
 ./slicers/slicer-rect.cpp
 ./slicers/slicer-rect.h
 ./src/config/configdialog.cpp
 ./src/config/configdialog.h
 ./src/config/configdialog_p.h
 ./src/creator/propertywidget.cpp
 ./src/creator/propertywidget.h
 ./src/creator/propertywidget_p.h
 ./src/creator/puzzlecreator.cpp
 ./src/creator/puzzlecreator.h
 ./src/creator/slicerconfwidget.cpp
 ./src/creator/slicerconfwidget.h
 ./src/creator/slicerselector.cpp
 ./src/creator/slicerselector.h
 ./src/engine/basics.h
 ./src/engine/constraintinteractor.cpp
 ./src/engine/constraintinteractor.h
 ./src/engine/constraintvisualizer.cpp
 ./src/engine/constraintvisualizer.h
 ./src/engine/interactor.cpp
 ./src/engine/interactor.h
 ./src/engine/interactormanager.cpp
 ./src/engine/interactormanager.h
 ./src/engine/interactors.cpp
 ./src/engine/interactors.h
 ./src/engine/mathtricks.h
 ./src/engine/mergegroup.h
 ./src/engine/piece_p.h
 ./src/engine/scene.cpp
 ./src/engine/scene.h
 ./src/engine/texturehelper.cpp
 ./src/engine/texturehelper.h
 ./src/engine/trigger.cpp
 ./src/engine/trigger.h
 ./src/engine/triggermapper.cpp
 ./src/engine/triggermapper.h
 ./src/engine/view.cpp
 ./src/engine/view.h
 ./src/engine/zoomwidget.cpp
 ./src/engine/zoomwidget.h
 ./src/file-io/collection-delegate.cpp
 ./src/file-io/collection-delegate.h
 ./src/file-io/collection-view.cpp
 ./src/file-io/collection-view.h
 ./src/file-io/collection.cpp
 ./src/file-io/collection.h
 ./src/file-io/collection_p.h
 ./src/file-io/components-archivestorage.cpp
 ./src/file-io/components-collectionstorage.cpp
 ./src/file-io/components-copy.cpp
 ./src/file-io/components-creationcontext.cpp
 ./src/file-io/components-directorystorage.cpp
 ./src/file-io/components-retailstorage.cpp
 ./src/file-io/components.h
 ./src/file-io/puzzle.cpp
 ./src/file-io/puzzle.h
 ./src/file-io/puzzlestructs.h
 ./src/importhelper.cpp
 ./src/importhelper.h
 ./src/main.cpp
 ./src/window/loadingwidget.cpp
 ./src/window/loadingwidget.h
 ./src/window/mainwindow.cpp
 ./src/window/mainwindow.h
 ./src/window/puzzletablewidget.cpp
 ./src/window/puzzletablewidget.h
Copyright: 2009, Stefan Majewsky <majewsky@gmx.net>
  2009-2010, Stefan Majewsky <majewsky@gmx.net>
  2009-2011, Stefan Majewsky <majewsky@gmx.net>
  2010, Stefan Majewsky <majewsky@gmx.net>
  2011, Stefan Majewsky <majewsky@gmx.net>
License: GPL-2+
 FIXME

Files: ./po/ast/palapeli.po
 ./po/da/palapeli.po
 ./po/el/palapeli.po
 ./po/en_GB/palapeli.po
 ./po/es/palapeli.po
 ./po/eu/palapeli.po
 ./po/hu/palapeli.po
 ./po/is/palapeli.po
 ./po/it/palapeli.po
 ./po/kk/palapeli.po
 ./po/km/palapeli.po
 ./po/lv/palapeli.po
 ./po/mr/palapeli.po
 ./po/nds/palapeli.po
 ./po/nl/palapeli.po
 ./po/pl/palapeli.po
 ./po/ro/palapeli.po
 ./po/ru/palapeli.po
 ./po/sl/palapeli.po
 ./po/sv/palapeli.po
 ./po/tr/palapeli.po
 ./po/ug/palapeli.po
 ./po/zh_TW/palapeli.po
Copyright: YEAR This_file_is_part_of_KDE
License: UNKNOWN
 FIXME

Files: ./libpala/slicer.cpp
 ./libpala/slicer.h
 ./libpala/slicerjob.cpp
 ./libpala/slicerjob.h
 ./libpala/slicermode.cpp
 ./libpala/slicermode.h
 ./libpala/slicerproperty.cpp
 ./libpala/slicerproperty.h
 ./libpala/slicerpropertyset.cpp
 ./libpala/slicerpropertyset.h
 ./src/config/elidinglabel.cpp
 ./src/config/elidinglabel.h
 ./src/config/mouseinputbutton_p.h
 ./src/config/triggerconfigwidget.cpp
 ./src/config/triggerconfigwidget.h
 ./src/config/triggerlistview.cpp
 ./src/config/triggerlistview.h
 ./src/config/triggerlistview_p.h
Copyright: 2009, Stefan Majewsky <majewsky@gmx.net>
  2009-2010, Stefan Majewsky <majewsky@gmx.net>
  2010, Stefan Majewsky <majewsky@gmx.net>
License: LGPL-2+
 FIXME

Files: ./slicers/goldberg/goldberg-engine.h
 ./slicers/goldberg/grid-cairo.cpp
 ./slicers/goldberg/grid-hex.cpp
 ./slicers/goldberg/grid-preset.cpp
 ./slicers/goldberg/grid-rect.cpp
 ./slicers/goldberg/grid-rotrex.cpp
 ./slicers/goldberg/grid-voronoi.cpp
 ./slicers/goldberg/pointfinder.cpp
 ./slicers/goldberg/pointfinder.h
 ./slicers/goldberg/utilities.cpp
 ./slicers/goldberg/utilities.h
 ./src/engine/puzzlepreview.cpp
 ./src/engine/puzzlepreview.h
Copyright: 2010, Johannes Loehnert <loehnert.kde@gmx.de>
License: GPL-2+
 FIXME

Files: ./po/ca/palapeli.po
 ./po/ca@valencia/palapeli.po
 ./po/et/palapeli.po
 ./po/fi/palapeli.po
 ./po/ga/palapeli.po
 ./po/lt/palapeli.po
 ./po/uk/palapeli.po
Copyright: 2008, This_file_is_part_of_KDE
  2008-2017, This_file_is_part_of_KDE
  2009, This_file_is_part_of_KDE
  2009-2017, This_file_is_part_of_KDE
  2010, This_file_is_part_of_KDE
License: UNKNOWN
 FIXME

Files: ./src/engine/mergegroup.cpp
 ./src/engine/piece.cpp
 ./src/engine/piece.h
 ./src/engine/piecevisuals.cpp
 ./src/engine/piecevisuals.h
Copyright: 2009-2010, Stefan Majewsky <majewsky@gmx.net>
  2009-2011, Stefan Majewsky <majewsky@gmx.net>
  2010, Johannes LÃ¶hnert <loehnert.kde@gmx.de>
  2010, Stefan Majewsky <majewsky@gmx.net>
License: GPL-2+
 FIXME

Files: ./puzzles/cincinnati-bridge.jpg
 ./puzzles/citrus-fruits.jpg
 ./puzzles/panther-chameleon-female.jpg
Copyright:  ® ² · ¼ Á Æ Ë Ð Õ Û à å ë ð ö û
  ±¹ÁÉÑÙáéòú
License: UNKNOWN
 FIXME

Files: ./src/window/pieceholder.cpp
 ./src/window/pieceholder.h
Copyright: 2014, Ian Wadham <iandw.au@gmail.com>
License: GPL-2+
 FIXME

Files: ./src/engine/gameplay.cpp
 ./src/engine/gameplay.h
Copyright: 2009, Stefan Majewsky <majewsky@gmx.net>
  2014, Ian Wadham <iandw.au@gmail.com>
License: GPL-2+
 FIXME

Files: ./slicers/goldberg/grid.h
 ./slicers/goldberg/slicer-goldberg.h
Copyright: 2009, Stefan Majewsky <majewsky@gmx.net>
  2010, Johannes Loehnert <loehnert.kde@gmx.de>
  2010, Stefan Majewsky <majewsky@gmx.net>
License: GPL-2+
 FIXME

Files: ./slicers/goldberg/goldberg-engine.cpp
 ./slicers/goldberg/slicer-goldberg.cpp
Copyright: 2009, Stefan Majewsky <majewsky@gmx.net>
  2010, Johannes Loehnert <loehnert.kde@gmx.net>
License: GPL-2+
 FIXME

Files: ./src/config/mouseinputbutton.cpp
 ./src/config/mouseinputbutton.h
Copyright: 2009, Chani Armitage <chani@kde.org>
  2010, Stefan Majewsky <majewsky@gmx.net>
License: LGPL-2+
 FIXME

Files: ./COPYING.DOC
 ./COPYING.LIB
Copyright: 1991, Free Software Foundation, Inc.
  2000-2002, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./slicers/goldberg/README
Copyright: 2010, Johannes Loehnert (loehnert.kde@gmx.net)
License: GPL-2+
 FIXME

Files: ./doc/puzzletable1.png
Copyright:  NâN³FñÈ¸½^ª0«FpÕ»P}cAP+¥.
  ?Ú¡"g´¤ÌÊ
  _ó?¾fR«Tfk-ÜùN¦¤óY
  ¨oz4×f)+¹Ze:¾Fr4Ij-JÄ±ø-qD¬Øta|ks¡¤×p
  µ×¸¿Ãi·P»ÓVRdÑ(#ÙÚEËÝÉp00¬¡§aIy½JS·Ü)Ó[_(YÊFD²*®Cf/ÎÿÍAB(úÚUD<0U.¿çnY0a¿åm;ì0 ÊbowdÓ=·îºt	Å0èOP-P¶Ômõf[+oû½µàÔ¬ÎÎ!,ºé ¸Vú'MuüðÂÏ3¤?í©/é­Yä¹	!,¨_@¨{½vß#1Û
  ²ïµÈã t  ýÎÏøVC­,h@ @2@¨M
  %ûÐðHnà	ö
  2"âxpk;Q$UÚºoG
  56¶Á¸)ÅzÅ1ÖuîÈ×jd·h·Ç÷à£bÍ<m=i.B ù+I²Ä§×è1Ó­§43Éä:t¡:µ)¹*XÔÃ9Úp@^Ùm·w#|k9}A°ñ:rOG}Èý¤Õ¾Ç<ÂxÐxÄ¬­¬}oô*áHÝÈ#÷©®ªa½c¹¾¾NNéÍxSÃ#ÃaHcØ@¨Xxd»Þ&aëÇ¼ØNÎãX÷ï½Åiãðßz])5rµ¥RØ±FóåÕè^pgÕ¨»9Õ0­Ñ+
  5LÌØ¹ZÇz:ä+50Lz³-Lgz¶Mc+Bi&^ûFyYâ|«a­¶v4Ý ÂÖ+)"
  5Öi¦"®µf¨ çðÖ­EìzÛTÅmà»«¶(=%O?leÛ~ð:ÊºàLh{Í"$íaiëM
  6m½Cnâ´®v/#Â
  9|,8Iþú
  F-«g«
  Lé¢Ò
  Q!W®®®O»12×5Qlm0F¯°Ö;Ze|RuñlÌµmHù¸yÀb2Ky¦!M#»c²
  RÌÐÀÀò´ÈW"Z£8A]]ríÈ ç	<g§Mîc±v:®Õ¸d
  Só
  W
  W;ù=Äkæ	;-÷ÁXôðâíYÍKý`Ñ4d(ÔìX^Ù`¤6ÿe¸ùbn%¼f«º%µI.;ÂÂð[M¼E>Ç÷¥p
  XEXÕY	¬ÖáÖG­6Ê66õ6ÏlUm¹¶¶ÓvZvñv§ì^ÙØóíÛí,¶9rD]
  c
  gÒkybú{ïvÑ±jç$¯¹Ò$ç	"ÞZe¸Q5Íf'')E@ì²ì5±Ö7þo8ÿ­øµ¤ûYØàú¤=§Ýÿ³¿Ôû÷¿ùi{<­b:Ø¢ì)Ì[¸KBÔ8	<0A¶»²³
  j×­ÁØ:íª³^¬	ù ß¼dõ§ì´tÆè¦Kå·EåÍ[ º7mØ4
  nöPqqëÃ´4ÙúÎPwÕQöç£»¿ä2ZQØm«6sÓ7ç»§y;K÷Lk¼(Êñh²Ï d_Éç²Î*
  qéWÝå#º?ªÀ=GÌzgl&sÝnwÓ!GÙOÃº
  |Ê{N~^>­4Ý4c4s~Öyöîõ/¦^&¿ËÿSâÏW¯~ýËö¯ùàù©×ü×Ë½ysâ­ñÛÞ
  æÒÕ·|é/7ZIÌÄÛL»ãÁDR²9±ä¯næÆðèªøËËï_MGÓ³úi¯C×?Ã*ò¿ÿÏ·ý7£g
  õ
  îWä/H®8©TÝb÷=ßó³ÝÖñßñë¾éÿÌgÙ
  ²k"¡Ï}®A *ßp&%xÄÁ7êH±
  ÜôkÇ¥î+ER)!7÷È)uh±'ókÎ,/E&9Ø9D¤ÂßL?5HYàë¨`QìÔÍÍk/=¹³³3­mJ½¨Ã{ðU|×ò£Ñ]	1i¾ÈQ5/¼4ßlîbÓ¤¹E¿Pâû©Éà'Köñ7½¡×ð)î(«,Ô}±{Öy%N§ì4:§
  ¡«½öqb°B
  ¥wpä¨T5>9¬hve½;
  ¦;ÐÆMH¼qIOµ`Ö"6Qà8ÁkàÁ×#bC4
  ¨ºSGoÀpïýÑýïýÐÕë´zòåÇkå¦'K#äVK)	
  «{
  ®rg)Êæóý=äºÌ2³EC-D3VC(R­fc9	Ãjsô·sÇ-bÔQ&®>·¥ (^P»¹²£±r7ÜöÍJ--Cc
  ³n§¿Zm¬ØFp'K´^³jê¥8$*R@I:oS&Ç1æ#:ÝA»³·BÎw$ÖI
  µ!òèáG.¯.:Ó!æµ#:ÙmwE	eH¨ Ç
  »¥©x$ÑÞ³Ö<#R	Mo
  À?
  Å|qÃ¼Ä:îf½¥7%>g±Z°|?ì<8{ÈFØg¬Æ¨Ì&çAHß¤Öj±<Fäe=BwÍF¬ê	8£¡Ùè»Ïi²CõÀ#MØ¢A¤ªá¦klr¸Þ±ÅVÌx££<R)P<ÆVÅFÌÜ	¢´²Tèà£¸@eQ¬¯#©ZíV»Âî¢mE6:75<S%
  ÏÂÄ¢aµÈª#QkÓðïêIÚX[Ý¤fGØ[¬IÛÐúý3­Sà³_reÅ%h¯¯ÐíÎQt½ (AWMV9©ð?óyäò·þµµî¼&E(éE¢9EèhËè,±£i¸ªP-Z½ÆÕ¥î(¨ñ|Õ·#Úh¡!.ñÌÍ¬GLÕ½ZÓp{ûÒ4§Ûf2)Ç#¤ûPc-éNÑz<Ë¶+±`XR¬ÿÅ{<@:ôËßc'P»NyT·C¶Ìã¿6Û­¥µ4?>Piè)_þôµßþmoýGþ6Øyé¤Fø4
  ÓlÉYÞ²¸!©{òrÒ ¥[È
  ÔuIGÿaæY»hBñWþ¦;¨Fßgcôª]àfE¶KÇßº¾ºðC/pÉÍBº'"rÏz
  ÖêvÚÜ¹MIk#AÛ»K
  Ùò"65«eºZÍñÙÜ÷³?ÿËîÇ?ñ¿þ·¿1[þþWÿ{Âê¶¶õª_§Ñüs?Õïu1zD¿Dõnúf²
  ß-Ö
  áÉôt»]$é¤<rF À29ÎG8Po<â·RLÏ<ÙGÝï
  æFB5ªçÏÎÏ
  çW76*Ã¨
  ç«õ£O3Â»%s	J©}E
  óÍnG
  üT[5Rm_ÝV#_³·fñ0÷ððÛ#­µ
  üÓúßµ~o
License: UNKNOWN
 FIXME

Files: ./pics/16-apps-palapeli.png
Copyright:  d@>Gp]YW¨ö[Ö>³l2Xý08á,Eú;rÉdM$ë6(_b~À³ùøÏl
  £Ok¤ÁYU«üù®
  ¥ËômÝ    IEND®B`
  È¥¤K (VªÐÄÊCZPï[u²L&ÿ)½c¶ÆnÂTÁzwÚë,82*ºÏJ
License: UNKNOWN
 FIXME

Files: ./src/pics/Time-For-Lunch-2.jpg
Copyright:  s¹£QjØ
  ÔÞ=öÖ
  xÀJong/¬JÐª3ÿ om|gUÖBÚ¥çP¦ñE¾»µZ!n­ñq©7ö­ë#H1÷ë ÐÑx®þ",£ñ¦ÔµvÔ{b¸êºò<êFsÐÜçR­Ð v_â	QhæU^Kkö4Hl@²S¨ÛÉËIü,£-b>IÄÝµSBZº@%£m¬H=-V£ED21º}9Õæ2
  1É|Ö7±®àØáá¶B¡$¶R47×ZÌåeÊ³®nöÑlhNkù¯z
  P
  W'ºiD¤üÅZ
  uWÈC Ö±èñUã¢VKXhª7£&7ÛQHÃÐè}ôµfñ§TT) ÜUÂÎ/xÉÚá=ý$w"³éN¡sF¢Ày×NGÎ­à®
  DBßsÌüèRA©VKûS
  ]ýêûÌÚWÿ Q¢ÓÃmö:u¥cpF;â0¬J|¼Ö»¹Å_Ä2¥^
  °Àqnhû»Ã¸,EI:å;U9AU#qÛ_÷þùPÅ.`
  ±®ækÙIí¥)Ö¨:®VPÃ{z-¶¿s»ÈTh¶ð"?ôÔ°Ì|øHOý4À¶¨"õbUá_ºÍ[­Çö
  µ»ï}y_SZ
  º­ùU¶
  íCko¹«$Ku¥i@Q
  î-TÞ+%lK¼r,²|9vK)!If×Më°xi±hÐÌò±
  õ6º¿üêâhdOBÅÀÙ #Ö:§/
  úßJ?ì-¬ncÚ³¸W0,Jg¾C}ÀÞ´ãÉ,bH[:ôË¾
  ûWÊãÕ®H¦5OéDn
License: UNKNOWN
 FIXME

Files: ./puzzles/european-honey-bee.jpg
Copyright:  ® ² · ¼ Á Æ Ë Ð Õ Û à å ë ð ö û
  ±¹ÁÉÑÙáéòú
  1998, Hewlett-Packard Company  desc       sRGB IEC61966-2.1           sRGB IEC61966-2.1                                                  XYZ       óQ    ÌXYZ                 XYZ       o¢  8õ  XYZ       b  ·
License: UNKNOWN
 FIXME

Files: ./pics/24-apps-palapeli.png
Copyright: ·?v¿~äýS¼rYøú7~&îmÇl¢¸£Eg¸XÛ@
  	ëL´ß½£ }{]zG¢Iâ®´ÙÚ¼PV2fõÛ
  l¿ýúÏ:9~ìÄß ´UÍµqµÁÿÿ}«:ÛÍq    IEND®B`
  ²ð¦coª6SÁL<HkÅ!®kÆH4%ÀÓk ».xÒÓ¨7tÉCÈ¦²åZ7Ý|u¯¸áo½aÞÒì[ª)A5WÐp
  µÞ
  Òíß¿µ9ø©wÐ~qJÏÉ'nÀÌEOô
License: UNKNOWN
 FIXME

Files: ./pics/48-apps-palapeli.png
Copyright: l&êaw¤©Ù2bÉ¨I#ít%m]L=(lÎN¸ðJêì·çÖbL|º®À÷î¹é¶dn<0bÿ¹7¦_N§ÃNÝðéè
  -)$ã>vPx!;Ú3fw;yópá¯Ýøu9½7±êÂßùÜéìàñ[ýÞ3ß4(,VýPh@4P%î0u¿FÌötà¬ÒBî¢÷Ï¾üÀ'ä|s 7FñäÏ,°t:EÃ þpÅ}ºõ¿ùhÉ9qw6qqpnË²¢Ë"`e4SXË
  4×ÿjr~ÌUM
  R:    IEND®B`
  cÅ{ROÃ@QF m_¿ËøÑ0Ì¥:xà*Êy¨T*gø]É¹;^jÿÈåë ¸ÚÂ£M"&o@J
  wây î2t%5n|Î§zÀVÖÌ¥Ø¶
  ¼±$béülgÎ]¹nL²SÊ&7 ®
  î
License: UNKNOWN
 FIXME

Files: ./pics/64-mimetypes-application-x-palapeli.png
Copyright: ßØÒ?¸¹
  ¥QúQ$TE¢ZHE
  YÀIïYûû>l¼4þ1WXÜ¡+*s´¦õúß§®$àqCfwÏñ0tbScE¨*ç>LÓD×eKÁNYBÜ?0b½7ÿìs IÙæxíÝRÍ³=üÇþI¬YÂÎL¥bÅM¨ºBÀev· ?×®ÉÎÐÉÒ²äõÕ-	ñ
License: UNKNOWN
 FIXME

Files: ./pics/24-mimetypes-application-x-palapeli.png
Copyright: I^_eå©sX¿ë!ÀÅï×paâ0N?ßÕ
  ì'}ö½ï½of¢ TÕS	Ô Úhúõý&qÿÆPÃSétúý¡¡¡>@Tr
  9ç
  AÝ!LôÅl£T*I*ÇÔÔÔ
  ÔKäJ·>¦-ð@Á{'uùìËÎ!Z,±¸¸xkllìz,å¶÷ c/0ÆdY
  îKè8{¢_þp,ËÀk/ÍZìßøËÑDL²Öqn´Wn°´	c&R#6ûMû~#Rç.Þ;fÄBô@í4}qi~µ Ã
License: UNKNOWN
 FIXME

Files: ./puzzles/castle-maintenon.jpg
Copyright: RÇîêê÷8l­û©AÌJ»vÀÒ¯¥ÃÙBãwþxºg!è¼yq:
  ,Ñg83PeEH^O
  Ã JVq
  @äSá	$23'èJ×1
  ¬¸BåÈdÃ
  Ò¯;¥ÇE&k¼÷ñºg ¼mq;Ï1Ü]àW®j­ðöÍØ.Ö0
  ã¤hÎû­n%?ë	÷(ÇS<¾3}Hà?ý-Q¥òß7z}ÚHÇÒÞGbt}£Åô0ÿàÒ®D°1u
License: UNKNOWN
 FIXME

Files: ./src/pics/Rear-Admiral-Diplomat-1.jpg
Copyright: lu¶ã
  Æ"&sÆz÷§I+Ó#mFÆ?s±8éÊ°+c¿ûÒ<²<tFØßâ½+*EXÆä°´²dR0GrEæ¸mHÜ`Sª2úwþ}(Ë²
  â$
  ²#ÆZc+Ê»jéíW_;,B6`]¶
  c$}ÞÆ´ $¶ HUÁÁÍHË	~fÎ*PË¼9$Á]Ñ¿µy
  ÷«e)."HÎÈ$ËeÔmp{ºÝÒÊúâa³
  ÙÎX´zzÆqLÉDÄÃq(å=jKu0Æ<â
  ÍeúÂ`ÎÙ# ùö5¢WS#qZÕÃÄEÅ»@Û¶ýGÓÓÇ²OÐ[AÕ£aÈÀÆû»W½GÓ×6pý<º8ê ËÏïJô{«T_ÀXÁ|õeG¸Îõw©ªÝÜðm&ud@ ÆØõÇÛô­ó)cúx-¼Ð3sª2ß~|W×ñ»Ëyí%0ªb
  ¾âv
  ÝF@Â=éÐÂÑÄ%(¡ý(ñö'Þ©={*.QwB»gäöªiSð=ÔzºêÜp±RÎ@Ò7
  )Ë!É-íD¼Hå:®ò`¶*dË9Y*¾)|)aÛÜô;íVmñ¢6ldäg4èBÊ@ßÚ¦¶
  éÞSì;U:nçTõ
  $dáI?Øÿ Jêú¤*c[Û þu/LHúØÒ$Y¤
  2A¨eT##=Å=d D
  5a&H
  9X
  D·6**7ß5Á?ÒzsÀHêäýøéûQêá[39Pò
  J¦H
  PE-ÛN´Ñ'*¯l÷aííMuuc
  QÄ [;³
  Yf4×Þ(`ÂD|ê8ì?±þõxõ	òãbH[SÌj;ä'(mD§[.°Bí¤veîäþ¥Ê {
  w°­#1âÜ6Ê£¢Ó%@¼
  [Å´úæØ³wsâ§R7SÝºð"ÀXP÷$U |Jûi:1ÔºüÅ XK!/ÌÛmÚ¦xÊ«(ræ½³2"Sw$È¡@Òä±-²çÑ4LJÇ$µ:ÊN;Êa ÅÔÝ	ëAÅ|ZÚ¤	üI
  ÂQË wÏ½-RªÓ
  B F»»1*°®ÊÓ
  ÈF9Wþb
  b]
  ï9ØèSàà ©à1¬ÛS¸&¹ÄvÓ¶õ¤ªCù8;ïÒ,ém¬JÙ²Kv5*	BªXô,±²Èú1°&ÓôÌEj5HdsUF;ÓHd
  ààG=3ßK8×2Æ»cïër#p3¯	ôá	Ü7Å=DDeGþÃea
  ¥à(9x'Rgæ¯×³çe|{Råâ,§	#vÁÚ+i!eË©ÉVìj¡ë©¢¹·mz²5®äÖ¥j¡æ9Û|t¬Õ¸GÆñ³1'§ó«=5
   äByN|c½kÙ%á»h$µfhÇ1PHcóIõe
  ¢ï¡Gozi-pxJs
  ¤o,;Ô·rýV×vÐ$
  ¹ýi3qÃ²ÛÇµX±m³Ã,ÍÃDcö¯ZËqÇ5ÈÖÝ±íPÛHÈd òØÏÍ>ÑÜÅ
  ÁrW
  Ãá Ã)Æôät}2+`ÛÍe4:"[©-¾)oÇIeRr®ãý¨áT¨
  Çc5Yíïì5ð£F"Ë*+esT£vUy4ó¶Ù5ó~ê3Ç
  ÉÆõ=Ýôð @²·)ó]!£ü
  Ê
  Ìd't'
  Îì|Ò!GàÉrÄùcÚ
  Òóþ
  Ögg$X!ÎÀîM	*ÈÈ®ìxÅºÅ-á»§¦1ªVÒ¬î¢@Mc?­Ku¬`4®è¹fEØf¡5$Ä"ntOÍzy£ ºöÛ;,Ípá!MjXå»|SAWU
  Ö1Ç[ùµ(ÜF§aûÖ+ÏvDCmÒ¨_ðbR8i3µ¸2³<6ìSí±ÌQB p·¹©uan§
  Ý$oÁvc2&MÃ @Ú®¸h-ÄÇ¨ÎàR¤z
  ÝIs´rhÊ¶ìÕ©Y°Ú)÷
  ßÀ6PÄ¹$wÞÆ-ÕÚI;ÙÅ"Fi@Hwj0
  à¹ñ³Ñì@ûUö×
  äW´¼~.âÃ¹èã·Ãz¡bÝhx[pó¬¿Õ¬Òt*®¾Gô®G#¬©&K
  åq­÷½2²Çp%ÇðãSö/,²<qÌ®Èªr¡>õ£­
  íK[¨.äÂþâÈ4°?æªµÚFÜh~×ûÐú"ö0cKQÊWõïWÞQD(ÝÈ(6nß¹®gÖ
  ðÄÚ-íS©óÓÚµ&n)6qÍ!kÒ$*13db:ÒåiÀmOÜFsncMÌel½XüÒIú¤·f'ûÓ£$TPÕ
  ÷
  ù¡lrûau#º	á9 
  ýV5ã=ÐTá	Ò&JæÏgñ¼<p4ô>qÌhîl@D¼`s6rÈ©
License: UNKNOWN
 FIXME

Files: ./pics/64-apps-palapeli.png
Copyright: .²Oöw;ÿtU/¦½ã×è¥
  ¨¬e
  9'»î<¤
  BW^±öé+mï±¤ïw,ÍïÁ¾ Ûp.ü&+QDûZ¡Û·
  F°FCeAcÑR*ú3·í]ËÊW×Âß6	¨¾@µ,qöÒ
  Hli©Ãª1~¹ÎP©3
  RÂP6Ýçp
  TÚÓ)%$0ïD l¿B£È»0y?ÄqÃ§Kwl 'Ö×µJ¥±·.
  d u"à`où¼
  q¶<Á>ýUún1Äïóh@ÛfãmX¹@hèy-a6Ë·GhÛßO¢ãÈ3`/R-ËßÙ²uÅNBÈ;XëÊ±lLU(3ñAJ¤èøÛ¥Jòh&!Ä0;¯ª¯>]}ìU2ï@ORxà¥ô_u¹ýÐb(|¼7×" 0Æ®R6Ì¼ êÍßÐÒ
  {Î,2'ö1©e,Qbå´lË¡;õ6°~´¡'O
  |x'ÚH«¾ý½Õ%yçáiá~ ÈG!
  ¥vXBtÍ¯7hÄ8£@oBøÝð{]­µÒÚÎ°:øåa]»}.O¾psïüP¥ÄÿmÆÆò)UþÆîçÿJðñCj¶Ù##V]å¡=	©ä¦íÛo¹Àè«.=ßúXÁ¿;5"ýª°ÚÈ¾=ÙÞªÆB¶ °<m¶¢K,í#®-¬Üº¥ð-2B
  ´íÍOÁ,±³Ab-I$°$[ßË
  ¹J529¦-´ÓÙ0¥h§ùþ9WÛ÷
  Í:R_È»9¥?^²¼Ô1Ä.ü
  Óú{z
  ãÞ7ÍäqúDuëmÛ×]EÙ
  ê;«ox«à5U
License: UNKNOWN
 FIXME

Files: ./pics/128-apps-palapeli.png
Copyright: ¿|è­?ÌZÔN/z[W¨6ØtSx(AV!;@Û
  urÈs	¹p
  ²W©DMØ
License: UNKNOWN
 FIXME

Files: ./po/fr/docs/palapeli/puzzletable1.png
Copyright: ÌôÆyú´M$5WÂåh#¨x¥¢Dafàa:#
  *"´¿ÛÉz}-H¢(¢ï¸,.5xÏ»³ô_Ä´èÁÇ?óum¢ªÏç©¥'?óìÂ^´+?N5ã2½g
  YGøê±ÏxæòÌ1Á4æf[ÂÞ=8{~={GYåYÏ~ÇN§R©rìØ9J¥<ÁF«ÁJ}¶pæü÷ñ7N:öWNsãD@6£ÉeRÒpbäØØ;5F.A×TjÅæËÊÖ÷»Ç£®P I'ãQÓc­+k^/|ñkèeÆøÀÃ´Çê%ß¬¼!Èg:ýG¿è/
  ª
  µk3 Û
  Ãþé¬´º÷12J¢ÐeÏ"ÍL¦ÂÄh±©N_à
  ÄÎ×ÐVôx×û
  æÊÌÊ¬Ìc¾qç{îÏ~¸QYY¼Çú|âû¬³÷ÚûìõÛkÚâß÷5ùc7fÈ÷¸íÿ|ÐxEg±î¨í7W>£ÒÄW
  òù	Ô$¡Xt«	'ÐYV¹vz/öM×E b©×À0-ûG¨
License: UNKNOWN
 FIXME

Files: ./src/pics/gon-defends-the-critters-1.jpg
Copyright: X&P37í^b|¨,=h#GsdPaS~DÒ:oc]Y$s!B¹ËEjAT±1§°Í¦ÂñûaÒ| Q,Ë +ê9¹¡e¶ÁHºÙ
  ã­*i
  
  4!¬,
  6¥H@çE_
  7­£cÀG*Ýñ0¤Ã£æñÿ ëûÖt³`£ÓÄB9
  B}F»Ñ*
  [Y1fÔ?µ+@HØã5
  a$dïcù»ßæ³å¡Oñÿ Ôuõ¥Å	µÖµM
  ilXê)¬;Ò[v%ô$?6%4f£sÍhf*XÖdò1rCû
  i¸0NºRærÍõ±÷¤ùµ®iã&Ke¿åë²pÂÞnüë3Ç*ÃÄPwçVàñ	&Ü-²°¸e^õ¸ÚØ
  s¶GH¾£Mù/
  '²Hu±|Ö¡ÙwïY¸?hæ	¯°¹¤x
  dUù­ÂÊma] o§­pHÇ¨f·zLá
  ¥`¬°M
  YH uÔZ¨Á%ØÒÐ|9Eò ,Àn@©ð1ñ[<±>µjO>+
  ¬£:èh4¸hWÂ,³óOÒ=:úÐ49¤2bÄêIÞ§0[`E>&$ ÄÞ¢L
  ­°«r	c¢ô·:/®ÇÎÕKLx²E1Æ-=Oô®cWHsÂâü»U
  ®Çe>`-MZÈÌl
  ·Z- ²ø'V',Áí¸æ)ë-eIE¹%%86¿­FøÜ^ÌjâL¦ÅkóUjÓób­Ò¹»øó[M
  ·½
  ¸¦´c$ÕOñ9ÿ §ûÖ?Ål{
  ¿ÅD¤û@"ð¿f[Lh$ÆJSC:µê¬b¬{õµMÂ$$uQq}nwµy[³GþEâCN,¥EÝº
  Ê0¾Vß¥j§¼7`ÐF5Ê.HµÎÄÖ~>ÏvkKàód-~QÊªÅÆ®G;TJÙ]£1£@Àê}ö­$êVT
  ÐV´¬¢Ë 
  ÔuçUø¨VÝGå?Òõ*ïñUaà¨`>®a¸4ÜrCÏ½uUËEÃ*8Å#ùü­ÓÓ¥z,]¬®¨YÅs·!E4)ÙÍj+Ø¶ÌûÐâZ¡NA§,FÒÁo­M;J¥ôÊ
  ÕÎÀv©q½Ú18n>J]Q¸éZÎ9íROÿ NaOâ_
  ÜÎvª¼	9¡÷4¸wW>M9V*fÊ!ÃWÑ=þ¥;Î¢©
  ÜÞ©Åz/&}xØ}j}énNJÅRãf·½WÏ¤p¯æ
  ß-í¡ö­xÝ09TJ$øO
  æ@1ôÿ 5?f®~{ØvA}
  îÈYÁ
  ñ±ùA¹çj	í±´bQ
  ñ¯AT[E¨gC9<É¦g
  ý=½jèBxÖ3ÀÃüFÐÛCÂs	<BÖ&£ÄHÒLY½JÔàBm,Û{tMÿ D§lÚ@¨¡)"öéC<§!ËµJe/!'rv®
License: UNKNOWN
 FIXME

Files: ./src/pics/mahogany-handjob-1.jpg
Copyright: ÇX¢Â	
  FÈ7Þ®pN­X%¯*è6"©j7CÌwÄ¶?¤NC
  Sµö´k5ÞaRM¼ÃBOJEi¹
  uH»7ä/ÕÏû	)	}L:[ûÏ37Ï/Âý1µ²Á)õÌQéS¯ÅªÐ¥'{-ÏÅ¡ #!®1réQ8Ñ~ÐÉ7@NmÇHêò7[³ïÅy~L,¨¬ûEKYP{i
  ³©E[3N¬IþÃKÉ0R.FùxÎ$z?øøÏcê?0SáU­f~äs¼½ö÷oø¯È×üGC6~ñô¯ûÌ9?r&~@,XÜmh""mg,ÖÌÌ~ÂE%/úe	ygcÄ~Ó9>ÚHCÞ¹	ÿ Q3Rº,bÞõS91¨!g
  #MNÊh,ûKþc1n·§HLy¶0@GÜË¢2r¦	S*(­"ò¨Vz¦³¶ríÖæäEIÈúï~Ò¸¢*úFç¹!Rß~ò|'#éÌBÊ<UÔ3
  g¡øNÓWêÔýÌiUDØîÞb;è#rrÿ I­Ý(ûÄÇbÍÐãPr;
  õÕ}ÌÖÂrÇMMÔ
  «4%aqänpØ:*ó
  H¯N¾d¼F
  þ-9ÇäAhó7eÎÄ=".Ò-<KM%Ýéµ¾Ñ?Qh·11öØèí#*
  ¡÷âsµ+%¹í*e¿»'«#íÞ=@ô¨¿DyHà%èÕË÷fÿ 
  ªÔ{Då@òLjw²u~óbg4Ì	ó
  «ûÈW¼ê 
  ¹Øã0§(_Ý¿y:PQà­P << ä
  ÓÆYEó)$û
  Ø¦°ÔßÚ`@Æÿ Þ4«
  åáç
  ûÃj²»2FÀÁàø±éRXjêºs1"Ñ-å:c3íBÚÿ X*·Ì©Ù(h=,LbOPgÓB> ]y³Q­ù8ó^íGo¾P[F0Vµ?ylÕèâ¯ü[N÷­a:ûÂ
License: UNKNOWN
 FIXME

Files: ./pics/128-mimetypes-application-x-palapeli.png
Copyright: $ê)Þ¼;ï$¸"hæ5)Øìçwá>¹3
  *ª
  -û82fc&´¬ÛÃ¾æPAfp4wxÙÔFn
  3¨¥èõÍ¸åO3ø¹ßÅcj¸þ8r2
  9j`ÍàÏBËÝf¶
  K
  Sv¢¨¯çmí>ÿË×Øq(Åómûø·W"øÒ[pÑ
  XªèÕP(@?çñÇdBà/ÿþ ®¦ÐV0DVáØHÛÈ2Pàºÿ½Ç5ÃGÏoåãþnoÍ¿nê5ÉSÁ`I¦>;ã?''plß [äB_îÔ+~ÖÉ[|öbO6á#Áæñ]SøºæÍn±´<¼ey:÷½eWT	@_^W]!¼dºàÃ^°þ:`¦oª)ß²0tpÎ
  a"fW _¹ïÕSz/g|.+L7ýøîúEÐu¹áªN|þÃhîjá
  nðè¶1Ù0
  ræø.('ÌêÞÏø<«á<?[àk½¬0~ÒÔÔp_%¬
  òïê+ÔìÎI:ã'Çsf_
  +Às$ÃýzáIöÚ
  ¡ôÐÉ¢º}=
  ³
  ãzåÏâá«H?3òý.ÛÐÏÂê¦"íç{;FÚ_ðu 1ü9Çé×
  òyn!3­²Z|ÍLÀOEHFãÅÇV@(èy&GZe»léë¿Ì­3ð²9U
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./pics/48-mimetypes-application-x-palapeli.png
Copyright: -Äy7tÛB·¥Ûn,íÎ¶;÷yüî7vÚ¡A0ýº_Ïsîýý¾÷ìjRJ|E#=Gàsþ($ø$aUð* £úúNDzÙ!¾Vµ¼IÚG"ÕñÉ4G:AÐK5GI,ëy»l.¡þ¯k+/µ,óZú¾i¹¯µ¨ª
  xìÅ,g%bs¢ðesîú
  Î0ÿ¬¼
License: UNKNOWN
 FIXME

Files: ./po/gl/palapeli.po
Copyright: <xosecalvo@gmail.com>, 2012.
  YEAR This_file_is_part_of_KDE
License: UNKNOWN
 FIXME

Files: ./pics/32-mimetypes-application-x-palapeli.png
Copyright: A<Ìê
  z·=Ï~ïAsCÀL8!ºdÿ/ÆîT<ÛtÀ$7±¤àê^z½N¹æÐ7ãñ
  Î2=ðìönØ´®Èëæ¨Ì$Qb"ùª
  smlÍ
  ¤^þ)­~
  ²mVuäpsBÉÃ+æÍù»gÐÅyÃº l-YÛ³ýÅø­¢eôòê] %ý]-kV,¾ÒòñyðÖ
  ðcðEÃÅLDiY%Øº¾gÞø/Ñó»¹·Z­~ø
License: UNKNOWN
 FIXME

Files: ./COPYING
Copyright: 1989, 1991, Free Software Foundation, Inc.,
License: UNKNOWN
 FIXME

Files: ./po/pt_BR/palapeli.po
Copyright: 2008-2014, This_file_is_part_of_KDE
  Marcelo Alvarenga <alvarenga@kde.org>, 2009, 2010, 2011, 2012, 2014.
License: UNKNOWN
 FIXME

Files: ./po/pt/palapeli.po
Copyright: Nuno Coelho Pires <zepires@gmail.com>n"
License: UNKNOWN
 FIXME

Files: ./po/bs/palapeli.po
Copyright: 2010, Rosetta Contributors and Canonical Ltd 2010
License: UNKNOWN
 FIXME

Files: ./po/cs/palapeli.po
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./po/fr/palapeli.po
Copyright: YEAR This_file_is_part_of_KDE
  bastien Renard <Sebastien.Renard@digitalfox.org>, 2008.
License: UNKNOWN
 FIXME

Files: ./slicers/palapeli_goldbergslicer.desktop
Copyright: coupeurs Palapeli
  ny
License: UNKNOWN
 FIXME

Files: ./slicers/palapeli_jigsawslicer.desktop
Copyright: dieliky skladaÄky
License: UNKNOWN
 FIXME

Files: ./puzzles/european-honey-bee.desktop
Copyright: enne Ã
  z mÃ©h
License: UNKNOWN
 FIXME

Files: ./debian/changelog
Copyright: file for new upstream.
License: UNKNOWN
 FIXME

Files: ./slicers/palapeli_rectslicer.desktop
Copyright: glalap alakÃº elemek
License: UNKNOWN
 FIXME

Files: ./puzzles/panther-chameleon-female.desktop
Copyright: leonok
  lÃ©on panthÃ¨re
  ë
License: UNKNOWN
 FIXME

Files: ./puzzles/castle-maintenon.desktop
Copyright: lya
License: UNKNOWN
 FIXME

Files: ./src/pics/Eliminator-Funk-2.jpg
Copyright: lïOitÎÜÔ,³ÑôSz§êuúå¦;tp®×4¾ÿ OÄº(Gê§ª±{j³çÓK©õ	[S«wSÉ%ºfÏVÔèµ¯§DªÚm:qþcýÖ[G§mò¬/`y#ÿ 8Fgô}F¤£´aÏËôý.O^¤IFLUOÍ_÷Ë7¨I¦ÔG£]	xå)ë¦zúîôéåÄ
  Ê¢bõÀ³õæÛØ`;·á.:bIÄ#eÜÙú²ÒÚ£vcÓ+j&<·5ãëDÿ í	ÝÁíÆ<:4|c Öò"X7Âü
License: UNKNOWN
 FIXME

Files: ./po/sk/palapeli.po
Copyright: nastavenia"
License: UNKNOWN
 FIXME

Files: ./src/palapeli.notifyrc
Copyright: pkirakÃ³ jÃ¡tÃ©k
License: UNKNOWN
 FIXME

Files: ./puzzles/citrus-fruits.desktop
Copyright: plody
License: UNKNOWN
 FIXME

Files: ./mime/palapeli_servicemenu.desktop
Copyright: r til din samling af Palapeli-puslespil
License: UNKNOWN
 FIXME

Files: ./debian/control
Copyright: rez Meyer <lisandro@debian.org>,
License: UNKNOWN
 FIXME

Files: ./pics/32-apps-palapeli.png
Copyright: oo³[È¡X
  H ç]YÆ¿æ;»³né÷/ô¢Ôøy[×vu®ý®¥±=iKÈªMYò¯Þ|ZQ*Z$_äd¨Ð!¸D>MÝëèaè>÷U¬¹lççþyÄéRèÏèÌ)dê l¢¿'Ñ(ßÏfÖ#
  i±0ÃÌ´3w÷qæ>Îco×ÞçÞ¦@?ønÖÜÙë®ï[ß^{Ï¦Â	÷ÝùÛ«Ïµí=Vqýöælß®ë®½>ÂûÀ»:pÉ¥_°×_0tß¾³­¹ïyèGn¼õ¶
  ¤¿|
  ·
  Þ·¯{ª@Ê%re!
  ìbÖÊ#²6erÓKá­Û¿xaEüüáUVº§ödnMý¿OØ¼ZD¬ÐF³Æàª$m=ócb®Éx­g@RoëYµ IWÂåÀ	+¹õRXW<üÒuâÑqC~<
  ý,¶ëA'çVûg|~òÏ²<ÈböÂÃ¡&æð²½Nb$Cä`
License: UNKNOWN
 FIXME

Files: ./pics/16-mimetypes-application-x-palapeli.png
Copyright: ¤Ëå²®T*ºZ­êZ­¦kõº®ÿáf³©Óéô,`NôBH)}'¹RÂôÝ
  ÛðÙ,¬9è3¹7­hÔ£äóyÏÕùùùW³ç
License: UNKNOWN
 FIXME

Files: ./puzzles/cincinnati-bridge.desktop
Copyright: Ð¿ÑÑÑ
License: UNKNOWN
 FIXME

Files: ./po/ja/palapeli.po
Copyright: ã«ãè¨­å®"
License: UNKNOWN
 FIXME

Files: ./libpala/libpala-slicerplugin.desktop
Copyright: ì
License: UNKNOWN
 FIXME

