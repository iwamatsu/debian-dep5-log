Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: FIXME
Upstream-Contact: FIXME
Source: FIXME
Disclaimer: Autogenerated by licensecheck

Files: ./src/arv.h
 ./src/arvbuffer.c
 ./src/arvbuffer.h
 ./src/arvbufferprivate.h
 ./src/arvcamera.c
 ./src/arvcamera.h
 ./src/arvchunkparser.c
 ./src/arvchunkparser.h
 ./src/arvchunkparserprivate.h
 ./src/arvdebug.c
 ./src/arvdebug.h
 ./src/arvdevice.c
 ./src/arvdevice.h
 ./src/arvdeviceprivate.h
 ./src/arvdomcharacterdata.c
 ./src/arvdomcharacterdata.h
 ./src/arvdomdocument.c
 ./src/arvdomdocument.h
 ./src/arvdomdocumentfragment.c
 ./src/arvdomdocumentfragment.h
 ./src/arvdomelement.c
 ./src/arvdomelement.h
 ./src/arvdomimplementation.c
 ./src/arvdomimplementation.h
 ./src/arvdomnamednodemap.c
 ./src/arvdomnamednodemap.h
 ./src/arvdomnode.c
 ./src/arvdomnode.h
 ./src/arvdomnodelist.c
 ./src/arvdomnodelist.h
 ./src/arvdomparser.c
 ./src/arvdomparser.h
 ./src/arvdomtext.c
 ./src/arvdomtext.h
 ./src/arvenums.c
 ./src/arvenums.h
 ./src/arvevaluator.c
 ./src/arvevaluator.h
 ./src/arvfakecamera.c
 ./src/arvfakecamera.h
 ./src/arvfakedevice.c
 ./src/arvfakedevice.h
 ./src/arvfakedeviceprivate.h
 ./src/arvfakegvcamera.c
 ./src/arvfakeinterface.c
 ./src/arvfakeinterface.h
 ./src/arvfakeinterfaceprivate.h
 ./src/arvfakestream.c
 ./src/arvfakestream.h
 ./src/arvfakestreamprivate.h
 ./src/arvfeatures.h.in
 ./src/arvgc.c
 ./src/arvgc.h
 ./src/arvgcboolean.c
 ./src/arvgcboolean.h
 ./src/arvgccategory.c
 ./src/arvgccategory.h
 ./src/arvgccommand.c
 ./src/arvgccommand.h
 ./src/arvgcconverter.c
 ./src/arvgcconverter.h
 ./src/arvgcenumentry.c
 ./src/arvgcenumentry.h
 ./src/arvgcenumeration.c
 ./src/arvgcenumeration.h
 ./src/arvgcfeaturenode.c
 ./src/arvgcfeaturenode.h
 ./src/arvgcfloat.c
 ./src/arvgcfloat.h
 ./src/arvgcfloatnode.c
 ./src/arvgcfloatnode.h
 ./src/arvgcgroupnode.c
 ./src/arvgcgroupnode.h
 ./src/arvgcindexnode.c
 ./src/arvgcindexnode.h
 ./src/arvgcinteger.c
 ./src/arvgcinteger.h
 ./src/arvgcintegernode.c
 ./src/arvgcintegernode.h
 ./src/arvgcinvalidatornode.c
 ./src/arvgcinvalidatornode.h
 ./src/arvgcnode.c
 ./src/arvgcnode.h
 ./src/arvgcport.c
 ./src/arvgcport.h
 ./src/arvgcpropertynode.c
 ./src/arvgcpropertynode.h
 ./src/arvgcregister.c
 ./src/arvgcregister.h
 ./src/arvgcregisterdescriptionnode.c
 ./src/arvgcregisterdescriptionnode.h
 ./src/arvgcregisternode.c
 ./src/arvgcregisternode.h
 ./src/arvgcstring.c
 ./src/arvgcstring.h
 ./src/arvgcstructentrynode.c
 ./src/arvgcstructentrynode.h
 ./src/arvgcswissknife.c
 ./src/arvgcswissknife.h
 ./src/arvgcvalueindexednode.c
 ./src/arvgcvalueindexednode.h
 ./src/arvgvcp.c
 ./src/arvgvcp.h
 ./src/arvgvdevice.h
 ./src/arvgvdeviceprivate.h
 ./src/arvgvfakecamera.c
 ./src/arvgvfakecamera.h
 ./src/arvgvinterface.c
 ./src/arvgvinterface.h
 ./src/arvgvinterfaceprivate.h
 ./src/arvgvsp.c
 ./src/arvgvsp.h
 ./src/arvgvstream.c
 ./src/arvgvstream.h
 ./src/arvgvstreamprivate.h
 ./src/arvinterface.c
 ./src/arvinterface.h
 ./src/arvinterfaceprivate.h
 ./src/arvmisc.c
 ./src/arvmisc.h
 ./src/arvstr.c
 ./src/arvstr.h
 ./src/arvstream.c
 ./src/arvstream.h
 ./src/arvstreamprivate.h
 ./src/arvsystem.c
 ./src/arvsystem.h
 ./src/arvtypes.h
 ./src/arvuvcp.c
 ./src/arvuvcp.h
 ./src/arvuvdevice.c
 ./src/arvuvdevice.h
 ./src/arvuvdeviceprivate.h
 ./src/arvuvinterface.c
 ./src/arvuvinterface.h
 ./src/arvuvinterfaceprivate.h
 ./src/arvuvsp.c
 ./src/arvuvsp.h
 ./src/arvuvstream.c
 ./src/arvuvstream.h
 ./src/arvuvstreamprivate.h
 ./src/arvversion.h.in
 ./src/arvxmlschema.c
 ./src/arvxmlschema.h
 ./src/arvzip.c
 ./src/arvzip.h
 ./tests/js/arv-camera-test.js
 ./tests/js/arv-evaluator-test.js
 ./tests/js/arv-fake-test.js
 ./viewer/arvviewer.c
 ./viewer/arvviewertypes.h
 ./viewer/main.c
Copyright: 2007-2008, Emmanuel Pacaud
  2007-2009, Emmanuel Pacaud
  2007-2010, Emmanuel Pacaud
  2007-2012, Emmanuel Pacaud
  2009, Emmanuel Pacaud
  2009-2010, Emmanuel Pacaud
  2009-2011, Emmanuel Pacaud
  2009-2012, Emmanuel Pacaud
  2009-2013, Emmanuel Pacaud
  2009-2014, Emmanuel Pacaud
  2009-2016, Emmanuel Pacaud
  2009-2017, Emmanuel Pacaud
  2010, Emmanuel Pacaud
  2017, Emmanuel Pacaud
License: LGPL-2+
 FIXME

Files: ./.travis.yml
 ./AUTHORS
 ./Makefile.am
 ./Makefile.decl
 ./NEWS
 ./RELEASING
 ./TODO
 ./aravis.doap
 ./aravis.pc.in
 ./aravis.rules
 ./arvconfig.h.in
 ./autogen.sh
 ./configure.ac
 ./debian/aravis-tools.install
 ./debian/changelog
 ./debian/compat
 ./debian/control
 ./debian/gir1.2-aravis-0.6.install
 ./debian/io.aravisproject.aravis.metainfo.xml
 ./debian/libaravis-0.6-0.install
 ./debian/libaravis-dev.install
 ./debian/patches/extradist.diff
 ./debian/patches/gtk-doc.make.diff
 ./debian/patches/series
 ./debian/rules
 ./debian/source/format
 ./debian/watch
 ./docs/Makefile.am
 ./docs/reference/Makefile.am
 ./docs/reference/aravis/Makefile.am
 ./docs/reference/aravis/aravis-docs.xml
 ./docs/reference/aravis/aravis-gv.xml
 ./docs/reference/aravis/aravis-overview.xml
 ./docs/reference/aravis/aravis-sections.txt
 ./docs/reference/aravis/aravis.types
 ./docs/reference/aravis/meson.build
 ./gst-0.10/Makefile.am
 ./gst-0.10/gst-aravis-inspect
 ./gst-0.10/gst-aravis-launch
 ./gst/Makefile.am
 ./gst/gst-aravis-inspect
 ./gst/gst-aravis-launch
 ./gst/meson.build
 ./gst/pipelines.md
 ./m4/appstream-xml.m4
 ./m4/ax_valgrind_check.m4
 ./man/Makefile.am
 ./man/arv-tool-0.6.1
 ./man/arv-viewer.1
 ./meson.build
 ./meson_options.txt
 ./module/Makefile
 ./module/aravis-module.c
 ./po/LINGUAS
 ./po/POTFILES.in
 ./po/POTFILES.skip
 ./po/lv.po
 ./po/meson.build
 ./po/pl.po
 ./src/Makefile.am
 ./src/arv-fake-camera.xml
 ./src/arv-gev-bootstrap.xml
 ./src/arvenumtypes.c.template
 ./src/arvenumtypes.h.template
 ./src/arvrealtime.h
 ./src/arvrealtimeprivate.h
 ./src/arvtest.c
 ./src/arvtool.c
 ./src/meson.build
 ./tests/Makefile.am
 ./tests/aravis.supp
 ./tests/arvacquisitiontest.c
 ./tests/arvautopacketsizetest.c
 ./tests/arvcameratest.c
 ./tests/arvchunkparsertest.c
 ./tests/arvdevicescantest.c
 ./tests/arvdevicetest.c
 ./tests/arvevaluatortest.c
 ./tests/arvexample.c
 ./tests/arvgenicamtest.c
 ./tests/arvheartbeattest.c
 ./tests/arvziptest.c
 ./tests/buffer.c
 ./tests/cpp.cc
 ./tests/data/genicam.xml
 ./tests/data/pipelines.txt
 ./tests/evaluator.c
 ./tests/fake.c
 ./tests/fakegv.c
 ./tests/genicam.c
 ./tests/js/launch
 ./tests/loadhttptest.c
 ./tests/meson.build
 ./tests/misc.c
 ./tests/python/arv-buffer-test.py
 ./tests/python/arv-camera-test.py
 ./tests/python/arv-enum-test.py
 ./tests/python/arv-evaluator-test.py
 ./tests/python/arv-feature-test.py
 ./tests/python/arv-genicam-test.py
 ./tests/python/arv-single-buffer-test.py
 ./tests/python/launch
 ./tests/python/launch-dbg
 ./tests/timetest.c
 ./tests/valgrind-memcheck
 ./tools/gvcp.README
 ./tools/gvcp.lua
 ./tools/render-symbolic.rb
 ./tools/wireshark-snapshots/Basler-acA1920-USB-Properties.txt
 ./tools/wireshark-snapshots/PtGrey-Flea3-USB-Properties.txt
 ./tools/wireshark-snapshots/Ximea-MQ013MG-ON-USB-Properties.txt
 ./tools/wireshark-snapshots/gc1380-10x20image.wsk
 ./tools/wireshark-snapshots/gc1380-snap-320x200.wsk
 ./viewer/Makefile.am
 ./viewer/arvviewer.h
 ./viewer/data/Makefile.am
 ./viewer/data/arv-viewer.desktop.in.in
 ./viewer/data/meson.build
 ./viewer/icons/README
 ./viewer/icons/gnome/scalable/devices/aravis-fake-symbolic.svg
 ./viewer/icons/gnome/scalable/devices/aravis-gigevision-symbolic.svg
 ./viewer/icons/gnome/scalable/devices/aravis-usb3vision-symbolic.svg
 ./viewer/icons/src/aravis.svg
 ./viewer/meson.build
Copyright: NONE
License: UNKNOWN
 FIXME

Files: ./po/ca.po
 ./po/cs.po
 ./po/da.po
 ./po/de.po
 ./po/el.po
 ./po/es.po
 ./po/fi.po
 ./po/id.po
 ./po/pt.po
 ./po/pt_BR.po
 ./po/sk.po
 ./po/sl.po
 ./po/sr.po
 ./po/sr@latin.po
 ./po/sv.po
 ./po/tr.po
 ./po/zh_CN.po
Copyright: 2013, aravis's COPYRIGHT HOLDER
  2014, aravis's COPYRIGHT HOLDER
  2015, aravis's COPYRIGHT HOLDER
  2017, aravis's COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./gst-0.10/gstaravis.c
 ./gst-0.10/gstaravis.h
 ./gst/gstaravis.c
 ./gst/gstaravis.h
Copyright: 2006, Antoine Tremblay <hexa00@gmail.com>
  2006, Eric Jonas <jonas@mit.edu>
  2010, Emmanuel Pacaud <emmanuel@gnome.org>
  2010, United States Government, Joshua M. Doe <joshua.doe@us.army.mil>
  2010-2011, Emmanuel Pacaud <emmanuel@gnome.org>
License: LGPL-2+
 FIXME

Files: ./m4/ltoptions.m4
 ./m4/ltsugar.m4
 ./m4/lt~obsolete.m4
 ./m4/nls.m4
Copyright: 1995-2003, 2005-2006, 2008-2014, 2016, Free Software
  2004-2005, 2007, 2009, 2011-2015, Free Software
  2004-2005, 2007-2008, 2011-2015, Free Software
  2004-2005, 2007-2009, 2011-2015, Free Software
License: UNKNOWN
 FIXME

Files: ./COPYING
 ./m4/ltversion.m4
 ./po/hu.po
Copyright: 1991, Free Software Foundation, Inc.
  2004, 2011-2015, Free Software Foundation, Inc.
  2013-2014, 2016, Free Software Foundation, Inc.
License: UNKNOWN
 FIXME

Files: ./src/GenApiSchema_Version_1_0.xsd
 ./src/GenApiSchema_Version_1_1.xsd
Copyright: 2006, Basler Vision Technologies
  2008, Basler Vision Technologies
License: BSD~unspecified
 FIXME

Files: ./src/arvrealtime.c
 ./tests/realtimetest.c
Copyright: 2009, Lennart Poettering
  2010, David Henningsson <diwic@ubuntu.com>
  2014, Emmanuel Pacaud <emmanuel@gnome.org>
License: Expat
 FIXME

Files: ./tools/wireshark-dissectors/packet-gvcp.c
 ./tools/wireshark-dissectors/packet-gvsp.c
Copyright: 2012, AIA <www.visiononline.org>
License: UNKNOWN
 FIXME

Files: ./po/eu.po
 ./po/ru.po
Copyright: YEAR THE PACKAGE'S COPYRIGHT HOLDER
License: UNKNOWN
 FIXME

Files: ./tools/render-icon-theme.py
Copyright: NONE
License: CC-BY-SA-3.0
 FIXME

Files: ./m4/pkg.m4
Copyright: 2004, Scott James Remnant <scott@netsplit.com>.
  2012-2015, Dan Nicholson <dbn.lists@gmail.com>
License: GPL
 FIXME

Files: ./m4/libtool.m4
Copyright: 1996-2001, 2003-2015, Free Software Foundation, Inc.
License: GPL
 FIXME

Files: ./m4/glib-gettext.m4
Copyright: 1995-2002, Free Software Foundation, Inc.
  2001-2004, Red Hat, Inc.
License: GPL
 FIXME

Files: ./README.md
Copyright: NONE
License: LGPL-2
 FIXME

Files: ./viewer/arv-viewer.ui
Copyright: 
  This library is free software; you can redistribute it and/or
License: LGPL-2.1+
 FIXME

Files: ./tools/wireshark-snapshots/Basler-acA1300-chunk-data.pcapng
Copyright:    
  Û~?ÉÜ5µXM=­©k
  ùKdRÌE~¶£)¦"ïz¦È»¥Rä"ï)òÎ"ïlý	=^Fðe
  -ò¿dë}þ0ªÏ8Ðô
  ÙÓø°?c0Æ)È
  qlµµ­#PÄsÖå/ýÙ%ÊF<ó6ª£C¼£F@
  L7Ï¨
  ÏýÈ@·1Ô~[¬1õÆÞyÅ ~=xþèßD¾°ñéè¬½È½ÌÁ»
  7K²,*5ÙÏãeæÑ9K
  7¼ñÓ0ÍÆ¢çFÌB«yÍ@íÑ
  MÍB2Âè´å$¦@(vC$I:÷B¡&|»#Ã65$¹Ø6®zj§9ï0ç.9_0ç
  Ogu×AáÃÇÏ  X            Å  +k-h:   :    0S¤Ñ !jÃ E  ,n@ @2ÊÀ¨ !À¨ ÎÙt |2B  ÿ&À              X      Å  ª-h6  6   !jÃ 0S¤Ñ E (    ÿ8<À¨ À¨ !tÎÙ[   
  bYË
  fúëk±U{³¶ã*Ût0Ïòd*6é^Í"ÿ~/ö©W,XþµwFaþðápÎ}-}aVuk
  jrb(ÖûY+'ÕÙ8#M X'ZÏeÐA[Ñ²Zv_¾
  r³4µ×ä_w¬n£&ÓWeÚmüV_ÎâüjÑ
  tvH¯?Cd+@Ú)Æ=Ô¹]f}u~è¾á+ë¾a1ìç¯w%9ÔyÏw5Ç ü¶Ï@}ÏiGè(»]Ø;Ü}}~¸2ôuÅëø"ä=Üí6I
  ÷ÆU
  ñy³·)r¼¶
  ±ý¦v4Ý^¦Û4Ýv
  LéI`jéa­y6JKKò_
  'pAr®¢]KQ1ÉÚ·S·o´ôlU×ØHå½
  Ï¶Sjî¶³ÖýÑ¡/«9Á
  ¢àGmGRLº
  «É	Ï±æQ´y4#Tè¶:$ 4çDMP¥Á,¤Mâß»0¿BÒÉdl90¿ £ ý0JÓh­J2¤9*é
  ½Eñv»tkänc2.&w·,åîÜÝ^òµt{î¥É hº½ØÍtÛòjºíp7}»d÷í*¦É húv±Óô­åiúÖtºý£ÁÎÇ«r°
  ¿û×:¹NÏpk7ñ~<rQÈ7 2j«@cZ{aFyïO>D>#;}d  X            Å  =<,h:   :    0S¤Ñ !jÃ E  ,`@ @2ØÀ¨ !À¨ ÎÙt @B  ÿÀ             X      Å  ñT,h6  6   !jÃ 0S¤Ñ E (    ÿ8<À¨ À¨ !tÎÙèÃ   
  Â(È­ÓÂÌ¢±E£âÇA
  ÉÈÁùå
  Ø~8´õX~¬ÆZË>=YÏCðäÞMìíÈ
  Þ¥ÈèïépÃW@`O£¶½é¨>LgvÄö[3%ÝùÈ)Í©ÙíA¿^r½Ï"Ð¾  X            Å  ;C-h:   :    0S¤Ñ !jÃ E  ,l@ @2ÌÀ¨ !À¨ ÎÙt 4B  ÿ$À             X      Å  X-h6  6   !jÃ 0S¤Ñ E (    ÿ8<À¨ À¨ !tÎÙ¬   
  òõ¡J#·i*¤YåØR{ÁmÈ2<sSsôàÝî8°5EhåyèÏèK5G¾úñCY
  óQýx[C6a7GÙÇ}{Ta<ÞæÜ"Õ6I­±~Tw·QêFqÈ?Ã,Éi8 ÓßYÙ¬¹¾_äA®i@Î¼ÝHÛÜOsú!Ô¸3Óù¢Ó0ØÓ%¸(»:S
  ÿ*³ÜÁOÞGÿßIÊáüó§-òEK_¸QøÓà?ýù+õÏüÔf?ygi2óÃy8úçO[·;?o¾úùÝöæ«í`4zõ~ckýÕþæþûõÍõ7ïvw~òØªãn_×Gg__Ì¯«¿@²`j3óÛÿL¾¯·%P.$º
License: UNKNOWN
 FIXME

Files: ./viewer/icons/gnome/48x48/apps/aravis.png
Copyright: !Dk©Ó
  js¼1o×'À0(ñÖ3
  l¡Z
  ¥É
  ÎvdºÚ0¿jÃ0¨a÷
License: UNKNOWN
 FIXME

Files: ./viewer/data/aravis.png
Copyright: Ëeee±{÷nãÂÔO?ü°Õ×BÝùíààN§Ã××¨¨(6nÜhL/--eÇL:Õlzs«ÍåÛ½{wT*¶z;ÍàGá©§¢¦¦Æâå4
  B!°9	J
  ¢MÚ»?Ã£G3yÆ]ÔÖÖ^ÓuUTTòÙ²ìíwZ¼ì|áÑ£Ùß í¥ÿ}áÑ£Ù³wßU­C!n
  ¢Í(**">1aC°g[qúþÛoP«ÕlþùçfO<p xãÇÑ¯¯IzÌ¯wÊ¹î+23³º¯àïÏóÏ>ÍÆÿÆ2:zçÒÓùåhRó
  J+
  Êªkñwû­G,§¤%__ `þïü,7¡grªXo|ïòsËÿ~_ìÏ3¨Ë·dñéÃèêïÀ©Ëz*ß½
  ãïÿ;*
  3¥
  9U&CÍ'3+É+­á§äbªôµÌàÅKSyô³T·³køÚ)%È¨kèw*av¤P·úwpæîg(­2Paà§cE
  <ùä£££²zõj´3f(
  P
  S'¼½½),,$22'O6ÛCÖ;¢×ë9qâÑÑÑ5ÔÔT¼½½)--mpãOII	>>>fÓjgÌÕÃæò=}ú4jµºÉÏ¥1AAA
  S'ãë/¾ø»îºËªuÝ¨Úãq)Ä¢¹ó­¢¢ùóç&L ==ÝKLL
  S9vì³gÏÆßß V¬Xa¿
  S×=Ë¯kÎtéÜÊJ 77Ïêr Cß>}ÈËË'ñàAÎ_¸Z­iòñ-BãÆ¥O¯^dfeqä_pwwcêäÉx{yµmæí¨¸¸¸päèQy2Í5¦V«éÛ§ööääæ
  S§2wî^~ùe:uêÔìçq#vùÆÐîÒÔÔTcÀ8iÒ$ãûõ´Z->>>øøøàîîÞà[IséDQÉÃÃWWWÂÃÃÙ¹sge³d'''ÂÂÂ7¾W_/?°°0
  SÍ¦×KLLäñÇç/¾0¹¸^XÎÖÇ¥7+sõí¶mÛèÑ£sæÌ¡[·nW5¢(8pÀâe¬Y¿¢(
  T
  TJ
  TTWW§©***ª
  Ttñw0é
  T×l=B!76,]º)44Ô¦eÊÈÈ`åÊ¼ðÂ
  Tüãÿ 66¸¸8qvvÆÇÇGyàà`^yå«Ö-ínûÕ®Òz¢`0(++cÛ¶môèÑ9sæÐ­[7]]MæU©TL2
  amÂ%òË
  eMæ}¹wäÔ<4îéÆ¼Ï;/ÎÖåcí¾¹7÷ó6¾>]ÉSÿI·¨lWÏÜ6ÿTwþo;YlÜ¯ÿÜÍs:Ñ'Èìm»£0½úÊ®{?<cì¡iÉq°õd	Ç3êþÿ¿¸l¾ÓîÆsãÊÏîZÈ/«1	À®gBZK×_4¾z«'U5µ¤dVrÿPÞÞlþ¾æê^KÒ­e®í°¤m¹å»¤
  ii¤¦¥ß×ëõìÞ»±£cvÏ¾ý,}åèÓ»÷8å¼µü]cú<DÂDöÅÇÑ·/ÞÞÞTUWäÀç¯â£U·}üû#~øi3«>aìq¼Òèèhâ°}çNÒÎC­V7èÙ½««+ wNJÌ¨Q&ia¡! Á¿V®">1¢¢"
  i¨T*z÷êBÂdg>ïÝ«'ÁAA ôéÕÃ3pÀ ³ëüyë6ÄÞÁýo¥[.8884_¯=-þ<JEP` :=P,JI¡[·®têÔ3©©lÝ¾ÃìrBXÃsà§-?tì>ÞÞôëÓÂ¢"ÖoøÜÛ6KmqýôèÞÚÚZÖ~õ5;vî";'§Á<­QYR§iÒ2())!¼cGnûÝX BCBøþZ#{!Ú¼ÀÀ |||8zìEÅE ôéÓäãÇMæmüßÍÍõ¾'#3Ð ü|}<ñv Ù»?ÙÙtqq¥9;t #3.Ñ±cBÍ®ÓÅÅ
  qº***
  ½
  EaÔÈy×KK;
  Gæ/K_4_SB4§Sx8¿MbÞ
  V]ô
  -sôÈ(Þ|çìØ¹'æ>Jaa'N$rØ0]æÈÑ£ùå(wNÊy³éçØ»?EÅÅÁò¾ÏáÃ¿pÛØ±8t'''
  ¡w#y¥u'X³oìµjaîZ*,ìÑh¬|Oõor+ôµ¨¨
  £°¼ÂdH|ÛíO[Ý·ðAáÎ<ñù9c/
  ¨;êO­&.jRóëT®økµå°·×Ð§w/:
  ¨¨àý÷ßçÈ#Üwß}-.K{ÕK!nDÍoÌ1yóæqñâEÊÊÊøé§ÈÉÉá»ï¾#00!CäwçwÐìF{á
  ©`ooO÷®]ÉÍË3æW?ÜÙ!,QQ#-GPP ÿZ¹AIfV6;1&&êêjöî'==ÚÚZ6GGGãö7¶-eé9	uC²û9Nee%Þ^^
  ©!==1cÆCFF {÷îeúôéO«««éÞ½;S¦LaàÀ,Z´¨Á]l«¿.00X¼x1ÙÙÙ&óiµ¿=EëðáÃxzzLEEE
  ©iðÄÝÝsçÎÚdÙ,'''À$p¿ò	999øøø4Ô}#vqqi°¬çÎ3~¥êêj,YÂÂ
  ªªREQÊÊÊ@Y°`rðàA%##CY±b
  ¬JÞý9«Þ7³º|µJÛ¿ÿ7-b@'üÜìHÉªdù,2~¦nÉ6¨é<ù¿¸,´1-=î Æ÷qçOÑ~ø¹i9YÉ?¶d?kM¡®Wô£Â	÷µ'µÀòJ®þ69¿±ý¤eÔ»²îmizKk;²õµ-×ª|×´áí[î)m)øb_s£ýõÔñí¡kÌÑ¨Uü)ÆÕûr[|Bª ?7;"»¸0s>.Z.VóÙÞ<ö§]Þ×ÕZ²¯¨¬3ôø¸h[T>­FEÿ0'f
  ¬¬¤¦¦'N´Êú5
  ­­eùòå3oooüüüä>0Î£×ëY½z5}ûömt[Ö¬YÃ!CðóócæÌZ~ðàA¦MFhh(L2
  ®¼£ênú÷ªO :x°ñÕ¯úÄ$ðµ$?!h©
  »äÁTwwweÈ!ÊÁ'6úÊêêjÅÓÓSy÷ÝwEi¾>5
  Á{ÖòpÒwÙzæöNÍ_¾½FÅ[w±þHg²?®«{Í¥×*J=Ãæ#sm¹tKËßÖYÚF6EÚð¶áªÒÁáÎ<1Úoæ³É/«á½¸,ö[Ø«Ñ~½Ðý|9/Læ®¼ùc&)YÑÓû{âb¯á¥©ÁÆ÷´j¨`Ýºp×û§Í®/»X¿»'.kLòË
  Ù
  Ùôæ|þùçJ``
  èë-x{ymÛÌÛjµW
  ñlnÏ¡´Ê Àôþ<åK­ò[>y¥5Ì^jñç",;.ÿqo/o¸Ô`ÿZkÙ]¡&Çùõ¢RÁþ^Ä(&¿¬¦Õóo­óÕÝJ¯ ÇïÿùËtR²*mP"ë¨T09Â¡èäkO­
  ñuÕ²p ñi¥¤_§BÔ)©4°%¹'Fûæ¥ÃNSpô
  ò
License: UNKNOWN
 FIXME

Files: ./viewer/icons/gnome/256x256/apps/aravis.png
Copyright: ³©! ;mµÝà_½^Ç;ï¼¥¥¥È|ixqÎ;ésÛIð@³ÙÄÊÊ
  $«W¯>3s÷Ü¹ó3§Ovùr^¤}²à³uÜ¬%ÍßkïFÇÙÏæpæI
  %¯ýë¯{jöggÏÌêS'§P*
  6Ðø$.
  MÛ²j_Á'
  T¸¤@)ÑÓ"ü MZ"`_º lÛÆ«¯¾!=ËÁ0Âo·É¤®nMRWZI§Ý±éfªÕ*ªUÿÉÝùùyüýßÿ=þäOþDS§(ýM!RH áöíÛðcrr2Rø3dh4¸{÷.nß¾
  VQ©±»·õõu¬¯¯ï®®®þùöæö7÷àÈÀ×¿ñõWfNüæèèØÌää	¡TD>®ëÀ«ÎÆ!ìúA
  xÇýýý]ï[ãááaïx@ûK©ÿô@·¶_ÊptÀoëÚ®zcuZ*P-ó$À4t£
  /£×Á¸$yÓà>´º~¤Ð«zã%<	XåYåróæñÁÛ»îÍOû¶áÔY £ã#_C__r+øÝÐqqXÁ¹¤ùÒl	ô²î¸Æ¬M  !!}}}ÇäÄÄÐöÖÖW ü·¡§ ©zÀÕ«WÇFGFþllt
  «`Y6VV·ÉO8a'0.Ò!ðóðåHë>¸=õõmWÀxAu>"-äºT^kY^qM
  ² J¥/E£48.Ðþx'8· Iþ4XÝ¨#e³,+Ö4
  Ð#Huyõ ÈåöËUÅ4zO(BféTmúóåûú
  ×ë¸qãFlaè(óÚÒF§}3
  ú
License: UNKNOWN
 FIXME

Files: ./debian/copyright
Copyright: -format/1.0/
License: UNKNOWN
 FIXME

Files: ./viewer/data/aravis-video.png
Copyright: 1ørÑ(
  BC¨æÄXÁ(ïv
  Bå©Xb® ä¨Ay*ÞB¬ÉP
  DKç²e|éü
  OaÊ²åËY¾b;4¯¸ÿÁèììDi½Î°~ýúõÙu'"dÛo·J)
  SÙaìÍfßKyõ½¿µz©ÆY|ò3cþüù=¶õ¦NÞ-HE °Ùóï§ÉYû"K¾ÜZ²t)g}î¼ó´37zù+))))yc²!uÉ«ëÌum[kýtöÎaæ¬Y5+/ÿW^þFÅ¬Ù³ùÌç¿¸QÓÚè£ï#ì}Ïäç]x}å¼-º²ÎS'òÔÄzÙå[}a¿ä[ßàù,3gÍéÞ7sÖFl¿=ßùæ×a3È#Z.»ô;ý
  T*üùo7ÓÕÕÀÿt1F8ü°õÚ{ï
  [ÍäùoÝ?Î>ë ÜwÿüáOÚ(×¼±ìóÞcDøÞÅßæÓÿÞG¾ÿo#6êÔZb÷~s¨Í;g<<-F.ûî%|ê³eúÝbþ{YKãdkä¿¯úQwgr­C
  pÈAþËzmeh7%ÏMÌÎÛ    IDATÑ£·©¤dcPÖOu~¾/)))))))))ÙßOJJJJJJJJJJQZRRRRRRRRRb_k¼ÆEi½^/s¡¤¤¤¤¤¤¤¤dËÒ+¹PRRRRRRRR²eEé_þrc
  w.¦XÇ¿°EH×*òU+~94«WÑi(°¹P¨H«äØ
  rª$Q#¢ 
  EÂ:C6§i+ÁÒJ@bAPE%D¢ÊXÅ-]
  ÍB$Ì3çcÇ½È|þó?óÄOüPçå·úÍF^¾|ù³jÙ²eÓ1£Ël)))))))))ÙÜ(¥Èó|ª×ëY!)³¤¤¤¤¤¤¤¤¤dK`É¬¸µÆ5k
  Ð	¤Á«
  ¤k=çcO<É¡[íE]§ðÕ¼4ûDó÷Xn¹õxï±¶ç+vÒ)Ûaî©ÖÖÖÓÏ<Ã¶¶¥_GõF½{ÿ©S5{6g~Z··
  §0Ægô¨Qì·ï¾
  ¯Ò
  °Û®»°MÿþMa|ÆédyÞk^÷å~õëe6ö¶6BW½k>Þ{î{à~N9é$:Ú_jpè!ë¦)#FlÏ
  ³:m:£Gbô¨QÌ3ú3,^²dóRÙ
  ×ë¼çÌ3º=§|?ýÅ/¹í;ºEéºì;fô«RÁÛnÛ-|[[[ËBRRRÖO¯mQZRRRòZàíÇÇåÿ}%ïûà¿sÌGðãcÂN;õ)îò+øáB{[ü×ðàÃôïmZ°3gÐÒRãcg}ç&MfÛA8î£y÷g`é5þ¼yóØaìË:ÚÛ¼í¶<¿`aì¸óî{xð¡6c:O<ùú×tú_±r%F#=¥5vÜüûÙyÂ²à¼vDiéI+í+í+y-pæi§²Í6¸òªsÝ7qÝ7qÐ[àcù0#¶ß³fsÝ
  ñ uOZÕøBat$·£ëäæï×ýª§"2|õå¿Gá£fEæÁT
License: UNKNOWN
 FIXME

Files: ./viewer/icons/gnome/22x22/apps/aravis.png
Copyright: 6Ávú÷Çìß+â$IÌ_C°â.õ    IEND®B`
  PJ¡d¥¦QF
  iz¶u¥-(3ÁF§Bë­C¢(d¶²1mð¾¾>r¹<7çn3;»1îûrH%[{Òv3W^~>½H0Ú### ´î$É©2¶m#àÊk<öø£T«f#ØhÍ±coµ&Ã0ä±·Y­­R÷ëA@aÈððÞ´­M>ïñÅ¹Ó8¬üÍ¥Öe¬
  Wõ¦LpB"1$õäÿ'VSIkÀvº´UHK·n³óï½ñ0»Û¨ñ¼ÌoÞ{ùä7¿ïï½±$áüùó=;
  ælÞçÞeQ¥âÐÓ£DEFeÛÌ|j?íl,
  ®¬â:YöñÝÅ¶máå²³¼¼¼ÁÈîîn~~$bc0Áu<yV)6øõ:sówPJÒÛ»;·rýúõ6(ÀÒÒJ
License: UNKNOWN
 FIXME

Files: ./m4/ax_code_coverage.m4
Copyright: 2012, 2016, Philip Withnall
  2012, Christian Persch
  2012, Xan Lopez
License: UNKNOWN
 FIXME

Files: ./src/arvgvdevice.c
Copyright: 2009-2016, Emmanuel Pacaud
License: UNKNOWN
 FIXME

Files: ./po/fr.po
Copyright: 2010, Emmanuel Pacaud <emmanuel@gnome.org>
License: UNKNOWN
 FIXME

Files: ./po/oc.po
Copyright: 2010, Emmanuel Pacaud <emmanuel@gnome.org>
  dric Valmary (Tot en Ã²c) <cvalmary@yahoo.fr>, 2015.
  dric Valmary (totenoc.eu) <cvalmary@yahoo.fr>, 2015.
  dric Valmary (totenoc.org) <cvalmary@yahoo.fr>, 2013.
License: UNKNOWN
 FIXME

Files: ./viewer/data/arv-viewer.appdata.xml.in
Copyright: 2014, Emmanuel Pacaud <emmanuel@gnome.org> -->
License: UNKNOWN
 FIXME

Files: ./m4/introspection.m4
Copyright: 2009, Johan Dahlin
License: UNKNOWN
 FIXME

Files: ./po/bs.po
Copyright: 2014, Rosetta Contributors and Canonical Ltd 2014
License: UNKNOWN
 FIXME

Files: ./tools/wireshark-snapshots/Basler-acA1920-genicam-data.pcapng
Copyright: !ÿÿC	 - `û
  !ÿÿS	  <`û
License: UNKNOWN
 FIXME

Files: ./viewer/icons/gnome/32x32/apps/aravis.png
Copyright: ³U*ß?þþ~ÛÏÿësr¸nztQ[[ËêjUUøâôÇ¨jáfÓî§r àÔþ¯ðñQPùqú3Þï'Çwß'sp5MMLÝZäÓONEßv¯
  ¥-Ö¾(Ð>FçÞ¹ãbæÞÎ«å!	_rfÎëï;ßùRò"¡¼Pv@+.¤R©FEgä ðã	¤e	óäË2ÿµ÷@ÿö¯¦CNvH¥R¨ÁD¢µ½¡yw³ªi%Úþ7²Ù,óósÞíéÛ«{yp0/"b6±§½¡¥¹Eu]Çq(âÙTË?®Î²,:;÷k
  ªúdÂÙ¶µYºûÝI:Ú÷0::ÊVMÑÖÖLmmm~×eU,Aq
License: UNKNOWN
 FIXME

Files: ./tools/wireshark-snapshots/Basler-acA1920-5-image-acquisition.pcapng
Copyright: Ó
License: UNKNOWN
 FIXME

Files: ./tools/wireshark-snapshots/Basler-acA1920-Detection-without-connection.pcapng
Copyright: é
License: UNKNOWN
 FIXME

